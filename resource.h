#ifndef __RESOURCE_H__
#define __RESOURCE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define BMP_GUI_LOGO_ID      0x0
#define BMP_CAPTION_ID          0x1
#define BMP_CLOSE_BOX_ID        0x2
#define BMP_PUSH_BUTTON_ID      0x3
#define BMP_CHECK_BUTTON_ID     0x4
#define BMP_SCROLL_BAR_ID       0x5

#define BMP_MAX_ID              6

#define CURSOR_ARROW_ID         0x0
#define CURSOR_BEAM_ID          0x1

#define CURSOR_MAX_ID           2

bool32_t  InitSystemBitmap(void);
BITMAP *GetSystemBitmap(int32_t);
void ReleaseSystemBitmap(void);

bool32_t  InitSystemCursor(void);
CURSOR *GetSystemCursor(int32_t);
void ReleaseSystemCursor(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
