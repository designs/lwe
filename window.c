/*
  Copyright (C), 2008, zhanbin
  File name:    window.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.4
  Date:         20081205
  Description:
  不支持多线程同时创建和访问窗口。
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081222    zhanbin         最上层窗口实现。
    20081220    zhanbin         控件背景透明实现。
    20081219    zhanbin         实现新的控件的想法，跟窗口使用同样的结构。
    20081215    zhanbin         实现对话框，菜单的想法。
    20081214    zhanbin         窗口关闭实现。
    20081213    zhanbin         窗口无效区域实现。
    20081211    zhanbin         窗口控件实现。
    20081210    zhanbin         窗口ACTIVE状态，标题栏，移动实现。
    20081208    zhanbin         窗口区域实现。
    20081205    zhanbin         创建文件
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

static bool32_t IsTopWindow(WINDOW *Window);
static void TopWindow(WINDOW* Window);
static void AddToParent(WINDOW *pParentWnd, WINDOW *pAddWnd);

GUI* pGUI;
static GUI  thisGUI;

static void DumpAllWindow0(WINDOW *Window, int32_t nLevel)
{
	int32_t i, nPri = 0;
	WINDOW* _list_;
	struct list_head *_head_ = &Window->ChildListHead;
#undef _item_
#define _item_ ChildListNode

	for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
			& ((_list_)->_item_) != (_head_);
			(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
	{
		for (i = 0; i < nLevel; i++)
		{
			GUIDEBUG ("    ");
		}
		GUIDEBUG ("    LEVEL:%d PRI:%d NAME:%s\n", nLevel, nPri++, _list_->pCaption);
		DumpAllWindow0 (_list_, nLevel + 1);
	}
}

void DumpAllWindow(void)
{
	DumpAllWindow0 (pGUI->pDesktopWnd, 1);
}

static void UpdateAllWindow0(WINDOW *Window)
{
	WINDOW* _list_;
	struct list_head *_head_ = &Window->ChildListHead;
#undef _item_
#define _item_ ChildListNode

	for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
			& ((_list_)->_item_) != (_head_);
			(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
	{
		if (FALSE == WindowIsVisible (_list_))
		{
			continue;
		}

		UpdateWindow (_list_, TRUE);//窗口更新

		UpdateAllWindow0 (_list_);//递归子窗口更新
	}
}

void UpdateAllWindow(void)
{
	if (NULL != pGUI->pDesktopWnd)
	{
		UpdateWindow (pGUI->pDesktopWnd, TRUE);

		UpdateAllWindow0 (pGUI->pDesktopWnd);
	}
}

static WINDOW *FindWindowByPos1(WINDOW *Window, LPPOINT lpPoint)
{
	WINDOW* _list_;
	struct list_head *_head_ = &Window->ChildListHead;
#undef _item_
#define _item_ ChildListNode
	WINDOW *FindWnd;

	for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
			& ((_list_)->_item_) != (_head_);
			(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
	{
		if (TRUE == PtInRegion (_list_->ClipRegion, lpPoint->x, lpPoint->y))
		{
			FindWnd = FindWindowByPos1 (_list_, lpPoint);
			if ((WINDOW *)NULL != FindWnd)
			{
				return FindWnd;
			}
			else
			{
				return _list_;
			}
		}
	}

	return NULL;
}

WINDOW *FindWindowByPos(LPPOINT lpPoint)
{
	WINDOW *FindWnd;

	if (TRUE == PtInRegion (pGUI->pDesktopWnd->ClipRegion, lpPoint->x, lpPoint->y))
	{
		FindWnd = FindWindowByPos1 (pGUI->pDesktopWnd, lpPoint);
		if ((WINDOW *)NULL != FindWnd)
		{
			return FindWnd;
		}
		else
		{
			return pGUI->pDesktopWnd;
		}
	}

	return NULL;
}

WINDOW *GetDesktopWindow(void)
{
	return pGUI->pDesktopWnd;
}

static void ResetWindowInvalidRegion(WINDOW *Window)//重设窗口为无效区
{
	RECT WndRect;

	GetWindowRect (Window, &WndRect);
	SetRectRegionIndirect (Window->InvalidRegion, &WndRect);
}
//传进的矩形，减去传进的窗口的无效区
static void WindowGCChangeByNC(WINDOW *Window, CLIPREGION *pRng)
{
	RECT WndRect, ScreenRect;
	int32_t left, top, right, bottom;

	if (FALSE == WindowHaveNC (Window))
	{
		return;
	}

	switch (Window->nStyle & (WS_CAPTION | WS_BORDER))
	{
	case (WS_CAPTION | WS_BORDER):
		left   = GetSystemMetrics (SM_CXBORDER);
		top    = GetSystemMetrics (SM_CYCAPTION);
		right  = GetSystemMetrics (SM_CXBORDER);
		bottom = GetSystemMetrics (SM_CYBORDER);
		break;

	case (WS_CAPTION):
		left   = 0;
		top    = GetSystemMetrics (SM_CYCAPTION);
		right  = 0;
		bottom = 0;
		break;

	case (WS_BORDER):
		left   = GetSystemMetrics (SM_CXBORDER);
		top    = GetSystemMetrics (SM_CYBORDER);
		right  = GetSystemMetrics (SM_CXBORDER);
		bottom = GetSystemMetrics (SM_CYBORDER);
		break;

	default:
		left   = 0;
		top    = 0;
		right  = 0;
		bottom = 0;
		break;
	}

	GetWindowRect (Window, &WndRect);//得到 窗口的矩形
	SetRect (&ScreenRect, WndRect.left, WndRect.top, WndRect.left + left, WndRect.bottom);//窗口左边框的矩形
	SubtractRectFromRegion (&ScreenRect, pRng);//传进来的矩形减去左边框
	SetRect (&ScreenRect, WndRect.right - right, WndRect.top, WndRect.right, WndRect.bottom);//窗口右边框的矩形
	SubtractRectFromRegion (&ScreenRect, pRng);
	SetRect (&ScreenRect, WndRect.left, WndRect.top, WndRect.right, WndRect.top + top);//窗口上标题的矩形
	SubtractRectFromRegion (&ScreenRect, pRng);
	SetRect (&ScreenRect, WndRect.left, WndRect.bottom - bottom, WndRect.right, WndRect.bottom);//窗口下边框的矩形
	SubtractRectFromRegion (&ScreenRect, pRng);
}

//计算与父窗口的在屏幕中的客户区相交区
static void WindowGCLimitInParentClient(WINDOW *pParentWnd, WINDOW *Window, CLIPREGION *pRng)
{
	RECT ClientRect;
	CLIPREGION *pTempRng;

	if (WindowIsSysControl (Window))
	{
		return;
	}
//得到父窗口的客户区（左上为0,0）
	GetClientRect (pParentWnd, &ClientRect);
//得到父窗口的在屏幕中的客户区
	ClientToScreenRect (pParentWnd, &ClientRect);
//分配一个剪切域，给父窗口的在屏幕中的客户区
	pTempRng = AllocRectRegionIndirect (&ClientRect);
//计算与父窗口的在屏幕中的客户区相交区
	IntersectRegion (pRng, pRng, pTempRng);
	FreeClipRegion (pTempRng);
}

static void WindowClientDCChangeByFrame(WINDOW *Window, DC *dc)
{
	RECT ClientRect;
	CLIPREGION *pTempRng;
	CLIPREGION *pRng = dc->ClipRegion;

	GetClientRect (Window, &ClientRect);
	ClientToScreenRect (Window, &ClientRect);

	pTempRng = AllocRectRegionIndirect (&ClientRect);
	IntersectRegion (pRng, pRng, pTempRng);
	FreeClipRegion (pTempRng);
}

// 更改所有子窗口GC、IC,并发出子窗口重画的消息
// Window：要更改的父窗口
// ParentInvidRect：父窗口无效的区域
// bRedraw：是否重画子窗口
static void ChangeChildWindowGC(WINDOW *Window, LPRECT lpParentInvidRect, bool32_t bRedraw)
{
	WINDOW* _list_;
	struct list_head *_head_ = &Window->ChildListHead;
#undef _item_
#define _item_ ChildListNode
	RECT WndRect, IntersectRect;
	CLIPREGION *ClipRng;

	if (list_empty (_head_))
	{
		return;
	}
//如果窗口不显示
	if (FALSE == WindowIsVisible (Window))
	{
		for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
				& ((_list_)->_item_) != (_head_);
				(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
		{
			if (FALSE == WindowIsVisible (_list_))
			{
				continue;
			}
			//如子窗口为可见
			// 清空子窗口剪切域
			EmptyClipRgn (_list_->ClipRegion);

			// 清空子窗口无效区
			EmptyClipRgn (_list_->InvalidRegion);

			// 更改下级子窗口剪切域
			ChangeChildWindowGC (_list_, NULL, bRedraw);
		}
	}
	else
	{
		// 临时剪切域
		ClipRng = MallocRegion ();
		CopyRegion (ClipRng, Window->ClipRegion);
//ClipRng减去Window的无效区
		WindowGCChangeByNC (Window, ClipRng);

		for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
				& ((_list_)->_item_) != (_head_);
				(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
		{
			if (FALSE == WindowIsVisible (_list_))
			{
				continue;
			}
//如子窗口为可见
			// 求子窗口GC剪切域
			GetWindowRect (_list_, &WndRect);
			EmptyClipRgn (_list_->ClipRegion);
			UnionRectWithRegion (&WndRect, _list_->ClipRegion);
			//去掉无效区的父窗口剪切域 与子窗口剪切域的交集
			IntersectRegion (_list_->ClipRegion, _list_->ClipRegion, ClipRng);
			WindowGCLimitInParentClient (Window, _list_, _list_->ClipRegion);

			if (NULL == lpParentInvidRect)//无传入矩形
			{
				// 更改下级子窗口GC
				ChangeChildWindowGC (_list_, NULL, bRedraw);

				if (bRedraw && FALSE == WindowIsTrans (_list_))
				{
					InvalidateRect (_list_, NULL, TRUE);
				}
			}
			else
			{
				// 子窗口与传入的矩形有相交，相交结果放入IntersectRect
				if (TRUE == IntersectRECT (&IntersectRect, &WndRect, lpParentInvidRect))
				{
					// 更改下级子窗口GC
					ChangeChildWindowGC (_list_, &IntersectRect, bRedraw);

					if (bRedraw && FALSE == WindowIsTrans (_list_))
					{
						ScreenToClientRect (_list_, &IntersectRect);
						InvalidateRect (_list_, &IntersectRect, TRUE);
					}
				}
			}

			// 更改全局GC
			SubtractRegion (ClipRng, ClipRng, _list_->ClipRegion);
		}
//释放变量ClipRng
		FreeClipRegion (ClipRng);
	}
}

// 窗口显示、移动、变活动时，需要更改被掩盖姐妹窗口DC
// Window： 操作的窗口
// OldRect：为NULL时强制更改，否则为原来的窗口屏幕坐标
// bRedraw：是否重画被掩盖姐妹窗口
static void ChangeSiblingGC(WINDOW *Window, LPRECT lpOldRect, LPRECT lpNewRect, bool32_t bRedraw)
{
	CLIPREGION *ClipRng;
	RECT WndRect, IntersectRect;

	// 临时全局GC
	ClipRng = MallocRegion ();

	if ((WINDOW *)NULL != Window->pParentWnd)
	{
		WINDOW* _list_;
		struct list_head *_head_ = &Window->pParentWnd->ChildListHead;
#undef _item_
#define _item_ ChildListNode

		CopyRegion (ClipRng, Window->pParentWnd->ClipRegion);
//父窗口的子窗口们
		for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
				(_list_) != (Window);
				(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
		{
			if (FALSE == WindowIsVisible (_list_))
			{
				continue;
			}

			SubtractRegion (ClipRng, ClipRng, _list_->ClipRegion);
		}

		SubtractRegion (ClipRng, ClipRng, Window->ClipRegion);
//当前窗口的兄弟窗口
		for ((_list_) = list_entry((&Window->_item_)->next, WINDOW, _item_);
				& ((_list_)->_item_) != (_head_);
				(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
		{
			if (FALSE == WindowIsVisible (_list_))
			{
				continue;
			}

			GetWindowRect (_list_, &WndRect);
			EmptyClipRgn (_list_->ClipRegion);
			UnionRectWithRegion (&WndRect, _list_->ClipRegion);//以上三步，只留兄弟窗口矩形为剪切域
			WindowGCChangeByNC (_list_->pParentWnd, _list_->ClipRegion);//让兄弟剪切域在父窗口中变成有效（从父无域区减去）
			IntersectRegion (_list_->ClipRegion, _list_->ClipRegion, ClipRng);//兄弟窗口在上面处理过的父窗口剪切域相交区
			WindowGCLimitInParentClient (Window->pParentWnd, _list_, _list_->ClipRegion);

			if (NULL != lpOldRect)
			{
				if (TRUE == IntersectRECT (&IntersectRect, lpOldRect, &WndRect))
				{
					ChangeChildWindowGC (_list_, &IntersectRect, bRedraw);

					if (bRedraw && FALSE == WindowIsTrans (_list_))
					{
						ScreenToClientRect (_list_, &IntersectRect);
						InvalidateRect (_list_, &IntersectRect, TRUE);
					}
				}
				else
				{
					if (NULL != lpNewRect && TRUE == IntersectRECT (&IntersectRect, lpNewRect, &WndRect))
					{
						ChangeChildWindowGC (_list_, &IntersectRect, bRedraw);
					}
				}
			}
			else
			{
				ChangeChildWindowGC (_list_, NULL, bRedraw);

				if (bRedraw && FALSE == WindowIsTrans (_list_))
				{
					InvalidateRect (_list_, NULL, TRUE);
				}
			}

			// 更改全局GC
			SubtractRectFromRegion (&WndRect, ClipRng);
		}
	}

	FreeClipRegion (ClipRng);
}

//剪切域中减去可见的兄弟矩形
static void WindowGCChangeBySibling(WINDOW *Window)
{
	RECT WndRect;

	if ((WINDOW *)NULL != Window->pParentWnd)
	{
		WINDOW* _list_;
		struct list_head *_head_ = &Window->pParentWnd->ChildListHead;
#undef _item_
#define _item_ ChildListNode

		for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
				(_list_) != (Window);
				(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
		{
			if (FALSE == WindowIsVisible (_list_))
			{
				continue;
			}

			GetWindowRect (_list_, &WndRect);
			//剪切域中减去可见的兄弟矩形
			SubtractRectFromRegion (&WndRect, Window->ClipRegion);
		}
	}
}

static void WindowDCChangeByChildWindow(WINDOW *Window, DC *dc)
{
	WINDOW* _list_;
	struct list_head *_head_ = &Window->ChildListHead;
#undef _item_
#define _item_ ChildListNode

	for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
			& ((_list_)->_item_) != (_head_);
			(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
	{
		if (FALSE == WindowIsVisible (_list_))
		{
			continue;
		}

		if (TRUE == WindowIsTrans (_list_))
		{
			continue;
		}

		SubtractRegion (dc->ClipRegion, dc->ClipRegion, _list_->ClipRegion);
	}
}

//剪切域限定在父窗口客户区内，剪切掉可见（需显示）的兄弟窗口，更改子窗口GC，并重画子窗口
static void ChangeWindowGC(WINDOW *Window)
{
	RECT WndRect;

	if (TRUE == WindowIsVisible (Window))
	{
// 更改窗口GC(剪切域)，清空原剪切域，以窗口外形为新剪切域
		EmptyClipRgn (Window->ClipRegion);
		GetWindowRect (Window, &WndRect);//windows的窗口区windowRect,赋给WndRect
		UnionRectWithRegion (&WndRect, Window->ClipRegion);

//有父窗口，与父窗口相交的为新剪切域（不能超过父窗口）
		if ((WINDOW *)NULL == Window->pParentWnd)  //无父窗口相交剪切域
		{
			IntersectRegion (Window->ClipRegion, Window->ClipRegion, pGUI->ClipRegion);
		}
		else
		{
			IntersectRegion (Window->ClipRegion, Window->ClipRegion, Window->pParentWnd->ClipRegion);
		//剪切域去掉父窗口的无效区	
			WindowGCChangeByNC (Window->pParentWnd, Window->ClipRegion);
		//计算与父窗口的在屏幕中的客户区相交区
			WindowGCLimitInParentClient (Window->pParentWnd, Window, Window->ClipRegion);
		}
		WindowGCChangeBySibling (Window);
		//剪切域中减去可见的兄弟矩形

		// 更改子窗口GC，并重画子窗口
		ChangeChildWindowGC (Window, NULL, TRUE);
	}
	else
	{
		EmptyClipRgn (Window->ClipRegion);

		EmptyClipRgn (Window->InvalidRegion);

		ChangeChildWindowGC (Window, NULL, FALSE);
	}
}

static bool32_t ActiveWindowChangeGC(WINDOW *Window)//更改活动窗口剪切域
{
	NEW_LIST (WndListHead);//一个next和prev指向自身的链表
	WINDOW* _list_;
	struct list_head *_head_ = &WndListHead;
#undef _item_//回朔窗口节点
#define _item_ TraceListNode0

	WINDOW *pParentWnd, *FindWnd = NULL;

// 用_head_来表示父子间TraceListNode0 回朔表
	list_add (&Window->_item_, _head_);//回朔窗口节点加到WndListHead前
	pParentWnd = Window->pParentWnd;
	while ((WINDOW *)NULL != pParentWnd)
	{
		list_add (&pParentWnd->_item_, _head_);
		pParentWnd = pParentWnd->pParentWnd;
	}//依次回朔，把父窗口回朔节点加到 WndListHead前
//回朔，找到一个非上层窗口跳出
// 查找回朔链接中，找到一个非上层窗口的父窗口
	for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
			& ((_list_)->_item_) != (_head_);
			(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
	{
		if (TRUE != IsTopWindow (_list_))
		{
			FindWnd = _list_;//findWnd 不是项层窗口
			break;
		}
	}

//找到的非顶层窗口为空，清空链表，返回
	if ((WINDOW *)NULL == FindWnd)
	{
		// 清空链表
		while (!list_empty (_head_))
		{
			_list_ = list_entry((_head_)->next, WINDOW, _item_);
			list_del (&_list_->_item_);
		}

		return TRUE;
	}

// 改变窗口的ORDER
//从findwnd再回朔，topwindow删除窗口原来的节点，重设，使顶层窗口在父窗口的子窗口链表中位置正确，
	for ((_list_) = FindWnd;
			& ((_list_)->_item_) != (_head_);
			(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
	{
		TopWindow (_list_);
	}

	// 计算窗口GC，子窗口GC（重画）
	ChangeWindowGC (FindWnd);

	// 更改被掩盖兄妹窗口GC
	ChangeSiblingGC (FindWnd, NULL, NULL, FALSE);

	// 重画窗口
	InvalidateRect (FindWnd, NULL, TRUE);

	// 清空链表
	while (!list_empty (_head_))
	{
		_list_ = list_entry((_head_)->next, WINDOW, _item_);
		list_del (&_list_->_item_);
	}

	return FALSE;
}
//更改父窗口无效区
static void ChangeParentICbyWindow(WINDOW *Window)
{
	RECT WndRect;
	WINDOW* pParentWnd = Window->pParentWnd;

	if ((WINDOW *)NULL != pParentWnd)
	{
		GetWindowRect (Window, &WndRect);

		UnionRectWithRegion (&WndRect, pParentWnd->InvalidRegion);

		ScreenToClientRect (pParentWnd, &WndRect);//根据父窗口标题，控件、边框，把子窗口做左上偏移
		InvalidateRect (pParentWnd, &WndRect, TRUE);//当前矩形是父窗口的无效区域
	}
}

static void ChangeChildWindowPos(WINDOW *Window)
{
	WINDOW* _list_;
	struct list_head *_head_ = &Window->ChildListHead;
#undef _item_
#define _item_ ChildListNode
	POINT pt;

	for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
			& ((_list_)->_item_) != (_head_);
			(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
	{
		SetPoint (&pt, Window->WindowRect.left + _list_->ParentPos.x, Window->WindowRect.top + _list_->ParentPos.y);

		SetRect (&_list_->WindowRect, 0, 0,
				 _list_->WindowRect.right - _list_->WindowRect.left,
				 _list_->WindowRect.bottom - _list_->WindowRect.top);
		OffsetRect (&_list_->WindowRect, &pt);

		ChangeChildWindowPos (_list_);
	}
}

static void ChangeSysControlSize(WINDOW *Window)
{
	WINDOW* _list_;
	struct list_head *_head_ = &Window->ChildListHead;
#undef _item_
#define _item_ ChildListNode

	for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
			& ((_list_)->_item_) != (_head_);
			(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
	{
		if (WindowIsSysControl (_list_))
		{
			SendMessage (_list_, EV_RESIZE, 0, 0);
		}
	}
}
//是顶层窗口，是兄弟窗口中的顶层窗口
static bool32_t IsTopWindow(WINDOW *Window)
{
	if ((WINDOW *)NULL == Window->pParentWnd)
	{
		return TRUE;
	}
	else
	{//不是桌面窗口的情况，判断在在父窗口的子窗口中节点的位置是不是正确
		WINDOW* _list_;
		struct list_head *_head_ = &Window->pParentWnd->ChildListHead;
#undef _item_
#define _item_ ChildListNode

		if (WindowIsTopmost (Window))
		{
			(_list_) = list_entry((_head_)->next, WINDOW, _item_);
			if (_list_ == Window)
			{
				return TRUE;
			}

			return FALSE;
		}
		else
		{
			for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
					& ((_list_)->_item_) != (_head_);
					(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
			{
				if (FALSE == WindowIsTopmost (_list_))
				{
					if (_list_ == Window)
					{
						return TRUE;
					}
					else
					{
						return FALSE;
					}
				}
			}
		}
	}

	return FALSE;
}

static void TopWindow(WINDOW* Window)
{
	if ((WINDOW *)NULL == Window->pParentWnd)
	{
		return;
	}

	list_del (&Window->ChildListNode);
	AddToParent (Window->pParentWnd, Window);
}
//判断添加的窗口，在顶层添加到父窗口的子窗口节点，
//不在顶层，添加到其它不在顶层的父窗口的子窗口节点前
static void AddToParent(WINDOW *pParentWnd, WINDOW *pAddWnd)
{
	WINDOW* _list_;
	struct list_head *_head_ = &pParentWnd->ChildListHead;
#undef _item_
#define _item_ ChildListNode
//桌面窗口的情况
	if ((WINDOW *)NULL == pParentWnd)
	{
		pGUI->pDesktopWnd = pAddWnd;
		return;
	}
//添加的窗口在界面顶层
	if (WindowIsTopmost (pAddWnd))
	{
		list_add (&pAddWnd->_item_, _head_);
	}
	else
	{
		if (WindowIsControl (pAddWnd))
		{
			list_add_tail (&pAddWnd->_item_, _head_);
		}
		else
		{
			for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);//得到子窗口的指针
					& ((_list_)->_item_) != (_head_);
					(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))  //下个子窗口指针
			{
				if (FALSE == WindowIsTopmost (_list_))
				{
					list_add_tail (&pAddWnd->_item_, &_list_->_item_);
					return;
				}
			}

			list_add_tail (&pAddWnd->_item_, _head_);
		}
	}
}

static WINDOW* GetActiveChildWindow(WINDOW* Window)
{
	WINDOW* _list_;
	struct list_head *_head_ = &Window->ChildListHead;
#undef _item_
#define _item_ ChildListNode

	if (list_empty(_head_))
	{
		return (WINDOW*)NULL;
	}

	for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
			& ((_list_)->_item_) != (_head_);
			(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
	{
		if (FALSE == WindowIsMainWnd (_list_))
		{
			continue;
		}

		if (FALSE == WindowIsVisible (_list_))
		{
			continue;
		}

		if (FALSE == IsWindowEnabled (_list_))
		{
			continue;
		}

		return _list_;
	}

	return (WINDOW*)NULL;
}
////更改窗口有无效状态
static void ChangeNCState(WINDOW *pNewWnd, WINDOW *pOldWnd)
{
	NEW_LIST (WndListHead1);//声明列表 WndListHead1----------------------
	WINDOW* _list1_;
	struct list_head *_head1_ = &WndListHead1;
#undef _item1_
#define _item1_ TraceListNode1//回朔窗口节点1

	NEW_LIST (WndListHead2);//声明列表 WndListHead2-----------------------
	WINDOW* _list2_;
	struct list_head *_head2_ = &WndListHead2;
#undef _item2_
#define _item2_ TraceListNode2//回朔窗口节点2

	WINDOW *pParentWnd;

	if (NULL == pNewWnd)
	{
		return;
	}

	if (pNewWnd == pOldWnd)
	{
		return;
	}

	if (NULL != pOldWnd)
	{
		// 旧窗口回朔父窗口，回朔节点2,形成链表2
		list_add (&pOldWnd->_item2_, _head2_);
		pParentWnd = pOldWnd->pParentWnd;
		while ((WINDOW *)NULL != pParentWnd)
		{
			list_add (&pParentWnd->_item2_, _head2_);
			pParentWnd = pParentWnd->pParentWnd;
		}
	}

	// 新窗口回朔父窗口，回朔节点1,形成链表1
	list_add (&pNewWnd->_item1_, _head1_);
	pParentWnd = pNewWnd->pParentWnd;
	while ((WINDOW *)NULL != pParentWnd)
	{
		list_add (&pParentWnd->_item1_, _head1_);
		pParentWnd = pParentWnd->pParentWnd;
	}

//对于不同的窗口，并发出EV_NCACTIVE消息
	if (NULL != pOldWnd)
	{//
		for ((_list2_) = list_entry((_head2_)->next, WINDOW, _item2_),
				(_list1_) = list_entry((_head1_)->next, WINDOW, _item1_);
				&((_list2_)->_item2_) != (_head2_)     &&     &((_list1_)->_item1_) != (_head1_);
				(_list2_) = list_entry((_list2_)->_item2_.next, WINDOW, _item2_),
				(_list1_) = list_entry((_list1_)->_item1_.next, WINDOW, _item1_))
		{
			if (_list2_ != _list1_)
			{
				break;//两个节点回朔窗口比较，有不同跳出
			}
		}

		if (&((_list1_)->_item1_) != (_head1_))
		{//继续向后，并发出EV_NCACTIVE消息
			for (;
					& ((_list2_)->_item2_) != (_head2_);
					(_list2_) = list_entry((_list2_)->_item2_.next, WINDOW, _item2_))
			{
				SendMessage (_list2_, EV_NCACTIVE, FALSE, 0);
			}

			for (;
					& ((_list1_)->_item1_) != (_head1_);
					(_list1_) = list_entry((_list1_)->_item1_.next, WINDOW, _item1_))
			{
				SendMessage (_list1_, EV_NCACTIVE, TRUE, 0);
			}
		}
	}
	else
	{
		for ((_list1_) = list_entry((_head1_)->next, WINDOW, _item1_);
				& ((_list1_)->_item1_) != (_head1_);
				(_list1_) = list_entry((_list1_)->_item1_.next, WINDOW, _item1_))
		{
			SendMessage (_list1_, EV_NCACTIVE, TRUE, 0);
		}
	}
//清空用于查找的链表
	if (NULL != pOldWnd)
	{
		if (NULL != pOldWnd)
		{
			// 清空链表2
			while (!list_empty (_head2_))
			{
				_list2_ = list_entry((_head2_)->next, WINDOW, _item2_);
				list_del (&_list2_->_item2_);
			}
		}
	}

	// 清空链表1
	while (!list_empty (_head1_))
	{
		_list1_ = list_entry((_head1_)->next, WINDOW, _item1_);
		list_del (&_list1_->_item1_);
	}
}

// 窗口GC、DC改变，上层窗口的GC、DC改变，子窗口GC、DC改变
// 返回是否要重画窗口
static bool32_t ActiveWindow(WINDOW* Window)
{
	WINDOW *pOldActive, *pParentWnd, *pActiveChild, *pFocusWnd;

	if (NULL == Window)
	{
		return FALSE;
	}
//不可见，不使能，返回错误
	if (FALSE == WindowIsVisible (Window) ||
			FALSE == IsWindowEnabled (Window))
	{
		return FALSE;
	}
//是控件返回
	if (TRUE == WindowIsControl (Window))
	{
		return FALSE;
	}
//是菜单或输入法，激活更改GC.
	if (TRUE == WindowIsMenu (Window) ||
			TRUE == WindowIsIme (Window))
	{//更改窗口剪切域，重画子窗口
		ActiveWindowChangeGC (Window);
		return TRUE;
	}

//递归向上检查父窗口，需要全是可显示的，如有不显示就返回
	pParentWnd = Window->pParentWnd;
	while (NULL != pParentWnd)
	{
		if (FALSE == WindowIsVisible (pParentWnd) ||
				FALSE == IsWindowEnabled (pParentWnd))
		{
			return FALSE;
		}

		pParentWnd = pParentWnd->pParentWnd;
	}


//递归向上，设置为激活
	pActiveChild = Window;
	pParentWnd = pActiveChild->pParentWnd;
	while (NULL != pParentWnd)
	{
		pParentWnd->pActiveChild = pActiveChild;

		pActiveChild = pParentWnd;
		pParentWnd = pActiveChild->pParentWnd;
	}
//复查父窗口的激活窗口
	pParentWnd = Window;
	do
	{
		if (NULL != pParentWnd->pActiveChild)
		{
			pActiveChild = pParentWnd->pActiveChild;
		}
		else
		{
			pActiveChild = GetActiveChildWindow (pParentWnd);
		}

		if (NULL == pActiveChild)
		{
			break;
		}

		pParentWnd = pActiveChild;
	}while (TRUE);
	
//激活窗口为父窗口，更改激活窗口的剪切域
	if (pGUI->pActiveWnd == pParentWnd)
	{
		ActiveWindowChangeGC (pParentWnd);

		return FALSE;
	}
	
//更改激活窗口为父窗口
	pOldActive = pGUI->pActiveWnd;
	pGUI->pActiveWnd = pParentWnd;
	ChangeNCState (pParentWnd, pOldActive);

	pFocusWnd = pParentWnd;
	while (NULL != pFocusWnd->pFocusWnd)
	{
		pFocusWnd = pFocusWnd->pFocusWnd;
	}
	
//设置焦点窗口
	SetFocus (pFocusWnd);

	if (NULL != pOldActive)
	{
		if (GetDesktopWindow () != pParentWnd)
		{
			ActiveWindowChangeGC (pParentWnd);

			return TRUE;
		}

		return FALSE;
	}

	return TRUE;
}

WINDOW *SetFocus(WINDOW *Window)
{
	WINDOW* _list_;
	struct list_head *_head_ = &pGUI->FocusWndHead;
#undef _item_
#define _item_ FocusListNode
	NEW_LIST (WndListHead1);
	WINDOW* _list1_;
	struct list_head *_head1_ = &WndListHead1;
#undef _item1_
#define _item1_ TraceListNode1
	WINDOW *pParentWnd, *pOldFocusWnd, *pFocusWnd;

	if (NULL == Window ||
			TRUE == WindowIsIme (Window))
	{//窗口为输入法返回
		return NULL;
	}

//递归向上查父窗口是否可见和使能的
	pParentWnd = Window;
	while (NULL != pParentWnd)
	{
		if (FALSE == WindowIsVisible (pParentWnd) ||
				FALSE == IsWindowEnabled (pParentWnd))
		{

			return NULL;
		}

		pParentWnd = pParentWnd->pParentWnd;
	}

	pParentWnd = Window;
	while (NULL != pParentWnd)
	{
		if (TRUE == WindowIsControl (pParentWnd) &&
				FALSE == WindowIsTabStop (pParentWnd))
		{//是控件，不是tabstop
			pParentWnd = pParentWnd->pParentWnd;

			continue;
		}

		break;
	}
	Window = pParentWnd;
//焦点窗口切换
	pOldFocusWnd   = pGUI->pFocusWnd;

	if (Window == pOldFocusWnd)
	{
		return pOldFocusWnd;
	}

	pGUI->pFocusWnd = Window;

	pFocusWnd = Window;
	pParentWnd = pFocusWnd->pParentWnd;
	while (NULL != pParentWnd)
	{
		list_add (&pParentWnd->_item1_, _head1_);

		pParentWnd->pFocusWnd = pFocusWnd;

		pFocusWnd = pParentWnd;
		pParentWnd = pFocusWnd->pParentWnd;
	}

	list_add_tail (&Window->_item1_, _head1_);

	if (FALSE == WindowIsControl (Window))
	{
		if (NULL != Window->pFocusWnd)
		{
			pFocusWnd = Window->pFocusWnd;
		}
		else
		{
			pFocusWnd = GetActiveChildWindow (Window);
		}

		while (NULL != pFocusWnd)
		{
			list_add_tail (&pFocusWnd->_item1_, _head1_);

			if (NULL != pFocusWnd->pFocusWnd)
			{
				pFocusWnd = pFocusWnd->pFocusWnd;
			}
			else
			{
				pFocusWnd = GetActiveChildWindow (pFocusWnd);
			}
		}
	}

	for ((_list_) = list_entry((_head_)->next, WINDOW, _item_),
			(_list1_) = list_entry((_head1_)->next, WINDOW, _item1_);
			& ((_list_)->_item_) != (_head_) && &((_list1_)->_item1_) != (_head1_);
			(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_),
			(_list1_) = list_entry((_list1_)->_item1_.next, WINDOW, _item1_))
	{
		if (_list_ != _list1_)
		{
			break;
		}
	}
//取消焦点
	for (; & ((_list_)->_item_) != (_head_); (_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
	{
		SendMessage (_list_, EV_KILLFOCUS, 0, 0);
	}
//设置焦点
	for (; & ((_list1_)->_item1_) != (_head1_); (_list1_) = list_entry((_list1_)->_item1_.next, WINDOW, _item1_))
	{
		SendMessage (_list1_, EV_SETFOCUS, 0, 0);
	}

	while (!list_empty (_head_))
	{
		_list_ = list_entry((_head_)->next, WINDOW, _item_);
		list_del (&_list_->_item_);
	}
//head1的回朔窗口加入到head焦点窗口中
	for ((_list1_) = list_entry((_head1_)->next, WINDOW, _item1_);
			& ((_list1_)->_item1_) != (_head1_);
			(_list1_) = list_entry((_list1_)->_item1_.next, WINDOW, _item1_))
	{
		list_add_tail (&_list1_->_item_, _head_);
	}

	while (!list_empty (_head1_))
	{
		_list1_ = list_entry((_head1_)->next, WINDOW, _item1_);
		list_del (&_list1_->_item1_);
	}

	return pOldFocusWnd;
}

DC *BeginNcPaint(WINDOW *Window, uint32_t wParam, uint32_t lParam)
{
	DC* hdc;

	hdc = GetWindowDC (Window);
	if (NULL == hdc)
	{
		return NULL;
	}

	if (FALSE == wParam)
	{
		IntersectRegion (hdc->ClipRegion, hdc->ClipRegion, Window->InvalidRegion);
	}

	return hdc;
}

void EndNcPaint(WINDOW *Window, DC *hdc, uint32_t wParam, uint32_t lParam)
{
	ReleaseDC (Window, hdc);
}

DC *BeginErase(WINDOW *Window, uint32_t wParam, uint32_t lParam)
{
	DC* hdc;

	if (TRUE == WindowIsTrans (Window))
	{
		return NULL;
	}

	hdc = GetDC (Window);
	if (NULL == hdc)
	{
		return NULL;
	}

	if (NULL != (void *)lParam)
	{
		CLIPREGION *InvalidRegion = AllocRectRegionIndirect ((RECT *)lParam);
		IntersectRegion (hdc->ClipRegion, hdc->ClipRegion, InvalidRegion);
		FreeClipRegion (InvalidRegion);
	}
	else
	{
		IntersectRegion (hdc->ClipRegion, hdc->ClipRegion, Window->InvalidRegion);
	}

	return hdc;
}

void EndErase(WINDOW *Window, DC *hdc, uint32_t wParam, uint32_t lParam)
{
	ReleaseDC (Window, hdc);
}

DC *BeginPaint(WINDOW *Window, uint32_t wParam, uint32_t lParam)
{
	DC* hdc;

	if (TRUE == WindowIsTrans (Window))
	{
		RECT InvRect;
		WINDOW* pParentWnd = Window->pParentWnd;

		if (NULL != (void *)lParam)
		{
			CopyRect (&InvRect, (RECT *)lParam);
		}
		else
		{
			GetWindowRect (Window, &InvRect);
		}

		SendMessage (pParentWnd, EV_ERASEBKGND, 1, (uint32_t)&InvRect);
		SendMessage (pParentWnd, EV_PAINT, 1, (uint32_t)&InvRect);
	}

	hdc = GetDC (Window);
	if (NULL == hdc)
	{
		return NULL;
	}

	if (NULL != (void *)lParam)
	{
		CLIPREGION *InvalidRegion = AllocRectRegionIndirect ((RECT *)lParam);
		IntersectRegion (hdc->ClipRegion, hdc->ClipRegion, InvalidRegion);
		FreeClipRegion (InvalidRegion);
	}
	else
	{
		IntersectRegion (hdc->ClipRegion, hdc->ClipRegion, Window->InvalidRegion);
	}

	return hdc;
}

void EndPaint(WINDOW *Window, DC *hdc, uint32_t wParam, uint32_t lParam)
{
	if (NULL == (void *)lParam)
	{
		EmptyClipRgn (Window->InvalidRegion);
	}

	if (0 == wParam)
	{
		WINDOW* _list_;
		struct list_head *_head_ = &Window->ChildListHead;
#undef _item_
#define _item_ ChildListNode

		for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
				& ((_list_)->_item_) != (_head_);
				(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
		{
			if (TRUE == WindowIsTrans (_list_))
			{
				RECT WndRect;

				GetWindowRect (_list_, &WndRect);

				if (TRUE == RectInRegion (hdc->ClipRegion, &WndRect))
				{
					InvalidateRect (_list_, NULL, FALSE);
				}
			}
		}
	}

	ReleaseDC (Window, hdc);
}

static void KillFocus(WINDOW *Window)
{
	WINDOW* _list_;
	struct list_head *_head_ = &pGUI->FocusWndHead;
#undef _item_
#define _item_ FocusListNode
	WINDOW *pParentWnd;

	pParentWnd = Window->pParentWnd;
	if (pParentWnd->pFocusWnd == Window)
	{
		pParentWnd->pFocusWnd = NULL;
	}

	for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
			& ((_list_)->_item_) != (_head_);
			(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
	{
		if (_list_ == Window)
		{
			break;
		}
	}

	if (&((_list_)->_item_) != (_head_))
	{
		WINDOW* _del_, *pFocusWnd = NULL;

		for (; & ((_list_)->_item_) != (_head_); )
		{
			_del_ = _list_;
			SendMessage (_del_, EV_KILLFOCUS, 0, 0);

			if (pGUI->pFocusWnd == _list_)
			{
				pFocusWnd = Window->pParentWnd;
			}

			(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_);

			list_del (&_del_->_item_);
		}

		if (NULL != pFocusWnd)
		{
			SetFocus (pFocusWnd);
		}
	}
}

static void DeactiveWindow(WINDOW *Window)
{
	WINDOW *pParentWnd;

	if (TRUE == IsWindowNcActive (Window))
	{
		pParentWnd = Window->pParentWnd;

		if (NULL != pParentWnd)
		{
			pParentWnd->pActiveChild = NULL;

			ShowWindow (pParentWnd, SW_SHOW);
		}
	}
}

int32_t DefWinProc(WINDOW *Window, uint32_t Message, uint32_t wParam, uint32_t lParam)
{
	int32_t Result = 0;

	switch(Message)
	{
	case EV_NCPAINT:
	{
		DC* hdc;
		RECT WndRect, TmpRect;

		hdc = BeginNcPaint (Window, wParam, lParam);
		if (NULL == hdc)
		{
			break;
		}

		GetWindowRect (Window, &WndRect);
		ScreenToWindowRect (Window, &WndRect);

		if (TRUE == WndIsMainWnd (Window) &&
				TRUE == WindowHaveVScrollBar (Window))
		{
			GetWindowMenuRect (Window, &TmpRect);

			TmpRect.left  = TmpRect.right;
			TmpRect.right = TmpRect.left + GetSystemMetrics (SM_CXVSCROLL);

			SetFgColor (hdc, RGB (239, 238, 226));
			FillRect (hdc, &TmpRect);
		}

		if (TRUE == WindowHaveHScrollBar (Window) &&
				TRUE == WindowHaveVScrollBar (Window))
		{
			GetWindowScrollBarRect (Window, TRUE, &TmpRect);
			TmpRect.left  = TmpRect.right;
			TmpRect.right = TmpRect.left + GetSystemMetrics (SM_CXVSCROLL);

			SetFgColor (hdc, RGB (239, 238, 226));
			FillRect (hdc, &TmpRect);
		}

		if (TRUE == WindowHaveCaption (Window))
		{
			int32_t i, w, h;

			w = 10;
			h = GetSystemMetrics (SM_CYCAPTION);
			SetRect (&TmpRect, WndRect.left, WndRect.top, WndRect.right, h);

			if (TRUE == IsWindowNcActive (Window))
			{
				for (i = 0; i < (WndRect.right + 9) / 10; i++)
				{
					DrawBitmap (hdc, i * w, 0, GetSystemBitmap (BMP_CAPTION_ID),
								0, 0, w, h);
				}
			}
			else
			{
				for (i = 0; i < (WndRect.right + 9) / 10; i++)
				{
					DrawBitmap (hdc, i * w, 0, GetSystemBitmap (BMP_CAPTION_ID),
								w, 0, w, h);
				}
			}

			if (NULL != Window->pCaption)
			{
				SetFgColor (hdc, RGB (255, 255, 255));
				DrawText (hdc, Window->pCaption, strlen (Window->pCaption), &TmpRect, DT_CENTER);
			}

			w = GetSystemMetrics (SM_CXCLOSEBOX);
			h = GetSystemMetrics (SM_CYCLOSEBOX);
			GetCloseBoxRect (Window, &TmpRect);
			ScreenToWindowRect (Window, &TmpRect);
			if (Window->NcMouseFlag == 0x2)
			{
				DrawBitmap (hdc, TmpRect.left, TmpRect.top, GetSystemBitmap (BMP_CLOSE_BOX_ID),
							w, 0, w, h);
			}
			else
			{
				DrawBitmap (hdc, TmpRect.left, TmpRect.top, GetSystemBitmap (BMP_CLOSE_BOX_ID),
							0, 0, w, h);
			}
		}

		if (TRUE == WindowHaveBoard (Window))
		{
			if (TRUE == IsWindowNcActive (Window))
			{
				SetFgColor (hdc, RGB (0, 30, 160));
			}
			else
			{
				SetFgColor (hdc, RGB (127, 127, 127));
			}
			DrawRect (hdc, &WndRect);
		}

		EndNcPaint (Window, hdc, wParam, lParam);
		break;
	}

	case EV_ERASEBKGND:
	{
		DC* hdc;
		RECT ClientRect;

		hdc = BeginErase (Window, wParam, lParam);
		if (NULL == hdc)
		{
			break;
		}

		GetClientRect (Window, &ClientRect);

		SetFgColor (hdc, RGB (239, 238, 226));
		FillRect (hdc, &ClientRect);

		EndErase (Window, hdc, wParam, lParam);
		break;
	}

	case EV_PAINT:
	{
		DC* hdc;

		hdc = BeginPaint (Window, wParam, lParam);
		if (NULL == hdc)
		{
			break;
		}

		EndPaint (Window, hdc, wParam, lParam);
		break;
	}

	case EV_LBUTTONDOWN:
	{
		if (TRUE == WindowHaveCaption (Window))
		{
			POINT pt;
			RECT WndRect, TmpRect;

			pt.x = LOuint16_t (lParam);
			pt.y = HIuint16_t (lParam);
			ScreenToWindow (Window, &pt);//点变为相对于窗口左上的位置点

			GetCloseBoxRect (Window, &TmpRect);
			ScreenToWindowRect (Window, &TmpRect);//矩形相对于窗口左上的位置
			if (PtInRect (&TmpRect, &pt))//点击在关闭按键上
			{
				Window->NcMouseFlag = 0x2;//在关闭键上

				WindowToClientRect (Window, &TmpRect);
				InvalidateRect (Window, &TmpRect, FALSE);

				SetCapture (Window);
			}
			else
			{
				GetWindowRect (Window, &WndRect);
				ScreenToWindowRect (Window, &WndRect);
				WndRect.bottom = GetSystemMetrics (SM_CYCAPTION);
				if (PtInRect (&WndRect, &pt))
				{
					Window->NcMouseFlag = 0x1;//在标题栏上

					SetCapture (Window);
					SetPoint (&Window->MovePos, pt.x, pt.y);
				}
			}
		}

		if (TRUE == WindowIsControl (Window))
		{
			WINDOW *ParentWnd = Window->pParentWnd;

			while (TRUE == WindowIsControl (ParentWnd))
			{
				ParentWnd = ParentWnd->pParentWnd;
			}

			ShowWindow (ParentWnd, SW_SHOW);
		}
		else
		{
			ShowWindow (Window, SW_SHOW);
		}

		SetFocus (Window);
		break;
	}

	case EV_LBUTTONUP:
	{
		if (0x1 == Window->NcMouseFlag)
		{
			Window->NcMouseFlag = 0x0;
			ReleaseCapture ();
			SetPoint (&Window->MovePos, 0, 0);
		}
		else if (0x2 == Window->NcMouseFlag)
		{
			POINT pt;
			RECT TmpRect;

			pt.x = LOuint16_t (lParam);
			pt.y = HIuint16_t (lParam);
			ScreenToWindow (Window, &pt);

			GetCloseBoxRect (Window, &TmpRect);
			ScreenToWindowRect (Window, &TmpRect);
			if (PtInRect (&TmpRect, &pt))
			{
				PostMessage (Window, EV_CLOSE, 0, 0);
			}
			Window->NcMouseFlag = 0x0;
			ReleaseCapture ();

			WindowToClientRect (Window, &TmpRect);
			InvalidateRect (Window, &TmpRect, FALSE);
		}
		break;
	}

	case EV_RBUTTONDOWN:
	case EV_RBUTTONUP:
		if (TRUE == WindowIsControl (Window))
		{
			WINDOW *ParentWnd = Window->pParentWnd;

			while (TRUE == WindowIsControl (ParentWnd))
			{
				ParentWnd = ParentWnd->pParentWnd;
			}

			ShowWindow (ParentWnd, SW_SHOW);
		}
		else
		{
			ShowWindow (Window, SW_SHOW);
		}

		SetFocus (Window);
		break;

	case EV_MOUSEMOVE:
	{
		POINT p;
		RECT ClientRect;
		bool32_t bResetCursor;
		WINDOW *pParentWnd;

		p.x = LOuint16_t (lParam);
		p.y = HIuint16_t (lParam);

		if (Window->NcMouseFlag == 0x1)
		{
			bResetCursor = FALSE;

			pParentWnd = GetParent (Window);
			GetClientRect (pParentWnd, &ClientRect);
			ClientToScreenRect (pParentWnd, &ClientRect);
			if (p.x < ClientRect.left)
			{
				p.x = ClientRect.left;
				bResetCursor = TRUE;
			}
			if (p.x > ClientRect.right)
			{
				p.x = ClientRect.right;
				bResetCursor = TRUE;
			}
			if (p.y < ClientRect.top)
			{
				p.y = ClientRect.top;
				bResetCursor = TRUE;
			}
			if (p.y > ClientRect.bottom)
			{
				p.y = ClientRect.bottom;
				bResetCursor = TRUE;
			}

			if (TRUE == bResetCursor)
			{
				SetCursorPos (p.x, p.y);
			}
			else
			{
				SetPoint (&pGUI->CursorPos, p.x, p.y);
			}

			ScreenToWindow (pParentWnd, &p);
			MoveWindow (Window, p.x - Window->MovePos.x, p.y - Window->MovePos.y, -1, -1, TRUE);
		}
		else if (Window->NcMouseFlag == 0x2)
		{
			SetPoint (&pGUI->CursorPos, p.x, p.y);
		}
		else
		{
			SetPoint (&pGUI->CursorPos, p.x, p.y);
		}

		SetCursor (GetWindowCursor (Window));
		break;
	}

	case EV_KEYDOWN:
	case EV_KEYUP:
	case EV_CHAR:
	{
		WINDOW* pSendWnd;

		pSendWnd = Window->pFocusWnd;
		if ((WINDOW*)NULL != pSendWnd)
		{
			SendMessage (pSendWnd, Message, (uint32_t)wParam, (uint32_t)lParam);
			break;
		}
		break;
	}

	case EV_SETFOCUS:
	{
		POINT pt;
		RECT WndRect;

		if (WindowIsControl (Window))
		{
			if (WindowNeedIme (Window))
			{
				ShowIme (Window, TRUE);
			}
		}

		GetCursorPos (&pt);
		GetWindowRect (Window, &WndRect);

		if (PtInRect (&WndRect, &pt))
		{
			SetCursor (GetWindowCursor (Window));
		}
		break;
	}

	case EV_KILLFOCUS:
	{
		if (WindowIsControl (Window))
		{
			if (WindowNeedIme (Window))
			{
				ShowIme (Window, FALSE);
			}
		}
		break;
	}

	case EV_NCACTIVE:////窗口无效区
	{
		Window->bNcActive = (bool32_t)wParam;

		SendMessage (Window, EV_NCPAINT, TRUE, 0);
		break;
	}

	case EV_MOVEING:
	{
		RECT OldRect, NewRect;
		WINDOW* pParentWnd = Window->pParentWnd;

		GetWindowRect (Window, &OldRect);

		// 更新父窗口IC
		ChangeParentICbyWindow (Window);

		CopyRect (&Window->WindowRect, (RECT *)lParam);

		SetPoint (&Window->ParentPos, Window->WindowRect.left, Window->WindowRect.top);
		if (NULL != pParentWnd)
		{
			ScreenToWindow (pParentWnd, &Window->ParentPos);
		}
		GetWindowRect (Window, &NewRect);

		// 更改所有子窗口坐标
		ChangeChildWindowPos (Window);

		// 计算本窗口GC，子窗口GC（重画）
		ChangeWindowGC (Window);

		// 更改被掩盖兄妹窗口GC
		ChangeSiblingGC (Window, &OldRect, &NewRect, TRUE);

		// 本窗口DC改变，重画
		InvalidateRect (Window, NULL, TRUE);
		break;
	}

	case EV_SIZEING:
	{
		int32_t w, h;
		RECT OldRect, NewRect;

		w = LOuint16_t (lParam);
		h = HIuint16_t (lParam);

		GetWindowRect (Window, &OldRect);

		// 更新父窗口IC
		ChangeParentICbyWindow (Window);

		Window->WindowRect.right  = Window->WindowRect.left + w;
		Window->WindowRect.bottom = Window->WindowRect.top  + h;
		GetWindowRect (Window, &NewRect);

		// 更改所有子窗口大小
		ChangeSysControlSize (Window);

		// 计算本窗口GC，子窗口GC（重画）
		ChangeWindowGC (Window);

		// 更改被掩盖兄妹窗口GC
		ChangeSiblingGC (Window, &OldRect, &NewRect, TRUE);

		// 本窗口DC改变，重画
		InvalidateRect (Window, NULL, TRUE);
		break;
	}

	case EV_ENABLE:
		if (TRUE == (bool32_t)lParam)
		{
			ExcludeWindowStyle (Window, WS_DISABLED);
		}
		else
		{
			IncludeWindowStyle (Window, WS_DISABLED);

			// Focus control
			KillFocus (Window);

			// Active window
			DeactiveWindow (Window);
		}
		break;

	case EV_CLOSE:
		return DestroyWindow (Window);
		break;

	default:
		Result = 0;
		break;
	}

	return Result;
}

WINDOW* CreateWindow(const int8_t* pClassName, const int8_t* pCaption, uint32_t nStyle,
					 int32_t X, int32_t Y, int32_t W, int32_t H, WINDOW *pParentWnd, MENU *pMenu, void *pPrivData)
{
	int32_t                 nLen;
	WINDOW *           Window;
	WNDCLASS *         pWndClass;
	RECT                MenuRect, ScrollRect;

//内存区分配一个窗口
	Window = (WINDOW*)Malloc (sizeof (WINDOW));
	if ((WINDOW*)NULL == Window)
	{
		return NULL;
	}
	memset (Window, 0, sizeof (WINDOW));

//参数传入
	//传递进窗口类型
	Window->nStyle       = nStyle;
	//标题分配
	if (NULL == pCaption)
	{
		pCaption = "";
	}

	nLen = strlen (pCaption);
	Window->pCaption = (char *)Malloc (nLen + 1);
	if (NULL == Window->pCaption)
	{
		goto FAILED;
	}
	strcpy (Window->pCaption, pCaption);//char *strcpy(char* dest, const char *src);
	//其它
	Window->EraseBackGndMsg  = NULL;
	Window->PaintMsg   = NULL;
	Window->NcPaintMsg = NULL;
//根据窗口类名，获取窗口类
	pWndClass = GetClassInfo (pClassName);
	if (NULL == pWndClass)
	{
		goto FAILED;
	}
//取得窗口类的消息处理函数
	Window->WinProc = pWndClass->WinProc;
	if (NULL == Window->WinProc)
	{
		Window->WinProc = DefWinProc;
	}
//是控件
	if (WindowIsControl (Window))
	{
		if (pParentWnd == NULL)
		{
			goto FAILED;
		}

		if (WindowIsTopmost (Window))  //控件父窗口不能为空，不能是顶层
		{
			goto FAILED;
		}
	}
	else  //不是控件
	{
		ExcludeWindowStyle (Window, WS_VISIBLE);//排斥窗口类型，取消显示
	}
//不是会话和菜单
	if (FALSE == WindowIsDialog (Window) && FALSE == WindowIsMenu (Window))
	{
		if (NULL != GetDesktopWindow () && NULL == pParentWnd)  //有桌面窗口，且父窗口为空
		{
			pParentWnd = GetDesktopWindow ();
		}

		Window->pBindWnd    = NULL;
		Window->pParentWnd  = pParentWnd;

		if ((WINDOW *)NULL == pParentWnd)
		{
			SetRect (&Window->WindowRect, X, Y, X + W, Y + H);//无父窗口，传递进来的尺寸为窗口的矩形
		}
		else
		{
			POINT pt;

			if (FALSE == WindowIsSysControl (Window))
			{
				SetPoint (&pt, X, Y);
				ClientToWindow (pParentWnd, &pt);//子窗口位置，要加上父窗口的标题或边框

				X = pt.x;
				Y = pt.y;
			}

			SetRect (&Window->WindowRect,
					 pParentWnd->WindowRect.left + X, pParentWnd->WindowRect.top + Y,
					 pParentWnd->WindowRect.left + X + W, pParentWnd->WindowRect.top + Y + H);//加上父窗口的偏置
		}
	}
	else  //是会话or菜单
	{
		Window->pBindWnd    = pParentWnd;//会话or菜单，父窗口为边框窗口
		Window->pParentWnd  = GetDesktopWindow ();//桌面窗口为做为会话or菜单父窗口

		SetRect (&Window->WindowRect,
				 Window->pBindWnd->WindowRect.left + X, Window->pBindWnd->WindowRect.top + Y,
				 Window->pBindWnd->WindowRect.left + X + W, Window->pBindWnd->WindowRect.top + Y + H);
	}
	SetPoint (&Window->ParentPos, X, Y);//父窗口位置，转到当前位置

	AddToParent (Window->pParentWnd, Window);//子窗口加入父窗口的子窗口节点链表中ChildList Node

	Window->pMenu     = pMenu;
	Window->pCursor       = pWndClass->pCursor;
	Window->pCaret        = NULL;
	Window->pFocusWnd     = NULL;
	Window->pActiveChild  = NULL;
	LIST_HEAD_RESET(&Window->ChildListHead);
	LIST_HEAD_RESET(&Window->TimerListHead);

	Window->bNcActive     = FALSE;
	Window->NcMouseFlag   = 0;
	SetPoint (&Window->MovePos, 0, 0);

	Window->ClipRegion    = MallocRegion ();//分配空域做窗口剪切域
	Window->InvalidRegion = MallocRegion ();//分配空域做无效区域
	ResetWindowInvalidRegion (Window);//窗口矩形，传递给窗口无效矩形

	Window->WndThread = GetCurrentThreadId ();

	Window->pPrivData = pPrivData;

	if (WndIsMainWnd (Window))
	{
		WINDOW *pMenuWnd;
		GetWindowMenuRect (Window, &MenuRect);////把window的窗口矩形，改成以左上为0,0,去掉标题、边框、菜单、卷栏后的矩形给MenuRect矩形
		//如果有标题和边框和菜单，MenuRect矩形相应向右下移动
		pMenuWnd = CreateWindow ("mainmenu", "menu", WS_CONTROL | WS_VISIBLE | WS_SYSCONTROL,
								 MenuRect.left, MenuRect.top, MenuRect.right - MenuRect.left, MenuRect.bottom - MenuRect.top,
								 Window, (MENU *)8002, Window->pMenu);

		if (NULL == pMenuWnd)
		{
			goto FAILED;
		}
	}

	if (WindowHaveHScrollBar (Window))
	{
		GetWindowScrollBarRect (Window, TRUE, &ScrollRect);
		Window->pHSrollBar = CreateWindow ("bmpscrollbar", "", WS_CONTROL | WS_VISIBLE | SBS_HORZ | WS_SYSCONTROL,
										   ScrollRect.left, ScrollRect.top, ScrollRect.right - ScrollRect.left, ScrollRect.bottom - ScrollRect.top,
										   Window, (MENU *)8000, GetSystemBitmap (BMP_SCROLL_BAR_ID));
		if (NULL == Window->pHSrollBar)
		{
			goto FAILED;
		}

		SetScrollRange (Window, SB_HORZ, 0, 0, FALSE);
		SetScrollPos (Window, SB_HORZ, 0);
	}
	else
	{
		Window->pHSrollBar = NULL;
	}

	if (WindowHaveVScrollBar (Window))
	{
		GetWindowScrollBarRect (Window, FALSE, &ScrollRect);
		Window->pVSrollBar = CreateWindow ("bmpscrollbar", "", WS_CONTROL | WS_VISIBLE | SBS_VERT | WS_SYSCONTROL,
										   ScrollRect.left, ScrollRect.top, ScrollRect.right - ScrollRect.left, ScrollRect.bottom - ScrollRect.top,
										   Window, (MENU *)8001, GetSystemBitmap (BMP_SCROLL_BAR_ID));
		if (NULL == Window->pVSrollBar)
		{
			goto FAILED;
		}

		SetScrollRange (Window, SB_VERT, 0, 0, FALSE);
		SetScrollPos (Window, SB_VERT, 0);
	}
	else
	{
		Window->pVSrollBar = NULL;
	}

	if (SendMessage (Window, EV_CREATE, (uint32_t)NULL, (uint32_t)pPrivData) < 0)
	{
		goto FAILED;
	}

	if (WindowIsControl (Window) && WindowIsVisible (Window))
	{
		ShowWindow (Window, SW_SHOW);
	}

	return Window;

FAILED:
	DestroyWindow (Window);
	return NULL;
}

static void DestroyAllChildWindow(WINDOW *Window)
{
	WINDOW* _list_;
	struct list_head *_head_ = &Window->ChildListHead;
#undef _item_
#define _item_ ChildListNode

	while (!list_empty (_head_))
	{
		_list_ = list_entry((_head_)->next, WINDOW, _item_);
		DestroyWindow (_list_);
	}
}

bool32_t DestroyWindow(WINDOW *Window)
{
	MSG Msg;

	KillAllTimer (Window);

	if (WndIsMainWnd (Window))
	{
		HideMenu (Window->pMenu);
	}

	DestroyAllChildWindow (Window);

	ShowWindow (Window, SW_HIDE);

	SendMessage (Window, EV_DESTROY, (uint32_t)NULL, (uint32_t)NULL);

	if ((WINDOW *)NULL != Window->pParentWnd)
	{
		list_del (&Window->ChildListNode);
	}

	while (PeekMessage (&Msg, Window, 0, 0, PM_REMOVE))
	{
		DiscardMessage (&Msg);
	}

	FreeClipRegion (Window->ClipRegion);
	FreeClipRegion (Window->InvalidRegion);

	if (Window->pCaption)
	{
		Free (Window->pCaption);
	}

	Free (Window);

	return TRUE;
}

void ShowWindow(WINDOW *Window, uint32_t nShow)
{
	if (NULL == Window)
	{
		return;
	}

	switch (nShow)
	{
	case SW_HIDE:
	{
		RECT OldRect;

		if (TRUE == WindowIsDesktop (Window))
		{
			break;
		}

		if (FALSE == WindowIsVisible (Window))
		{
			break;
		}

		Window->nStyle &= ~WS_VISIBLE;

		// 保存旧坐标，保存window窗口到OldRect
		GetWindowRect (Window, &OldRect);

		// 更新父窗口IC
		ChangeParentICbyWindow (Window);

		// 更改本窗口GC
		ChangeWindowGC (Window);

		// 更改被掩盖兄妹窗口GC，并重画被掩盖兄妹窗口
		ChangeSiblingGC (Window, &OldRect, NULL, TRUE);

		// Focus control
		KillFocus (Window);

		// Active window
		DeactiveWindow (Window);
		break;
	}

	case SW_SHOW:
	default:
	{
		bool32_t bNeedDraw = !WindowIsVisible (Window);

		Window->nStyle |= WS_VISIBLE;//改为显示

		if (FALSE == ActiveWindow (Window))
		{
			if (FALSE == bNeedDraw)
			{
				break;
			}
		}

		// 更改本窗口GC(剪切域)
		ChangeWindowGC (Window);

		// 更改被掩盖兄妹窗口GC(剪切域)
		ChangeSiblingGC (Window, NULL, NULL, FALSE);

		// 重画窗口
		InvalidateRect (Window, NULL, TRUE);
		break;
	}
	}
}

bool32_t EnableWindow(WINDOW *Window, bool32_t bEnable)
{
	bool32_t bOldStatus;

	bOldStatus = IsWindowEnabled (Window);

	if (bEnable == bOldStatus)
	{
		return bOldStatus;
	}

	SendMessage (Window, EV_ENABLE, 0, bEnable);

	return bOldStatus;
}

// 使矩形无效,发出重画消息
// Window：指定窗口
// lpRect：无效区域（客户区坐标）为NULL是相当于更新窗口
// bErase：是否重画背景
bool32_t InvalidateRect(WINDOW *Window, LPRECT lpRect, bool32_t bErase)
{
	RECT WndRect;

	if (FALSE == WindowIsVisible (Window))
	{
		EmptyClipRgn (Window->ClipRegion);
		EmptyClipRgn (Window->InvalidRegion);

		return FALSE;
	}
//没有传入的矩形，整外形做为无效区，
//有传入的矩形，设置矩形在窗口的客户区，并添加为窗口的无效区
	if (NULL == lpRect)
	{
		GetWindowRect (Window, &WndRect);

		EmptyClipRgn (Window->InvalidRegion);
		UnionRectWithRegion (&WndRect, Window->InvalidRegion);
	}
	else
	{
		CopyRect (&WndRect, lpRect);
		ClientToScreenRect (Window, &WndRect);

		UnionRectWithRegion (&WndRect, Window->InvalidRegion);
	}
//如有窗口标题、外框、卷栏，发送无效区重画
	if (WindowHaveNC (Window))
	{
		PostMessage (Window, EV_NCPAINT, 0, 0);
	}
//传送有擦除，发送擦除背景
	if (bErase)
	{
		PostMessage (Window, EV_ERASEBKGND, 0, 0);
	}
//传递重画消息
	PostMessage (Window, EV_PAINT, 0, 0);

	return TRUE;
}

void UpdateWindow (WINDOW *Window, bool32_t bErase)
{
	if (FALSE == WindowIsVisible (Window))
	{
		return;
	}

	InvalidateRect (Window, NULL, bErase);
}

void CloseWindow(WINDOW *Window)
{
	PostMessage (Window, EV_CLOSE, 0, 0);
}

// 窗口GC、DC改变，被掩盖兄妹窗口GC、DC改变（需要重画），父亲DC改变，子窗口GC、DC改变
bool32_t MoveWindow(WINDOW *Window, int32_t X, int32_t Y, int32_t W, int32_t H, bool32_t bRepaint)
{
	RECT NewRect;
	int32_t OldW, OldH;
	bool32_t bResult = FALSE, bMove = TRUE;
	WINDOW* pParentWnd = Window->pParentWnd;

	if (X == Window->ParentPos.x && Y == Window->ParentPos.y)
	{
		bMove = FALSE;
	}
	else
	{
		if ((WINDOW *)NULL != pParentWnd)
		{
			X += pParentWnd->WindowRect.left;
			Y += pParentWnd->WindowRect.top;
		}
	}

	OldW = Window->WindowRect.right - Window->WindowRect.left;
	OldH = Window->WindowRect.bottom - Window->WindowRect.top;
	if (W < 0 || H < 0)
	{
		W = OldW;
		H = OldH;
	}

	SetRect (&NewRect, X, Y, X + W, Y + H);
	if (bMove == TRUE)
	{
		bResult = SendMessage (Window, EV_MOVEING, 0, (uint32_t)&NewRect);
		if (FALSE == bResult)
		{
			return FALSE;
		}

		bResult = SendMessage (Window, EV_MOVE, 0, MAKEuint32_t (X, Y));
		if (FALSE == bResult)
		{
			return FALSE;
		}
	}

	W = NewRect.right  - NewRect.left;
	H = NewRect.bottom - NewRect. top;
	if (OldW == W && OldH == H)
	{
		return bResult;
	}

	bResult = SendMessage (Window, EV_SIZEING, 0, MAKEuint32_t (W, H));
	if (FALSE == bResult)
	{
		return FALSE;
	}

	return SendMessage (Window, EV_SIZE, 0, MAKEuint32_t (W, H));
}

DC *GetWindowDC(WINDOW *Window)
{
	DC *dc;

	if (FALSE == WindowIsVisible (Window))
	{
		return (DC *)NULL;
	}

	dc = (DC *)Malloc (sizeof (struct _DC));
	if ((DC *)NULL == dc)
	{
		return (DC *)NULL;
	}

	dc->nType     = TYPE_WINDOW_DC;

	dc->pWnd      = Window;

	dc->pPhyDC    = &pGUI->PhyDC;

	dc->fgPixel   = RGB (0, 0, 0);
	dc->bkPixel   = RGB (255, 255, 255);
	dc->bkMode    = TRANSPARENT;
	dc->lineWidth = 1;
	dc->lineStyle = 1;
	dc->fillStyle = 1;
	dc->CurPos.x  = 0;
	dc->CurPos.y  = 0;

	dc->ClipRegion = MallocRegion();
	if ((CLIPREGION *)NULL == dc->ClipRegion)
	{
		Free (dc);

		return (DC *)NULL;
	}

	CopyRegion (dc->ClipRegion, Window->ClipRegion);

	WindowDCChangeByChildWindow (Window, dc);

	return dc;
}

DC *GetDC(WINDOW *Window)
{
	DC *dc;

	if (FALSE == WindowIsVisible (Window))
	{
		return (DC *)NULL;
	}

	dc = (DC *)Malloc (sizeof (struct _DC));
	if ((DC *)NULL == dc)
	{
		return (DC *)NULL;
	}

	dc->nType     = TYPE_CLIENT_DC;

	dc->pWnd      = Window;

	dc->pPhyDC    = &pGUI->PhyDC;

	dc->fgPixel   = RGB (0, 0, 0);
	dc->bkPixel   = RGB (255, 255, 255);
	dc->bkMode    = TRANSPARENT;
	dc->lineWidth = 1;
	dc->lineStyle = 1;
	dc->fillStyle = 1;
	dc->CurPos.x  = 0;
	dc->CurPos.y  = 0;

	dc->ClipRegion = MallocRegion();
	if ((CLIPREGION *)NULL == dc->ClipRegion)
	{
		Free (dc);

		return (DC *)NULL;
	}

	CopyRegion (dc->ClipRegion, Window->ClipRegion);

	WindowDCChangeByChildWindow (Window, dc);

	WindowClientDCChangeByFrame (Window, dc);

	return dc;
}

void ReleaseDC(WINDOW *Window, DC *dc)
{
	if ((DC *)NULL != dc)
	{
		FreeClipRegion (dc->ClipRegion);
		Free (dc);
	}
}

WINDOW *GetParent(WINDOW *Window)
{
	return Window->pParentWnd;
}

WINDOW *GetBind(WINDOW *Window)
{
	if (FALSE == WindowIsDialog (Window) &&
			FALSE == WindowIsMenu (Window))
	{
		return NULL;
	}
	else
	{
		WINDOW *pBindWnd;

		pBindWnd = Window = Window->pBindWnd;
		while (Window)
		{
			pBindWnd = Window;
			Window = Window->pBindWnd;
		}

		return pBindWnd;
	}
}

WINDOW *GetDlgItem (WINDOW *pDlg, int32_t nIDDlgItem)
{
	WINDOW* _list_;
	struct list_head *_head_ = &pDlg->ChildListHead;
#undef _item_
#define _item_ ChildListNode

	for ((_list_) = list_entry((_head_)->next, WINDOW, _item_);
			& ((_list_)->_item_) != (_head_);
			(_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_))
	{
		if (FALSE == WindowIsControl (_list_))
		{
			continue;
		}

		if (nIDDlgItem == (int32_t)_list_->pMenu)
		{
			return _list_;
		}
	}

	return NULL;
}

int32_t GetDlgCtrlID(WINDOW *Window)
{
	if (FALSE == WindowIsControl (Window))
	{
		return -1;
	}

	return (int32_t)Window->pMenu;
}

WINDOW *SetCapture(WINDOW *Window)
{
	WINDOW *OldWnd;

	OldWnd = pGUI->pCaptureWnd;
	pGUI->pCaptureWnd = Window;

	return OldWnd;
}

WINDOW *GetCapture(void)
{
	return pGUI->pCaptureWnd;
}

void ReleaseCapture(void)
{
	pGUI->pCaptureWnd = NULL;
}

/* 初始化相关 */
static bool32_t InitGUI(void * pParam)
{
	PHYDC *pDC;
	RECT ScreenRect;

	pGUI = &thisGUI;

	memset(pGUI, 0, sizeof(GUI));
	pthread_mutex_init (&pGUI->MutexObject.hMutex, NULL);
	sem_init(&pGUI->Semph_t.semaphore_t, 0, 0);
	/*
	    if (FALSE == MutexCreate (&pGUI->MutexObject)) {
	        return FALSE;
	    }
	    if (FALSE == SemaphoreInit (&pGUI->Semph_t, 0)) {
	        ReleaseMutex (&pGUI->MutexObject);
	        return FALSE;
	    }
	*/
	LIST_HEAD_RESET(&pGUI->MsgQueueHead);
	pGUI->TimerTail = NULL;

	pDC = &pGUI->PhyDC;
	if (FALSE == InitPhyDC (pDC, pParam))
	{
		ReleaseMutex (&pGUI->MutexObject);
		EventDestroy (&pGUI->Semph_t);
		return FALSE;
	}
	SetRect (&ScreenRect, 0, 0, pDC->w, pDC->h);//ScreenRect临时矩形变量赋值
	pGUI->ClipRegion = AllocRectRegionIndirect (&ScreenRect);//矩形分配内存，提取地址(指针)

	pGUI->pDesktopWnd = NULL;
	pGUI->pActiveWnd  = NULL;
	pGUI->pCaptureWnd = NULL;

	pGUI->bShowCursor = TRUE;
	pGUI->pSysCursor  = NULL;

	pGUI->pSysIme     = NULL;

	pGUI->pActiveMenu = NULL;

	LIST_HEAD_RESET(&pGUI->WCListHead);
	LIST_HEAD_RESET(&pGUI->FocusWndHead);

	return TRUE;
}

static void ReleaseGUI(void)
{
	PHYDC *pDC;

	pDC = &pGUI->PhyDC;
	ReleasePhyDC (pDC);
}

static bool32_t RegisterAllClass(void)
{
	RegisterMenuClass ();
	RegisterStaticClass ();
	RegisterClockClass ();
	RegisterBmpButtonClass ();
	RegisterCheckButtonClass ();
	RegisterEditClass ();
	RegisterBmpScrollBarClass ();
	RegisterListBoxClass ();
	RegisterProgressBarControl ();
	RegisterMLEditControl ();
	RegisterListViewControl ();
	RegisterMainMenuClass ();
	RegisterDialogClass ();

	return TRUE;
}

static void ReleaseWindowClass(void)
{
}

int32_t GuiInit(void *pParam)
{
	int32_t nResult;

	if (FALSE == InitGUI (pParam))
	{
		GUIDEBUG ("InitGUI failed\n");
		return -1;
	}

	if (FALSE == InitMessage ())
	{
		GUIDEBUG ("InitMessage failed\n");
		return -2;
	}

	if (FALSE == InitTimer (pParam))  //开启定时器线程通知，会出现额外两个线程
	{
		GUIDEBUG ("InitTimer failed\n");
		return -3;
	}

	if (FALSE == InitCursor (pParam))
	{
		GUIDEBUG ("InitCursor failed\n");
		return -4;
	}
/*
	if (FALSE == InitSystemCursor ())
	{
		GUIDEBUG ("InitSystemCursor failed\n");
		return -5;
	}
*/
	if (FALSE == InitSystemBitmap ())
	{
		GUIDEBUG ("InitSystemBitmap failed\n");
		return -6;
	}

	if (FALSE == RegisterAllClass ())
	{
		GUIDEBUG ("RegisterAllClass failed\n");
		return -7;
	}
	if (FALSE == InitKeyboard (pParam)) {
		GUIDEBUG ("InitKeyboard failed\n");
		return -8;
	}

	/*    if (FALSE == InitIme ()) {
	      GUIDEBUG ("InitIme failed\n");
	        return -9;
	    }

	    if (FALSE == InitMouse (pParam)) {
	        GUIDEBUG ("InitMouse failed\n");
	        return -10;
	    }
	*/
	if (NULL == CreateDesktopWnd ())
	{
		return -11;
	}

	nResult = WinMain ();

	ReleaseMessage ();

	ReleaseTimer ();

	ReleaseCursor ();

	ReleaseSystemBitmap ();

	ReleaseSystemCursor ();

	ReleaseWindowClass ();

	ReleaseIme ();

	CloseKeyboard ();

	CloseMouse ();

	ReleaseGUI ();

	return nResult;
}
