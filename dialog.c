/*
  Copyright (C), 2008~2009, zhanbin
  File name:    dialog.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.2
  Date:         20081215
  Description:
  Others:
  Function List:
  History:
    20090221    zhanbin         简单实现MessageBox
    20081215    zhanbin         创建文件
*/
#include "window.h"

static bool32_t IsChildWindow(WINDOW *pDlg, WINDOW *pMatchWnd)
{
    WINDOW* _list_;
    struct list_head *_head_ = &pDlg->ChildListHead;
    #undef _item_
    #define _item_ ChildListNode
    
    for ((_list_) = list_entry((_head_)->next, WINDOW, _item_); 
        &((_list_)->_item_) != (_head_);
        (_list_) = list_entry((_list_)->_item_.next, WINDOW, _item_)) {
        if (_list_ == pMatchWnd) {
            return TRUE;
        }
        
        if (TRUE == IsChildWindow (_list_, pMatchWnd)) {
            return TRUE;
        }
    }
    
    return FALSE;
}

static int32_t DialogDispatchMessage(MSG *pMsg, WINDOW *pDlg)
{
    WINDOW *Window;
    
    switch (pMsg->Message) {
    case EV_LBUTTONDOWN:
    case EV_LBUTTONUP:
    case EV_RBUTTONDOWN:
    case EV_RBUTTONUP:
    case EV_MOUSEMOVE:
    {
        Window = pMsg->Window;
        
        if (TRUE == WindowIsIme (Window)) {
            break;
        }
        
        if (FALSE == IsChildWindow (pDlg, Window)) {
            Window = pDlg;
        }
        
        pMsg->Window = Window;
        break;
    }
        
    default:
        break;
    }
    
    return DispatchMessage (pMsg);
}

int32_t DefDialogProc(WINDOW *pDlg,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    switch(Message){
    case EV_CHAR:
        switch (LOuint16_t (wParam)) {
        case VK_ESCAPE:
            EndDialog (pDlg, IDCANCEL);
            return 0;
            
        default:
            break;
        }
        break;
        
    case EV_COMMAND:
    {
        switch (LOuint16_t (wParam)) {
        case IDOK:
            switch (HIuint16_t (wParam)) {
            case BN_CLICKED:
                EndDialog (pDlg, IDOK);
                return 0;
            }
            break;
            
        case IDCANCEL:
            switch (HIuint16_t (wParam)) {
            case BN_CLICKED:
                EndDialog (pDlg, IDCANCEL);
                return 0;
            }
            break;
        }
        break;
    }
        
    case EV_CLOSE:
        EndDialog (pDlg, IDCANCEL);
        return 0;
        break;
        
    default:
        break;
    }
    
    return DefWinProc (pDlg, Message, wParam, lParam);
}

static int32_t DialogProc(WINDOW *pDlg,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    WINPROC pDlgProc;
    DLGCRTDATA *pDlgCrtData = (DLGCRTDATA *)pDlg->pPrivData;
    
    if (NULL == pDlgCrtData) {
        return -1;
    }
    
    pDlgProc = pDlgCrtData->pDlgProc;
    if (NULL != pDlgProc) {
        return pDlgProc (pDlg, Message, wParam, lParam);
    }
    
    return -1;
}

bool32_t RegisterDialogClass (void)
{
    WNDCLASS DialogClass;
    
    DialogClass.pClassName = "dialog";
    DialogClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    DialogClass.WinProc    = DialogProc;
    
    return RegisterClass (&DialogClass);
}

int32_t CreateDialog(const int8_t* lpCaption,uint32_t nStyle,int32_t W,int32_t H,WINDOW *pParentWnd, WINPROC pDlgProc)
{
    int32_t X, Y;
    MSG Msg;
    WINDOW *pDlg;
    RECT ParentRect;
    DLGCRTDATA DlgCrtData;
    
    if (NULL == pDlgProc) {
        return -1;
    }
    
    if (NULL == pParentWnd) {
        pParentWnd = GetDesktopWindow ();
    }
    
    GetWindowRect (pParentWnd, &ParentRect);
    X = (ParentRect.right  - ParentRect.left - W) / 2;
    Y = (ParentRect.bottom - ParentRect.top - H) / 2;
    
    DlgCrtData.pDlgCtrl = NULL;
    DlgCrtData.pDlgProc = pDlgProc;
    pDlg = CreateWindow ("dialog", lpCaption, WS_DIALOG | WS_TOPMOST | nStyle, 
        X, Y, W, H, pParentWnd, NULL, &DlgCrtData);
    if (NULL == pDlg) {
        return -1;
    }
    
    ShowWindow (pDlg, SW_SHOW);
    
    while (GetMessage (&Msg, NULL) == TRUE) {
        TranslateMessage (&Msg);
        DialogDispatchMessage (&Msg, pDlg);
    }
    
    DestroyWindow (pDlg);
    ShowWindow (pParentWnd, SW_SHOW);
    
    return Msg.wParam;
}

int32_t EndDialog (WINDOW *pDlg, int32_t nResult)
{
    if (NULL == pDlg || FALSE == WindowIsDialog (pDlg)) {
        return FALSE;
    }
    
    PostQuitMessage (nResult);
    
    return TRUE;
}

static int32_t MessageBoxProc(WINDOW *pDlg,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    DLGCRTL *pDlgCtrl;
    DLGCRTDATA *pDlgCrtData = (DLGCRTDATA *)pDlg->pPrivData;
    
    switch(Message){
    case EV_CREATE:
    {
        int32_t i;
        
        pDlgCtrl = pDlgCrtData->pDlgCtrl;
        
        for (i = 0; i < 3; i++) {
            if (-2 != pDlgCtrl[i].id) {
                CreateWindow (pDlgCtrl[i].pClass, pDlgCtrl[i].pCaption, pDlgCtrl[i].nStyle, 
                    pDlgCtrl[i].x, pDlgCtrl[i].y, pDlgCtrl[i].w, pDlgCtrl[i].h, 
                    pDlg, (MENU *)pDlgCtrl[i].id, pDlgCtrl[i].pPrivData);
            }
        }
        break;
    }
    
    case EV_PAINT:
    {
        DC *hdc;
        int8_t *pText;
        int32_t i, x, y, nLen, nPos;
        pDlgCtrl = pDlgCrtData->pDlgCtrl;
        
        nLen = strlen (pDlgCtrl[3].pCaption);
        if (nLen <= 0) {
            break;
        }
        pText = pDlgCtrl[3].pCaption;
        
        hdc = BeginPaint (pDlg, wParam, lParam);
        if (NULL == hdc) {
            return 0;
        }
        
        nPos = 0;
        x = pDlgCtrl[3].x;
        y = pDlgCtrl[3].y;
        for (i = 0; i < nLen; i++) {
            if (pText[i] == '\r' || pText[i] == '\n') {
                TextOutLen (hdc, x, y, &pText[nPos], i - nPos);
                i++;
                nPos = i;
                y += FONT_POINT;
            }
            
            while (pText[i] == '\r' || pText[i] == '\n') {
                i++;
            }
        }
        if (nPos < nLen) {
            TextOut (hdc, x, y, &pText[nPos]);
        }
        
        EndPaint (pDlg, hdc, wParam, lParam);
        return 0;
        break;
    }
    
    case EV_COMMAND:
    {
        switch (HIuint16_t (wParam)) {
        case BN_CLICKED:
            EndDialog (pDlg, LOuint16_t (wParam));
            return 0;
            break;
        }
        break;
    }
        
    default:
        break;
    }
    
    return DefDialogProc (pDlg, Message, wParam, lParam);
}

int32_t MessageBox(WINDOW *pParentWnd, int8_t *lpText, int8_t *lpCaption, uint32_t uType)
{
    SIZE s;
    int32_t X, Y;
    int32_t W, H;
    MSG Msg;
    WINDOW *pDlg;
    RECT ParentRect;
    DLGCRTL DlgCtrl[4] = {
        {"bmpbutton", WS_CONTROL | WS_VISIBLE | WS_TABSTOP, -2, 0, 0, 0, 0, NULL, NULL},
        {"bmpbutton", WS_CONTROL | WS_VISIBLE | WS_TABSTOP, -2, 0, 0, 0, 0, NULL, NULL},
        {"bmpbutton", WS_CONTROL | WS_VISIBLE | WS_TABSTOP, -2, 0, 0, 0, 0, NULL, NULL},
        {"static",    WS_CONTROL | WS_VISIBLE               , -2, 5, 5, 0, 0, lpText, NULL},
    };
    DLGCRTDATA DlgCrtData = {DlgCtrl, MessageBoxProc};

    switch (uType) {
    case MB_OK:
        W = 80;
        H = GetSystemMetrics (SM_CYCAPTION) + 30;
        break;
        
    case MB_OKCANCEL:
        W = 160;
        H = GetSystemMetrics (SM_CYCAPTION) + 30;
        break;
        
    case MB_YESNO:
        W = 160;
        H = GetSystemMetrics (SM_CYCAPTION) + 30;
        break;
        
    case MB_ABORTRETRYIGNORE:
        W = 240;
        H = GetSystemMetrics (SM_CYCAPTION) + 30;
        break;
    }
    
    GetTextExtent (NULL, lpText, -1, &s);
    if (s.cx + 20 > W) {
        W = s.cx + 20;
    }
    if (s.cy + GetSystemMetrics (SM_CYCAPTION) + 40 > H) {
        H = s.cy + GetSystemMetrics (SM_CYCAPTION) + 40;
    }
    
    switch (uType) {
    case MB_OK:
        DlgCtrl[0].id = IDOK;
        DlgCtrl[0].x  = (W - 75) / 2;
        DlgCtrl[0].y  = H - 50;
        DlgCtrl[0].w  = 75;
        DlgCtrl[0].h  = 21;
        DlgCtrl[0].pCaption = "确定";
        DlgCtrl[0].pPrivData = GetSystemBitmap (BMP_PUSH_BUTTON_ID);
        break;
        
    case MB_OKCANCEL:
        DlgCtrl[0].id = IDOK;
        DlgCtrl[0].x  = (W - 75 * 2) / 3;
        DlgCtrl[0].y  = H - 50;
        DlgCtrl[0].w  = 75;
        DlgCtrl[0].h  = 21;
        DlgCtrl[0].pCaption = "确定";
        DlgCtrl[0].pPrivData = GetSystemBitmap (BMP_PUSH_BUTTON_ID);
        DlgCtrl[1].id = IDCANCEL;
        DlgCtrl[1].x  = ((W - 75 * 2) / 3) * 2 + 75;
        DlgCtrl[1].y  = H - 50;
        DlgCtrl[1].w  = 75;
        DlgCtrl[1].h  = 21;
        DlgCtrl[1].pCaption = "取消";
        DlgCtrl[1].pPrivData = GetSystemBitmap (BMP_PUSH_BUTTON_ID);
        break;
        
    case MB_YESNO:
        DlgCtrl[0].id = IDYES;
        DlgCtrl[0].x  = (W - 75 * 2) / 3;
        DlgCtrl[0].y  = H - 50;
        DlgCtrl[0].w  = 75;
        DlgCtrl[0].h  = 21;
        DlgCtrl[0].pCaption = "是";
        DlgCtrl[0].pPrivData = GetSystemBitmap (BMP_PUSH_BUTTON_ID);
        DlgCtrl[1].id = IDNO;
        DlgCtrl[1].x  = ((W - 75 * 2) / 3) * 2 + 75;
        DlgCtrl[1].y  = H - 50;
        DlgCtrl[1].w  = 75;
        DlgCtrl[1].h  = 21;
        DlgCtrl[1].pCaption = "否";
        DlgCtrl[1].pPrivData = GetSystemBitmap (BMP_PUSH_BUTTON_ID);
        break;
        
    case MB_ABORTRETRYIGNORE:
        DlgCtrl[0].id = IDABORT;
        DlgCtrl[0].x  = (W - 75 * 3) / 4;
        DlgCtrl[0].y  = H - 50;
        DlgCtrl[0].w  = 75;
        DlgCtrl[0].h  = 21;
        DlgCtrl[0].pCaption = "终止";
        DlgCtrl[0].pPrivData = GetSystemBitmap (BMP_PUSH_BUTTON_ID);
        DlgCtrl[1].id = IDRETRY;
        DlgCtrl[1].x  = ((W - 75 * 3) / 4) * 2 + 75;
        DlgCtrl[1].y  = H - 50;
        DlgCtrl[1].w  = 75;
        DlgCtrl[1].h  = 21;
        DlgCtrl[1].pCaption = "重试";
        DlgCtrl[1].pPrivData = GetSystemBitmap (BMP_PUSH_BUTTON_ID);
        DlgCtrl[2].id = IDIGNORE;
        DlgCtrl[2].x  = ((W - 75 * 3) / 4) * 3 + 75 * 2;
        DlgCtrl[2].y  = H - 50;
        DlgCtrl[2].w  = 75;
        DlgCtrl[2].h  = 21;
        DlgCtrl[2].pCaption = "忽略";
        DlgCtrl[2].pPrivData = GetSystemBitmap (BMP_PUSH_BUTTON_ID);
        break;
        
    case MB_YESNOCANCEL:
        DlgCtrl[0].id = IDYES;
        DlgCtrl[0].x  = (W - 75 * 3) / 4;
        DlgCtrl[0].y  = H - 50;
        DlgCtrl[0].w  = 75;
        DlgCtrl[0].h  = 21;
        DlgCtrl[0].pCaption = "是";
        DlgCtrl[0].pPrivData = GetSystemBitmap (BMP_PUSH_BUTTON_ID);
        DlgCtrl[1].id = IDNO;
        DlgCtrl[1].x  = ((W - 75 * 3) / 4) * 2 + 75;
        DlgCtrl[1].y  = H - 50;
        DlgCtrl[1].w  = 75;
        DlgCtrl[1].h  = 21;
        DlgCtrl[1].pCaption = "否";
        DlgCtrl[1].pPrivData = GetSystemBitmap (BMP_PUSH_BUTTON_ID);
        DlgCtrl[2].id = IDCANCEL;
        DlgCtrl[2].x  = ((W - 75 * 3) / 4) * 3 + 75 * 2;
        DlgCtrl[2].y  = H - 50;
        DlgCtrl[2].w  = 75;
        DlgCtrl[2].h  = 21;
        DlgCtrl[2].pCaption = "取消";
        DlgCtrl[2].pPrivData = GetSystemBitmap (BMP_PUSH_BUTTON_ID);
        break;
        
    default:
        return -1;
    }
    
    if (NULL == pParentWnd) {
        pParentWnd = GetDesktopWindow ();
    }
    GetWindowRect (pParentWnd, &ParentRect);
    X = (ParentRect.right  - ParentRect.left - W) / 2;
    Y = (ParentRect.bottom - ParentRect.top - H) / 2;
    
    pDlg = CreateWindow ("dialog", lpCaption, WS_DIALOG | WS_TOPMOST | WS_CAPTION | WS_BORDER, 
        X, Y, W, H, pParentWnd, NULL, &DlgCrtData);
    if (NULL == pDlg) {
        return -1;
    }
    
    ShowWindow (pDlg, SW_SHOW);
    
    while (GetMessage (&Msg, NULL) == TRUE) {
        TranslateMessage (&Msg);
        DialogDispatchMessage (&Msg, pDlg);
    }
    
    DestroyWindow (pDlg);
    ShowWindow (pParentWnd, SW_SHOW);
    
    return Msg.wParam;
}
