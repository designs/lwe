/*
  Copyright (C), 2008~2009, zhanbin
  File name:    pckbd.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090107
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090107    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

#ifdef PC_KBD
static int32_t PCKbdOpen(KEYBOARD *);
static int32_t PCKbdRead(KEYBOARD *pKbdDev, uint8_t *s, uint8_t *p, uint8_t *m);
static void PCKbdClose(KEYBOARD *);

KEYBOARD PCKbdDev = {
    -1,
    PCKbdOpen,
    PCKbdClose,
    PCKbdRead
};
static int32_t old_mode;
static struct termios old_flags;

static int32_t PCKbdOpen(KEYBOARD *pKbdDev)
{
    struct termios new_flags;
    
    if ((pKbdDev->fd = open ("/dev/tty0", O_RDONLY | O_NOCTTY)) < 0) {
        DebugPrint ("Can't open keyboard!\n");
        return -1;
    }
	
	tcgetattr(pKbdDev->fd, &old_flags);
	new_flags = old_flags;
	new_flags.c_lflag &= ~(ICANON | ECHO | ISIG);
	new_flags.c_iflag &= ~(ISTRIP | IGNCR | ICRNL | INLCR | IXOFF | IXON | BRKINT);
	new_flags.c_cc[VMIN] = 0;
	new_flags.c_cc[VTIME] = 0;
	tcsetattr(pKbdDev->fd, TCSAFLUSH, &new_flags);
	
	if (ioctl(pKbdDev->fd, KDGKBMODE, &old_mode) < 0) {
		PCKbdClose (pKbdDev);
		return -1;
	}
	if (ioctl(pKbdDev->fd, KDSKBMODE, K_MEDIUMRAW) < 0) {
		PCKbdClose (pKbdDev);
		return -1;
	}
	
    return pKbdDev->fd;
}

static int32_t PCKbdRead(KEYBOARD *pKbdDev, uint8_t *s, uint8_t *p, uint8_t *m)
{
    uint8_t ch;
    int32_t nLen;
    
    nLen = read (pKbdDev->fd, &ch, 1);
    if (nLen != 1) {
        return 0;
    }
    
    *p = (ch & 0x80) ? 0 : 1;
    *s = ch & 0x7f;
    *m = 0;
    
    return 1;
}

static void PCKbdClose(KEYBOARD *pKbdDev)
{
    if (pKbdDev->fd >= 0) {
        ioctl(pKbdDev->fd, KDSKBMODE, old_mode);
        tcsetattr(pKbdDev->fd, TCSANOW, &old_flags);
        close (pKbdDev->fd);
    }
    pKbdDev->fd = -1;
}
#endif
