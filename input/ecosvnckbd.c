/*
  Copyright (C), 2008~2009, zhanbin
  File name:    ecosvnckbd.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090222
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090222    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

#ifdef VNC_KBD
#include <pkgconf/vnc_server.h>  /* CYGDAT_VNC_SERVER_KEYBOARD_NAME */
#define XK_MISCELLANY
#include "keysymdef.h"
#include "keymap.h"


static int32_t VncKbdOpen(KEYBOARD *);
static int32_t VncKbdRead(KEYBOARD *pKbdDev, uint8_t *s, uint8_t *p, uint8_t *m);
static void VncKbdClose(KEYBOARD *);

KEYBOARD VncKbdDev = {
    -1,
    VncKbdOpen,
    VncKbdClose,
    VncKbdRead
};

static int32_t VncKbdOpen(KEYBOARD *pKbdDev)
{
    if ((pKbdDev->fd = open(CYGDAT_VNC_SERVER_KEYBOARD_NAME, O_RDWR)) < 0) {
        DebugPrint ("Can't open keyboard!\n");
    }
    
    return pKbdDev->fd;
}

static int32_t VncKbdRead(KEYBOARD *pKbdDev, uint8_t *s, uint8_t *p, uint8_t *m)
{
    uint8_t buf[4];
    uint32_t i, nNum, keysym;
    
    if (read(pKbdDev->fd, buf, sizeof (buf)) != 4) {
        return 0;
    }
    
    keysym = buf[2] * 256 + buf[3];
    if (keysym < 0x0080) {
        *s = toupper (buf[3]);
        if (buf[1]) {
            *p = 1;
        }
        else {
            *p = 0;
        }
        
        return 2;
    }
    
    nNum = sizeof (keymap) / sizeof (keymap[0]);
    for (i = 0; i < nNum; i++) {
        if (keymap[i].keysym == keysym) {
            *s = keymap[i].vk;
            if (buf[1]) {
                *p = 1;
            }
            else {
                *p = 0;
            }
            
            return 2;
        }
    }
    
    return 0;
}

static void VncKbdClose(KEYBOARD *pKbdDev)
{
    if (pKbdDev->fd >= 0) {
        close (pKbdDev->fd);
    }
    pKbdDev->fd = -1;
}
#endif
