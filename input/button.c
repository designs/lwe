#include    "window.h"

#ifdef KEY_BUTTON


static int32_t Button_Open(KEYBOARD *);
static int32_t Button_Read(KEYBOARD *pKbdDev, uint8_t *s, uint8_t *p, uint8_t *m);
static void Button_Close(KEYBOARD *);

KEYBOARD Button_Dev = {
    -1,
    Button_Open,
    Button_Close,
    Button_Read
};

static int32_t Button_Open(KEYBOARD *pKbdDev)
{
    if ((pKbdDev->fd = open("/dev/button", O_RDWR)) < 0) {
        DebugPrint ("Can't open keyboard!\n");
    }
    
    return pKbdDev->fd;
}

static int32_t Button_Read(KEYBOARD *pKbdDev, uint8_t *s, uint8_t *p, uint8_t *m)
{
    uint8_t ch;
    int32_t nLen;
    
    nLen = read (pKbdDev->fd, &ch, 1);
    if (nLen != 1) {
        return 0;
    }
    
    *p = 0;//(ch & 0x80) ? 0 : 1;
//	switch(ch)//ch=0/1/2/3/4/5/6
if(0==ch)
	*s = ch+11;
else
    *s = (ch & 0x7f)+1;
    *m = 0;
    
    return 1;

}

static void Button_Close(KEYBOARD *pKbdDev)
{
    if (pKbdDev->fd >= 0) {
        close (pKbdDev->fd);
    }
    pKbdDev->fd = -1;
}
#endif
