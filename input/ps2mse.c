/*
  Copyright (C), 2008~2009, zhanbin
  File name:    ps2mse.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090108
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090108    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

#ifdef PS2_MSE
static int32_t Ps2MseOpen(TMouse *);
static int32_t Ps2MseRead(TMouse *, int32_t *x, int32_t *y, int32_t *b, int32_t *w);
static void Ps2MseClose(TMouse *);

TMouse Ps2MseDev = {
    -1,
    0,
    0,
    0,
    0,
    FALSE,
    FALSE,
    Ps2MseOpen,
    Ps2MseClose,
    Ps2MseRead
};

static int32_t Ps2MseOpen(TMouse *pMseDev)
{
    if ((pMseDev->fd = open("/dev/psaux", O_RDWR)) < 0) {
        if ((pMseDev->fd = open("/dev/input/mice", O_RDWR)) < 0) {
            DebugPrint ("Can't open mouse!\n");
        }
    }
    
    return pMseDev->fd;
}

static int32_t Ps2MseRead(TMouse *pMseDev, int32_t *x, int32_t *y, int32_t *b, int32_t *w)
{
    int32_t n;
    static uint8_t buf[3];
    static int32_t nbytes=0;
    
    n = read(pMseDev->fd, &buf[nbytes], 3 - nbytes);
    if (n < 0) {
        return -1;
    }
    
    nbytes += n;
    if (nbytes == 3) {
		if ((buf[0] & 0xc0) != 0) {
			buf[0] = buf[1];
			buf[1] = buf[2];
			nbytes = 2;
			return 0;
		}
		
        *b = buf[0];
        *x = (buf[0] & 0x10) ? buf[1] - 256 : buf[1];
        *y = (buf[0] & 0x20) ? -(buf[2] - 256) : -buf[2];
        *w = 0;
        nbytes = 0;
        
        return 3;
    }
    
    return 0;
}

static void Ps2MseClose(TMouse *pMseDev)
{
    if (pMseDev->fd >= 0) {
        close (pMseDev->fd);
    }
    pMseDev->fd = -1;
}
#endif
