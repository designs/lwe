/*
  Copyright (C), 2008~2009, zhanbin
  File name:    mouse.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090106
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090106    zhanbin         创建文件
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

extern TMouse    UsbMseDev;
extern TMouse    VncMseDev;
extern TMouse    Ps2MseDev;
extern TMouse    DumMseDev;
static TMouse    *pMouseDev;
static THREAD *pThread;
static bool32_t       bExitFlag;
static SEMAPHOR  ExitEvent;
static MUTEX  MseMutex;

static int32_t MouseThreadFun(THREAD *pThread, void *pData)
{
    fd_set rfds;
    struct timeval tv;
    int32_t n, fd, x, y, nButtonState, nWheelState;
    
    fd = pMouseDev->Open (pMouseDev);
    if (fd < 0) {
        return -1;
    }
    
    pMouseDev->x = GetSystemMetrics (SM_CXSCREEN) / 2;
    pMouseDev->y = GetSystemMetrics (SM_CYSCREEN) / 2;
    
    while (FALSE == bExitFlag) {
        FD_ZERO(&rfds);
        
        tv.tv_sec  = 1;
        tv.tv_usec = 0;
        
        FD_SET(fd, &rfds);
        n = fd;
        
        n++;
        switch (select(n, &rfds, NULL, NULL, &tv)) {
        case -1:        // Error
        case 0:         // Timeout
            continue;
        }
        
        if (FD_ISSET(fd, &rfds)) {
            if (pMouseDev->Read (pMouseDev, &x, &y, &nButtonState, &nWheelState) > 0) {
                x += pMouseDev->x;
                y += pMouseDev->y;
                SetMousePos (x, y);
                
                if (nButtonState & 0x1) {
                    if (!pMouseDev->bLBDown) {
                        pMouseDev->bLBDown = TRUE;
                        
                        PostMouseMessage (EV_LBUTTONDOWN, (uint32_t)0, MAKEuint32_t(pMouseDev->x, pMouseDev->y));
                    }
                }
                else {
                    if (pMouseDev->bLBDown) {
                        pMouseDev->bLBDown = FALSE;
                        
                        PostMouseMessage (EV_LBUTTONUP, (uint32_t)0, MAKEuint32_t(pMouseDev->x, pMouseDev->y));
                    }
                }
                
                if (nButtonState & 0x2) {
                    if (!pMouseDev->bRBDown) {
                        pMouseDev->bRBDown = TRUE;
                        
                        PostMouseMessage (EV_RBUTTONDOWN, (uint32_t)0, MAKEuint32_t(pMouseDev->x, pMouseDev->y));
                    }
                }
                else {
                    if (pMouseDev->bRBDown) {
                        pMouseDev->bRBDown = FALSE;
                        
                        PostMouseMessage (EV_RBUTTONUP, (uint32_t)0, MAKEuint32_t(pMouseDev->x, pMouseDev->y));
                    }
                }
            }
        }
    }
    
    pMouseDev->Close (pMouseDev);
    
    SemaphoreADD (&ExitEvent);
    
    return 0;
}

bool32_t InitMouse(void *pParam)
{
#ifdef DUM_MSE
    pMouseDev = &DumMseDev;
#endif
#ifdef USB_MSE
    pMouseDev = &UsbMseDev;
#endif
#ifdef VNC_MSE
    pMouseDev = &VncMseDev;
#endif
#ifdef PS2_MSE
    pMouseDev = &Ps2MseDev;
#endif
    
    MutexCreate (&MseMutex);
    SemaphoreInit (&ExitEvent, 0);
    bExitFlag = FALSE;
    
    pThread = ThreadCreate (MouseThreadFun, NULL);//创建鼠标线程
    if (NULL == pThread) {
        return FALSE;
    }
    
    ThreadSetPriority (pThread, 8);
    
    return TRUE; 
}

void SetMousePos(int32_t x, int32_t y)
{
    int32_t nMaxX, nMaxY;
    
    nMaxX = GetSystemMetrics (SM_CXSCREEN);
    nMaxY = GetSystemMetrics (SM_CYSCREEN);
    
    MutexLock (&MseMutex);
    
    pMouseDev->x = x;
    pMouseDev->y = y;
    
    if (pMouseDev->x < 0) {
        pMouseDev->x = 0;
    }
    else if (pMouseDev->x >= nMaxX) {
        pMouseDev->x = nMaxX - 1;
    }
    
    if (pMouseDev->y < 0) {
        pMouseDev->y = 0;
    }
    else if (pMouseDev->y >= nMaxY) {
        pMouseDev->y = nMaxY - 1;
    }
    
    if (pMouseDev->x != pMouseDev->OldX || pMouseDev->y != pMouseDev->OldY) {
        PostMouseMessage (EV_MOUSEMOVE , (uint32_t)0, MAKEuint32_t(pMouseDev->x, pMouseDev->y));
        
        pMouseDev->OldX = pMouseDev->x;
        pMouseDev->OldY = pMouseDev->y;
    }
    
    MutexUnlock (&MseMutex);
}

void CloseMouse(void)
{
    bExitFlag = TRUE;
    SemaphoreWait (&ExitEvent);
    ReleaseMutex (&MseMutex);
    
    ThreadDestroy (pThread);
}

bool32_t PostMouseMessage(uint32_t Message, uint32_t wParam, uint32_t lParam)
{
#if 0
    POINT pt;
    WINDOW *Window;
    
    Window = GetCapture ();
    if (NULL == Window) {
        pt.x = LOuint16_t(lParam);
        pt.y = HIuint16_t(lParam);
        Window = FindWindowByPos (&pt);
    }
    
    if (NULL == Window) {
        return FALSE;
    }
    
    return PostMessage (Window, Message, wParam, lParam);
#else
    return PostMessage (NULL, Message, wParam, lParam);
#endif
}
