/*
  Copyright (C), 2008~2009, zhanbin
  File name:    ecosusbmse.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090106
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090106    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

#ifdef USB_MSE
struct _mouseevent {
    unsigned char button_state;
    char xPos, yPos;
    char wheel_state;
};

static int32_t UsbMseOpen(TMouse *);
static int32_t UsbMseRead(TMouse *, int32_t *x, int32_t *y, int32_t *b, int32_t *w);
static void UsbMseClose(TMouse *);

TMouse UsbMseDev = {
    -1,
    0,
    0,
    0,
    0,
    FALSE,
    FALSE,
    UsbMseOpen,
    UsbMseClose,
    UsbMseRead
};

static int32_t UsbMseOpen(TMouse *pMseDev)
{
    if ((pMseDev->fd = open("/dev/mou", O_RDWR)) < 0) {
        DebugPrint ("Can't open mouse!\n");
    }
    
    return pMseDev->fd;
}

static int32_t UsbMseRead(TMouse *pMseDev, int32_t *x, int32_t *y, int32_t *b, int32_t *w)
{
    struct _mouseevent env;
    
    if (read(pMseDev->fd, &env, sizeof (env)) != 0) {
        *b = env.button_state;
        *x = env.xPos;
        *y = env.yPos;
        *w = env.wheel_state;
        
        return sizeof (env);
    }
    
    return 0;
}

static void UsbMseClose(TMouse *pMseDev)
{
    if (pMseDev->fd >= 0) {
        close (pMseDev->fd);
    }
    pMseDev->fd = -1;
}
#endif
