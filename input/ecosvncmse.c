/*
  Copyright (C), 2008~2009, zhanbin
  File name:    ecosvncmse.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090222
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090222    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

#ifdef VNC_MSE
#include <pkgconf/vnc_server.h>  /* CYGDAT_VNC_SERVER_MOUSE_NAME */

static int32_t VncMseOpen(TMouse *);
static int32_t VncMseRead(TMouse *, int32_t *x, int32_t *y, int32_t *b, int32_t *w);
static void VncMseClose(TMouse *);

TMouse VncMseDev = {
    -1,
    0,
    0,
    0,
    0,
    FALSE,
    FALSE,
    VncMseOpen,
    VncMseClose,
    VncMseRead
};

static int32_t VncMseOpen(TMouse *pMseDev)
{
    if ((pMseDev->fd = open(CYGDAT_VNC_SERVER_MOUSE_NAME, O_RDWR)) < 0) {
        DebugPrint ("Can't open mouse!\n");
    }
    
    return pMseDev->fd;
}

static int32_t VncMseRead(TMouse *pMseDev, int32_t *x, int32_t *y, int32_t *b, int32_t *w)
{
    uint8_t data[8];
    int32_t bytes_read;

    bytes_read = read (pMseDev->fd, data, sizeof(data));
    if (bytes_read != sizeof(data)) {
        return 0;
    }
    
    *b = 0;
    *x = (data[2] * 256 + data[3]) - pMseDev->x;
    *y = (data[4] * 256 + data[5]) - pMseDev->y;
    *w = 0;
    
    if (data[1] & 0x01) {
        *b += 0x01;
    }
    if (data[1] & 0x4) {
        *b += 0x02;
    }
    
    return 8;
}

static void VncMseClose(TMouse *pMseDev)
{
    if (pMseDev->fd >= 0) {
        close (pMseDev->fd);
    }
    pMseDev->fd = -1;
}
#endif
