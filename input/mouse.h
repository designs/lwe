#ifndef __MOUSE_H__
#define __MOUSE_H__

#ifdef __cplusplus
extern "C"
{
#endif

struct _TMouse;
typedef struct _TMouse TMouse;

struct _TMouse
{
    int32_t         fd;
    int32_t         x;
    int32_t         y;
    bool32_t         bLBDown;
    bool32_t         bRBDown;
    
    int32_t         OldX;
    int32_t         OldY;
    
    int32_t         (*Open)(TMouse *);
    void        (*Close)(TMouse *);
    int32_t         (*Read)(TMouse *, int32_t *, int32_t *, int32_t *, int32_t *);
};

bool32_t  InitMouse(void *);
void SetMousePos(int32_t, int32_t);
bool32_t PostMouseMessage(uint32_t, uint32_t, uint32_t);
void CloseMouse(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
