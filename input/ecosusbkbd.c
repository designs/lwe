/*
  Copyright (C), 2008~2009, zhanbin
  File name:    ecosusbkbd.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090107
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090107    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

#ifdef USB_KBD
static int32_t UsbKbdOpen(KEYBOARD *);
static int32_t UsbKbdRead(KEYBOARD *pKbdDev, uint8_t *s, uint8_t *p, uint8_t *m);
static void UsbKbdClose(KEYBOARD *);

KEYBOARD UsbKbdDev = {
    -1,
    UsbKbdOpen,
    UsbKbdClose,
    UsbKbdRead
};

static int32_t UsbKbdOpen(KEYBOARD *pKbdDev)
{
    int32_t mode = 1;
    
    if ((pKbdDev->fd = open("/dev/kbd", O_RDWR)) < 0) {
        DebugPrint ("Can't open keyboard!\n");
    }
    
    if(cyg_fs_fsetinfo (pKbdDev->fd, 0x1, &mode, sizeof(mode)) < 0) {
        close (pKbdDev->fd);
        pKbdDev->fd = -1;
    }
    
    return pKbdDev->fd;
}

static int32_t UsbKbdRead(KEYBOARD *pKbdDev, uint8_t *s, uint8_t *p, uint8_t *m)
{
    uint8_t buf[4];
    
    if (read(pKbdDev->fd, buf, sizeof (buf)) != 0) {
        *p = buf[1];
        *m = buf[2];
        *s = buf[3];
        
        return 1;
    }
    
    return 0;
}

static void UsbKbdClose(KEYBOARD *pKbdDev)
{
    if (pKbdDev->fd >= 0) {
        close (pKbdDev->fd);
    }
    pKbdDev->fd = -1;
}
#endif
