/*
  Copyright (C), 2008~2009, zhanbin
  File name:    keyboard.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090107
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090107    zhanbin         创建文件
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

extern KEYBOARD PCKbdDev;
extern KEYBOARD UsbKbdDev;
extern KEYBOARD VncKbdDev;
extern KEYBOARD DumKbdDev;
extern KEYBOARD Button_Dev;
static KEYBOARD *pKeyboardDev;
static THREAD *pThread;
static bool32_t       bExitFlag;
static SEMAPHOR  ExitEvent;

typedef struct _TKeymapEntry
{
    int16_t             normal;
    int16_t             shifted;
}KeymapEntry;

static const KeymapEntry Keymap[] = 
{
    {VK_UNKNOWN, VK_UNKNOWN },      // 0x0
    {VK_ESCAPE, VK_ESCAPE },
    {'1', '!' },
    {'2', '@' },
    {'3', '#' },
    {'4', '$' },
    {'5', '%' },
    {'6', '^' },
    {'7', '&' },
    {'8', '*' },
    {'9', '(' },
    {'0', ')' },
    {'-', '_' },
    {'=', '+' },
    {VK_BACK, VK_BACK },
    {VK_TAB, VK_TAB },
    {'q', 'Q' },                    // 0x10
    {'w', 'W' },
    {'e', 'E' },
    {'r', 'R' },
    {'t', 'T' },
    {'y', 'Y' },
    {'u', 'U' },
    {'i', 'I' },
    {'o', 'O' },
    {'p', 'P' },
    {'[', '{' },
    {']', '}' },
    {VK_RETURN, VK_RETURN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {'a', 'A' },
    {'s', 'S' },
    {'d', 'D' },                    // 0x20
    {'f', 'F' },
    {'g', 'G' },
    {'h', 'H' },
    {'j', 'J' },
    {'k', 'K' },
    {'l', 'L' },
    {';', ':' },
    {'\'', '"' },
    {'`', '~' },
    {VK_UNKNOWN, VK_UNKNOWN },
    {'\\', '|' },
    {'z', 'Z' },
    {'x', 'X' },
    {'c', 'C' },
    {'v', 'V' },
    {'b', 'B' },                    // 0x30
    {'n', 'N' },
    {'m', 'M' },
    {',', '<' },
    {'.', '>' },
    {'/', '?' },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {' ', ' ' },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },      // 0x40
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },    
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },      // 0x50
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },      // 0x60
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },      // 0x70
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
    {VK_UNKNOWN, VK_UNKNOWN },
};
static const uint8_t Scode2VK[] = 
{
    VK_UNKNOWN ,                    // 0x0
    VK_ESCAPE ,
    '1' ,
    '2' ,
    '3' ,
    '4' ,
    '5' ,
    '6' ,
    '7' ,
    '8' ,
    '9' ,
    '0' ,
    VK_OEM_MINUS ,//-
    VK_OEM_PLUS ,//=
    VK_BACK ,
    VK_TAB ,
    'Q' ,                           // 0x10
    'W' ,
    'E' ,
    'R' ,
    'T' ,
    'Y' ,
    'U' ,
    'I' ,
    'O' ,
    'P' ,
    VK_OEM_4 ,
    VK_OEM_6 ,
    VK_RETURN ,
    VK_LCONTROL ,
    'A' ,
    'S' ,
    'D' ,                           // 0x20
    'F' ,
    'G' ,
    'H' ,
    'J' ,
    'K' ,
    'L' ,
    VK_OEM_1 ,
    VK_OEM_7 ,
    VK_OEM_3 ,
    VK_LSHIFT ,
    VK_OEM_5 ,
    'Z' ,
    'X' ,
    'C' ,
    'V' ,
    'B' ,                           // 0x30
    'N' ,
    'M' ,
    VK_OEM_COMMA ,
    VK_OEM_PERIOD ,
    VK_OEM_2 ,
    VK_RSHIFT ,
    VK_UNKNOWN ,
    VK_LMENU ,
    VK_SPACE ,
    VK_CAPITAL ,
    VK_F1 ,
    VK_F2 ,
    VK_F3 ,
    VK_F4 ,
    VK_F5 ,
    VK_F6 ,                         // 0x40
    VK_F7 ,
    VK_F8 ,
    VK_F9 ,
    VK_F10 ,
    VK_NUMLOCK ,
    VK_SCROLL ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,    
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,                    // 0x50
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_F11 ,
    VK_F12 ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,                    // 0x60
    VK_RCONTROL ,
    VK_UNKNOWN ,
    VK_PRINT ,
    VK_RMENU ,
    VK_UNKNOWN ,
    VK_HOME ,
    VK_UP ,
    VK_PRIOR ,
    VK_LEFT ,
    VK_RIGHT ,
    VK_END ,
    VK_DOWN ,
    VK_NEXT ,
    VK_INSERT ,
    VK_DELETE ,
    VK_UNKNOWN ,                    // 0x70
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
    VK_IN ,
    VK_UNKNOWN ,
    VK_UNKNOWN ,
};

uint8_t VK2Scode(uint16_t nVKCode)
{
    uint8_t i, nCount;
    
    nCount = sizeof (Scode2VK) / sizeof (Scode2VK[0]);
    for (i = 0; i < nCount; i++) {
        if (Scode2VK[i] == nVKCode) {
            return i;
        }
    }
    
    return 0;
}

uint8_t MapKey2Char(uint32_t nKey)
{
    int32_t nCount;
    uint8_t nChar, nMod, nScan;
    
    nMod = LOuint16_t (nKey);
    nScan = HIuint16_t (nKey);
    nCount = sizeof (Keymap) / sizeof (Keymap[0]);
    if (/*(nScan < 0) || */(nScan > nCount)) {
        return VK_UNKNOWN;
    }
    
    // NUM LOCK
    if (nMod & (0x1 << 8)) {
    }
    
    // CAPS LOCK
    if (nMod & (0x1 << 9)) {
        nChar = Keymap[nScan].normal;
        if (nChar >= 'a' && nChar <= 'z') {
            if (nMod & 0x22) {
            }
            else {
                nChar = Keymap[nScan].shifted;
            }
        }
        else {
            if (nMod & 0x22) {
                nChar = Keymap[nScan].shifted;
            }
        }
        
        return nChar;
    }
    
    // SHIFT DOWN
    if (nMod & 0x22) {
        nChar = Keymap[nScan].shifted;
    }
    else {
        nChar = Keymap[nScan].normal;
    }
    
    return nChar;
}

static void KbdPase(uint16_t nVKCode, uint8_t nScan, bool32_t bPressed, uint8_t nMod)
{
    static uint16_t nOldMod = 0;
    
    if (bPressed == 1) {
        switch (nVKCode) {
            case VK_LCONTROL:
                nOldMod |= (0x1 << 0);
                break;
                
            case VK_LSHIFT:
                nOldMod |= (0x1 << 1);
                break;
                
            case VK_LMENU:
                nOldMod |= (0x1 << 2);
                break;
                
            case VK_IN:
                nOldMod |= (0x1 << 3);
                break;
                
            case VK_RCONTROL:
                nOldMod |= (0x1 << 4);
                break;
                
            case VK_RSHIFT:
                nOldMod |= (0x1 << 5);
                break;
                
            case VK_RMENU:
                nOldMod |= (0x1 << 6);
                break;
                
            case VK_RWIN:
                nOldMod |= (0x1 << 7);
                break;
                
            case VK_NUMLOCK:
                nOldMod |= (0x1 << 8);
                break;
                
            case VK_CAPITAL:
                nOldMod |= (0x1 << 9);
                break;
                
            default:
                break;
        }
        
        if (nVKCode != VK_UNKNOWN) {
            PostKeyMessage (EV_KEYDOWN, MAKEuint32_t (nVKCode, 0), MAKEuint32_t (nOldMod, nScan));
        }
    }
    else {
        switch (nVKCode) {
            case VK_LCONTROL:
                nOldMod &= ~(0x1 << 0);
                break;
                
            case VK_LSHIFT:
                nOldMod &= ~(0x1 << 1);
                break;
                
            case VK_LMENU:
                nOldMod &= ~(0x1 << 2);
                break;
                
            case VK_IN:
                nOldMod &= ~(0x1 << 3);
                break;
                
            case VK_RCONTROL:
                nOldMod &= ~(0x1 << 4);
                break;
                
            case VK_RSHIFT:
                nOldMod &= ~(0x1 << 5);
                break;
                
            case VK_RMENU:
                nOldMod &= ~(0x1 << 6);
                break;
                
            case VK_RWIN:
                nOldMod &= ~(0x1 << 7);
                break;
                
            case VK_NUMLOCK:
                nOldMod &= ~(0x1 << 8);
                break;
                
            case VK_CAPITAL:
                nOldMod &= ~(0x1 << 9);
                break;
                
            default:
                break;
        }
        
        if (nVKCode != VK_UNKNOWN) {
            PostKeyMessage (EV_KEYUP, MAKEuint32_t (nVKCode, 0), MAKEuint32_t (nOldMod, nScan));
        }
    }
}

static int32_t KeyboardThreadFun(THREAD *pThread, void *pData)
{
    fd_set rfds;
    struct timeval tv;
    int32_t n, fd, nVKCode;
    uint8_t nRst, nKey, bPressed, nMod;
    
    fd = pKeyboardDev->Open (pKeyboardDev);
    if (fd < 0) {
        return -1;
    }
    
    while (FALSE == bExitFlag) {//初始化键盘时，关闭标志位设为false,
        FD_ZERO(&rfds);//将指定的文件描述符集清空，在对文件描述符集合进行设置前，必须对其进行初始化，如果不清空，由于在系统分配内存空间后，通常并不作清空处理，所以结果是不可知的。
        
        tv.tv_sec  = 1;
        tv.tv_usec = 0;
        
        FD_SET(fd, &rfds);
        n = fd;
        
        n++;
		//检查是否有数据，很多情况下就是是否有数据(注意,只是说很多情况) 
        switch (select(n, &rfds, NULL, NULL, &tv)) {//检查套节字的可读性,可写性,和列外数据性质,返回满足条件的套接口的数目
        case -1:            // Error
        case 0:             // Timeout
            continue;//结束单次循环,这里为0为负都跳到wile再判断
        }
        
        if (FD_ISSET(fd, &rfds)) {//检查fd是否在这个集合里面, 
        //select将更新这个集合,把其中不可读的套节字去掉
//只保留符合条件的套节字在这个集合里面
            nRst = pKeyboardDev->Read (pKeyboardDev, &nKey, &bPressed, &nMod);//nkey是键值，bPressed是上当键
            switch (nRst) {
            case 0:         // error
                break;
                
            case 1:         // scancode
                nVKCode = Scode2VK[nKey];
                KbdPase (nVKCode, nKey, bPressed, nMod);
                break;
                
            default:        // vk
                nVKCode = nKey;
                nKey = VK2Scode (nKey);
                if (0 == nKey) {
                    nVKCode = VK_UNKNOWN;
                }
                KbdPase (nVKCode, nKey, bPressed, nMod);
                break;
            }
        }
    }
    
    pKeyboardDev->Close (pKeyboardDev);
    
    SemaphoreADD (&ExitEvent);
    
    return 0;
}

bool32_t InitKeyboard(void *pParam)
{
#ifdef DUM_KBD
    pKeyboardDev = &DumKbdDev;
#endif
#ifdef USB_KBD
    pKeyboardDev = &UsbKbdDev;
#endif
#ifdef VNC_KBD
    pKeyboardDev = &VncKbdDev;
#endif
#ifdef PC_KBD
    pKeyboardDev = &PCKbdDev;
#endif
#ifdef KEY_BUTTON
    pKeyboardDev = &Button_Dev;
#endif    
    pThread = ThreadCreate (KeyboardThreadFun, NULL);//创建键盘线程
    if (NULL == pThread) {
        return FALSE;
    }
    
    ThreadSetPriority (pThread, 8);
    
    SemaphoreInit (&ExitEvent, 0);
    bExitFlag = FALSE;
    
    return TRUE; 
}

void CloseKeyboard(void)
{
    bExitFlag = TRUE;
    SemaphoreWait (&ExitEvent);
    
    ThreadDestroy (pThread);
}

bool32_t PostKeyMessage(uint32_t Message, uint32_t wParam, uint32_t lParam)
{
#if 0
    WINDOW *Window;
    
    Window = GetMenuActiveWindow ();
    if (NULL == Window) {
        Window = GetImeActiveWindow ();
    }
    
    if (NULL == Window) {
        Window = pGUI->pFocusWnd;
    }
    
    if (NULL == Window) {
        return FALSE;
    }
    
    return PostMessage (Window, Message, wParam, lParam);
#else
    return PostMessage (NULL, Message, wParam, lParam);
#endif
}
