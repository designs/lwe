/*
  Copyright (C), 2008~2009, zhanbin
  File name:    dumkbd.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090107
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090107    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

#ifdef DUM_KBD
static int32_t DumKbdOpen(KEYBOARD *);
static int32_t DumKbdRead(KEYBOARD *pKbdDev, uint8_t *s, uint8_t *p, uint8_t *m);
static void DumKbdClose(KEYBOARD *);

KEYBOARD DumKbdDev = {
    -1,
    DumKbdOpen,
    DumKbdClose,
    DumKbdRead
};

static int32_t DumKbdOpen(KEYBOARD *pKbdDev)
{
    return -1;
}

static int32_t DumKbdRead(KEYBOARD *pKbdDev, uint8_t *s, uint8_t *p, uint8_t *m)
{
    return 0;
}

static void DumKbdClose(KEYBOARD *pKbdDev)
{
}
#endif
