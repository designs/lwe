/*
  Copyright (C), 2008, zhanbin
  File name:    event.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.2
  Date:         20081205
  Description:
    使用消息池完成消息的处理过程在窗口创建太多时可能导致系统死锁，
    但是分配足够的消息可以解决这个问题，我想在嵌入式系统中这已经足
    够应付。
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081220    zhanbin         用LIST重写消息处理
    20081215    zhanbin         实现菜单相关功能
    20081214    zhanbin         PeekMessage实现
    20081205    zhanbin         创建文件
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "window.h"

static void DealPaintMessage(MSG *pMsg)//分配重画消息
{
    WINDOW *Window = pMsg->Window;
    EventAddData *pSyncEventAddData = pMsg->pAddData;//消息附加消息
    
    if (NULL != pSyncEventAddData) {//有附加消息返回
        return;
    }
    
    if (NULL == Window) {//没有对应窗口返回
        return;
    }
    
    switch (pMsg->Message) {
    case EV_ERASEBKGND:
        Window->EraseBackGndMsg  = NULL;//重画背景区为空
        break;
        
    case EV_PAINT:
        Window->PaintMsg   = NULL;//重画客户区为空
        break;
        
    case EV_NCPAINT:
        Window->NcPaintMsg = NULL;//重画标题栏（无效区）为空
        break;
        
    default:
        break;
    }
}
//返回&pGUI->pMsgQueue->EmptyMsgQueueHead消息队列的第一个节点的消息
static MSG* Get1stEmptyMsg(void)
{
    MSG *_list_;
    struct list_head *_head_ = &pGUI->pMsgQueue->EmptyMsgQueueHead;
    #undef _item_
    #define _item_ MsgListNode
    
Retry:
    if (FALSE == SemaphoreWait (&pGUI->pMsgQueue->Semph_t)) {//队列释放一个消息，信号减1
        return NULL;
    }
    
    MutexLock (&pGUI->pMsgQueue->MutexObject);
    
    if (list_empty (_head_)) {//没有消息节点
        MutexUnlock (&pGUI->pMsgQueue->MutexObject);
        goto Retry;
    }
    
    (_list_) = list_entry((_head_)->next, MSG, _item_);//得到第一个节点的msg的首地址
    list_del (&_list_->_item_);//节点脱钩
    
    MutexUnlock (&pGUI->pMsgQueue->MutexObject);
    
    return (_list_);
}

//&pGUI->pMsgQueue->EmptyMsgQueueHead队列紧接着的一个消息
static void AddMsgToEmptyList(MSG* msg)//将原消息作为空消息，再放回空消息队列。
{
    if (msg == NULL){
        return;
    }
    
    MutexLock (&pGUI->pMsgQueue->MutexObject);
    
    list_add (&msg->MsgListNode, &pGUI->pMsgQueue->EmptyMsgQueueHead);//消息队列紧跟后面放一个消息
    
    SemaphoreADD (&pGUI->pMsgQueue->Semph_t);//消息队列信号量加一
    
    MutexUnlock (&pGUI->pMsgQueue->MutexObject);
}

//已有，放入&pGUI->MsgQueueHead队列尾，
//没有, 从&pGUI->pMsgQueue->EmptyMsgQueueHead取1st新建一个放到&pGUI->MsgQueueHead队列尾

//更新窗口的消息（EraseBackGndMsg/PaintMsg/NcPaintMsg）,把消息放入队列尾，
bool32_t PostMessage(WINDOW* Window, uint32_t Message, uint32_t wParam, uint32_t lParam)
{
    MSG *_list_;
    struct list_head *_head_ = &pGUI->MsgQueueHead;
    #undef _item_
    #define _item_ MsgListNode
    
    MutexLock (&pGUI->MutexObject);
//已有，放入&pGUI->MsgQueueHead队列尾，没有从&pGUI->pMsgQueue->EmptyMsgQueueHead取1st脱勾，放到&pGUI->MsgQueueHead队列尾	
//如有窗口相应的消息作替换，链接到链表尾，否则新分配一个（紧接head的那个），链接到链表尾
    if ((WINDOW*)NULL != Window) {
        switch (Message) {//消息类型切换
        case EV_ERASEBKGND://擦除背景
            if (NULL != Window->EraseBackGndMsg) {
                _list_ = Window->EraseBackGndMsg;
                list_del (&_list_->_item_);
                list_add_tail (&_list_->_item_, _head_);
                
                MutexUnlock (&pGUI->MutexObject);
                
                return TRUE;
            }
            else {
                if((_list_ = Get1stEmptyMsg ()) == NULL){
                    MutexUnlock (&pGUI->MutexObject);
                    
                    return FALSE;
                }
                Window->EraseBackGndMsg = _list_;
            }
            break;
            
        case EV_PAINT:
            if (NULL != Window->PaintMsg) {
                _list_ = Window->PaintMsg;
                list_del (&_list_->_item_);
                list_add_tail (&_list_->_item_, _head_);
                
                MutexUnlock (&pGUI->MutexObject);
                
                return TRUE;
            } 
            else {
                if((_list_ = Get1stEmptyMsg ()) == NULL){
                    MutexUnlock (&pGUI->MutexObject);
                    
                    return FALSE;
                }
                Window->PaintMsg = _list_;
            }
            break;
            
        case EV_NCPAINT:
            if (NULL != Window->NcPaintMsg) {
                _list_ = Window->NcPaintMsg;
                list_del (&_list_->_item_);
                list_add_tail (&_list_->_item_, _head_);
                
                MutexUnlock (&pGUI->MutexObject);
                
                return TRUE;
            }
            else {
                if((_list_ = Get1stEmptyMsg ()) == NULL){
                    MutexUnlock (&pGUI->MutexObject);
                    
                    return FALSE;
                }
                Window->NcPaintMsg = _list_;
            }
            break;
            
        default:
            if((_list_ = Get1stEmptyMsg ()) == NULL){
                MutexUnlock (&pGUI->MutexObject);
                
                return FALSE;
            }
            break;
        }
    }
    else {//窗口无的情况
        if((_list_ = Get1stEmptyMsg ()) == NULL){
            MutexUnlock (&pGUI->MutexObject);
            
            return FALSE;
        }
    }
//上面为指针地址操作，这里传入内容，做具体赋值
    _list_->Window   = Window;
    _list_->Message  = Message;
    _list_->wParam   = wParam;
    _list_->lParam   = lParam;
    _list_->pAddData = NULL;
    
    if (Message == EV_TIMER) {
        list_add (&_list_->_item_, _head_);
    }
    else {
        list_add_tail (&_list_->_item_, _head_);
    }
    //信号量加1
    SemaphoreADD (&pGUI->Semph_t);
    
    MutexUnlock (&pGUI->MutexObject);
    
    return TRUE;
}

int32_t SendMessage(WINDOW* Window, uint32_t Message, uint32_t wParam, uint32_t lParam)
{
    EventAddData SyncEventAddData;
    MSG *_list_;
    struct list_head *_head_ = &pGUI->MsgQueueHead;
    #undef _item_
    #define _item_ MsgListNode
    
    if ((WINDOW*)NULL == Window) {
        return -1;
    }
    
    if (Window->WndThread == GetCurrentThreadId ()) {//窗口线程是当前线程，进入消息处理函数
        return (int32_t)Window->WinProc (Window, Message, wParam, lParam);
    }
    
    if((_list_ = Get1stEmptyMsg ()) == NULL){//从堆上得到一个节点脱勾的pMSG
        return -2;
    }
//消息放入队列
    SemaphoreInit (&SyncEventAddData.Semph_t, 0);
    
    _list_->Window   = Window;
    _list_->Message  = Message;
    _list_->wParam   = wParam;
    _list_->lParam   = lParam;
    _list_->pAddData = &SyncEventAddData;
    
    MutexLock (&pGUI->MutexObject);
    
    if (Message == EV_TIMER) {
        list_add (&_list_->_item_, _head_);
    }
    else {
        list_add_tail (&_list_->_item_, _head_);//把得到的pMSG重新挂上去
    }
    
    SemaphoreADD (&pGUI->Semph_t);
    
    MutexUnlock (&pGUI->MutexObject);
    
    SemaphoreWait (&SyncEventAddData.Semph_t);
    
    EventDestroy (&SyncEventAddData.Semph_t);
    
    return SyncEventAddData.nResult;
}

bool32_t GetMessage(MSG *pMsg, WINDOW *Window)
{
    MSG *_list_;
    struct list_head *_head_ = &pGUI->MsgQueueHead;
    #undef _item_
    #define _item_ MsgListNode
    
Retry:
    while (FALSE == SemaphoreWait (&pGUI->Semph_t));//信号量减1
    
    MutexLock (&pGUI->MutexObject);//消息队列，互斥上锁
    
    if (list_empty (_head_)) {//为空，再来
        MutexUnlock (&pGUI->MutexObject);
        goto Retry;
    }
    
    (_list_) = list_entry((_head_)->next, MSG, _item_);//从队列中提出消息
    list_del (&_list_->_item_);//从消息节点上脱钩消息
    
    DealPaintMessage (_list_);//把提出的消息的对应窗口，相关重画消息状态清空
    
    MutexUnlock (&pGUI->MutexObject);//消息队列，互斥锁解锁
    
    memcpy (pMsg, _list_, sizeof (struct _MSG));//把消息队列的消息通过参数1,传递出去
    
    AddMsgToEmptyList(_list_);//将原消息作为空消息，再放回空消息队列。
    
    if (pMsg->Message == EV_QUIT) {
        return FALSE;
    }
    
    return TRUE;
}

bool32_t PeekMessage(MSG *pMsg, WINDOW *Window, uint32_t wMsgFilterMin, uint32_t wMsgFilterMax, uint32_t wRemoveMsg)
{
    MSG *_list_;
    struct list_head *_head_ = &pGUI->MsgQueueHead;
    #undef _item_
    #define _item_ MsgListNode
    
    MutexLock (&pGUI->MutexObject);
    
    if (list_empty (_head_)) {
        MutexUnlock (&pGUI->MutexObject);
        
        return FALSE;
    }
    
    if (NULL == Window) {
        (_list_) = list_entry((_head_)->next, MSG, _item_);
        
        if (PM_REMOVE == wRemoveMsg) {
            list_del (&_list_->_item_);
            
            DealPaintMessage (_list_);
            
            memcpy (pMsg, _list_, sizeof (struct _MSG));
            
            AddMsgToEmptyList(_list_);
        }
        else {
            memcpy (pMsg, _list_, sizeof (struct _MSG));
        }
        
        MutexUnlock (&pGUI->MutexObject);
        
        return TRUE;
        
    }
    else {
        for ((_list_) = list_entry((_head_)->next, MSG, _item_); 
            &((_list_)->_item_) != (_head_);
            (_list_) = list_entry((_list_)->_item_.next, MSG, _item_)) {
            if (_list_->Window != Window) {
                continue;
            }
            
            if (PM_REMOVE == wRemoveMsg) {
                list_del (&_list_->_item_);
                
                DealPaintMessage (_list_);
                
                memcpy (pMsg, _list_, sizeof (struct _MSG));
                
                AddMsgToEmptyList(_list_);
            }
            else {
                memcpy (pMsg, _list_, sizeof (struct _MSG));
            }
            
            MutexUnlock (&pGUI->MutexObject);
            
            return TRUE;
        }
    }
    
    MutexUnlock (&pGUI->MutexObject);
    
    return FALSE;
}
//寻找适当的窗口，作为消息的窗口
bool32_t TranslateMessage (MSG *pMsg)
{
    uint16_t nKey;
    POINT pt;
    WINDOW *Window;
    //消息类型是大于开始键消息，且小于最后键消息
    if (pMsg->Message >= EV_FIRSTKEYMSG && pMsg->Message <= EV_LASTKEYMSG) {
        if (NULL == pMsg->Window) {//没有消息窗口的情况下
            Window = GetMenuActiveWindow ();//用激活的菜单窗口
            if (NULL == Window) {//菜单窗口为空，用激活的输入窗口
                Window = GetImeActiveWindow ();
            }
    //使用焦点窗口
            if (NULL == Window) {
                Window = pGUI->pFocusWnd;
            }
            
            pMsg->Window = Window;
        }
        
        if (EV_KEYDOWN == pMsg->Message) {//消息类型是按键按下
            nKey = MapKey2Char (pMsg->lParam);
            if (nKey != VK_UNKNOWN) {//按键转字符
                PostMessage (pMsg->Window, EV_CHAR, MAKEuint32_t (nKey, 0), pMsg->lParam);
            }
        }
    }
    //消息类型是大于开始鼠标消息，且小于最后鼠标消息
    if (pMsg->Message >= EV_FIRSTMOUSEMSG && pMsg->Message <= EV_LASTMOUSEMSG) {
        if (NULL == pMsg->Window) {//没有消息窗口的情况下
            Window = GetCapture ();//用标题栏窗口
            if (NULL == Window) {//标题栏窗口为空，根据鼠标位置的窗口
                pt.x = LOuint16_t(pMsg->lParam);
                pt.y = HIuint16_t(pMsg->lParam);
                Window = FindWindowByPos (&pt);
            }
            
            pMsg->Window = Window;
        }
    }
    
    return TRUE;
}
//执行传递的消息的消息处理函数。
int32_t ProcessMessage(WINDOW *Window, MSG *pMsg)
{
    EventAddData *pSyncEventAddData;
    
    if (NULL != pMsg->pAddData) {//如有附加数据
        pSyncEventAddData = pMsg->pAddData;
        
        if (NULL == Window) {//如无窗口
            pSyncEventAddData->nResult = -1;
        }
        else {//运行消息处理函数
            pSyncEventAddData->nResult = Window->WinProc (Window, pMsg->Message, pMsg->wParam, pMsg->lParam);
        }//信号量: 只要信号量的value大于0，其他线程就可以sem_wait成功，成功后信号量的value减一。
        SemaphoreADD (&pSyncEventAddData->Semph_t);//信号量+1
        
        return pSyncEventAddData->nResult;
    }
    else {//没有附加数据
        if (NULL == Window) {
            return -1;
        }
        
        return Window->WinProc (Window, pMsg->Message, pMsg->wParam, pMsg->lParam);
    }
}

int32_t DispatchMessage(MSG *pMsg)
{
    switch (pMsg->Message) {
    case EV_UPDATEALLWINDOWS:
        UpdateAllWindow ();
        return 0;
        break;
        
	case EV_LBUTTONDOWN:
	case EV_LBUTTONUP:
	case EV_RBUTTONDOWN:
	case EV_RBUTTONUP:
	{
	    WINDOW *Window = pMsg->Window;
	    
	    while (NULL != Window) {
	        if (FALSE == IsWindowEnabled (Window)) {
	            DiscardMessage (pMsg);
	            
	            return 0;
	        }
	        
	        Window = Window->pParentWnd;
	    }
        break;
    }
    
    default:
        break;
    }
    
    return ProcessMessage (pMsg->Window, pMsg);
}

void DiscardMessage(MSG *pMsg)
{
    EventAddData *pSyncEventAddData;
    
    if (NULL != pMsg->pAddData) {
        pSyncEventAddData = pMsg->pAddData;
        
        pSyncEventAddData->nResult = -1;
        
        SemaphoreADD (&pSyncEventAddData->Semph_t);
    }
}

void PostQuitMessage(int32_t nExitCode)
{
	PostMessage(NULL, EV_QUIT, nExitCode, 0L);
}

bool32_t InitMessage(void)
{
    uint32_t i;
    MSG* msg;
    
    if((pGUI->pMsgQueue = (Msg_Queue*)Malloc (sizeof (Msg_Queue) + MAX_EVENT * sizeof (MSG))) == NULL){
        return FALSE;
    }
    
    memset (pGUI->pMsgQueue, 0, sizeof(Msg_Queue) + MAX_EVENT * sizeof (MSG));
    
    if (FALSE == MutexCreate (&pGUI->pMsgQueue->MutexObject)) {
        Free (pGUI->pMsgQueue);
        return FALSE;
    }
    if (FALSE == SemaphoreInit (&pGUI->pMsgQueue->Semph_t, 0)) {
        ReleaseMutex (&pGUI->pMsgQueue->MutexObject);
        Free (pGUI->pMsgQueue);
        return FALSE;
    }
    
    LIST_HEAD_RESET(&pGUI->pMsgQueue->EmptyMsgQueueHead);
    
    for (i = 0; i < MAX_EVENT; i++){//pGUI->pMsgQueue + 1从pGUI->pMsgQueue跨过一个消息队列，即消息队列后跟消息
        msg = (MSG*)((char*)(pGUI->pMsgQueue + 1) + i * sizeof(MSG));
        
        AddMsgToEmptyList(msg);
    }
    
    return TRUE;
}

void ReleaseMessage(void)
{
}
