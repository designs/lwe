/*
  Copyright (C), 2008~2009, zhanbin
  File name:    os.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090106
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090106    zhanbin         创建文件
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

bool32_t SemaphoreInit(SEMAPHOR *pSem, int32_t nInit)
{
#ifdef ECOS
    cyg_semaphore_init (&pSem->semaphore_t, nInit);
#elif defined(LINUX)
    sem_init (&pSem->semaphore_t, 0, nInit);
#endif
    
    return TRUE;
}

bool32_t SemaphoreADD(SEMAPHOR *pEvent)
{
#ifdef ECOS
    cyg_semaphore_post (&pEvent->semaphore_t);
#elif defined(LINUX)
    sem_post (&pEvent->semaphore_t);
#endif
    
    return TRUE;
}

bool32_t SemaphoreWait(SEMAPHOR *pEvent)
{
#ifdef ECOS
    return cyg_semaphore_wait (&pEvent->semaphore_t);
#elif defined(LINUX)
    sem_wait (&pEvent->semaphore_t);
    
    return TRUE;
#endif
}

bool32_t EventDestroy(SEMAPHOR *pEvent)
{
#ifdef ECOS
    cyg_semaphore_destroy (&pEvent->semaphore_t);
#elif defined(LINUX)
    sem_destroy (&pEvent->semaphore_t);
#endif
    
    return TRUE;
}

bool32_t MutexCreate(MUTEX *pMutex)
{
#ifdef ECOS
    cyg_mutex_init (&pMutex->hMutex);
#elif defined(LINUX)
    pthread_mutex_init (&pMutex->hMutex, NULL);
#endif
    
    return TRUE;
}

bool32_t MutexLock(MUTEX *pMutex)
{
#ifdef ECOS
    cyg_mutex_lock (&pMutex->hMutex);
    
    return TRUE;
#elif defined(LINUX)
    pthread_mutex_lock (&pMutex->hMutex);
    
    return TRUE;
#endif
}

bool32_t MutexUnlock(MUTEX *pMutex)
{
#ifdef ECOS
    cyg_mutex_unlock (&pMutex->hMutex);
#elif defined(LINUX)
    pthread_mutex_unlock (&pMutex->hMutex);
#endif
    
    return TRUE;
}

bool32_t ReleaseMutex(MUTEX *pMutex)
{
#ifdef ECOS
    cyg_mutex_destroy (&pMutex->hMutex);
#elif defined(LINUX)
    pthread_mutex_destroy (&pMutex->hMutex);
#endif
    
    return TRUE;
}

#ifdef ECOS
static void EcosThreadProc(cyg_addrword_t pData)
{
    int32_t nResult;
    THREAD *pThread;
    
    pThread = (THREAD *)pData;
    nResult = pThread->pFun (pThread, pThread->pUserData);
}
#elif defined(LINUX)
static void *LinuxThreadProc(void *pData)
{
    int32_t nResult;
    THREAD *pThread;
    
    pThread = (THREAD *)pData;
    nResult = pThread->pFun (pThread, pThread->pUserData);
    
    return (void *)nResult;
}
#endif

THREAD *ThreadCreate(ThreadProc pFun, void *pData)
{
    THREAD *pThread;
    
    pThread = (THREAD *)Malloc (sizeof (THREAD));
    if (NULL == pThread) {
        return NULL;
    }
    
    pThread->pFun      = pFun;
    pThread->pUserData = pData;
    
#ifdef ECOS
    pThread->pStack = (void *)Malloc (32 * 1024);
    cyg_thread_create (11, EcosThreadProc, (cyg_addrword_t)pThread, "", pThread->pStack, 
        32 * 1024, &pThread->hThread, &pThread->ThreadObj);
    cyg_thread_resume (pThread->hThread);
#elif defined(LINUX)
    pthread_attr_t a_thread_attribute;
    
    SemaphoreInit (&pThread->SuspendEvent, 0);
    
    pthread_attr_init (&a_thread_attribute);
    pthread_attr_setdetachstate (&a_thread_attribute, PTHREAD_CREATE_DETACHED);
    
    pthread_create (&pThread->hThread, NULL, LinuxThreadProc, (void*)pThread);
#endif
    
    return pThread;
}

bool32_t ThreadSetPriority(THREAD *pThread, int32_t nPriority)
{
    if (nPriority < 0) {
        nPriority = 0;
    }
    
    if (nPriority > 31) {
        nPriority = 31;
    }
    
#ifdef ECOS
    cyg_thread_set_priority (pThread->hThread, nPriority);
#elif defined(LINUX)
    {
        struct sched_param SchedParam;//线程参数结构体
        
        if (nPriority < 10) {
            SchedParam.sched_priority = 9 - nPriority;//线程优先级
            //线程调度策略SCHED_RR实时调度策略，时间片轮转 
            pthread_setschedparam (pThread->hThread, SCHED_RR, &SchedParam);
        }
        else {
            SchedParam.sched_priority = 0;
            //指定调度策略policy——SCHED_OTHER（默认）分时调度策略
            pthread_setschedparam (pThread->hThread, SCHED_OTHER, &SchedParam);
        }
    }
#endif
    
    return TRUE;
}

bool32_t ThreadSuspend(THREAD *pThread)
{
#ifdef ECOS
    cyg_thread_suspend (pThread->hThread);
#elif defined(LINUX)
    SemaphoreWait (&pThread->SuspendEvent);
#endif
    
    return TRUE;
}

bool32_t ThreadResume(THREAD *pThread)
{
#ifdef ECOS
    cyg_thread_resume (pThread->hThread);
#elif defined(LINUX)
    SemaphoreADD (&pThread->SuspendEvent);
#endif
    
    return TRUE;
}

bool32_t ThreadDestroy(THREAD *pThread)
{
#ifdef ECOS
    cyg_thread_delete (pThread->hThread);
    if (pThread->pStack) {
        Free (pThread->pStack);
        pThread->pStack = NULL;
    }
#elif defined(LINUX)
    SemaphoreADD (&pThread->SuspendEvent);
    EventDestroy (&pThread->SuspendEvent);
#endif
    Free (pThread);
    
    return TRUE;
}

uint32_t GetCurrentThreadId(void)
{
#ifdef ECOS
    return (uint32_t)cyg_thread_get_id (cyg_thread_self ());
#elif defined(LINUX)
    return (uint32_t)pthread_self ();
#endif
}

void Sleep(uint32_t nMs)
{
#ifdef ECOS
    cyg_thread_delay (nMs / 10);
#elif defined(LINUX)
    usleep (nMs * 1000);
#endif
}

uint64_t GetTime(void)
{
    uint64_t nMs = 0;
    
#ifdef ECOS
    nMs = (uint64_t)cyg_current_time () * 10;
#elif defined(LINUX)
    struct timeb tm;
    
    ftime (&tm);
    nMs = ((uint64_t)tm.time * 1000) + (uint64_t)tm.millitm;
#endif
    
    return nMs;
}

void DebugPrint(const int8_t *fmt, ...)
{
    va_list args;
    int8_t buffer[500];

    va_start( args, fmt );

    vsprintf( buffer, fmt, args );
    
#ifdef ECOS
    diag_printf (buffer);
#elif defined(LINUX)
    printf (buffer);
#endif
}
