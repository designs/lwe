#------------------------------------------------------------------------------#
# Library directory                                                            #
#------------------------------------------------------------------------------#
LIB_INSTALL_DIR = .

#------------------------------------------------------------------------------#
# Source files                                                                 #
#------------------------------------------------------------------------------#
SSRC =
SOBJS = $(patsubst %.S,%.o, $(SSRC))

CSRC =  main.c                  \
		caret.c                 \
		cursor.c                \
		desktop.c               \
		dialog.c                \
		event.c                 \
		menu.c                  \
		os.c                    \
		phydc.c                 \
		rect.c                  \
		resource.c              \
		rgn.c                   \
		timer.c                 \
		window.c                \
		winmisc.c               \
		                        \
		app/detect.c              \
		app/ctrldemo.c          \
		app/housekeeper.c       \
		app/map.c               \
		app/WinMain.c           \
		                        \
		gdi/bmp.c               \
		gdi/clip.c              \
		gdi/draw3d.c            \
		gdi/gdi.c               \
		gdi/fb16.c              \
		gdi/fb24.c              \
		gdi/fb32.c              \
		gdi/font.c              \
		gdi/icon.c              \
		gdi/softcursor.c        \
		                        \
		ime/ime.c               \
		ime/pinyin.c            \
		                        \
		control/bmpbutton.c     \
		control/bmpscrollbar.c  \
		control/checkbutton.c   \
		control/clock.c         \
		control/edit.c          \
		control/listbox.c       \
		control/listview.c      \
		control/mainmenu.c      \
		control/medit.c         \
		control/progressbar.c   \
		control/static.c        \
		                        \
		input/mouse.c           \
		input/ecosusbmse.c      \
		input/dummymse.c        \
		input/ps2mse.c          \
		input/keyboard.c        \
		input/ecosusbkbd.c      \
		input/dummykbd.c        \
		input/pckbd.c           \
		input/button.c          \
		                        \
		hw/dummycursor.c

COBJS = $(patsubst %.c,%.o, $(CSRC))

OBJS = $(SOBJS) $(COBJS)

#------------------------------------------------------------------------------#
# Local include path							                               #
#------------------------------------------------------------------------------#
INCLUDE_PATH = -I./ -I./control -I./gdi -I./ime -I./input
EXTRALIB_PATH =

#------------------------------------------------------------------------------#
# Libraries to link                                                            #
#------------------------------------------------------------------------------#
LD_LIBS =-L../lib/ -lrt -lpthread -lm

#------------------------------------------------------------------------------#
# Target compiler                                                              #
#------------------------------------------------------------------------------#
CROSS_COMPILER = arm-linux-

AS      = $(CROSS_COMPILER)as
CC      = $(CROSS_COMPILER)gcc
CXX     = $(CROSS_COMPILER)g++
LD      = $(CC)
AR      = $(CROSS_COMPILER)ar
NM      = $(CROSS_COMPILER)nm
RANLIB  = $(CROSS_COMPILER)ranlib
OBJCOPY = $(CROSS_COMPILER)objcopy

#------------------------------------------------------------------------------#
# Compiler flag                                                                #
#------------------------------------------------------------------------------#
CPPFLAGS =
SFLAGS   =
CFLAGS   = $(INCLUDE_PATH) -DLINUX -DFB_16 -DSOFT_CURSOR -DKEY_BUTTON -g -Wall# -O2
CXXFLAGS =
LDFLAGS  = $(EXTRALIB_PATH)
LIBS     = $(LD_LIBS)

#------------------------------------------------------------------------------#
#                                                                              #
#------------------------------------------------------------------------------#
all :
	$(MAKE) Image

clean:
	-rm -f $(OBJS) *.o *.map *.bak Image Image.* boot.*

clear:
	$(MAKE) clean

$(SOBJS): %.o : %.S
	$(CC) $(CFLAGS) -c $< -o $@

$(COBJS): %.o : %.c
	$(CC) $(CFLAGS) -c $< -o $@

Image : Image.bin
	cp Image.bin $@
	@echo "Output Image"

Image.bin : Image.elf
	$(OBJCOPY) -R .comment -R .note -O binary -S Image.elf $@

Image.elf : $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $(OBJS) $(LIBS)
	$(NM) $@ | grep -v '\(compiled\)\|\(\.o$$\)\|\( [aUw] \)\|\(.\.ng$$\)\|\(LASH[RL]DI\)' | sort > System.map
