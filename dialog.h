#ifndef __DIALOG_H__
#define __DIALOG_H__

#ifdef      __cplusplus
extern "C" {
#endif

#define MB_OK                   0x00000000L
#define MB_OKCANCEL             0x00000001L
#define MB_ABORTRETRYIGNORE     0x00000002L
#define MB_YESNOCANCEL          0x00000003L
#define MB_YESNO                0x00000004L

#define IDC_STATIC              -1
#define IDOK                    1
#define IDCANCEL                2
#define IDABORT                 3
#define IDRETRY                 4
#define IDIGNORE                5
#define IDYES                   6
#define IDNO                    7

typedef struct _TDlgCtrl
{
    int8_t*             pClass;
    int32_t             nStyle;
    int32_t             id;
    int32_t             x;
    int32_t             y;
    int32_t             w;
    int32_t             h;
    int8_t*             pCaption;
    void*           pPrivData;
}DLGCRTL;

typedef struct _TDlgCreateData
{
    DLGCRTL*       pDlgCtrl;
    WINPROC       pDlgProc;
}DLGCRTDATA;

int32_t DefDialogProc(WINDOW *, uint32_t, uint32_t, uint32_t);
bool32_t RegisterDialogClass (void);
int32_t CreateDialog(const int8_t *, uint32_t, int32_t, int32_t,WINDOW *, WINPROC);
int32_t EndDialog (WINDOW *, int32_t);
int32_t MessageBox(WINDOW *, int8_t *, int8_t *, uint32_t);

#ifdef      __cplusplus
}
#endif

#endif
