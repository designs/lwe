/*
  Copyright (C), 2008~2009, zhanbin
  File name:    cursor.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081223
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090109    zhanbin         软件光标实现
    20081223    zhanbin         创建文件
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "window.h"

bool32_t InitCursor(void *pParam)
{
#ifdef SOFT_CURSOR
    return InitSoftCursor ();
#else
    return InitHWCursor ();
#endif
}

void ReleaseCursor(void)
{
}

CURSOR *CreateCursor(int32_t xHotSpot,int32_t yHotSpot,int32_t nWidth,int32_t nHeight,
    const void *pvANDPlane,const void *pvXORPlane)
{
    CURSOR *pCursor;

    pCursor = (CURSOR *)Malloc (sizeof (CURSOR));
    if (NULL == pCursor) {
        return NULL;
    }

    pCursor->x = xHotSpot;
    pCursor->y = yHotSpot;
    pCursor->w = nWidth;
    pCursor->h = nHeight;

    pCursor->pAndMask = (void *)pvANDPlane;
    pCursor->pXorMask = (void *)pvXORPlane;

    return pCursor;
}

static uint8_t *Cursor2Glyph(uint8_t *pMask, int32_t nWidth, int32_t nHeight, int32_t nBit)
{
    switch (nBit) {
    case 1:
    {
        int i, k;
        uint8_t *pGlyph, *pSrc;

        pGlyph = (uint8_t *)Malloc (nWidth * nHeight / 8);
        if (NULL == pGlyph) {
            return NULL;
        }

        k = 0;
        pSrc = pMask;
        pGlyph += nWidth * nHeight / 8;
        for (i = 0; i < nHeight; i++) {
            pGlyph -= nWidth / 8;
            memcpy (pGlyph, pSrc, nWidth / 8);
            pSrc += nWidth / 8;
        }
        return pGlyph;
        break;
    }

    case 4:
    {
        int i, j, k, t = 0;
        uint8_t *pGlyph, *pSrc, *pDes;


        pGlyph = (uint8_t *)Malloc (nWidth * nHeight / 8);
        if (NULL == pGlyph) {
            return NULL;
        }

        k = 0;
        pSrc = pMask;
        pGlyph += nWidth * nHeight / 8;
        for (i = 0; i < nHeight; i++) {
            pDes = pGlyph -= nWidth / 8;
            for (j = 0; j < nWidth; j++) {
                if (j % 2 == 0) {
                    t = *pSrc++;
                }

                *pDes = (*pDes) << 1;

                if (j % 2 == 0) {
                    if (t & 0xf0) {
                        *pDes |= 0x1;
                    }
                }
                else {
                    if (t & 0x0f) {
                        *pDes |= 0x1;
                    }
                }

                if (++k % 8 == 0) {
                    pDes++;
                }
            }
        }

        return pGlyph;
        break;
    }

    default:
        break;
    }

    return NULL;
}

CURSOR *LoadCursorFromFile(const int8_t *pFileName)
{
    FILE *fp;
    uint32_t nSize;
    ICONDIR icondir;
    CURSOR *pCursor;
    ICONFILEHEADER fileheader;
    BITMAPINFOHEADER infoheader;
    uint8_t *cursordata, *andmask, *xormask;

    if ( (fp = fopen (pFileName, "rb")) == NULL ) {
        return FALSE;
    }

    fread (&fileheader, sizeof(ICONFILEHEADER), 1, fp);
    if (fileheader.idType != 2) {
        fclose (fp);
        return NULL;
    }

    fread (&icondir, sizeof(ICONDIR), 1, fp);
    if (icondir.bWidth % 8 || icondir.bHeight % 8) {
        fclose (fp);
        return NULL;
    }

    fseek(fp, icondir.dwImageOffset, SEEK_SET);
    fread (&infoheader, sizeof(BITMAPINFOHEADER), 1, fp);
    if (infoheader.biPlanes != 1 || infoheader.biBitCount > 4) {
        fclose (fp);
        return NULL;
    }

    fseek(fp, sizeof (RGBQUAD) << infoheader.biBitCount, SEEK_CUR);

    nSize = icondir.bWidth * icondir.bHeight / 8;
    if (infoheader.biSizeImage < nSize * (infoheader.biBitCount + 1)) {
        infoheader.biSizeImage = nSize * (infoheader.biBitCount + 1);
    }

    cursordata = (uint8_t *)Malloc (infoheader.biSizeImage);
    if (NULL == cursordata) {
        fclose (fp);
        return NULL;
    }

    fread (cursordata, infoheader.biSizeImage, 1, fp);

    fclose (fp);

    xormask = Cursor2Glyph (cursordata, icondir.bWidth, icondir.bHeight, infoheader.biBitCount);
    if (NULL == xormask) {
        Free(cursordata);
        return NULL;
    }

    andmask = Cursor2Glyph (cursordata + (nSize * infoheader.biBitCount),
        icondir.bWidth, icondir.bHeight, 1);
    if (NULL == andmask) {
        Free (cursordata);
        Free (xormask);
        return NULL;
    }

    Free (cursordata);

    pCursor = CreateCursor (icondir.wPlanes, icondir.wBitCount,
        icondir.bWidth, icondir.bHeight, andmask, xormask);
    if (NULL == pCursor) {
        Free (xormask);
        Free (andmask);
        return NULL;
    }

    return pCursor;
}

bool32_t DestroyCursor(CURSOR *pCursor)
{
    if (NULL == pCursor) {
        return FALSE;
    }

    Free (pCursor->pAndMask);
    Free (pCursor->pXorMask);
    Free (pCursor);

    return TRUE;
}

CURSOR *SetWindowCursor(WINDOW *Window, CURSOR *pCursor)
{
    CURSOR *pOldCursor;

    pOldCursor = Window->pCursor;
    Window->pCursor = pCursor;

    return pOldCursor;
}

CURSOR *GetWindowCursor(WINDOW *Window)
{
    return Window->pCursor;
}

CURSOR *SetCursor(CURSOR *pCursor)
{
    CURSOR *pOldCursor;

    pOldCursor = pGUI->pSysCursor;
    pGUI->pSysCursor = pCursor;

#ifdef SOFT_CURSOR
    ShowCursor(pGUI->bShowCursor);
#else
    if (NULL != pGUI->pSysCursor) {
        SetHWCursor (pGUI->pSysCursor);

        ShowCursor(pGUI->bShowCursor);
    }
    else {
        ShowCursor(pGUI->bShowCursor);
    }
#endif

    return pOldCursor;
}

CURSOR *GetCursor(void)
{
    return pGUI->pSysCursor;
}

int32_t ShowCursor(bool32_t bShow)
{
    pGUI->bShowCursor = bShow;

#ifdef SOFT_CURSOR
    LockSoftCursor ();
    if (bShow) {
        ResumeCursorBmp ();
        SaveCursorBmp (pGUI->CursorPos.x, pGUI->CursorPos.y);
        DrawSoftCursor (pGUI->CursorPos.x, pGUI->CursorPos.y);
    }
    else {
        ResumeCursorBmp ();
        SaveCursorBmp (pGUI->CursorPos.x, pGUI->CursorPos.y);
    }
    UnlockSoftCursor ();
#else
    if (bShow) {
        SetHWCursorEnable (TRUE);
    }
    else {
        SetHWCursorEnable (FALSE);
    }
#endif

    return (int32_t)bShow;
}

bool32_t SetCursorPos(int32_t X, int32_t Y)
{
//    SetMousePos (X, Y);
    SetPoint (&pGUI->CursorPos, X, Y);

#ifdef SOFT_CURSOR
    LockSoftCursor ();
    ResumeCursorBmp ();
    SaveCursorBmp (pGUI->CursorPos.x, pGUI->CursorPos.y);
    DrawSoftCursor (pGUI->CursorPos.x, pGUI->CursorPos.y);
    UnlockSoftCursor ();
#else
    if (NULL != pGUI->pSysCursor) {
        SetHWCursorPos (X, Y, pGUI->pSysCursor->x, pGUI->pSysCursor->y);
    }
#endif

    return TRUE;
}

bool32_t GetCursorPos(LPPOINT lpPoint)
{
    lpPoint->x = pGUI->CursorPos.x;
    lpPoint->y = pGUI->CursorPos.y;

    return TRUE;
}
