/*
  Copyright (C), 2008~2009, zhanbin
  File name:    phydc.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090115
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090115    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "window.h"

#ifdef ECOS
#ifndef FB_VNC16
#include <cyg/io/fbctl.h>
#else
#include <vnc-server.h>
#endif
#endif

bool32_t InitPhyDC(PHYDC *pPhyDC, void *pParam)
{
#ifdef ECOS
#ifndef FB_VNC16
    int32_t fd, err;
    VIDEO_MODE_INFO VideoModeInfo;
    
    fd = open("/dev/fb0", O_RDWR);
    if (fd < 0) {
    	DebugPrint ("open /dev/fb0 failed.\n");
    	return FALSE;
    }
    
    err = cyg_fs_fgetinfo (fd, VIDEO_CTL_QUERY_MODE_INFO, &VideoModeInfo, sizeof(VideoModeInfo));
    if (0 != err) {
        close (fd);
        return FALSE;
    }
    
    pPhyDC->fb = (void *)VideoModeInfo.fb;
    if (NULL == pPhyDC->fb) {
        close (fd);
        return FALSE;
    }
    
    pPhyDC->w         = VideoModeInfo.w;
    pPhyDC->h         = VideoModeInfo.h;
    pPhyDC->bpp       = VideoModeInfo.bpp;
    pPhyDC->linesize  = pPhyDC->w * pPhyDC->bpp / 8;
    pPhyDC->get_pixel = fb_get_pixel;
    pPhyDC->set_pixel = fb_set_pixel;
    pPhyDC->line      = fb_line;
    pPhyDC->fill_rect = fb_fill_rect;
    pPhyDC->copy_rect = fb_copy_rect;
    pPhyDC->set_rect  = fb_set_rect;
    
    close (fd);
#else
    vnc_frame_format_t *display_info;
    
    /* Get information about the display */
    display_info = VncGetInfo();
    
    /* Initialise the VNC server display */
    VncInit(VNC_WHITE);
    
    pPhyDC->w         = display_info->frame_width;
    pPhyDC->h         = display_info->frame_height;
    pPhyDC->bpp       = 16;
    pPhyDC->linesize  = pPhyDC->w * pPhyDC->bpp / 8;
    pPhyDC->fb        = display_info->frame_buffer;
    pPhyDC->get_pixel = fb_get_pixel;
    pPhyDC->set_pixel = fb_set_pixel;
    pPhyDC->line      = fb_line;
    pPhyDC->fill_rect = fb_fill_rect;
    pPhyDC->copy_rect = fb_copy_rect;
    pPhyDC->set_rect  = fb_set_rect;
#endif
#elif defined(LINUX)
    int32_t fd;
    struct fb_var_screeninfo vInfo;
    
    fd = open("/dev/fb0", O_RDWR);
    if (fd < 0) {
    	DebugPrint ("open /dev/fb0 failed.\n");
    	return FALSE;
    }
    
    ioctl (fd, FBIOGET_VSCREENINFO, &vInfo);
    pPhyDC->w         = vInfo.xres;
    pPhyDC->h         = vInfo.yres;
    pPhyDC->bpp       = vInfo.bits_per_pixel;
    pPhyDC->linesize  = vInfo.xres * vInfo.bits_per_pixel / 8;
    pPhyDC->fb        = mmap (NULL, pPhyDC->linesize * pPhyDC->h, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    pPhyDC->get_pixel = fb_get_pixel;
    pPhyDC->set_pixel = fb_set_pixel;
    pPhyDC->line      = fb_line;
    pPhyDC->fill_rect = fb_fill_rect;
    pPhyDC->copy_rect = fb_copy_rect;
    pPhyDC->set_rect  = fb_set_rect;
    
    close (fd);
    
#ifndef FBTEXTMODE
    fd = open ("/dev/tty0", O_RDWR);
    if (fd >= 0) {
        ioctl (fd, KDSETMODE, KD_GRAPHICS);
        close(fd);
    }
    close(fd);
#endif
#endif
    
    return TRUE;
}

void ReleasePhyDC(PHYDC *pPhyDC)
{
#ifdef LINUX
#ifndef FBTEXTMODE
    int32_t fd;
    
    fd = open ("/dev/tty0", O_RDWR);
    if (fd >= 0) {
        ioctl (fd, KDSETMODE, KD_TEXT);
        close(fd);
    }
    close(fd);
#endif
#endif
}
