/*
  Copyright (C), 2008, zhanbin
  File name:    menu.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081215
  Description:
    菜单按多行多列方式显示。
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090119    zhanbin         窗口主菜单支持
    20081216    zhanbin         多级菜单实现，能够正常工作，需要优化
    20081215    zhanbin         功能实现
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

static WINDOW *MouseInUpMenu(MENU *pMenu, POINT *p)
{
    WINDOW *pMouseWnd;
    MENU *pUpMenu = pMenu->pUpMenu;
    
    pMouseWnd = FindWindowByPos (p);
    
    while (NULL != pUpMenu) {
        if (pMouseWnd == pUpMenu->pMenuWnd) {
            return pMouseWnd;
        }
        
        pUpMenu = pUpMenu->pUpMenu;
    }
    
    return NULL;
}

static MENU *GetRootMenu(MENU *pMenu)
{
    MENU *pUpMenu = pMenu;
    
    while (NULL != pUpMenu) {
        pMenu = pUpMenu;
        pUpMenu = pUpMenu->pUpMenu;
    }
    
    return pMenu;
}

static void DrawSubMenuTriangle(DC *hdc, RECT *lpRect)
{
    int32_t x, y;
    COLORREF OldColor;
    
    x = lpRect->right - 6;
    y = lpRect->top + (lpRect->bottom - lpRect->top) / 2 - 3;
    
    OldColor = SetFgColor (hdc, RGB (0, 0, 0));
    
    MoveToEx (hdc, x, y, NULL);
    LineTo (hdc, x, y + 6);
    x++;
    y++;
    MoveToEx (hdc, x, y, NULL);
    LineTo (hdc, x, y + 4);
    x++;
    y++;
    MoveToEx (hdc, x, y, NULL);
    LineTo (hdc, x, y + 2);
    x++;
    y++;
    MoveToEx (hdc, x, y, NULL);
    LineTo (hdc, x, y);
    
    SetFgColor (hdc, OldColor);
}

static int32_t DefMenuProc(WINDOW *Window,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    switch(Message){
    case EV_ERASEBKGND:
    {
        DC* hdc;
        RECT ClientRect;
        
        hdc = BeginErase (Window, wParam, lParam);
        if (NULL == hdc) {
            break;
        }
        
        GetClientRect (Window, &ClientRect);
        
        SetFgColor (hdc, RGB (255, 255, 255));
        FillRect (hdc, &ClientRect);
        SetFgColor (hdc, RGB (172, 168, 153));
        DrawRect (hdc, &ClientRect);
        
        EndErase (Window, hdc, wParam, lParam);
        return 0;
        break;
    }
        
    case EV_PAINT:
    {
        DC* hdc;
        int32_t i, X, Y;
        MENU *pMenu;
        RECT TmpRect;
        MENUItem* _list_;
        struct list_head *_head_;
        #undef _item_
        #define _item_ ChildListNode
        
        hdc = BeginPaint (Window, wParam, lParam);
        if (NULL == hdc) {
            break;
        }
        
        pMenu = Window->pMenu;
        _head_ = &pMenu->ChildListHead;
        
        i = 0;
        X = 0;
        Y = 0;
        for ((_list_) = list_entry((_head_)->next, MENUItem, _item_); 
            &((_list_)->_item_) != (_head_);
            (_list_) = list_entry((_list_)->_item_.next, MENUItem, _item_)) {
            SetRect (&TmpRect, _list_->ItemRect.left + 1, _list_->ItemRect.top + 1, 
            _list_->ItemRect.right - 1, _list_->ItemRect.bottom - 1);
            if (pMenu->pOldSelItem == _list_) {
                SetFgColor (hdc, RGB (49, 106, 197));
                FillRect (hdc, &TmpRect);
                
                // Draw item
                SetFgColor (hdc, RGB (0, 0, 0));
                DrawText (hdc, _list_->spName, strlen (_list_->spName), &TmpRect, DT_LEFT);
                
                // Draw triangle
                if (_list_->nType == MENU_TYPE_SUBMENU) {
                    DrawSubMenuTriangle (hdc, &TmpRect);
                }
            }
            else {
                // Draw item
                SetFgColor (hdc, RGB (0, 0, 0));
                DrawText (hdc, _list_->spName, strlen (_list_->spName), &TmpRect, DT_LEFT);
                
                // Draw triangle
                if (_list_->nType == MENU_TYPE_SUBMENU) {
                    DrawSubMenuTriangle (hdc, &TmpRect);
                }
            }
            
            Y += pMenu->nItemHeight;
            if ((i % pMenu->nItemsPerCol) == (pMenu->nItemsPerCol - 1)) {
                X = X + pMenu->nItemWidth;
                
                // Draw separator
                SetFgColor (hdc, RGB (172, 168, 153));
                MoveToEx (hdc, X, 4, NULL);
                LineTo (hdc, X, Y - 4);
                
                Y = 0;
            }
            
            i++;
        }
        
        EndPaint (Window, hdc, wParam, lParam);
        return 0;
        break;
    }
    
    case EV_LBUTTONDOWN:
    case EV_LBUTTONUP:
    case EV_RBUTTONDOWN:
    case EV_RBUTTONUP:
    {
        POINT p;
        RECT WndRect;
        WINDOW *pUpMenuWnd;
        MENU *pMenu;
        MENUItem* pSelItem;
        
        pMenu = Window->pMenu;
        pSelItem = pMenu->pOldSelItem;
        
        p.x = LOuint16_t (lParam);
        p.y = HIuint16_t (lParam);
        
        GetClientRect (Window, &WndRect);
        ClientToScreenRect (Window, &WndRect);
        if (FALSE == PtInRect (&WndRect, &p)) {
            pUpMenuWnd = MouseInUpMenu (pMenu, &p);
            if (NULL != pUpMenuWnd) {
                SendMessage (pUpMenuWnd, Message, 0x1, lParam);
                
                return 0;
            }
            
            pMenu = GetRootMenu (pMenu);
            HideMenu (pMenu);
            
            PostMouseMessage (Message, wParam, lParam);
            return 0;
        }
        
        if (EV_LBUTTONUP == Message && 
            NULL != pSelItem && 
            MENU_TYPE_SUBMENU != pSelItem->nType) {
            PostMessage (GetBind (Window), EV_COMMAND, pSelItem->id, 0);
            
            pMenu = GetRootMenu (pMenu);
            HideMenu (pMenu);
            return 0;
        }
        return 0;
        break;
    }
        
    case EV_MOUSEMOVE:
    {
        DC* hdc;
        int32_t nPos;
        int32_t i;
        POINT p;
        RECT WndRect;
        RECT TmpRect;
        WINDOW *pUpMenuWnd;
        MENU *pMenu;
        MENUItem* pNewSelItem = NULL;
        MENUItem* _list_;
        struct list_head *_head_;
        #undef _item_
        #define _item_ ChildListNode
        
        pMenu = Window->pMenu;
        _head_ = &pMenu->ChildListHead;
        
        p.x = LOuint16_t (lParam);
        p.y = HIuint16_t (lParam);
        
        DefWinProc(Window, Message, wParam, lParam);
        
        GetClientRect (Window, &WndRect);
        ClientToScreenRect (Window, &WndRect);
        if (FALSE == PtInRect (&WndRect, &p)) {
            pUpMenuWnd = MouseInUpMenu (pMenu, &p);
            if (NULL != pUpMenuWnd) {
                SendMessage (pUpMenuWnd, Message, 0x1, lParam);
                
                return 0;
            }
            
            return 0;
        }
        
        ScreenToWindow (Window, &p);
        nPos = p.x / pMenu->nItemWidth * pMenu-> nItemsPerCol + p.y / pMenu->nItemHeight;
        if (nPos >= pMenu->nChildCount) {
            return 0;
        }
        
        hdc = GetDC (Window);
        if (NULL == hdc) {
            break;
        }
        
        i = 0;
        for ((_list_) = list_entry((_head_)->next, MENUItem, _item_); 
            &((_list_)->_item_) != (_head_);
            (_list_) = list_entry((_list_)->_item_.next, MENUItem, _item_)) {
            if (pMenu->pOldSelItem == _list_) {
                if (i == nPos) {
                    pNewSelItem = _list_;
                    
                    ReleaseDC (Window, hdc);
                    return 0;
                }
                
                SetRect (&TmpRect, _list_->ItemRect.left + 1, _list_->ItemRect.top + 1, 
                    _list_->ItemRect.right - 1, _list_->ItemRect.bottom - 1);
                SetFgColor (hdc, RGB (255, 255, 255));
                FillRect (hdc, &TmpRect);
                
                // Draw item
                SetFgColor (hdc, RGB (0, 0, 0));
                DrawText (hdc, _list_->spName, strlen (_list_->spName), &TmpRect, DT_LEFT);
                
                // Draw triangle
                if (_list_->nType == MENU_TYPE_SUBMENU) {
                    DrawSubMenuTriangle (hdc, &TmpRect);
                }
            }
            
            if (i == nPos) {
                pNewSelItem = _list_;
                
                SetRect (&TmpRect, _list_->ItemRect.left + 1, _list_->ItemRect.top + 1, 
                    _list_->ItemRect.right - 1, _list_->ItemRect.bottom - 1);
                SetFgColor (hdc, RGB (49, 106, 197));
                FillRect (hdc, &TmpRect);
                
                // Draw item
                SetFgColor (hdc, RGB (0, 0, 0));
                DrawText (hdc, _list_->spName, strlen (_list_->spName), &TmpRect, DT_LEFT);
                
                // Draw triangle
                if (_list_->nType == MENU_TYPE_SUBMENU) {
                    DrawSubMenuTriangle (hdc, &TmpRect);
                }
            }
            i++;
        }
        
        if (NULL != pMenu->pOldSelItem && MENU_TYPE_SUBMENU == pMenu->pOldSelItem->nType) {
            HideMenu (pMenu->pOldSelItem->pSubMenu);
        }
        
        if (NULL != pNewSelItem && MENU_TYPE_SUBMENU == pNewSelItem->nType) {
            ShowMenu (pNewSelItem->pSubMenu, pMenu->BindWndPos.x + 
                pNewSelItem->ItemRect.left + pMenu->nItemWidth - 2, 
                pMenu->BindWndPos.y + pNewSelItem->ItemRect.top, Window->pBindWnd);
        }
        
        pMenu->pOldSelItem = pNewSelItem;
        
        ReleaseDC (Window, hdc);
        return 0;
        break;
    }
    
    case EV_KEYDOWN:
    {
        DC* hdc;
        MENU *pMenu;
        RECT TmpRect;
        MENUItem* pOldSelItem, *pNewSelItem = NULL;
        MENUItem* _list_;
        struct list_head *_head_;
        #undef _item_
        #define _item_ ChildListNode
        
        pMenu = Window->pMenu;
        _head_ = &pMenu->ChildListHead;
        
        switch (wParam) {
        case VK_UP:
            if (NULL == pMenu->pOldSelItem) {
                (pNewSelItem) = list_entry((_head_)->prev, MENUItem, _item_);
            }
            else {
                (pNewSelItem) = list_entry((pMenu->pOldSelItem->_item_).prev, MENUItem, _item_);
            }
            
            if (&((pNewSelItem)->_item_) == (_head_)) {
                pNewSelItem = list_entry((_head_)->prev, MENUItem, _item_);
            }
            break;
            
        case VK_DOWN:
            if (NULL == pMenu->pOldSelItem) {
                (pNewSelItem) = list_entry((_head_)->next, MENUItem, _item_);
            }
            else {
                (pNewSelItem) = list_entry((pMenu->pOldSelItem->_item_).next, MENUItem, _item_);
            }
            
            if (&((pNewSelItem)->_item_) == (_head_)) {
                pNewSelItem = list_entry((_head_)->next, MENUItem, _item_);
            }
            break;
            
        case VK_LEFT:
            if (NULL != pMenu->pUpMenu) {
                HideMenu (pMenu);
            }
            return 0;
            break;
            
        case VK_RIGHT:
            if (NULL != pMenu->pOldSelItem && MENU_TYPE_SUBMENU == pMenu->pOldSelItem->nType) {
                pOldSelItem = pMenu->pOldSelItem;
                _head_ = &pOldSelItem->pSubMenu->ChildListHead;
                pOldSelItem->pSubMenu->pOldSelItem = list_entry((_head_)->next, MENUItem, _item_);
                ShowMenu (pOldSelItem->pSubMenu, 
                    pMenu->BindWndPos.x + pOldSelItem->ItemRect.right - 2, 
                    pMenu->BindWndPos.y + pOldSelItem->ItemRect.top, 
                    Window->pBindWnd);
            }
            return 0;
            break;
            
        case VK_RETURN:
            if (NULL != pMenu->pOldSelItem) {
                if (MENU_TYPE_SUBMENU == pMenu->pOldSelItem->nType) {
                    pOldSelItem = pMenu->pOldSelItem;
                    _head_ = &pOldSelItem->pSubMenu->ChildListHead;
                    pOldSelItem->pSubMenu->pOldSelItem = list_entry((_head_)->next, MENUItem, _item_);
                    ShowMenu (pOldSelItem->pSubMenu, 
                        pMenu->BindWndPos.x + pOldSelItem->ItemRect.right - 2, 
                        pMenu->BindWndPos.y + pOldSelItem->ItemRect.top, 
                        Window->pBindWnd);
                }
                else {
                    PostMessage (GetBind (Window), EV_COMMAND, pMenu->pOldSelItem->id, 0);
                    
                    pMenu = GetRootMenu (pMenu);
                    HideMenu (pMenu);
                }
            }
            return 0;
            break;
            
        case VK_ESCAPE:
            HideMenu (pMenu);
            return 0;
            break;
            
        default:
            pNewSelItem = NULL;
            return 0;
            break;
        }
        
        if (NULL == pNewSelItem) {
            break;
        }
        
        hdc = GetDC (Window);
        if (NULL == hdc) {
            break;
        }
        
        for ((_list_) = list_entry((_head_)->next, MENUItem, _item_); 
            &((_list_)->_item_) != (_head_);
            (_list_) = list_entry((_list_)->_item_.next, MENUItem, _item_)) {
            if (pMenu->pOldSelItem == _list_) {
                if (pMenu->pOldSelItem == pNewSelItem) {
                    break;
                }
                
                SetRect (&TmpRect, _list_->ItemRect.left + 1, _list_->ItemRect.top + 1, 
                    _list_->ItemRect.right - 1, _list_->ItemRect.bottom - 1);
                SetFgColor (hdc, RGB (255, 255, 255));
                FillRect (hdc, &TmpRect);
                
                // Draw item
                SetFgColor (hdc, RGB (0, 0, 0));
                DrawText (hdc, _list_->spName, strlen (_list_->spName), &TmpRect, DT_LEFT);
                
                // Draw triangle
                if (_list_->nType == MENU_TYPE_SUBMENU) {
                    DrawSubMenuTriangle (hdc, &TmpRect);
                }
            }
            
            if (_list_ == pNewSelItem) {
                SetRect (&TmpRect, _list_->ItemRect.left + 1, _list_->ItemRect.top + 1, 
                    _list_->ItemRect.right - 1, _list_->ItemRect.bottom - 1);
                SetFgColor (hdc, RGB (49, 106, 197));
                FillRect (hdc, &TmpRect);
                
                // Draw item
                SetFgColor (hdc, RGB (0, 0, 0));
                DrawText (hdc, _list_->spName, strlen (_list_->spName), &TmpRect, DT_LEFT);
                
                // Draw triangle
                if (_list_->nType == MENU_TYPE_SUBMENU) {
                    DrawSubMenuTriangle (hdc, &TmpRect);
                }
            }
        }
        
        pMenu->pOldSelItem = pNewSelItem;
        
        ReleaseDC (Window, hdc);
        return 0;
        break;
    }
    
    case EV_HIDEMENU:
    {
        MENU *pMenu;
        
        pMenu = Window->pMenu;
        
        pMenu->pOldSelItem = NULL;
        
        DestroyWindow (Window);
        
        SetCapture (pMenu->pOldCaptureWnd);
        
        pMenu->pMenuWnd = NULL;
        return 0;
        break;
    }
    
    default:
        break;
    }
    
    return DefWinProc(Window, Message, wParam, lParam);
}

bool32_t RegisterMenuClass (void)
{
    WNDCLASS MenuClass;

    memset (&MenuClass, 0, sizeof (WNDCLASS));
    MenuClass.pClassName = "menu";
    MenuClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    MenuClass.WinProc    = DefMenuProc;
    
    return RegisterClass (&MenuClass);
}

static WINDOW *CreateMenuWindow(const int8_t* pCaption,uint32_t nStyle,int32_t X,int32_t Y,int32_t W,int32_t H,
    WINDOW *Parent,MENU *pMenu)
{
    WINDOW *pMenuWnd;
    
    pMenuWnd = CreateWindow ("menu", pCaption, WS_MENU  | WS_TOPMOST | nStyle, 
        X, Y, W, H, Parent, pMenu, NULL);
    
    return pMenuWnd;
}

MENU *CreateMenu(int8_t *spName)
{
    MENU *pMenu;
    
    pMenu = (MENU *)Malloc (sizeof (MENU));
    if (NULL == pMenu) {
        return NULL;
    }
    
    if (NULL != spName) {
        strcpy (pMenu->spName, spName);
    }
    else {
        strcpy (pMenu->spName, "");
    }
    
    pMenu->nItemWidth      = 0;
    pMenu->nItemHeight     = GetSystemMetrics (SM_CYMENU);
    
    pMenu->pMenuWnd        = NULL;
    pMenu->pOldCaptureWnd  = NULL;
    
    pMenu->pUpMenu         = NULL;
    
    pMenu->pOldSelItem     = NULL;
    pMenu->nChildCount     = 0;
    LIST_HEAD_RESET(&pMenu->ChildListHead);
    
    return pMenu;
}

MENU *CreateSubMenu(MENUItem *pItem)
{
    pItem->pSubMenu = CreateMenu (pItem->spName);
    if (NULL != pItem->pSubMenu) {
        pItem->nType = MENU_TYPE_SUBMENU;
        
        pItem->pSubMenu->pUpMenu = pItem->pParentMenu;
    }
    
    return pItem->pSubMenu;
}

bool32_t InsertMenuItem(MENU *pMenu, MENUItem *pItem)
{
    int32_t nWidth;
    MENUItem *pNewItem;
    
    pNewItem = (MENUItem *)Malloc (sizeof (MENUItem));
    if (NULL == pNewItem) {
        return FALSE;
    }
    
    memcpy (pNewItem, pItem, sizeof (MENUItem));
    
    pNewItem->nType = MENU_TYPE_ITEM;
    nWidth = (strlen (pItem->spName) + 1)* (FONT_POINT >> 1);
    if (pMenu->nItemWidth < nWidth) {
        pMenu->nItemWidth = nWidth;
    }
    
    list_add_tail (&pNewItem->ChildListNode, &pMenu->ChildListHead);
    
    pNewItem->pParentMenu = pMenu;
    pNewItem->pSubMenu    = NULL;
    
    pMenu->nChildCount++;
    
    return TRUE;
}

MENUItem *GetMenuItem(MENU *pMenu, int32_t id)
{
    MENUItem* _list_;
    struct list_head *_head_ = &pMenu->ChildListHead;
    #undef _item_
    #define _item_ ChildListNode
    
    for ((_list_) = list_entry((_head_)->next, MENUItem, _item_); 
        &((_list_)->_item_) != (_head_);
        (_list_) = list_entry((_list_)->_item_.next, MENUItem, _item_)) {
        if (_list_->id == id) {
            return _list_;
        }
    }
    
    return NULL;
}

void ShowMenu(MENU *pMenu, int32_t X, int32_t Y, WINDOW *pParent)
{
    RECT ParentRect;
    WINDOW *pMenuWnd;
    int32_t i, IX, IY, nCol, nRow, nLeft, nRight, nTop, nBottom;
    MENUItem* _list_;
    struct list_head *_head_ = &pMenu->ChildListHead;
    #undef _item_
    #define _item_ ChildListNode
    
    // 限制菜单项的宽度
    if (pMenu->nItemWidth < 120) {
        pMenu->nItemWidth = 120;
    }
    
    if (pMenu->nItemWidth > 240) {
        pMenu->nItemWidth = 240;
    }
    
    GetClientRect (pParent, &ParentRect);
    ClientToScreenRect (pParent, &ParentRect);
    nLeft = (ParentRect.left + X) / pMenu->nItemWidth;
    nRight = (GetSystemMetrics (SM_CXSCREEN) - (ParentRect.left + X)) / pMenu->nItemWidth;
    nTop = (ParentRect.top + Y) / pMenu->nItemHeight;
    nBottom = (GetSystemMetrics (SM_CYSCREEN) - (ParentRect.top  + Y)) / pMenu->nItemHeight;
    
    if (nBottom < pMenu->nChildCount && nTop < pMenu->nChildCount) {
        if (nBottom < nTop) {
            // UP
            nRow = nTop;
            Y -= nRow * pMenu->nItemHeight;
        }
        else {
            // DOWN
            nRow = nBottom;
        }
    }
    else {
        if (nBottom < pMenu->nChildCount) {
            // UP
            nRow = pMenu->nChildCount;
            Y -= nRow * pMenu->nItemHeight;
        }
        else {
            // DOWN
            nRow = pMenu->nChildCount;
        }
    }
    
    nCol = ((pMenu->nChildCount + nRow - 1) / nRow);
    if (nLeft < nCol && nRight < nCol) {
        if (nRight < nLeft) {
            // LEFT
            X -= nCol * pMenu->nItemWidth;
        }
        else {
            // RIGHT
        }
    }
    else {
        if (nRight < nCol) {
            // LEFT
            X -= nCol * pMenu->nItemWidth;
        }
        else {
            // RIGHT
        }
    }
    
    pMenu->nItemsPerCol = nRow;
    
    pMenu->BindWndPos.x = X;
    pMenu->BindWndPos.y = Y;
    
    i  = 0;
    IX = 0;
    IY = 0;
    for ((_list_) = list_entry((_head_)->next, MENUItem, _item_); 
        &((_list_)->_item_) != (_head_);
        (_list_) = list_entry((_list_)->_item_.next, MENUItem, _item_)) { 
        SetRect (&_list_->ItemRect, IX, IY, IX + pMenu->nItemWidth, IY + pMenu->nItemHeight);
        
        IY += pMenu->nItemHeight;
        if ((i % pMenu->nItemsPerCol) == (pMenu->nItemsPerCol - 1)) {
            IX = IX + pMenu->nItemWidth;
            IY = 0;
        }
        
        i++;
    }
    
    pMenuWnd = CreateMenuWindow (pMenu->spName, WS_VISIBLE, X, Y, 
        nCol * pMenu->nItemWidth, nRow * pMenu->nItemHeight, pParent, pMenu);
        
    ShowWindow (pMenuWnd, SW_SHOW);
    
    pMenu->pMenuWnd = pMenuWnd;
    pMenu->pOldCaptureWnd = SetCapture (pMenuWnd);
    
    pGUI->pActiveMenu = pMenu;
}

static void HideAllMenuItem(MENU *pMenu)
{
    MENUItem* _list_;
    struct list_head *_head_ = &pMenu->ChildListHead;
    #undef _item_
    #define _item_ ChildListNode
    
    for ((_list_) = list_entry((_head_)->next, MENUItem, _item_); 
        &((_list_)->_item_) != (_head_);
        (_list_) = list_entry((_list_)->_item_.next, MENUItem, _item_)) {
        if (MENU_TYPE_SUBMENU == _list_->nType) {
            HideMenu (_list_->pSubMenu);
        }
    }
}

void HideMenu(MENU *pMenu)
{
    if (NULL == pMenu) {
        return;
    }
    
    HideAllMenuItem (pMenu);
    
    if (NULL != pMenu->pMenuWnd) {
        SendMessage (pMenu->pMenuWnd, EV_HIDEMENU, 0, 0);
    }
    
    pGUI->pActiveMenu = pMenu->pUpMenu;
}

static void DestroyMenuItem(MENU *pMenu)
{
    MENUItem* _list_;
    struct list_head *_head_ = &pMenu->ChildListHead;
    #undef _item_
    #define _item_ ChildListNode
    
    for ((_list_) = list_entry((_head_)->next, MENUItem, _item_); 
        &((_list_)->_item_) != (_head_);
        (_list_) = list_entry((_list_)->_item_.next, MENUItem, _item_)) {
        DestroyMenu (_list_->pSubMenu);
        
        Free (_list_);
    }
}

bool32_t DestroyMenu(MENU *pMenu)
{
    if (NULL == pMenu) {
        return FALSE;
    }
    
    DestroyMenuItem (pMenu);
    
    if (NULL != pMenu->pMenuWnd) {
        DestroyWindow (pMenu->pMenuWnd);
    }
    
    Free (pMenu);
    
    return TRUE;
}

WINDOW *GetMenuActiveWindow(void)
{
    MENU *pMenu = pGUI->pActiveMenu;
    
    if (NULL == pMenu) {
        return NULL;
    }
    
    if (NULL == pMenu->pMenuWnd) {
        return NULL;
    }
    
    return pMenu->pMenuWnd;
}
