#ifndef __CURSOR_H__
#define __CURSOR_H__

#include    "window.h"

#ifdef SOFT_CURSOR
#ifndef     __SOFT_CURSOR_H__
#include    "softcursor.h"
#endif
#endif

#ifdef __cplusplus
extern "C"
{
#endif

struct _CURSOR
{
    int32_t     x;
    int32_t     y;
    int32_t     w;
    int32_t     h;
    void*   pAndMask;
    void*   pXorMask;
};

bool32_t  InitCursor(void *);
void ReleaseCursor(void);
CURSOR *CreateCursor(int32_t,int32_t,int32_t,int32_t,const void *,const void *);
CURSOR *LoadCursorFromFile(const int8_t *);
bool32_t  DestroyCursor(CURSOR *);
CURSOR *SetWindowCursor(WINDOW *, CURSOR *);
CURSOR *GetWindowCursor(WINDOW *);
CURSOR *SetCursor(CURSOR *);
CURSOR *GetCursor(void);
bool32_t  SetCursorPos(int32_t, int32_t);
bool32_t  GetCursorPos(LPPOINT);
int32_t  ShowCursor(bool32_t);

#ifndef SOFT_CURSOR
bool32_t  InitHWCursor(void);
void ReleaseHWCursor(void);
void SetHWCursor(CURSOR *);
void SetHWCursorPos(int32_t, int32_t, int32_t, int32_t);
void SetHWCursorEnable(bool32_t);
#endif

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
