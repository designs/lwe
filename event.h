#ifndef __EVENT_H__
#define __EVENT_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define     MAX_EVENT                   256

#define     EV_CREATE                   0x00000001
#define     EV_ERASEBKGND               0x00000002
#define     EV_PAINT                    0x00000003
#define     EV_CLOSE                    0x00000004
#define     EV_DESTROY                  0x00000005
#define     EV_KEYDOWN                  0x00000007
#define     EV_KEYUP                    0x00000008
#define     EV_CHAR                     0x00000009
#define     EV_LBUTTONDOWN              0x0000000A
#define     EV_LBUTTONUP                0x0000000B
#define     EV_RBUTTONDOWN              0x0000000C
#define     EV_RBUTTONUP                0x0000000D
#define     EV_MOUSEMOVE                0x0000000E
#define     EV_NCPAINT                  0x0000000F
#define     EV_TIMER                    0x00000010
#define     EV_COMMAND                  0x00000011
#define     EV_KILLFOCUS                0x00000012
#define     EV_SETFOCUS                 0x00000013
#define     EV_HSCROLL                  0x00000014
#define     EV_VSCROLL                  0x00000015
#define     EV_LBUTTONDBLCLK            0x00000016
#define     EV_HIDEMENU                 0x00000017
#define     EV_NCACTIVE                 0x00000018
#define     EV_MOVEING                  0x00000019
#define     EV_MOVE                     0x0000001A
#define     EV_SIZEING                  0x0000001B
#define     EV_SIZE                     0x0000001C
#define     EV_RESIZE                   0x0000001D
#define     EV_ENABLE                   0x0000001E
#define     EV_NEEDIME                  0x0000001F
#define     EV_QUIT                     0x00000020
#define     EV_CHOUSE                   0x00000021
#define     EV_UNCHOUSE                 0x00000022

#define     EV_FIRSTKEYMSG              EV_KEYDOWN
#define     EV_LASTKEYMSG               EV_CHAR

#define     EV_FIRSTMOUSEMSG            EV_LBUTTONDOWN
#define     EV_LASTMOUSEMSG             EV_MOUSEMOVE

#define     EV_SETTEXT                  0x00008000
#define     EV_GETTEXT                  0x00008001
#define     EV_SETTEXTLEN               0x00008002
#define     EV_GETTEXTLEN               0x00008003
#define     EV_SETTEXTPSW               0x00008004

#define     SBM_SETPOS                0x00009000
#define     SBM_GETPOS                0x00009001
#define     SBM_SETRANGE              0x00009002
#define     SBM_GETRANGE              0x00009003

#define     BM_GETCHECK               0x0000A000
#define     BM_SETCHECK               0x0000A001

#define     LB_ADDSTRING              0x0000B000
#define     LB_INSERTSTRING           0x0000B001
#define     LB_DELETESTRING           0x0000B002
#define     LB_SETCURSEL              0x0000B003
#define     LB_GETCURSEL              0x0000B004

#define     EV_USER                     0x80000000
#define     EV_UPDATEALLWINDOWS         (EV_USER + 1)

#define     PM_NOREMOVE                 0x00000000
#define     PM_REMOVE                   0x00000001

typedef struct _EventAddData
{
    SEMAPHOR            Semph_t;    // 消息处理完成信号
    int32_t                 nResult;        //结果 消息的返回结果
}EventAddData;

typedef struct _MSG
{
    struct _WINDOW*    Window;         // 消息发送窗口
    
    uint32_t                 Message;        // 消息类型
    uint32_t                 wParam;         // 消息参数0
    uint32_t                 lParam;         // 消息参数1
    
    EventAddData*      pAddData;       // 消息附加数据
    
    struct list_head    MsgListNode;  // 消息节点
}MSG;

typedef struct _Msg_Queue
{
    MUTEX            MutexObject;    // 消息池访问锁
    SEMAPHOR            Semph_t;    // 消息池有可用消息信号
    
    struct list_head    EmptyMsgQueueHead;  // 消息池可用消息队列
}Msg_Queue;

bool32_t  InitMessage(void);
bool32_t  PostMessage(WINDOW*,uint32_t,uint32_t,uint32_t);
int32_t  SendMessage(WINDOW*,uint32_t,uint32_t,uint32_t);
bool32_t  GetMessage(MSG *,WINDOW *);
bool32_t  PeekMessage(MSG *, WINDOW *, uint32_t, uint32_t, uint32_t);
bool32_t  TranslateMessage (MSG *);
int32_t  DispatchMessage(MSG *);
int32_t  ProcessMessage(WINDOW *, MSG *);
void DiscardMessage(MSG *);
void PostQuitMessage(int32_t);
void ReleaseMessage(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
