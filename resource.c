/*
  Copyright (C), 2008~2009, zhanbin
  File name:    resource.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090105
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090105    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

static BITMAP *SysBmp[BMP_MAX_ID];
static CURSOR *SysCursor[CURSOR_MAX_ID];

bool32_t InitSystemBitmap(void)
{
    SysBmp[BMP_GUI_LOGO_ID]  = LoadBmpFile (ROOT_PATH"/bmp/logo.bmp");
    SysBmp[BMP_CAPTION_ID]      = LoadBmpFile (ROOT_PATH"/bmp/caption.bmp");
    SysBmp[BMP_CLOSE_BOX_ID]    = LoadBmpFile (ROOT_PATH"/bmp/close.bmp");
    SysBmp[BMP_PUSH_BUTTON_ID]  = LoadBmpFile (ROOT_PATH"/bmp/button.bmp");
    SysBmp[BMP_CHECK_BUTTON_ID] = LoadBmpFile (ROOT_PATH"/bmp/check.bmp");
    SysBmp[BMP_SCROLL_BAR_ID]   = LoadBmpFile (ROOT_PATH"/bmp/sb.bmp");
    
    return TRUE;
}

void ReleaseSystemBitmap(void)
{
}

BITMAP *GetSystemBitmap(int32_t nID)
{
    if (nID < 0 || nID >= BMP_MAX_ID) {
        return NULL;
    }
    
    return SysBmp[nID];
}

bool32_t InitSystemCursor(void)
{
    SysCursor[CURSOR_ARROW_ID] = LoadCursorFromFile (ROOT_PATH"/cursor/arrow.cur");
    SysCursor[CURSOR_BEAM_ID]  = LoadCursorFromFile (ROOT_PATH"/cursor/beam.cur");
    
    return TRUE;
}

void ReleaseSystemCursor(void)
{
}

CURSOR *GetSystemCursor(int32_t nID)
{
    if (nID < 0 || nID >= CURSOR_MAX_ID) {
        return NULL;
    }
    
    return SysCursor[nID];
}
