#ifndef __DESKTOP_H__
#define __DESKTOP_H__

#ifdef      __cplusplus
extern "C" {
#endif

WINDOW *CreateDesktopWnd(void);

#ifdef      __cplusplus
}
#endif

#endif
