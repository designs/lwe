/*
  Copyright (C), 2008~2009, zhanbin
  File name:    caret.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090212
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090212    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "window.h"

static void DrawCaret(WINDOW *hWnd, bool32_t bBlink)
{
    DC *hdc;
    struct _CARET *pCaret;
    
    pCaret = hWnd->pCaret;
    
    hdc = GetDC (hWnd);
    if (NULL == hdc) {
        return;
    }
    
    if (bBlink) {
        SetFgColor (hdc, RGB (0, 0, 0));
    }
    else {
        SetFgColor (hdc, RGB (255, 255, 255));
    }
    
    MoveToEx (hdc, pCaret->x, pCaret->y, NULL);
    LineTo (hdc, pCaret->x, pCaret->y + pCaret->h);
    
    ReleaseDC (hWnd, hdc);
}

static void CaretTimerFunction(WINDOW *hWnd, uint32_t id, uint32_t nCurTime)
{
    struct _CARET *pCaret;
    
    pCaret = hWnd->pCaret;
    
    MutexLock (&pCaret->CaretMutex);
    
    pCaret->bBlink = !pCaret->bBlink;
    
    DrawCaret (hWnd, pCaret->bBlink);
    
    MutexUnlock (&pCaret->CaretMutex);
}

bool32_t CreateCaret(WINDOW *hWnd, BITMAP *hBitmap, int32_t nWidth, int32_t nHeight)
{
    struct _CARET *pCaret;
    
    if (NULL == hWnd->pCaret) {
        pCaret = Malloc (sizeof (struct _CARET));
        if (NULL == pCaret) {
            return FALSE;
        }
        
        pCaret->x           = 0;
        pCaret->y           = 0;
        pCaret->w           = nWidth;
        pCaret->h           = nHeight;
        pCaret->bShow       = FALSE;
        pCaret->bBlink      = FALSE;
        pCaret->nBlinkTime  = 50;
        
        if (FALSE == MutexCreate (&pCaret->CaretMutex)) {
            Free (pCaret);
            
            return FALSE;
        }
        
        hWnd->pCaret        = pCaret;
    }
    else {
        pCaret              = hWnd->pCaret;
        
        pCaret->w           = nWidth;
        pCaret->h           = nHeight;
    }
    
    return TRUE;
}

bool32_t DestroyCaret(WINDOW *hWnd)
{
    if (NULL == hWnd->pCaret) {
        return TRUE;
    }
    
    KillTimer (hWnd, CARET_TIMER_ID);
    
    ReleaseMutex (&hWnd->pCaret->CaretMutex);
    
    Free (hWnd->pCaret);
    hWnd->pCaret = NULL;
    
    return TRUE;
}

bool32_t ShowCaret(WINDOW *hWnd)
{
    struct _CARET *pCaret;
    
    pCaret = hWnd->pCaret;
    
    MutexLock (&pCaret->CaretMutex);
    
    KillTimer (hWnd, CARET_TIMER_ID);
    
    DrawCaret (hWnd, TRUE);
    
    pCaret->bShow = TRUE;
    pCaret->bBlink = TRUE;
    
    SetTimer (hWnd, CARET_TIMER_ID, pCaret->nBlinkTime, CaretTimerFunction);
    
    MutexUnlock (&pCaret->CaretMutex);
    
    return TRUE;
}

bool32_t HideCaret(WINDOW *hWnd)
{
    struct _CARET *pCaret;
    
    pCaret = hWnd->pCaret;
    
    MutexLock (&pCaret->CaretMutex);
    
    if (FALSE == pCaret->bShow) {
        MutexUnlock (&pCaret->CaretMutex);
        
        return FALSE;
    }
    
    KillTimer (hWnd, CARET_TIMER_ID);
    
    DrawCaret (hWnd, FALSE);
    
    pCaret->bShow = FALSE;
    pCaret->bBlink = FALSE;
    
    MutexUnlock (&pCaret->CaretMutex);
    
    return TRUE;
}

bool32_t SetCaretPos(WINDOW *hWnd, int32_t x, int32_t y)
{
    struct _CARET *pCaret;
    
    pCaret = hWnd->pCaret;
    
    MutexLock (&pCaret->CaretMutex);
    
    if (pCaret->bBlink) {
        KillTimer (hWnd, CARET_TIMER_ID);
        
        DrawCaret (hWnd, FALSE);
        
        pCaret->x   = x;
        pCaret->y   = y;
        
        DrawCaret (hWnd, TRUE);
        
        pCaret->bShow = TRUE;
        pCaret->bBlink = TRUE;
        
        SetTimer (hWnd, CARET_TIMER_ID, pCaret->nBlinkTime, CaretTimerFunction);
    }
    else {
        pCaret->x   = x;
        pCaret->y   = y;
        
        if (pCaret->bShow) {
            KillTimer (hWnd, CARET_TIMER_ID);
            
            DrawCaret (hWnd, TRUE);
            
            pCaret->bBlink = TRUE;
            
            SetTimer (hWnd, CARET_TIMER_ID, pCaret->nBlinkTime, CaretTimerFunction);
        }
    }
    
    MutexUnlock (&pCaret->CaretMutex);
    
    return TRUE;
}
