#ifndef __WIN_MISC_H__
#define __WIN_MISC_H__

#include "window.h"

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct _WNDCLASS
{
	int8_t*                 pClassName;         // 窗口类名

	struct _CURSOR*    pCursor;            // 窗口类光标

	WINPROC           WinProc;            // 类处理函数

	struct list_head    WCListNode;         // 窗口类节点
} WNDCLASS;

bool32_t  WindowIsVisible(WINDOW *);
bool32_t  WindowIsTrans(WINDOW *);
bool32_t  WindowIsTabStop(WINDOW *);
bool32_t  WindowHaveNC(WINDOW *);
bool32_t  WindowHaveCaption(WINDOW *);
bool32_t  WindowHaveBoard(WINDOW *);
bool32_t  WndIsMainWnd(WINDOW *);
bool32_t  WindowHaveHScrollBar(WINDOW *);
bool32_t  WindowHaveVScrollBar(WINDOW *);
bool32_t  WindowIsDesktop(WINDOW *);
bool32_t  WindowIsMainWnd(WINDOW *);
bool32_t  WindowIsControl(WINDOW *);
bool32_t  WindowIsDialog(WINDOW *);
bool32_t  WindowIsMenu(WINDOW *);
bool32_t  WindowIsIme(WINDOW *);
bool32_t  WindowIsTopmost(WINDOW *);
bool32_t  WindowNeedIme(WINDOW *);
bool32_t  IsWindowEnabled(WINDOW *);
bool32_t  IsWindowNcActive(WINDOW *);
bool32_t  WindowIsSysControl(WINDOW *);

bool32_t  GetWindowRect(WINDOW *, LPRECT);
bool32_t  GetClientRect(WINDOW *, LPRECT);
bool32_t  GetCloseBoxRect(WINDOW *, LPRECT);

void ClientToWindow(WINDOW *, LPPOINT);
void WindowToClient(WINDOW *, LPPOINT);
void WindowToScreen(WINDOW *, LPPOINT);
void ScreenToWindow(WINDOW *, LPPOINT);
void ClientToScreen(WINDOW *, LPPOINT);
void ScreenToClient(WINDOW *, LPPOINT);
void ChildToParent(WINDOW *, LPPOINT);
void ParentToChild(WINDOW *, LPPOINT);

void ClientToWindowRect(WINDOW *, LPRECT);
void WindowToClientRect(WINDOW *, LPRECT);
void WindowToScreenRect(WINDOW *, LPRECT);
void ScreenToWindowRect(WINDOW *, LPRECT);
void ClientToScreenRect(WINDOW *, LPRECT);
void ScreenToClientRect(WINDOW *, LPRECT);
void ChildToParentRect(WINDOW *, LPRECT);
void ParentToChildRect(WINDOW *, LPRECT);

bool32_t  GetWindowMenuRect(WINDOW *, LPRECT);

bool32_t  GetWindowScrollBarRect(WINDOW *, bool32_t, LPRECT);
bool32_t  SetScrollRange(WINDOW *, int32_t, int32_t, int32_t, bool32_t);
int32_t  SetScrollPos(WINDOW *, int32_t, int32_t);
int32_t  GetScrollPos(WINDOW *, int32_t);
bool32_t  EnableScrollBar (WINDOW *, int32_t, bool32_t);

int32_t  GetSystemMetrics(int32_t);
int32_t  GetDeviceCaps(int32_t);

WINDOW *GetRootWindow(WINDOW *);

uint32_t  GetWindowStyle (WINDOW *);
bool32_t  ExcludeWindowStyle (WINDOW *hWnd, uint32_t);
bool32_t  IncludeWindowStyle (WINDOW *hWnd, uint32_t);

const int8_t *GetWindowCaption (WINDOW *);

void NotifyParent(WINDOW *, int32_t, int32_t);

bool32_t RegisterClass(WNDCLASS *);
WNDCLASS *GetClassInfo(const int8_t*);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
