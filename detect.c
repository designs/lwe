/*
** $Id: detect.c,v 1.17 2003/08/15 08:45:46 weiym Exp $
**
** detect.c: The MineSweeper game.
**
** Copyright (C) 1999~ 2002 Zheng Xiang and others.
** Copyright (C) 2003 Feynman Software.
*/

/*
**  This source is free software; you can redistribute it and/or
**  modify it under the terms of the GNU General Public
**  License as published by the Free Software Foundation; either
**  version 2 of the License, or (at your option) any later version.
**
**  This software is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
**  General Public License for more details.
**
**  You should have received a copy of the GNU General Public
**  License along with this library; if not, write to the Free
**  Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
**  MA 02111-1307, USA
*/

/*
** Modify records:
**
**  Who             When        Where       For What                Status
**-----------------------------------------------------------------------------
**  zhanbin         2008/12/05              Adapted from MiniGUI    done
**-----------------------------------------------------------------------------
**
** TODO:
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <errno.h>
#include <dirent.h>
#include <string.h>

#include    <time.h>
#include    "window.h"
#include  "windef.h"
#define  BUTTON_UP		300
#define  BUTTON_DOWN	301  
#define  BUTTON_ENT		302
#define  BUTTON_RETURN	303
#define  BUTTON_LEFT	304
#define  BUTTON_RIGHT	305
#define  PROGRESSRECT_W	200
#define  PROGRESSRECT_H	16
#define BUTTON_START_YPOS 20
#define  EV_LINE		0x33
#define BUTTON_TOTAL 5
static WINDOW * hButton[BUTTON_TOTAL];
static WINDOW * FileListView;
float vv_output_0[1500];
float vv_output_1[1500];
float vv_output_2[1500];
float vv_output_3[1500];
unsigned int udelay = 4580;
#define CHANNEL 4
#define SIDE_COLUMN_WIDE	78
#define DEBUG 1
struct _SetChangInfo
{
	unsigned short	 sample_len;
	int 			zoom;
	unsigned char	 step_x;

};
typedef struct _SetChangInfo SETCHANGINFO;

struct _LineInfo
{
	float 			* line;
	SETCHANGINFO *thisChangs;
	uint32_t first_pos;
	uint32_t oneScrPoint;
	unsigned short	 BaseHorizontal;
};
typedef struct _LineInfo LINEINFO;
LINEINFO oneline[CHANNEL];
#if 0
void DrawMyBitmap(DC* hdc, BITMAP * bmp, int x, int y, int w, int h)
{
    RECT rect;

    if (bmp)
        DrawBitmap (hdc, x, y, bmp, 0, 0, w, h);
    else {
        SetRect (&rect, x, y, w, h);
        DrawText (hdc, "a", strlen ("a"), &rect, DT_CENTER);
    }
}
//loadBmpFile(rootPath/bmp/detect/filename)

#endif
void Delay(unsigned int Length)
{
	while(Length > 0)
		Length--;
}

void SetLineInfolen(SETCHANGINFO *Input,unsigned short	 sample_len)
{
	Input->sample_len=sample_len;
}

void SetLineInfozoom(SETCHANGINFO *Input,int 	zoom)
{
	Input->zoom=zoom;
}
void SetLineInfodeltaX(SETCHANGINFO *Input,unsigned char	 step_x)
{
	Input->step_x=step_x;
}

#define FirstBh    90//480*272,basehorizontal 100
#define Second	   180
void InitChangInfo(SETCHANGINFO *Input)
{
	SetLineInfodeltaX(Input, 1);
	SetLineInfolen(Input, 720);
	SetLineInfozoom(Input, 60);
}
#define MOVE_RIGHT 1
#define MOVE_LEFT 0
void SetLineScreenPos(WINDOW* hWnd,LINEINFO *oneline,unsigned short rightOrLeft)
{
	uint32_t temp_pos;
	if(rightOrLeft)//向右
	{
		temp_pos=oneline->first_pos+oneline->oneScrPoint;
		oneline->first_pos=(temp_pos <=oneline->thisChangs->sample_len ? temp_pos: oneline->first_pos);
		
	}
	else

	{
		temp_pos=oneline->first_pos-oneline->oneScrPoint;
		oneline->first_pos=(temp_pos <=0 ? 0 : oneline->first_pos);

	}
}

void CleanClient(WINDOW* hWnd)
{
	DC * hdc;	
	RECT ClientRect;
	hdc = GetDC(hWnd);
	SetRect(&ClientRect,0,GetSystemMetrics(SM_CYCAPTION),
				GetSystemMetrics(SM_CXSCREEN)-SIDE_COLUMN_WIDE,
				GetSystemMetrics(SM_CYSCREEN));
	fb_fill_rect(hdc->pPhyDC,&ClientRect,RGB(255,255,255));
	ReleaseDC(hWnd, hdc);
}

void showchar(WINDOW * hWnd,int32_t x, int32_t y, const int8_t * lpString, int32_t nCount)
{
    DC *hdc;
	hdc = GetDC(hWnd);
	TextOutLen(hdc,x,y,lpString, nCount);
	ReleaseDC(hWnd, hdc);
}

void save_files(WINDOW * hWnd)
{
	short i;
	FILE *fd_file;
	char pathname[255];
	char string[13];
	time_t timep;
	struct tm *p;
	time(&timep);//获取系统时间
	p=gmtime(&timep);//参数timep转成time_t结构信息
	sprintf(pathname, "/mnt/%d%02d%02d%02d%02d%02d.csv",(1900+p->tm_year),(1+p->tm_mon),p->tm_mday,p->tm_hour, p->tm_min, p->tm_sec);
	printf("files name is %s\n",pathname);
	if (-1 == access("/proc/scsi/usb-storage", F_OK))
	{
		showchar(hWnd,50,50,"无法打开U盘 ",strlen("无法打开U盘 "));
	}
	else
	{

		showchar(hWnd,50,50,"存储U盘中..", strlen("存储U盘中.."));
		fd_file = fopen(pathname, "w");
		if(fd_file == NULL)
		showchar(hWnd,50,70,"创建文件错误 ", strlen("创建文件错误 "));
		else
			for(i = 0; i < 720; i++)
			{
				sprintf(string, "%f,%f\r\n", vv_output_0[i], vv_output_1[i]);
				fputs(string, fd_file);
			}
		fclose(fd_file);
		showchar(hWnd,50,70,"已经保存... ",strlen("已经保存... "));	
	}
}


void DrawOneLine(WINDOW* hWnd,LINEINFO * oneline,COLORREF color)
{
	DC * hdc;
	float y_0,y_1;
	unsigned short m,n;
	unsigned short x=0;
	n=oneline->first_pos+oneline->oneScrPoint;
	hdc = GetDC(hWnd);	
	for(m=oneline->first_pos;m<n;m++)
	{
			y_0=-oneline->line[m]/oneline->thisChangs->zoom+oneline->BaseHorizontal;///3000;
			y_0=MAX(y_0,GetSystemMetrics(SM_CYCAPTION));//y限制在屏幕范围内
			y_0=MIN(y_0,GetSystemMetrics(SM_CYSCREEN));//y限制在屏幕范围内
			y_1=-oneline->line[m+1]/oneline->thisChangs->zoom+oneline->BaseHorizontal;///3000;
			y_1=MAX(y_1,GetSystemMetrics(SM_CYCAPTION));//y限制在屏幕范围内
			y_1=MIN(y_1,GetSystemMetrics(SM_CYSCREEN));//y限制在屏幕范围内
			fb_line(hdc->pPhyDC, x, y_0, x+oneline->thisChangs->step_x, y_1,color);
			x+=oneline->thisChangs->step_x;
	}
	ReleaseDC(hWnd, hdc);
}
#if 0
static BITMAP *LoadDetectBitmap (const char* filename)
{
	char full_path [MAX_PATH + 1];

	strcpy (full_path, ROOT_PATH"/bmp/detect/");
    strcat (full_path, filename);

    return LoadBmpFile (full_path);
}
#endif

bool32_t list_files(WINDOW * ListView)
{
	DIR *dp;
	int namelen;
	struct dirent *ptr;
	int32_t i = 0;
	LVITEM item;
	LVSUBITEM subdata;
	if ((dp = opendir("/mnt")) == NULL)
	{
	return 0;
	}
	chdir("/mnt");//进入目录
	seekdir(dp, 0);
	while ((ptr = readdir(dp)) != NULL) 
	{	
			if(strcmp(ptr->d_name, ".") == 0 || strcmp(ptr->d_name, "..") == 0) ///current dir OR parrent dir
				continue;
			else if(ptr->d_type == 8)
			{
				namelen = strlen(ptr->d_name);//取得文件名长度
				if(ptr->d_name[namelen - 4] == '.'//判断最后4位是不是“.csv”
						&& ( ptr->d_name[namelen - 3] == 'c' || ptr->d_name[namelen - 3] == 'C' )
						&& ( ptr->d_name[namelen - 2] == 's' || ptr->d_name[namelen - 2] == 'S' )
						&& ( ptr->d_name[namelen - 1] == 'v' || ptr->d_name[namelen - 1] == 'V' )) ///jpgfile
				{


	    item.nItem = ++i;
        item.itemData = telldir(dp);//telldir()函数的返回值记录着一个目录流的当前位置;
        SendMessage (ListView, LVM_ADDITEM, 0, (uint32_t) & item);

		ptr->d_name[namelen - 4] = '\0';
        subdata.nItem = i;
        subdata.subItem = 1;
        subdata.pszText = ptr->d_name;
        subdata.flags = LVFLAG_ICON;
        subdata.image = 0;
        subdata.nTextColor = RGB (0, 0, 0);
        SendMessage (ListView, LVM_FILLSUBITEM, 0, (uint32_t) & subdata);

				}
			}

		}
	chdir("..");
	closedir(dp);
	return 1;
}


bool32_t ReadFileFromTell(PLVSUBITEM fileInDirPos)
{
		FILE *fd;//文件描述符	
		char *line,*record;  
		char buffer[1024];
		float readdate[5];
		unsigned short j = 0;	
		unsigned short i=0;
		char pathname[255];

		sprintf(pathname, "./mnt/%s.csv",fileInDirPos->pszText);
		
		if ((fd = fopen(pathname, "rb")) == NULL) {  
			printf("open file error\n");  
			return 0;  
		}

		//从文件结构体指针stream中读取数据，每次读取一行。当读取NULL(文件末尾)时循环结束  
		while ((line = fgets(buffer, sizeof(buffer), fd))!=NULL)
		{  
			record = strtok(line, ",");  
			while (record != NULL)//读取每一行的数据  
			{
				readdate[j++]=atoi(record);
				record = strtok(NULL, ",");
			}
			j=0;
			vv_output_0[i]=readdate[0];
			vv_output_1[i]=readdate[1];
	//		printf("%f:%f \n",vv_output_0[i],vv_output_1[i]);
			i++;
		}
		fclose(fd);
		fd = NULL;
	return 1;
}


int AD_DA(short *buf, float *in0, float *in1,unsigned short get_total, unsigned short availability_number, unsigned char flag)
{

	unsigned short adc_point, k, re_detector;
	short out_val, zero_val, in_val[4];
	int fd_adc, fd_dac;
	zero_val = 0x800;
	for(adc_point = 0; adc_point < get_total; adc_point++)
	{
		in0[adc_point] = (float)0.0;
		in1[adc_point] = (float)0.0;

	}
	if(flag) re_detector = 24;
	else re_detector = 4;
	
	fd_adc = open("/dev/adc_dev", O_RDWR);
	if(fd_adc < 0)
	{
		printf("can't open adc!\n");
		return 0;
	}
	fd_dac = open("/dev/dac_dev", O_RDWR);
	if(fd_dac < 0)
	{
		printf("can't open dac!\n");
		return 0;
	}

#ifdef DEBUG
	printf("2d\n");
#endif
	for(k = 0; k < re_detector; k++)
	{
		for(adc_point = 0; adc_point < get_total; adc_point++)
		{
			if(flag)//flag=1 pulse
			{
				if(adc_point < availability_number)
				{
					out_val = buf[adc_point] * 5 + 0x800;
					write(fd_dac, &out_val, sizeof(out_val));
				}
				else
				{
					write(fd_dac, &zero_val, sizeof(zero_val));
				}
			}
			else
			{
				out_val = buf[adc_point % 24] * 5 + 0x800;
				write(fd_dac, &out_val, sizeof(out_val));
			}
			Delay(180);//ADC时间
			read(fd_adc, (char *)&in_val, sizeof(short) * 2);
			in0[adc_point] += in_val[0] / re_detector;
			in1[adc_point] += in_val[1] / re_detector;
			//			usleep(100);
			//			microseconds_sleep(2);
			Delay(udelay);
		}
		usleep(79000);
	}
#ifdef DEBUG
		printf("2e\n");
#endif
	close(fd_dac);
	close(fd_adc);
return 0;
}


void EnterTest(char taskId)
{
short cos_pulse[24] = {0, 26, 50, 71, 87, 97,
					   100, 97, 87, 71, 50, 26,
					   0, 0, 0, 0, 0, 0,
					   0, 0, 0, 0, 0, 0
					  };
short cos_2pulse[24] = {0, 26, 50, 71, 87, 97,
						100, 97, 87, 71, 50, 26,
						0, 26, 50, 71, 87, 97,
						100, 97, 87, 71, 50, 26
					   };
short sin_pulse[24] = {0, 26, 50, 71, 87, 97, 100, 97, 87, 71, 50, 26, 0, -26, -50, -71, -87, -97, -100, -97, -87, -71, -50, -26};
unsigned short get_total = 720; ///接收端Input接收的点数
unsigned short availability_number = 24; ///实际发送信号的点数

switch (taskId)
	{
		case 0://hButton1;单脉冲
		//打开adda

#ifdef DEBUG
		printf("2c\n");
#endif
		AD_DA(cos_pulse, vv_output_0, vv_output_1, get_total, availability_number, 1);	
		//发送信号
		//存储数据
		break;
		case 1://hButton2;多脉冲
		break;
		default:
		break;

	}


}


void doChangLightButton(char old_pos,char current_pos)
{

	SendMessage (hButton[old_pos],EV_UNCHOUSE,0,0);
	SendMessage (hButton[current_pos],EV_CHOUSE,0,0);

}
static int32_t BackGroundDialogProc(WINDOW *pDlg,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
	RECT ParentRect;
	int32_t W,H;
	LVCOLUMN s1;
    switch(Message){
    case EV_CREATE:
	GetWindowRect (pDlg, &ParentRect);
		W = ParentRect.right  - ParentRect.left;
		H = ParentRect.bottom - ParentRect.top;
	FileListView = CreateWindow ("listview", "", WS_CONTROL | WS_VISIBLE | WS_TABSTOP| WS_HSCROLL | WS_VSCROLL,
		0, 0,//起点
		W, H,//长宽
		pDlg, (MENU *)1001, NULL);
	    s1.nCols = 1;
        s1.pszHeadText = "文件名";
        s1.width = W-10;
        s1.pfnCompare = NULL;
        s1.colFlags = 0;
        SendMessage (FileListView, LVM_ADDCOLUMN, 0, (uint32_t) & s1);
		list_files(FileListView);
        break;
	case EV_KEYUP:
		SendMessage (FileListView, EV_KEYDOWN, wParam, lParam);
		break;
	case EV_GETTEXT:
	{	PLVSUBITEM p1=(PLVSUBITEM)Malloc(sizeof(struct _LVSUBITEM));
		char pathname[255];
		p1->nItem=(int32_t)wParam;
		p1->subItem=1;
		p1->pszText=pathname;
		SendMessage(FileListView, LVM_GETSUBITEMTEXT, wParam, (uint32_t)p1);
		ReadFileFromTell(p1);
		Free(p1);
		EndDialog (pDlg, -1);
		return 0;
	}
    case EV_CLOSE:
        EndDialog (pDlg, -1);
        return 0;   
    default:
        break;
    }
    
    return DefDialogProc (pDlg, Message, wParam, lParam);
}

static int32_t DetectWndProc(WINDOW* hWnd,uint32_t message,uint32_t wParam,uint32_t lParam)
{

	int32_t W,H;
    DC *hdc;
    short i;
	char button_current_pos;
	static char ChosedButtonId=0;
    RECT rcClient;
	SETCHANGINFO *Input;
    switch (message) {
    case EV_CREATE:
    {
	i=BUTTON_START_YPOS;
	unsigned char j=0;
	hButton[j++]=CreateWindow("bmpbutton","单脉冲",WS_CONTROL | WS_VISIBLE | WS_TABSTOP,
            GetSystemMetrics(SM_CXSCREEN)-SIDE_COLUMN_WIDE,i,
            75,21,
            hWnd,(MENU*)1001,GetSystemBitmap(BMP_PUSH_BUTTON_ID));

	hButton[j++]=CreateWindow("bmpbutton","多脉冲",WS_CONTROL | WS_VISIBLE | WS_TABSTOP,
				GetSystemMetrics(SM_CXSCREEN)-SIDE_COLUMN_WIDE,i+=30,
				75,21,
				hWnd,(MENU*)1002,GetSystemBitmap(BMP_PUSH_BUTTON_ID));

	hButton[j++]=CreateWindow("bmpbutton","存储数据",WS_CONTROL | WS_VISIBLE | WS_TABSTOP,
				GetSystemMetrics(SM_CXSCREEN)-SIDE_COLUMN_WIDE,i+=30,
				75,21,
				hWnd,(MENU*)1003,GetSystemBitmap(BMP_PUSH_BUTTON_ID));

	hButton[j++]=CreateWindow("bmpbutton","读取存储",WS_CONTROL | WS_VISIBLE | WS_TABSTOP,
				GetSystemMetrics(SM_CXSCREEN)-SIDE_COLUMN_WIDE,i+=30,
				75,21,
				hWnd,(MENU*)1004,GetSystemBitmap(BMP_PUSH_BUTTON_ID));

	hButton[j]=CreateWindow("bmpbutton","显示曲线",WS_CONTROL | WS_VISIBLE | WS_TABSTOP,
				GetSystemMetrics(SM_CXSCREEN)-SIDE_COLUMN_WIDE,i+=30,
				75,21,
				hWnd,(MENU*)1005,GetSystemBitmap(BMP_PUSH_BUTTON_ID));
//频率选择
//时间确定
		doChangLightButton(ChosedButtonId,ChosedButtonId);
		InvalidateRect (hWnd, NULL, TRUE);
        break;
    }
    case EV_TIMER:
        break;

    case EV_PAINT:
        hdc = BeginPaint (hWnd, wParam, lParam);
	    GetClientRect (hWnd, &rcClient);
	    SetRect(&rcClient,rcClient.left,rcClient.top,rcClient.right-SIDE_COLUMN_WIDE,rcClient.bottom);
	    SetBkColor (hdc, RGB (255, 255, 255));
	    SetFgColor (hdc, RGB (255, 255, 255));
	    FillRect(hdc, &rcClient);
        EndPaint (hWnd, hdc, wParam, lParam);
        return 0;
        break;
	case EV_COMMAND:
		break;
	case EV_LINE:
		GetClientRect (hWnd, &rcClient);
		CleanClient(hWnd);
		Input=(SETCHANGINFO *)Malloc(sizeof(SETCHANGINFO));
		InitChangInfo(Input);
		oneline[0].thisChangs=Input;
		oneline[0].BaseHorizontal=FirstBh;
		oneline[0].first_pos=0;			
		oneline[0].oneScrPoint=(rcClient.right-rcClient.left-SIDE_COLUMN_WIDE)/Input->step_x;
		oneline[0].line=vv_output_0;
		DrawOneLine(hWnd,oneline,RGB (255, 255, 0));
		
		
		oneline[1].thisChangs=Input;
		oneline[1].BaseHorizontal=Second;	
		oneline[1].first_pos=0;
		oneline[1].oneScrPoint=(rcClient.right-rcClient.left-SIDE_COLUMN_WIDE)/Input->step_x;
		oneline[1].line=vv_output_1;
		DrawOneLine(hWnd,oneline+1,RGB (255, 0, 255));
		break;
    case EV_KEYUP:
	    switch (LOuint16_t(wParam)) {
        case '0'://up 48
           button_current_pos=ChosedButtonId;
			if(0==button_current_pos)
			{
				button_current_pos=BUTTON_TOTAL-1;
				}
			else
			{	--button_current_pos;}
			doChangLightButton(ChosedButtonId,button_current_pos);
			ChosedButtonId=button_current_pos;
		break;

        case '1'://down  49
			button_current_pos=ChosedButtonId;
			if((++button_current_pos)>=BUTTON_TOTAL)
			{
				button_current_pos=0;
				}
			doChangLightButton(ChosedButtonId,button_current_pos);
            ChosedButtonId=button_current_pos;
            break;
        case '2'://enter
#if 0
//			 MessageBox (hWnd, "后台测试中...", "About", MB_OK);
#ifdef DRAWLINE
			hdc = BeginPaint (hWnd, wParam, lParam);

			fb_line(hdc->pPhyDC, 15,15, 15, 100,RGB (0, 255, 255));
			EndPaint (hWnd, hdc, wParam, lParam);
#endif
#define DRAWTXT 1
#ifdef DRAWTXT

			hdc = GetDC(hWnd);
			SetRect(&lpRect,0, 0, 75, 21);
			SetFgColor (hdc, RGB(0, 255, 0));
			DrawText(hdc, "a", strlen ("a"),&lpRect,DT_CENTER);
			ReleaseDC(hWnd, hdc);
#endif
#endif
		
		switch (ChosedButtonId)
			{
			case 0://按钮1、2为测试
			case 1:
			showchar(hWnd,50, 50,"后台测试中...请不要按键操作",  strlen ("后台测试中...请不要按键操作"));
			EnterTest(ChosedButtonId);
			SendMessage(hWnd,EV_LINE,0,0);
			break;
			case 2://hButton3;存储数据
			save_files(hWnd);
			break;
			case 3://hButton4;读取存储
			GetClientRect (hWnd, &rcClient);
			W=200;
			H=rcClient.bottom-rcClient.top-40;
			CreateDialog("读完按[显示曲线]显示",  WS_CAPTION | WS_BORDER, 
		         W, H, hWnd, &BackGroundDialogProc);
			break;
			case 4:
			SendMessage(hWnd, EV_LINE,0,0);
				break;
			default:
			break;	

		}

            break;

        case '3'://return
            printf("3\n");
            break;

        case '4'://left
        	SetLineScreenPos(hWnd,oneline,MOVE_LEFT);
			CleanClient(hWnd);
        	DrawOneLine(hWnd,oneline,RGB (255, 255, 0));
			DrawOneLine(hWnd,oneline+1,RGB (255, 0,255));
            printf("4\n");
            break;

        case '5'://right
        	SetLineScreenPos(hWnd,oneline,MOVE_RIGHT);
			CleanClient(hWnd);
        	DrawOneLine(hWnd,oneline,RGB (255, 255, 0));
			DrawOneLine(hWnd,oneline+1,RGB (255, 0, 255));
            printf("5\n");
            break;
        default:
            break;
    }/*
*/
        break;
    case EV_DESTROY:
		Free(Input);
        break;

    default:
        break;
    }

    return DefWinProc(hWnd, message, wParam, lParam);
}

bool32_t RegisterDetectWndClass (void)
{
    WNDCLASS DetectWndClass;

    memset (&DetectWndClass, 0, sizeof (WNDCLASS));
    DetectWndClass.pClassName = "DetectWnd";
    DetectWndClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    DetectWndClass.WinProc    = DetectWndProc;

    return RegisterClass (&DetectWndClass);
}

static MENU *TestWndCreateMainMenu(void)
{
    MENUItem Item, *pItem;
    MENU *pMenu, *pPlayMenu;

    pMenu = CreateMenu ("主菜单");

    Item.id = 2000;
    strcpy (Item.spName, "探测");
    InsertMenuItem (pMenu, &Item);//插入菜单项

    Item.id = 2001;
    strcpy (Item.spName, "级别");
    InsertMenuItem (pMenu, &Item);

    Item.id = 2002;
    strcpy (Item.spName, "关于");
    InsertMenuItem (pMenu, &Item);



    pItem = GetMenuItem (pMenu, 2000);
    pPlayMenu = CreateSubMenu (pItem);

    return pMenu;
}

WINDOW *CreateDetectWnd(void)
{
    MENU *pMainMenu;
    WINDOW *hDetectWnd;
    RegisterDetectWndClass ();

    pMainMenu = TestWndCreateMainMenu ();

    hDetectWnd = CreateWindow ("DetectWnd", "探测", WS_MAINWND | WS_CAPTION | WS_BORDER,
        0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), NULL,/*pMainMenu*/NULL, NULL);
    if (NULL == hDetectWnd) {
        return NULL;
    }

    ShowWindow (hDetectWnd, SW_SHOW);

    return hDetectWnd;
}
