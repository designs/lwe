#ifndef __CARET_H__
#define __CARET_H__

#ifdef __cplusplus
extern "C"
{
#endif

struct _CARET
{
    struct _WINDOW*    Window;
    
    uint32_t                 x;
    uint32_t                 y;
    uint32_t                 w;
    uint32_t                 h;
    bool32_t                 bShow;
    bool32_t                 bBlink;
    uint32_t                 nBlinkTime;
    
    MUTEX            CaretMutex;
};

bool32_t CreateCaret(WINDOW *hWnd, BITMAP *hBitmap, int32_t nWidth, int32_t nHeight);
bool32_t DestroyCaret(WINDOW *hWnd);
bool32_t ShowCaret(WINDOW *hWnd);
bool32_t HideCaret(WINDOW *hWnd);
bool32_t SetCaretPos(WINDOW *hWnd, int32_t x, int32_t y);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
