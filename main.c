/*
  Copyright (C), 2008~2009, zhanbin
  File name:    main.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090107
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090107    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifdef ECOS
#include <cyg/kernel/kapi.h>
#include <cyg/infra/cyg_type.h>
#include <cyg/infra/diag.h>
#include <cyg/io/io.h>
#include <cyg/io/devtab.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <sys/select.h>
#include <dirent.h>

#include "window.h"

#define STACKSIZE (65536)
#define NUM(x)      (sizeof(x)/sizeof(x[0]))

void startup_thread(CYG_ADDRESS);
void winsys_thread(CYG_ADDRESS);

typedef void    fun(CYG_ADDRWORD);
typedef struct _eCos_thread {
    char         *name;
    fun          *entry;
    int          prio;
    cyg_handle_t t;
    cyg_thread   t_obj;
    char         stack[STACKSIZE];
} eCos_thread;

eCos_thread _thread[] = {
    {"startup thread ", startup_thread, 11},        
    {"winsys thread", winsys_thread, 11},     
};

////////////////////////////////////////Thread opr function ///////////////////////////////////////
void startup_thread(CYG_ADDRESS data)
{
    int i;
    eCos_thread *nx;
    
    nx = &_thread[1];
    for (i = 1;  i < NUM(_thread);  i++, nx++) {
        cyg_thread_create(nx->prio,
                    nx->entry,
                    (cyg_addrword_t)NULL,
                    nx->name,
                    (void *)nx->stack, STACKSIZE,
                    &nx->t,
                    &nx->t_obj);
        
        cyg_thread_resume(nx->t);    
    }
}

void cyg_user_start(void)
{
    eCos_thread *nx;
    
    diag_printf("SYSTEM START.\n");
    
    nx = &_thread[0];
    cyg_thread_create(nx->prio,
                nx->entry,
                (cyg_addrword_t)NULL,
                nx->name,
                (void *)nx->stack, STACKSIZE,
                &nx->t,
                &nx->t_obj);
    cyg_thread_resume(nx->t);
    
    diag_printf("SYSTEM THREADS STARTED!\n");
}

void winsys_thread(CYG_ADDRESS data)
{
    int err;
    
    diag_printf("Begin window system.\n");
    
    err = mount ("/dev/hda/1", "/", "fatfs");
    if (err < 0) {
        diag_printf ("Mount fs failed!\n");
        return;
    }
    
    GuiInit (NULL);
    
    umount ("/");
    
    diag_printf("End window system.\n");
}
#else
#include <stdio.h>
#include <stdlib.h>

#include "window.h"

int main(void)
{
    return GuiInit (NULL);
}
#endif

