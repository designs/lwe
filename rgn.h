#ifndef __RGN_H__
#define __RGN_H__

#ifdef __cplusplus
extern "C"
{
#endif

/* dynamically allocated multi-rectangle clipping region*/
typedef struct {
	int32_t	        size;		/* malloc'd # of rectangles 矩形的数*/
	int32_t	        numRects;	/* # rectangles in use 使用的矩形数*/
	int32_t	        type; 		/* region type 区域类型*/
	RECT        *rects;		/* rectangle array 矩形数组*/
	RECT	    extents;	/* bounding box of region 区域的边界框*/
} CLIPREGION;

/* region types */
#define REGION_ERROR		0
#define REGION_NULL		    1
#define REGION_SIMPLE		2
#define REGION_COMPLEX	    3

/* RectInRegion return codes*/
#define RECT_OUT	        0	/* rectangle not in region*/
#define RECT_ALLIN	        1	/* rectangle all in region*/
#define RECT_PARTIN	        2	/* rectangle partly in region*/

#define DPRINTF             GUIDEBUG

bool32_t  PtInRegion(CLIPREGION *rgn, int32_t x, int32_t y);
int32_t  RectInRegion(CLIPREGION *rgn, const RECT *rect);
bool32_t  EqualRegion(CLIPREGION *r1, CLIPREGION *r2);
bool32_t  EmptyRegion(CLIPREGION *rgn);
bool32_t  EmptyClipRgn (CLIPREGION *rgn);
CLIPREGION *MallocRegion(void);
CLIPREGION *AllocRectRegion(int32_t left,int32_t top,int32_t right,int32_t bottom);
CLIPREGION *AllocRectRegionIndirect(RECT *prc);
void SetRectRegion(CLIPREGION *rgn, int32_t left, int32_t top,
    int32_t right, int32_t bottom);
void SetRectRegionIndirect(CLIPREGION *rgn, RECT *prc);
void FreeClipRegion(CLIPREGION *rgn);
void OffsetRegion(CLIPREGION *rgn, int32_t x, int32_t y);
int32_t  GetRegionBox(CLIPREGION *rgn, RECT *prc);
void UnionRectWithRegion(const RECT *rect, CLIPREGION *rgn);
void SubtractRectFromRegion(const RECT *rect, CLIPREGION *rgn);
void CopyRegion(CLIPREGION *d, CLIPREGION *s);
void IntersectRegion(CLIPREGION *d, CLIPREGION *s1, CLIPREGION *s2);
void UnionRegion(CLIPREGION *d, CLIPREGION *s1, CLIPREGION *s2);
void SubtractRegion(CLIPREGION *d, CLIPREGION *s1, CLIPREGION *s2);
void XorRegion(CLIPREGION *d, CLIPREGION *s1, CLIPREGION *s2);
#if 0
CLIPREGION *AllocBitmapRegion(MWIMAGEBITS *bitmap, int32_t width, int32_t height);
#endif
#if 1
void DumpRegion(CLIPREGION *pReg);
#endif

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
