#ifndef     __WINDOW_H__
#define     __WINDOW_H__

struct _PHYDC;
typedef struct _PHYDC      PHYDC;
struct _DC;
typedef struct _DC         DC;
struct _GUI;
typedef struct _GUI     GUI;
struct _TIMER;
typedef struct _TIMER    TIMER;
struct  _TIMER_T;
typedef struct _TIMER_T      TIMER_T;
struct  _WINDOW;
typedef struct _WINDOW     WINDOW;
struct _IME;
typedef struct _IME        IME;
struct  _MENU;
typedef struct _MENU       MENU;
struct _CURSOR;
typedef struct _CURSOR     CURSOR;
struct _CARET;
typedef struct _CARET      CARET;

#ifndef     __WINDEF_H
#include    "windef.h"
#endif

#ifndef     __OS_H__
#include    "os.h"
#endif

#ifndef     __LIST_H__
#include    "list.h"
#endif

#ifndef     __EVENT_H__
#include    "event.h"
#endif

#ifndef     __RGN_H__
#include    "rgn.h"
#endif

#ifndef     __PHY_DC_H__
#include    "phydc.h"
#endif

#ifndef     __TIMER_H__
#include    "timer.h"
#endif

#ifdef      __cplusplus
extern "C" {
#endif

#define SW_HIDE         0x00000000
#define SW_SHOW         0x00000001
#define SW_SHOWNA       0x00000002

#define HORZRES         0
#define VERTRES         1
#define BITSPIXEL       2

#define SM_CXSCREEN     0
#define SM_CYSCREEN     1
#define SM_CXVSCROLL    2
#define SM_CYHSCROLL    3
#define SM_CYCAPTION    4
#define SM_CXBORDER     5
#define SM_CYBORDER     6
#define SM_CYMENU       7
#define SM_CXCLOSEBOX   8
#define SM_CYCLOSEBOX   9

#define WS_TYPE_MASK  0x0000000F
#define WS_DESKTOP    0x00000001
#define WS_MAINWND    0x00000002
#define WS_DIALOG     0x00000003
#define WS_MENU       0x00000004
#define WS_CONTROL    0x00000005
#define WS_IME        0x00000006

#define WS_CAPTION    0x00000010
#define WS_BORDER     0x00000020
#define WS_CLOSEBOX   0x00000040
#define WS_VISIBLE    0x00000080
#define WS_TABSTOP    0x00000100
#define WS_TRANS      0x00000200
#define WS_TOPMOST    0x00000400
#define WS_VSCROLL    0x00000800
#define WS_HSCROLL    0x00001000
#define WS_FOCUS      0x00002000
#define WS_SYSCONTROL 0x00004000
#define WS_DISABLED   0x00008000

/*
 * User Button Notification Codes
 */
#define BN_CLICKED      0x0000
#define BN_DISABLE      0x0004
#define BN_SETFOCUS     0x0006
#define BN_KILLFOCUS    0x0007

/*
 * Edit Control Notification Codes
 */
#define EN_SETFOCUS     0x0100
#define EN_KILLFOCUS    0x0200
#define EN_CHANGE       0x0300
#define EN_UPDATE       0x0400
#define EN_ERRSPACE     0x0500
#define EN_MAXTEXT      0x0501

/*
 * Listbox Notification Codes
 */
#define LBN_ERRSPACE    (-2)
#define LBN_SELCHANGE   1
#define LBN_DBLCLK      2
#define LBN_SELCANCEL   3
#define LBN_SETFOCUS    4
#define LBN_KILLFOCUS   5
#define LBN_ENTER       6

#define TYPE_WINDOW_DC  0x0
#define TYPE_CLIENT_DC  0x1

typedef int32_t             (*WINPROC)(WINDOW *,uint32_t,uint32_t,uint32_t);

struct _DC
{
    int32_t                 nType;              // DC类型

    struct _WINDOW*    pWnd;               // 所属窗口

    struct _PHYDC*     pPhyDC;             // 物理设备DC

    COLORREF            fgPixel;            // 前景色
    COLORREF            bkPixel;            // 背景色
    int32_t                 bkMode;             // 背景模式
    int32_t                 lineWidth;          // 线宽
    int32_t                 lineStyle;          // 线型
    int32_t                 fillStyle;          // 填充模式

    POINT               CurPos;             // 当前位置

    CLIPREGION*         ClipRegion;         // 可见区域
};

struct _WINDOW
{
    uint32_t                 nStyle;             // 窗口属性 桌面、主窗口、对话、菜单、控件、输入法

    int8_t *                pCaption;           // 窗口名

    bool32_t                 bNcActive;
    uint32_t                 NcMouseFlag;        // 鼠标状态
    POINT               MovePos;            // 移动窗口时鼠标点击位置
    POINT               ParentPos;          // 窗口左上角坐标（相对于父窗口坐标）
    RECT                WindowRect;         // 窗口区（屏幕坐标）

    CLIPREGION*         ClipRegion;         // 剪切区域，称之为GC
    CLIPREGION*         InvalidRegion;      // 无效区域，称之为IC
//窗口内容
    struct _WINDOW*    pParentWnd;         // 父窗口，若为对话框/菜单，则是桌面窗口
    struct _WINDOW*    pBindWnd;           // 对话框/菜单邦定窗口,对话框/菜单的父窗口
    struct _CURSOR*    pCursor;            // 窗口光标
    struct _CARET*     pCaret;

    struct _MENU*      pMenu;          // 一般窗口指向窗口菜单，菜单窗口指向菜单结构，控件存放控件ID
    struct _WINDOW*    pHSrollBar;         // 水平滚动条
    struct _WINDOW*    pVSrollBar;         // 垂直滚动条
    struct _WINDOW*    pFocusWnd;          // 拥有输入焦点的子窗口
    struct _WINDOW*    pActiveChild;       // 活动子窗口
//窗口串
    struct list_head    ChildListHead;      // 子窗口列表
    struct list_head    ChildListNode;      // 父窗口节点
    struct list_head    FocusListNode;      // 输入焦点节点
    struct list_head    TraceListNode0;     // 回朔窗口节点0
    struct list_head    TraceListNode1;     // 回朔窗口节点1
    struct list_head    TraceListNode2;     // 回朔窗口节点2
    struct list_head    TimerListHead;      // 定时器列表
//消息
    struct _MSG*     EraseBackGndMsg;        // 重画背景区消息
    struct _MSG*     PaintMsg;         // 重画客户区消息
    struct _MSG*     NcPaintMsg;       // 重画标题栏消息

    uint32_t                 WndThread;          // 窗口所在线程

    WINPROC           WinProc;            // 窗口处理

    void*               pPrivData;          // 窗口私有数据
};

struct _GUI//界面
{
    struct _Msg_Queue* pMsgQueue;         // 消息池

    MUTEX            MutexObject;        // 消息队列访问锁
    SEMAPHOR            Semph_t;        // 消息队列有新消息信号

    struct list_head    MsgQueueHead;      // 消息队列
    struct _MSG*     TimerTail;          // 定时器队列尾(定时器是实时消息，要先于其它消息处理，暂未实现)

    struct _PHYDC      PhyDC;              // 物理设备DC
    CLIPREGION*         ClipRegion;         // 全局剪切区域
//界面窗口内容
    struct _WINDOW*    pDesktopWnd;        // 桌面窗口
    struct _WINDOW*    pActiveWnd;         // 当前活动窗口(主窗口)
    struct _WINDOW*    pFocusWnd;          // 当前输入焦点窗口(任意窗口)
    struct _WINDOW*    pCaptureWnd;        // 当前输入捕获窗口(任意窗口)

    int32_t                 bShowCursor;        // 是否显示光标
    POINT               CursorPos;          // 光标坐标
    struct _CURSOR*    pSysCursor;         // 系统光标

    TIMER*           pSysTimer;          // 系统定时器

    struct _IME*       pSysIme;            // 系统输入法

    struct _MENU*      pActiveMenu;        // 当前活动菜单

    struct list_head    WCListHead;         // 窗口类列表
    struct list_head    FocusWndHead;       // 拥有输入焦点窗口列表
};

// Init
int32_t  GuiInit(void*);

// Window
int32_t DefWinProc(WINDOW *,uint32_t,uint32_t,uint32_t);
WINDOW* CreateWindow(const int8_t *,const int8_t *,uint32_t,int32_t,int32_t,int32_t,int32_t,WINDOW *,MENU *,void *);
bool32_t  DestroyWindow(WINDOW *);
void ShowWindow(WINDOW *,uint32_t);
bool32_t  EnableWindow(WINDOW *, bool32_t);
void UpdateWindow (WINDOW *, bool32_t);
bool32_t  InvalidateRect(WINDOW *, LPRECT, bool32_t);
bool32_t  MoveWindow(WINDOW *, int32_t, int32_t, int32_t, int32_t, bool32_t);
DC *GetWindowDC(WINDOW *);
DC *GetDC(WINDOW *);
void ReleaseDC(WINDOW *, DC *);
DC *BeginErase(WINDOW *, uint32_t, uint32_t);
void EndErase(WINDOW *, DC *, uint32_t, uint32_t);
DC *BeginPaint(WINDOW *, uint32_t, uint32_t);
void EndPaint(WINDOW *, DC *, uint32_t, uint32_t);
WINDOW *GetParent(WINDOW *);
WINDOW *GetBind(WINDOW *);
WINDOW *GetDlgItem (WINDOW *, int32_t);
int32_t GetDlgCtrlID(WINDOW *);
WINDOW *SetCapture(WINDOW *);
WINDOW *GetCapture(void);
void ReleaseCapture(void);
WINDOW *SetFocus(WINDOW *);

// User
int32_t WinMain(void);

// Misc
WINDOW *GetDesktopWindow(void);
void DumpAllWindow(void);
WINDOW *FindWindowByPos(LPPOINT);
void UpdateAllWindow(void);

// GUI全局信息
extern GUI*         pGUI;

#ifdef      __cplusplus
}
#endif

#ifndef     __WIN_MISC_H__
#include    "winmisc.h"
#endif

#ifndef     __BMP_H__
#include    "bmp.h"
#endif

#ifndef     __ICON_H__
#include    "icon.h"
#endif

#ifndef     __GDI_H__
#include    "gdi.h"
#endif

#ifndef     __DRAW_3D_H__
#include    "draw3d.h"
#endif

#ifndef     __FB_H__
#include    "fb.h"
#endif

#ifndef     __FONT_H__
#include    "font.h"
#endif

#ifndef     __RESOURCE_H__
#include    "resource.h"
#endif

#ifndef     __RECT_H__
#include    "rect.h"
#endif

#ifndef     __CLIP_H__
#include    "clip.h"
#endif

#ifndef     __TIMER_H__
#include    "timer.h"
#endif

#ifndef     __CURSOR_H__
#include    "cursor.h"
#endif

#ifndef     __CARET_H__
#include    "caret.h"
#endif

#ifndef     __MENU_H__
#include    "menu.h"
#endif

#ifndef     __IME_H__
#include    "ime.h"
#endif

#ifndef     __PINYIN_H__
#include    "pinyin.h"
#endif

#ifndef     __MOUSE_H__
#include    "mouse.h"
#endif

#ifndef     __KEYBOARD_H__
#include    "keyboard.h"
#endif

#ifndef     __DESKTOP_H__
#include    "desktop.h"
#endif

#ifndef     __DIALOG_H__
#include    "dialog.h"
#endif

#ifndef     __STATIC_H__
#include    "static.h"
#endif

#ifndef     __CLOCK_H__
#include    "clock.h"
#endif

#ifndef     __BMP_BUTTON_H__
#include    "bmpbutton.h"
#endif

#ifndef     __CHECK_BUTTON_H__
#include    "checkbutton.h"
#endif

#ifndef     __EDIT_H__
#include    "edit.h"
#endif

#ifndef     __BMP_SCROLL_BAR_H__
#include    "bmpscrollbar.h"
#endif

#ifndef     __LIST_BOX_H__
#include    "listbox.h"
#endif

#ifndef     __MAIN_MENU_H__
#include    "mainmenu.h"
#endif

#ifndef     __MEDIT_H__
#include    "medit.h"
#endif

#ifndef     __PROGRESSBAR_H__
#include    "progressbar.h"
#endif

#ifndef     __LISTVIEW_H__
#include    "mgext.h"
#include    "listview.h"
#endif

#endif
