#ifndef __PINYIN_H__
#define __PINYIN_H__

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct _TPinyinIdx
{
    int8_t              py[8];
    int32_t             pos;
    int32_t             len;
}TPinyinIdx;

typedef struct _TPinyinData
{
    BITMAP*         pBitmap;
    int32_t             nMaxIdx;
    TPinyinIdx*     pPinyinIdx;
    int32_t             nHanziLen;
    int8_t*             pHanziBuf;
}TPinyinData;

#define IME_STATE_ENGLISH   0x1
#define IME_STATE_PINYIN    0x2
#define IME_STATE_SBC       0x4
#define IME_STATE_DBC       0x8

bool32_t  InitPinyin(IME *pIme);
void ReleasePinyin(IME *pIme);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
