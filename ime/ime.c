/*
  Copyright (C), 2008, zhanbin
  File name:    ime.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081226
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081226    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "window.h"

static TSbc2Dbc Sbc2DbcTable[]={
    {'0',"��"},
    {'1',"��"},
    {'2',"��"},
    {'3',"��"},
    {'4',"��"},
    {'5',"��"},
    {'6',"��"},
    {'7',"��"},
    {'8',"��"},
    {'9',"��"},
    {'A',"��"},
    {'B',"��"},
    {'C',"��"},
    {'D',"��"},
    {'E',"��"},
    {'F',"��"},
    {'G',"��"},
    {'H',"��"},
    {'I',"��"},
    {'J',"��"},
    {'K',"��"},
    {'L',"��"},
    {'M',"��"},
    {'N',"��"},
    {'O',"��"},
    {'P',"��"},
    {'Q',"��"},
    {'R',"��"},
    {'S',"��"},
    {'T',"��"},
    {'U',"��"},
    {'V',"��"},
    {'W',"��"},
    {'X',"��"},
    {'Y',"��"},
    {'Z',"��"},
    {'!',"��"},
    {'@',"��"},
    {'#',"��"},
    {'$',"��"},
    {'%',"��"},
    {'^',"����"},
    {'&',"��"},
    {'*',"��"},
    {'(',"��"},
    {')',"��"},
    {'-',"��"},
    {'_',"����"},
    {'+',"��"},
    {'=',"��"},
    {'[',"��"},
    {']',"��"},
    {'{',"��"},
    {'}',"��"},
    {'\\',"��"},
    {'|',"��"},
    {',',"��"},
    {'<',"��"},
    {'.',"��"},
    {'>',"��"},
    {'/',"��"},
    {'?',"��"},
    {'`',"��"},
    {'~',"��"},
    {':',"��"},
    {';',"��"}
};

static int8_t* SbcToDbc(int8_t ch)
{
    int32_t i;
    
    for (i = 0; i < sizeof (Sbc2DbcTable) / sizeof (Sbc2DbcTable[0]); i++) {
        if (Sbc2DbcTable[i].sbc == ch)
            break;
    }
    
    if (i == sizeof (Sbc2DbcTable) / sizeof (Sbc2DbcTable[0])) {
        return NULL;
    }
    
    return Sbc2DbcTable[i].dbc;
}

bool32_t InitIme(void)
{
    IME *pIme;
    
    pIme = Malloc (sizeof (IME));
    if (NULL == pIme) {
        return FALSE;
    }
    
    pIme->Sbc2Dbc   = SbcToDbc;
    pIme->pBindWnd  = NULL;
    pIme->pSelItem  = NULL;
    pIme->nImeCount = 0;
    LIST_HEAD_RESET(&pIme->ImeListHead);
    
    if (FALSE == InitPinyin (pIme)) {
        Free (pIme);
        return FALSE;
    }
    
    pGUI->pSysIme = pIme;
    
    return TRUE;
}


void ReleaseIme(void)
{
}

void ShowIme(WINDOW *pBindWnd, bool32_t bShow)
{
    IME *pIme = pGUI->pSysIme;
    IMEItem *pSelItem = pIme->pSelItem;
    
    if (NULL != pSelItem) {
        if (TRUE == bShow) {
            ShowWindow (pSelItem->pImeWnd, SW_SHOW);
            pIme->pBindWnd = pBindWnd;
        }
        else {
            ShowWindow (pSelItem->pImeWnd, SW_HIDE);
            pIme->pBindWnd = NULL;
        }
    }
}

void SelIme(int32_t nIndex)
{
    int32_t i;
    IME *pIme = pGUI->pSysIme;
    IMEItem *pOldSelItem, *pNewSelItem = NULL;
    IMEItem *_list_;
    struct list_head *_head_ = &pIme->ImeListHead;
    #undef _item_
    #define _item_ ImeListNode
    
    pOldSelItem = pIme->pSelItem;
    
    i = 0;
    nIndex = nIndex % pIme->nImeCount;
    for ((_list_) = list_entry((_head_)->next, IMEItem, _item_); 
        &((_list_)->_item_) != (_head_);
        (_list_) = list_entry((_list_)->_item_.next, IMEItem, _item_)) {
        if (i == nIndex) {
            pNewSelItem = _list_;
            break;
        }
    }
    
    if (NULL != pOldSelItem) {
        if (pNewSelItem == pOldSelItem) {
            return;
        }
        
        pOldSelItem->Release (pOldSelItem);
    }
    
    if (NULL == pNewSelItem) {
        return;
    }
    
    pNewSelItem->Init (pNewSelItem);
    
    pIme->pSelItem = pNewSelItem;
}

bool32_t  IsImeActive(void)
{
    IME *pIme;
    IMEItem *pSelItem;
    
    pIme = pGUI->pSysIme;
    if (NULL == pIme) {
        return FALSE;
    }
    
    pSelItem = pIme->pSelItem;
    if (NULL == pSelItem) {
        return FALSE;
    }
    
    return TRUE;
}

WINDOW *GetImeActiveWindow(void)
{
    IME *pIme;
    IMEItem *pSelItem;
    
    pIme = pGUI->pSysIme;
    if (NULL == pIme) {
        return NULL;
    }
    
    pSelItem = pIme->pSelItem;
    if (NULL == pSelItem) {
        return NULL;
    }
    
    if (FALSE == WindowIsVisible (pSelItem->pImeWnd)) {
        return NULL;
    }
    
    return pSelItem->pImeWnd;
}

WINDOW *GetImeBindWindow(void)
{
    IME *pIme;
    
    pIme = pGUI->pSysIme;
    if (NULL == pIme) {
        return NULL;
    }
    
    return pIme->pBindWnd;
}
