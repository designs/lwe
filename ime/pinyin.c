/*
  Copyright (C), 2008, zhanbin
  File name:    pinyin.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.2
  Date:         20081226
  Description:
  �򵥵�ƴ�����뷨��ʾ����
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081226    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "window.h"

static int8_t *PinyinFind(IMEItem *pImeItem, int32_t *pLen)
{
    int32_t i;
    int8_t *pFind;
    TPinyinData *pAddData;
    
    if (strlen (pImeItem->InputBuf) == 0) {
        *pLen = 0;
        return NULL;
    }
    
    pAddData = (TPinyinData *)pImeItem->pAddData;
    
    for (i = 0; i < pAddData->nMaxIdx; i++) {
        if (strncmp (pImeItem->InputBuf, pAddData->pPinyinIdx[i].py, strlen (pImeItem->InputBuf)) == 0) {
            *pLen = pAddData->pPinyinIdx[i].len;
            pFind = pAddData->pHanziBuf + pAddData->pPinyinIdx[i].pos;
            
            return pFind;
        }
    }
    
    *pLen = 0;
    return NULL;
}

static void PinyinCalCurPage(IMEItem *pImeItem)
{
    pImeItem->nPageBegin = pImeItem->nCurPage * pImeItem->nCharPerPage * 2;
    if (pImeItem->nCurPage == pImeItem->nTotalPage - 1) {
        pImeItem->nPageSize = pImeItem->nFindLen % pImeItem->nCharPerPage;
        if (pImeItem->nPageSize == 0) {
            pImeItem->nPageSize = pImeItem->nCharPerPage;
        }
    }
    else {
        pImeItem->nPageSize = pImeItem->nCharPerPage;
    }
}

static char *PinyinGetCurPageHZ(IMEItem *pImeItem, int8_t nIndex)
{
    return &pImeItem->pFindBuf[pImeItem->nPageBegin + (nIndex - 1) * 2];
}

static void PinyinReset(IMEItem *pImeItem)
{
    pImeItem->nInputLen    = 0;
    pImeItem->InputBuf[pImeItem->nInputLen] = '\0';
    pImeItem->nFindLen     = 0;
    pImeItem->pFindBuf     = NULL;
    pImeItem->nCharPerPage = 5;
    pImeItem->nTotalPage   = 0;
    pImeItem->nCurPage     = 0;
    pImeItem->nPageBegin   = 0;
    pImeItem->nPageSize    = 0;
}

static void PinyinChangeLang(IMEItem *pImeItem)
{
    if (pImeItem->nState & IME_STATE_ENGLISH) {
        pImeItem->nState &= ~IME_STATE_ENGLISH;
        pImeItem->nState |= IME_STATE_PINYIN;
    }
    else {
        pImeItem->nState &= ~IME_STATE_PINYIN;
        pImeItem->nState |= IME_STATE_ENGLISH;
    }
}

static void PinyinChangeSbc(IMEItem *pImeItem)
{
    if (pImeItem->nState & IME_STATE_SBC) {
        pImeItem->nState &= ~IME_STATE_SBC;
        pImeItem->nState |= IME_STATE_DBC;
    }
    else {
        pImeItem->nState &= ~IME_STATE_DBC;
        pImeItem->nState |= IME_STATE_SBC;
    }
}

static bool32_t IsPinyinInEng(IMEItem *pImeItem)
{
    return (IME_STATE_ENGLISH == (pImeItem->nState & IME_STATE_ENGLISH));
}

static bool32_t IsPinyinInSbc(IMEItem *pImeItem)
{
    return (IME_STATE_SBC == (pImeItem->nState & IME_STATE_SBC));
}

static void SendAscToBindWindow(IMEItem *pImeItem,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    int8_t *pDbc;
    IME *pIme;
    
    if (IsPinyinInSbc (pImeItem)) {
        SendMessage (GetImeBindWindow (), Message, wParam, lParam);
    }
    else {
        pIme = pImeItem->pIme;
        
        pDbc = pIme->Sbc2Dbc ((int8_t)LOuint16_t (wParam));
        if (NULL != pDbc) {
            SendMessage (GetImeBindWindow (), Message, MAKEuint16_t(pDbc[0], pDbc[1]), lParam);
        }
        else {
            SendMessage (GetImeBindWindow (), Message, wParam, lParam);
        }
    }
}

static void PinyinPaint(WINDOW *Window, DC* hdc)
{
    int32_t i, X, Y;
    BITMAP *pPinyinBmp;
    TPinyinData *pAddData;
    RECT WndRect, TmpRect;
    int8_t MsgBuf[16], HZBuf[3];
    IMEItem *pImeItem = (IMEItem *)Window->pPrivData;
    
    GetClientRect (Window, &WndRect);
    
    pAddData = (TPinyinData *)pImeItem->pAddData;
    
    // ״̬��Ϣ
    pPinyinBmp = (BITMAP *)pAddData->pBitmap;
    if (pImeItem->nState & IME_STATE_ENGLISH) {
        DrawBitmap (hdc, 0, 0, pPinyinBmp, 0, 0, 20, 20);
    }
    else {
        DrawBitmap (hdc, 0, 0, pPinyinBmp, 20, 0, 20, 20);
    }
    
    if (pImeItem->nState & IME_STATE_SBC) {
        DrawBitmap (hdc, 20, 0, pPinyinBmp, 40, 0, 20, 20);
    }
    else {
        DrawBitmap (hdc, 20, 0, pPinyinBmp, 60, 0, 20, 20);
    }
    
    // �����ַ�
    sprintf (MsgBuf, "%s", pImeItem->InputBuf);
    SetRect (&TmpRect, 40, WndRect.top, WndRect.right, 20);
    SetFgColor (hdc, RGB (255, 0, 0));
    DrawText (hdc, MsgBuf, strlen (MsgBuf), &TmpRect, DT_LEFT);
    
    // ��ҳ��Ϣ
    if (0 == pImeItem->nTotalPage) {
        sprintf (MsgBuf, "%d/%dҳ", pImeItem->nCurPage, pImeItem->nTotalPage);
    }
    else {
        sprintf (MsgBuf, "%d/%dҳ", pImeItem->nCurPage + 1, pImeItem->nTotalPage);
    }
    SetFgColor (hdc, RGB (0, 0, 0));
    SetRect (&TmpRect, WndRect.left, WndRect.top, WndRect.right, 20);
    DrawText (hdc, MsgBuf, strlen (MsgBuf), &TmpRect, DT_RIGHT);
    
    // ����ַ�
    if (0 != pImeItem->nTotalPage) {
        PinyinCalCurPage (pImeItem);
        
        X = 1;
        Y = 20;
        for (i = 0; i < pImeItem->nPageSize; i++) {
            memcpy (HZBuf, &pImeItem->pFindBuf[pImeItem->nPageBegin + i * 2], 2);
            HZBuf[2] = '\0';
            sprintf (MsgBuf, "%d%s", i + 1, HZBuf);
            SetFgColor (hdc, RGB (255, 0, 0));
            SetRect (&TmpRect, X, Y, WndRect.right, WndRect.bottom);
            DrawText (hdc, MsgBuf, strlen (MsgBuf), &TmpRect, DT_LEFT);
            X += 30;
        }
    }
}

static int32_t PinyinKeyDown(WINDOW *Window,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    int32_t nLen;
    int8_t *pFind;
    IMEItem *pImeItem = (IMEItem *)Window->pPrivData;
    
    switch (LOuint16_t (wParam)) {
    case VK_HOME:
        if (0 == pImeItem->nInputLen) {
            SendMessage (GetImeBindWindow (), Message, wParam, lParam);
        }
        else {
            pImeItem->nCurPage = 0;
            
            PinyinCalCurPage (pImeItem);
            InvalidateRect(Window, NULL, TRUE);
        }
        break;
        
    case VK_END:
        if (0 == pImeItem->nInputLen) {
            SendMessage (GetImeBindWindow (), Message, wParam, lParam);
        }
        else {
            pImeItem->nCurPage = pImeItem->nTotalPage - 1;
            
            PinyinCalCurPage (pImeItem);
            InvalidateRect(Window, NULL, TRUE);
        }
        break;
        
    case VK_LEFT:
    case VK_UP:
    case VK_PRIOR:  // PAGE UP
        if (0 == pImeItem->nInputLen) {
            SendMessage (GetImeBindWindow (), Message, wParam, lParam);
        }
        else {
            pImeItem->nCurPage--;
            if (pImeItem->nCurPage < 0) {
                pImeItem->nCurPage = pImeItem->nTotalPage - 1;
            }
            
            PinyinCalCurPage (pImeItem);
            InvalidateRect(Window, NULL, TRUE);
        }
        break;
        
    case VK_RIGHT:
    case VK_DOWN:
    case VK_NEXT:   // PAGE DOWN
        if (0 == pImeItem->nInputLen) {
            SendMessage (GetImeBindWindow (), Message, wParam, lParam);
        }
        else {
            pImeItem->nCurPage++;
            pImeItem->nCurPage %= pImeItem->nTotalPage;
            
            PinyinCalCurPage (pImeItem);
            InvalidateRect(Window, NULL, TRUE);
        }
        break;
        
    case VK_BACK:
        if (0 == pImeItem->nInputLen) {
            SendMessage (GetImeBindWindow (), Message, wParam, lParam);
            return 0;
        }
        
        pImeItem->InputBuf[pImeItem->nInputLen - 1] = '\0';
        pImeItem->nInputLen--;
        
        pFind = PinyinFind (pImeItem, &nLen);
        if (NULL != pFind) {
            pImeItem->pFindBuf = pFind;
            pImeItem->nFindLen = nLen;
            pImeItem->nFindLen /= 2;
            pImeItem->nTotalPage = (pImeItem->nFindLen + pImeItem->nCharPerPage - 1) 
                / pImeItem->nCharPerPage;
            pImeItem->nCurPage   = 0;
            
            InvalidateRect(Window, NULL, TRUE);
        }
        else {
            pImeItem->nFindLen   = 0;
            pImeItem->nTotalPage = 0;
            pImeItem->nCurPage   = 0;
            
            InvalidateRect(Window, NULL, TRUE);
        }
        break;
        
    default:
        SendMessage (GetImeBindWindow (), Message, wParam, lParam);
        break;
    }
    
    return 0;
}

static int32_t PinyinChar(WINDOW *Window,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    int32_t nLen;
    int8_t ch, *pFind;
    IMEItem *pImeItem = (IMEItem *)Window->pPrivData;
    
    ch = (int8_t)LOuint16_t (wParam);
    if (isdigit (ch)) {             // ���ּ�
        if (0 == pImeItem->nInputLen) {
            SendAscToBindWindow (pImeItem, Message, wParam, lParam);
        }
        else {
            ch -= '0';
            
            if (ch > 0 || ch <= pImeItem->nPageSize) {
                pFind = PinyinGetCurPageHZ (pImeItem, ch);
                
                SendMessage (GetImeBindWindow (), Message, MAKEuint16_t(pFind[0], pFind[1]), lParam);
                
                PinyinReset (pImeItem);
                
                InvalidateRect(Window, NULL, TRUE);
            }
        }
    }
    else if (islower (ch)) {        // Сд��ĸ��
        if (IsPinyinInEng (pImeItem)) {
            SendAscToBindWindow (pImeItem, Message, wParam, lParam);
            return 0;
        }
        
        pImeItem->InputBuf[pImeItem->nInputLen] = ch;
        pImeItem->nInputLen++;
        pImeItem->InputBuf[pImeItem->nInputLen] = '\0';
        
        pFind = PinyinFind (pImeItem, &nLen);
        if (NULL != pFind) {
            pImeItem->pFindBuf = pFind;
            pImeItem->nFindLen = nLen;
            pImeItem->nFindLen /= 2;
            pImeItem->nTotalPage = (pImeItem->nFindLen + pImeItem->nCharPerPage - 1) 
                / pImeItem->nCharPerPage;
            pImeItem->nCurPage   = 0;
            
            InvalidateRect(Window, NULL, TRUE);
        }
        else {
            pImeItem->nInputLen--;
            pImeItem->InputBuf[pImeItem->nInputLen] = '\0';
        }
    }
    else if (isupper (ch)) {        // ��д��ĸ��
        if (0 == pImeItem->nInputLen) {
            SendAscToBindWindow (pImeItem, Message, wParam, lParam);
        }
        else {
        }
    }
    else if (ch == VK_SPACE) {      // �ո��
        if (0 == pImeItem->nInputLen) {
            SendAscToBindWindow (pImeItem, Message, wParam, lParam);
        }
        else {
            SendMessage (Window, Message, MAKEuint32_t ('1', 0), lParam);
        }
    }
    else if (ch == VK_RETURN) {     // �س���
        if (0 == pImeItem->nInputLen) {
            SendAscToBindWindow (pImeItem, Message, wParam, lParam);
        }
        else {
            SendMessage (Window, Message, MAKEuint32_t ('1', 0), lParam);
        }
    }
    else {                          // ������
        if (0 == pImeItem->nInputLen) {
            SendAscToBindWindow (pImeItem, Message, wParam, lParam);
        }
        else {
        }
    }
    
    return 0;
}

static int32_t DefPinyinProc(WINDOW *Window,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    switch(Message){
    case EV_KEYDOWN:
    {
        return PinyinKeyDown (Window, Message, wParam, lParam);
        break;
    }
        
    case EV_CHAR:
    {
        return PinyinChar (Window, Message, wParam, lParam);
        break;
    }
        
    case EV_LBUTTONDOWN:
    {
        POINT pt;
        RECT WndRect;
        IMEItem *pImeItem = (IMEItem *)Window->pPrivData;
        
        pt.x = LOuint16_t (lParam);
        pt.y = HIuint16_t (lParam);
        ScreenToWindow (Window, &pt);
        
        SetRect (&WndRect, 0, 0, 20, 20);
        if (PtInRect (&WndRect, &pt)) {
            PinyinChangeLang (pImeItem);
            
            PinyinReset (pImeItem);
            
            InvalidateRect(Window, NULL, TRUE);
            return 0;
        }
        
        SetRect (&WndRect, 20, 0, 40, 20);
        if (PtInRect (&WndRect, &pt)) {
            PinyinChangeSbc (pImeItem);
            
            InvalidateRect(Window, NULL, TRUE);
            return 0;
        }
        
        GetClientRect (Window, &WndRect);
        
        if (PtInRect (&WndRect, &pt)) {
            Window->NcMouseFlag = 0x1;
            
            SetCapture (Window);
            SetPoint (&Window->MovePos, pt.x, pt.y);
        }
        return 0;
        break;
    }
    
    case EV_PAINT:
    {
        DC* hdc;
        
        hdc = BeginPaint (Window, wParam, lParam);
        if (NULL == hdc) {
            break;
        }
        
        PinyinPaint (Window, hdc);
        
        EndPaint (Window, hdc, wParam, lParam);
        return 0;
        break;
    }
        
    default:
        break;
    }
    
    return DefWinProc(Window, Message, wParam, lParam);
}

bool32_t RegisterPinyinClass (void)
{
    WNDCLASS PinyinClass;

    memset (&PinyinClass, 0, sizeof (WNDCLASS));
    PinyinClass.pClassName = "pinyin";
    PinyinClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    PinyinClass.WinProc    = DefPinyinProc;
    
    return RegisterClass (&PinyinClass);
}

static bool32_t PinyinInit(IMEItem *pImeItem)
{
    WINDOW *pImeWnd;
    
    pImeWnd = CreateWindow ("pinyin", "pinyin", 
        WS_IME | WS_TOPMOST | WS_BORDER, 
        0, GetSystemMetrics (SM_CYSCREEN) - 40, 150, 40, GetDesktopWindow (), NULL, pImeItem);
    if (NULL == pImeWnd) {
        return FALSE;
    }
    
    pImeItem->nState       = IME_STATE_ENGLISH | IME_STATE_DBC;
    pImeItem->pImeWnd      = pImeWnd;
    
    PinyinReset (pImeItem);
    
    return TRUE;
}

static void PinyinRelease(IMEItem *pImeItem)
{
    if (NULL != pImeItem->pImeWnd) {
        DestroyWindow (pImeItem->pImeWnd);
    }
    
    pImeItem->pImeWnd   = NULL;
    
    PinyinReset (pImeItem);
}

static bool32_t ReadPinyin(TPinyinData *pData)
{
    FILE *fp;
    
    if ((fp = fopen (ROOT_PATH"/ime/pinyin.dat", "rb")) == NULL) {
        return FALSE;
    }
    
    fread (&pData->nMaxIdx, sizeof (int32_t), 1, fp);
    fread (&pData->nHanziLen, sizeof (int32_t), 1, fp);
    
    pData->pPinyinIdx = Malloc (sizeof (TPinyinIdx) * pData->nMaxIdx);
    if (NULL == pData->pPinyinIdx) {
        fclose (fp);
        return FALSE;
    }
    fread (pData->pPinyinIdx, sizeof (TPinyinIdx), pData->nMaxIdx, fp);
    
    pData->pHanziBuf = Malloc (pData->nHanziLen);
    if (NULL == pData->pHanziBuf) {
        Free (pData->pPinyinIdx);
        fclose (fp);
        return FALSE;
    }
    fread (pData->pHanziBuf, pData->nHanziLen, 1, fp);
    
    fclose (fp);
    
    return TRUE;
}

bool32_t InitPinyin(IME *pIme)
{
    TPinyinData *pAddData;
    IMEItem *pImeItem = Malloc (sizeof (IMEItem));
    if (NULL == pImeItem) {
        return FALSE;
    }
    
    pAddData = Malloc (sizeof (TPinyinData));
    if (NULL == pAddData) {
        Free (pImeItem);
        return FALSE;
    }
    
    pAddData->pBitmap   = LoadBmpFile (ROOT_PATH"/ime/pinyin.bmp");
    if (NULL == pAddData->pBitmap) {
        Free (pImeItem);
        Free (pAddData);
        return FALSE;
    }
    
    if (FALSE == ReadPinyin (pAddData)) {
        UnloadBitmap (pAddData->pBitmap);
        Free (pImeItem);
        Free (pAddData);
        return FALSE;
    }
    
    RegisterPinyinClass ();
    
    pImeItem->Init      = PinyinInit;
    pImeItem->Release   = PinyinRelease;
    pImeItem->pImeWnd   = NULL;
    pImeItem->nInputLen = 0;
    pImeItem->nFindLen  = 0;
    pImeItem->pIme      = pIme;
    pImeItem->pAddData  = pAddData;
    list_add (&pImeItem->ImeListNode, &pIme->ImeListHead);
    pIme->nImeCount++;
    
    return TRUE;
}

void ReleasePinyin(IME *pIme)
{
}
