#ifndef __IME_H__
#define __IME_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define MAX_INPUTCHARS      6
#define MAX_FINDCHARS       256

struct _IMEItem;
typedef struct _IMEItem    IMEItem;

typedef int8_t*                 (*Sbc2Dbc)(int8_t);

typedef bool32_t                 (*ImeInit)(IMEItem *);
typedef void                (*ImeRelease)(IMEItem *);

typedef struct _TSbc2Dbc{
	int8_t                      sbc;
	int8_t*                     dbc;
}TSbc2Dbc;

struct _IMEItem
{
    int32_t                     nState;
    
    ImeInit                 Init;
    ImeRelease              Release;
    
    struct _WINDOW*        pImeWnd;
    
    int16_t                     nInputLen;
    int8_t                      InputBuf[MAX_INPUTCHARS + 1];
    int16_t                     nFindLen;
    int8_t                      *pFindBuf;
    int16_t                     nCharPerPage;
    int16_t                     nTotalPage;
    int16_t                     nCurPage;
    int16_t                     nPageBegin;
    int16_t                     nPageSize;
    
    IME*                   pIme;
    
    void*                   pAddData;
    
    struct list_head        ImeListNode;
};

struct _IME
{
    Sbc2Dbc                 Sbc2Dbc;
    
    struct _WINDOW*        pBindWnd;
    struct _IMEItem*       pSelItem;
    
    int32_t                     nImeCount;
    struct list_head        ImeListHead;
};

bool32_t  InitIme(void);
void ShowIme(WINDOW *, bool32_t);
bool32_t  IsImeActive(void);
WINDOW *GetImeActiveWindow(void);
WINDOW *GetImeBindWindow(void);
void SelIme(int32_t nIndex);
void ReleaseIme(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
