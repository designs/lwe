/*
  Copyright (C), 2008~2009, zhanbin
  File name:    ctrldemo.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090115
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090115    zhanbin         创建文件
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"
#include    "icon.h"

static ICON *pAudioIcon;
static WINDOW *hListView, *hProgressBar0, *hProgressBar1,
    *hListBox, *hPushButton, *hCheckButton, *hEidt, *hStatic;

static int32_t CtrlDemoProc(WINDOW* Window,uint32_t message,uint32_t wParam,uint32_t lParam)
{
    switch (message) {
    case EV_CREATE:
    {
        int32_t i = 0;
        LVCOLUMN s1;
        LVITEM item;
        LVSUBITEM subdata;

        pAudioIcon = LoadIconFile (ROOT_PATH"/icon/audio.ico");

        hListView = CreateWindow ("listview", "", WS_CONTROL | WS_VISIBLE | WS_TABSTOP
            | WS_HSCROLL | WS_VSCROLL,
            5, 0,
            200, 100,
            Window, (MENU *)1000, NULL);

        s1.nCols = 1;
        s1.pszHeadText = "文件名";
        s1.width = 100;
        s1.pfnCompare = NULL;
        s1.colFlags = 0;
        SendMessage (hListView, LVM_ADDCOLUMN, 0, (uint32_t) & s1);

        s1.nCols = 2;
        s1.pszHeadText = "文件大小";
        s1.width = 100;
        s1.pfnCompare = NULL;
        s1.colFlags = COLUME_TXT_RIGHTALIGN | HEADER_TXT_CENTERALIGN;
        SendMessage (hListView, LVM_ADDCOLUMN, 0, (uint32_t) & s1);

        item.nItem = ++i;
        item.itemData = (uint32_t) "1.mpg";
        SendMessage (hListView, LVM_ADDITEM, 0, (uint32_t) & item);

        subdata.nItem = i;
        subdata.subItem = 1;
        subdata.pszText = "1.mpg";
        subdata.flags = LVFLAG_ICON;
        subdata.image = (uint32_t)pAudioIcon;
        subdata.nTextColor = RGB (0, 0, 0);
        SendMessage (hListView, LVM_FILLSUBITEM, 0, (uint32_t) & subdata);

        subdata.subItem = 2;
        subdata.pszText = "2048K";
        subdata.flags = 0;
        subdata.image = 0;
        subdata.nTextColor = RGB (0, 0, 0);
        SendMessage (hListView, LVM_FILLSUBITEM, 0, (uint32_t) & subdata);

        item.nItem = ++i;
        item.itemData = (uint32_t) "2.mpg";
        SendMessage (hListView, LVM_ADDITEM, 0, (uint32_t) & item);

        subdata.nItem = i;
        subdata.subItem = 1;
        subdata.pszText = "2.mpg";
        subdata.flags = LVFLAG_ICON;
        subdata.image = (uint32_t)pAudioIcon;
        subdata.nTextColor = RGB (0, 0, 0);
        SendMessage (hListView, LVM_FILLSUBITEM, 0, (uint32_t) & subdata);

        subdata.subItem = 2;
        subdata.pszText = "1024K";
        subdata.flags = 0;
        subdata.image = 0;
        subdata.nTextColor = RGB (0, 0, 0);
        SendMessage (hListView, LVM_FILLSUBITEM, 0, (uint32_t) & subdata);

        item.nItem = ++i;
        item.itemData = (uint32_t) "3.mpg";
        SendMessage (hListView, LVM_ADDITEM, 0, (uint32_t) & item);

        subdata.nItem = i;
        subdata.subItem = 1;
        subdata.pszText = "3.mpg";
        subdata.flags = LVFLAG_ICON;
        subdata.image = (uint32_t)pAudioIcon;
        subdata.nTextColor = RGB (0, 0, 0);
        SendMessage (hListView, LVM_FILLSUBITEM, 0, (uint32_t) & subdata);

        subdata.subItem = 2;
        subdata.pszText = "1024K";
        subdata.flags = 0;
        subdata.image = 0;
        subdata.nTextColor = RGB (0, 0, 0);
        SendMessage (hListView, LVM_FILLSUBITEM, 0, (uint32_t) & subdata);

        item.nItem = ++i;
        item.itemData = (uint32_t) "4.mpg";
        SendMessage (hListView, LVM_ADDITEM, 0, (uint32_t) & item);

        subdata.nItem = i;
        subdata.subItem = 1;
        subdata.pszText = "4.mpg";
        subdata.flags = LVFLAG_ICON;
        subdata.image = (uint32_t)pAudioIcon;
        subdata.nTextColor = RGB (0, 0, 0);
        SendMessage (hListView, LVM_FILLSUBITEM, 0, (uint32_t) & subdata);

        subdata.subItem = 2;
        subdata.pszText = "4024K";
        subdata.flags = 0;
        subdata.image = 0;
        subdata.nTextColor = RGB (0, 0, 0);
        SendMessage (hListView, LVM_FILLSUBITEM, 0, (uint32_t) & subdata);

        item.nItem = ++i;
        item.itemData = (uint32_t) "5.mpg";
        SendMessage (hListView, LVM_ADDITEM, 0, (uint32_t) & item);

        subdata.nItem = i;
        subdata.subItem = 1;
        subdata.pszText = "5.mpg";
        subdata.flags = LVFLAG_ICON;
        subdata.image = (uint32_t)pAudioIcon;
        subdata.nTextColor = RGB (0, 0, 0);
        SendMessage (hListView, LVM_FILLSUBITEM, 0, (uint32_t) & subdata);

        subdata.subItem = 2;
        subdata.pszText = "4024K";
        subdata.flags = 0;
        subdata.image = 0;
        subdata.nTextColor = RGB (0, 0, 0);
        SendMessage (hListView, LVM_FILLSUBITEM, 0, (uint32_t) & subdata);

        item.nItem = ++i;
        item.itemData = (uint32_t) "6.mpg";
        SendMessage (hListView, LVM_ADDITEM, 0, (uint32_t) & item);

        subdata.nItem = i;
        subdata.subItem = 1;
        subdata.pszText = "6.mpg";
        subdata.flags = LVFLAG_ICON;
        subdata.image = (uint32_t)pAudioIcon;
        subdata.nTextColor = RGB (0, 0, 0);
        SendMessage (hListView, LVM_FILLSUBITEM, 0, (uint32_t) & subdata);

        subdata.subItem = 2;
        subdata.pszText = "4024K";
        subdata.flags = 0;
        subdata.image = 0;
        subdata.nTextColor = RGB (0, 0, 0);
        SendMessage (hListView, LVM_FILLSUBITEM, 0, (uint32_t) & subdata);

        hProgressBar0 = CreateWindow ("progressbar", "", WS_CONTROL | WS_VISIBLE | PBS_NOTIFY,
            5, 110,
            200, 15,
            Window, (MENU *)1001, NULL);
        SendMessage (hProgressBar0, PBM_SETRANGE, 0, 100);
        SendMessage (hProgressBar0, PBM_SETSTEP, 10, 0);

        hProgressBar1 = CreateWindow ("progressbar", "", WS_CONTROL | WS_VISIBLE | PBS_NOTIFY | PBS_VERTICAL,
            215, 0,
            15, 200,
            Window, (MENU *)1002, NULL);
        SendMessage (hProgressBar1, PBM_SETRANGE, 0, 100);
        SendMessage (hProgressBar1, PBM_SETSTEP, 10, 0);

        hListBox = CreateWindow ("listbox", "", WS_CONTROL | WS_VISIBLE | WS_TABSTOP | WS_VSCROLL | LBS_NOTIFY,
            5, 150, 200, 150, Window, (MENU *)1003, NULL);

        for (i = 0; i < 10; i++) {
            int8_t strMsg[10];

            sprintf (strMsg, "选项%d", i);
            SendMessage (hListBox, LB_ADDSTRING, (uint32_t)NULL, (uint32_t)strMsg);
        }

        hStatic = CreateWindow ("static", "静态文本框", WS_CONTROL | WS_VISIBLE,
            5, 130, 200, 20, Window, (MENU *)1004, NULL);

        hPushButton = CreateWindow ("bmpbutton", "按钮", WS_CONTROL | WS_VISIBLE | WS_TABSTOP,
            235,  10, 75, 21, Window, (MENU *)1005, GetSystemBitmap (BMP_PUSH_BUTTON_ID));

        hCheckButton = CreateWindow ("chedkbutton", "CHECK按钮", WS_CONTROL | WS_VISIBLE | WS_TABSTOP,
            235,  50, 108, 20, Window, (MENU *)1006, GetSystemBitmap (BMP_CHECK_BUTTON_ID));

        hEidt = CreateWindow ("edit", "编辑框", WS_CONTROL | WS_VISIBLE | WS_TABSTOP,
            235,  80, 75, 20, Window, (MENU *)1007, NULL);

        CreateWindow ("static", "ICON BOX", WS_CONTROL | WS_VISIBLE | SS_ICON,
            235, 110, 16, 16, Window, (MENU *)1008, pAudioIcon);

        CreateWindow ("static", "BITMAP BOX", WS_CONTROL | WS_VISIBLE | SS_BITMAP,
            235, 140, 60, 30, Window, (MENU *)1008, GetSystemBitmap (BMP_GUI_LOGO_ID));
        SetTimer (Window, 100, 20, NULL);
        break;
    }
#if 0
    case EV_TIMER:
    {
        int32_t nPos;

        nPos = SendMessage (hProgressBar0, PBM_GETPOS, 0, 0);
        nPos++;
        SendMessage (hProgressBar0, PBM_SETPOS, nPos, 0);

        nPos = SendMessage (hProgressBar1, PBM_GETPOS, 0, 0);
        nPos++;
        SendMessage (hProgressBar1, PBM_SETPOS, nPos, 0);
        break;
    }
#endif
    case EV_COMMAND:
    {
        switch (LOuint16_t (wParam)) {
        case 1000:
            switch (HIuint16_t (wParam)) {
            case LVN_SELCHANGE:
                SendMessage (hStatic, EV_SETTEXT, (uint32_t)0, (uint32_t)"LISTVIEW SELCHANGE\n");
                break;
            }
            break;

        case 1001:
        case 1002:
            switch (HIuint16_t (wParam)) {
            case PBN_REACHMAX:
                SendMessage ((WINDOW *)lParam, PBM_SETPOS, 0, 0);
                break;
            }
            break;

        case 1003:
            switch (HIuint16_t (wParam)) {
            case LBN_SELCHANGE:
                SendMessage (hStatic, EV_SETTEXT, (uint32_t)0, (uint32_t)"LISTBOX SELCHANGE\n");
                break;

            case LBN_ENTER:
                SendMessage (hStatic, EV_SETTEXT, (uint32_t)0, (uint32_t)"LISTBOX ENTER\n");
                break;
            }
            break;

        case 1005:
            switch (HIuint16_t (wParam)) {
            case BN_CLICKED:
                SendMessage (hStatic, EV_SETTEXT, (uint32_t)0, (uint32_t)"BUTTON CLICKED\n");
                break;
            }
            break;
        }
        break;
    }
	case EV_KEYUP:
    {
	SendMessage (hListView, EV_KEYDOWN, wParam, lParam);
	break;
	}	

    case EV_DESTROY:
    {
        DestroyIcon (pAudioIcon);
        break;
    }

    default:
        break;
    }

    return DefWinProc(Window, message, wParam, lParam);
}

static bool32_t RegisterCtrlDemoClass (void)
{
    WNDCLASS CtrlDemoClass;

    memset (&CtrlDemoClass, 0, sizeof (WNDCLASS));
    CtrlDemoClass.pClassName = "CtrlDemo";
    CtrlDemoClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    CtrlDemoClass.WinProc    = CtrlDemoProc;

    return RegisterClass (&CtrlDemoClass);
}

WINDOW *CreateCtrlDemoWnd(void)
{
    WINDOW *hWnd;

    RegisterCtrlDemoClass ();

    hWnd = CreateWindow ("CtrlDemo", "控件示例", WS_MAINWND | WS_CAPTION | WS_BORDER,
        0, 0, 480, 280, NULL, NULL, NULL);
    if (NULL == hWnd) {
        return NULL;
    }

    ShowWindow (hWnd, SW_SHOW);

    return hWnd;
}
