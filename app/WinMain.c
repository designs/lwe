/*
  Copyright (C), 2008, zhanbin
  File name:    WinMain.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081205
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081205    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

extern WINDOW *CreateDetectWnd(void);
extern WINDOW *CreateSokobanWnd(void);
extern WINDOW *CreateCtrlDemoWnd(void);

int32_t WinMain(void)
{
    MSG Msg;

//    SelIme (0);

    CreateDetectWnd ();

//    CreateSokobanWnd ();

//    CreateCtrlDemoWnd ();

    while (GetMessage (&Msg, NULL) == TRUE) {
        TranslateMessage (&Msg);
        DispatchMessage (&Msg);
    }

    return 0;
}
