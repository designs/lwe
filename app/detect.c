/*
** $Id: detect.c,v 1.17 2003/08/15 08:45:46 weiym Exp $
**
** detect.c: The MineSweeper game.
**
** Copyright (C) 1999~ 2002 Zheng Xiang and others.
** Copyright (C) 2003 Feynman Software.
*/

/*
**  This source is free software; you can redistribute it and/or
**  modify it under the terms of the GNU General Public
**  License as published by the Free Software Foundation; either
**  version 2 of the License, or (at your option) any later version.
**
**  This software is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
**  General Public License for more details.
**
**  You should have received a copy of the GNU General Public
**  License along with this library; if not, write to the Free
**  Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
**  MA 02111-1307, USA
*/

/*
** Modify records:
**
**  Who             When        Where       For What                Status
**-----------------------------------------------------------------------------
**  zhanbin         2008/12/05              Adapted from MiniGUI    done
**-----------------------------------------------------------------------------
**
** TODO:
*/
#include    <time.h>
#include    "window.h"
#define  BUTTON_UP		300
#define  BUTTON_DOWN	301  
#define  BUTTON_ENT		302
#define  BUTTON_RETURN	303
#define  BUTTON_LEFT	304
#define  BUTTON_RIGHT	305

#define BUTTON_START_YPOS 20
static WINDOW * hButton1,* hButton2,* hButton3,* hButton4;


void DrawMyBitmap(DC* hdc, BITMAP * bmp, int x, int y, int w, int h)
{
    RECT rect;

    if (bmp)
        DrawBitmap (hdc, x, y, bmp, 0, 0, w, h);
    else {
        SetRect (&rect, x, y, w, h);
        DrawText (hdc, "a", strlen ("a"), &rect, DT_CENTER);
    }
}

static BITMAP *LoadDetectBitmap (const char* filename)
{
	char full_path [MAX_PATH + 1];

	strcpy (full_path, ROOT_PATH"/bmp/detect/");
    strcat (full_path, filename);

    return LoadBmpFile (full_path);
}
WINDOW* GetWndFromButID(char id)
{
	switch (id)
		{
		case 1:
		return hButton1;
		break;
		case 2:
		return hButton2;
		break;
		case 3:
		return hButton3;
		break;
		case 4:
		return hButton4;
		break;
		default:
		return NULL;
			break;
		}

}

doChangLightButton(char old_pos,char current_pos)
{

	SendMessage (GetWndFromButID(old_pos),EV_UNCHOUSE,0,0);
	SendMessage (GetWndFromButID(current_pos),EV_CHOUSE,0,0);

/*	
switch (old_pos)
	{
	case 1:
	SendMessage (hButton1,EV_UNCHOUSE,0,0);
	break;
	case 2:
	SendMessage (hButton2,EV_UNCHOUSE,0,0);
	break;
	case 3:
	SendMessage (hButton3,EV_UNCHOUSE,0,0);
	break;
	case 4:
	SendMessage (hButton4,EV_UNCHOUSE,0,0);
	break;
	default:
		break;
	}
switch (current_pos)
	{
	case 1:
	SendMessage (hButton1,EV_CHOUSE,0,0);
	break;
	case 2:
	SendMessage (hButton2,EV_CHOUSE,0,0);
	break;
	case 3:
	SendMessage (hButton3,EV_CHOUSE,0,0);
	break;
	case 4:
	SendMessage (hButton4,EV_CHOUSE,0,0);
	break;
	default:
		break;
	}
*/

}

static int32_t DetectWndProc(WINDOW* hWnd,uint32_t message,uint32_t wParam,uint32_t lParam)
{
    DC *hdc;
    short i, j;
    static RECT detectregion, face, onerect, detectnumber, clock;
    static RECT winposition;
	char button_current_pos;
	static char old_pos=1;
    POINT pt;
    static uint32_t ButtonFlag = 0;

    RECT rcClient;
    switch (message) {
    case EV_CREATE:
	i=BUTTON_START_YPOS;
    hButton1=CreateWindow("bmpbutton","单脉冲",WS_CONTROL | WS_VISIBLE | WS_TABSTOP,
            GetSystemMetrics(SM_CXSCREEN)-78,i,
            75,21,
            hWnd,(MENU*)1001,GetSystemBitmap(BMP_PUSH_BUTTON_ID)
            );
	hButton2=CreateWindow("bmpbutton","多脉冲",WS_CONTROL | WS_VISIBLE | WS_TABSTOP,
				GetSystemMetrics(SM_CXSCREEN)-78,i+=30,
				75,21,
				hWnd,(MENU*)1002,GetSystemBitmap(BMP_PUSH_BUTTON_ID)
				);
	hButton3=CreateWindow("bmpbutton","存储数据",WS_CONTROL | WS_VISIBLE | WS_TABSTOP,
				GetSystemMetrics(SM_CXSCREEN)-78,i+=30,
				75,21,
				hWnd,(MENU*)1003,GetSystemBitmap(BMP_PUSH_BUTTON_ID)
				);
	hButton4=CreateWindow("bmpbutton","读取存储",WS_CONTROL | WS_VISIBLE | WS_TABSTOP,
				GetSystemMetrics(SM_CXSCREEN)-78,i+=30,
				75,21,
				hWnd,(MENU*)1004,GetSystemBitmap(BMP_PUSH_BUTTON_ID)
				);



		InvalidateRect (hWnd, NULL, TRUE);
        break;
    case EV_TIMER:
        break;

    case EV_PAINT:
        hdc = BeginPaint (hWnd, wParam, lParam);

    GetClientRect (hWnd, &rcClient);
    SetRect(&rcClient,rcClient.left,rcClient.top,rcClient.right-80,rcClient.bottom);
    SetBkColor (hdc, RGB (255, 255, 255));
    SetFgColor (hdc, RGB (255, 255, 255));
    FillRect(hdc, &rcClient);
        EndPaint (hWnd, hdc, wParam, lParam);
        return 0;
        break;
	case EV_COMMAND:
		break;
    case EV_KEYUP:
	    switch (LOuint16_t(wParam)) {
        case '0'://up
           button_current_pos=old_pos;
			if((--button_current_pos)<=0)
			{
				button_current_pos=4;
				}
			doChangLightButton(old_pos,button_current_pos);
			old_pos=button_current_pos;
		break;

        case '1'://down
			button_current_pos=old_pos;
			if((++button_current_pos)>=5)
			{
				button_current_pos=1;
				}
			doChangLightButton(old_pos,button_current_pos);
            old_pos=button_current_pos;
            break;
        case '2'://enter

            printf("2\n");
            break;

        case '3'://return

            printf("3\n");
            break;

        case '4'://left

            printf("4\n");
            break;

        case '5'://right

            printf("5\n");
            break;
        default:
            break;
    }/*
		SetRect (&rcClient,rcClient.left,rcClient.top, rcClient.left+ 18,rcClient.top + 18);
		DrawText (hdc,LOuint16_t(wParam), strlen (LOuint16_t(wParam)), &rcClient, DT_CENTER);
		EndPaint (hWnd, hdc, wParam, lParam);*/
        break;


    case EV_DESTROY:
        break;

    default:
        break;
    }

    return DefWinProc(hWnd, message, wParam, lParam);
}

bool32_t RegisterDetectWndClass (void)
{
    WNDCLASS DetectWndClass;

    memset (&DetectWndClass, 0, sizeof (WNDCLASS));
    DetectWndClass.pClassName = "DetectWnd";
    DetectWndClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    DetectWndClass.WinProc    = DetectWndProc;

    return RegisterClass (&DetectWndClass);
}

static MENU *TestWndCreateMainMenu(void)
{
    MENUItem Item, *pItem;
    MENU *pMenu, *pPlayMenu, *pLevelMenu, *pAboutMenu;

    pMenu = CreateMenu ("主菜单");

    Item.id = 2000;
    strcpy (Item.spName, "探测");
    InsertMenuItem (pMenu, &Item);//插入菜单项

    Item.id = 2001;
    strcpy (Item.spName, "级别");
    InsertMenuItem (pMenu, &Item);

    Item.id = 2002;
    strcpy (Item.spName, "关于");
    InsertMenuItem (pMenu, &Item);



    pItem = GetMenuItem (pMenu, 2000);
    pPlayMenu = CreateSubMenu (pItem);

    return pMenu;
}

WINDOW *CreateDetectWnd(void)
{
    MENU *pMainMenu;
    WINDOW *hDetectWnd;

    RegisterDetectWndClass ();

    pMainMenu = TestWndCreateMainMenu ();

    hDetectWnd = CreateWindow ("DetectWnd", "探测", WS_MAINWND | WS_CAPTION | WS_BORDER,
        0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), NULL,/*pMainMenu*/NULL, NULL);
    if (NULL == hDetectWnd) {
        return NULL;
    }

    ShowWindow (hDetectWnd, SW_SHOW);

    return hDetectWnd;
}
