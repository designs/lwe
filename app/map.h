/*
** Modify records:
**
**  Who             When        Where       For What                Status
**-----------------------------------------------------------------------------
**  zhanbin         2009/02/21              Adapted from MiniGUI    done
**-----------------------------------------------------------------------------
**
** TODO:
*/
#ifndef __MAP_H_
#define __MAP_H_

#define assert(x)               {}

/*
 *  $		the box		B_OBJECT
 *  .		place		B_GOAL
 *  @		man		B_MAN
 *  *		box + place	B_TREASURE
 *  +		man + place	B_SAVEMAN
 *  #		wall		B_STONE
 */

#define B_FORBID -1
#define B_GOAL 	0
#define B_MAN	1
#define B_OBJECT 2 
#define B_SAVEMAN 3
#define B_TREASURE 4
#define B_STONE 5
#define B_NOTHING 6


#define MAX_COLLECTION_NAME 20

#define LINE_TYPE_COLLECTION_BEGIN 100
#define LINE_TYPE_LEVEL_BEGIN 200
#define LINE_TYPE_OTHER 300

#define STEP_TYPE_MOVE 100
#define STEP_TYPE_PUSH 200

#define DIR_UP  101
#define DIR_DOWN  201
#define DIR_LEFT 301
#define DIR_RIGHT 401

#define BACK_STEP_SIZE 50
typedef struct tagLevel{
	int32_t iNo;
	int32_t col;
	int32_t row;
	int32_t manx;
	int32_t many;
	int32_t *data;
	struct tagLevel *next;
	struct tagLevel *prev;
} Level;
typedef Level* ptagLevel;

typedef struct tagStep{
	int32_t iType;
	int32_t pt1[3];
	int32_t pt2[3];
	int32_t pt3[3]; 
} Step;
typedef Step* ptagStep;

typedef struct tagLevelCollection{
	int32_t iNoOfLevels;
	int8_t *strName;
	struct tagLevelCollection *next;
	struct tagLevelCollection *prev;
	ptagLevel current;
	ptagLevel head;
	ptagLevel tail;
} LevelCollection;
typedef LevelCollection * ptagLevelCollection;


typedef struct tagMap{
	int32_t iNoOfCollection;//collection▓╔╝»
	ptagLevel currentLevel;
	ptagLevelCollection current;
	ptagLevelCollection head;
	ptagLevelCollection tail;
	ptagStep pSteps[BACK_STEP_SIZE];
	int32_t shead;
} Map;
typedef Map* ptagMap;

void CovertCoord(WINDOW *hWnd,int32_t *px, int32_t *py);
bool32_t ptChoosePlace(WINDOW *hWnd,int32_t *px, int32_t *py);
void DrawALittleBlock(WINDOW *hWnd, int32_t x, int32_t y, int32_t itype);	
//===============================================================================
//Init Functions:
void InitMap(void);
int32_t GotoCollection(ptagLevelCollection pColl);
//===============================================================================
//Destroy Functions:
void DestroyMap(void);
extern ptagMap theMap; 
//===============================================================================
//Play Functions:
int32_t PlayMove(WINDOW *hwnd, int32_t x, int32_t y);
int32_t PlayKeyboard(WINDOW *hwnd, int32_t iDir);
int32_t PlayUndo(WINDOW *hwnd);
void PlayRestart(void); 
ptagStep PopStep(void);
void PushStep(ptagStep pStep);
bool32_t CheckMissionComplete(void);
#endif // __MAP_H_
