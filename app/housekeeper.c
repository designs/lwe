/*
 * $Id: housekeeper.c,v 1.12 2003/08/15 08:45:46 weiym Exp $
 *
 *little game housekeeper, also named sokoban. 
 *written by Song Lixin(zjujoe@263.net) 2001.3.6
 */

/*
** Modify records:
**
**  Who             When        Where       For What                Status
**-----------------------------------------------------------------------------
**  zhanbin         2009/02/21              Adapted from MiniGUI    done
**-----------------------------------------------------------------------------
**
** TODO:
*/
#include    "window.h"
#include    "map.h"

#define ID_GOTO                7980
#define ID_NEXT                7990
#define ID_PREVIOUS         8000 
#define ID_RESTART            8010
#define ID_UNDO                8020
#define ID_EXIT                8040

#define    ID_MAP_A            8050
#define    ID_MAP_B            8060
#define    ID_MAP_C            8070
#define    ID_MAP_D            8080
#define    ID_MAP_E            8090
#define    ID_MAP_F            8100
#define    ID_MAP_G            8110
#define    ID_MAP_H            8120
#define ID_ABOUT            8130
    
#define WIDTH_LARGEWIN      380
#define HEIGHT_LARGEWIN     400

#define IDC_LEVELINPUT      9000

bool32_t  InitializeApp(void);
void OnClose(WINDOW *hWnd);
void OnDraw(WINDOW *hWnd,DC *hDC);

static BITMAP *bitmapAll[6];

static bool32_t FillBoxWithBitmap (DC *hdc, int32_t x, int32_t y, int32_t w, int32_t h, BITMAP* pBitmap)
{
    BITMAP *pBmp;
    float sh, sw;
    int8_t *lpNewPtr, *lpOldPtr;
    int32_t nw, nh, x0, y0, x1, y1;
    
    if (pBitmap->bmWidth == w && pBitmap->bmHeight == h) {
        DrawBitmap (hdc, x, y, pBitmap, 0, 0, 0, 0);
        
        return TRUE;
    }
    
    sh = (float)((float)pBitmap->bmWidth  / (float)w);
    sw = (float)((float)pBitmap->bmHeight / (float)h);
    nw = (uint32_t)(pBitmap->bmWidth / sw + 0.5);
    nh = (uint32_t)(pBitmap->bmHeight / sh + 0.5);
    
    pBmp = Malloc (sizeof (BITMAP));
    if (NULL == pBmp) {
        return FALSE;
    }
    pBmp->bmType       = 0;
    pBmp->bmWidth      = nw;
    pBmp->bmHeight     = nh;
    pBmp->bmBitsPixel  = 32;
    pBmp->bmWidthBytes = w * 4;
    pBmp->bmPlanes     = 1;
    pBmp->bmBits       = Malloc (pBmp->bmWidth * pBmp->bmHeight  * sizeof(RGBQUAD));
    if (NULL == pBmp->bmBits) {
        return FALSE;
    }
    
    for(y1 = 0; y1 < nh; y1++) {
        for(x1 = 0; x1 < nw; x1++) {
            x0 = (uint32_t)(x1 * sw);
            y0 = (uint32_t)(y1 * sh);
            if ((x0 >= 0) && (x0 < pBitmap->bmWidth) && (y0 >= 0) && (y0 < pBitmap->bmHeight)) {
                lpOldPtr = (int8_t *)pBitmap->bmBits + (y0 * pBitmap->bmWidth + x0) * sizeof(RGBQUAD);
                lpNewPtr = (int8_t *)pBmp->bmBits + (y1 * nw + x1) * sizeof(RGBQUAD);
                memcpy(lpNewPtr, lpOldPtr, sizeof(RGBQUAD));
            }
        }
    }
    
    DrawBitmap (hdc, x, y, pBmp, 0, 0, 0, 0);
    
    UnloadBitmap (pBmp);
    
    return TRUE;
}

static int32_t DialogInputLevelProc(WINDOW *pDlg, uint32_t message, uint32_t wParam, uint32_t lParam)
{
    int i;
    char strText[4];
    WINDOW *hStatic;

    switch (message) {
    case EV_CREATE:
        CreateWindow ("static", "Play Level:", WS_CONTROL | WS_VISIBLE, 
            14, 30, 100, 20, pDlg, (MENU *)1000, NULL);
            
        CreateWindow ("edit", "", WS_CONTROL | WS_VISIBLE | WS_TABSTOP, 
            120, 30, 60, 20, pDlg, (MENU *)IDC_LEVELINPUT, NULL);
            
        CreateWindow ("bmpbutton", "确定", WS_CONTROL | WS_VISIBLE | WS_TABSTOP, 
            20, 80, 75, 21, pDlg, (MENU *)IDOK, 
            GetSystemBitmap (BMP_PUSH_BUTTON_ID));
            
        CreateWindow ("bmpbutton", "取消", WS_CONTROL | WS_VISIBLE | WS_TABSTOP, 
            105, 80, 75, 21, pDlg, (MENU *)IDCANCEL, 
            GetSystemBitmap (BMP_PUSH_BUTTON_ID));
        break;
        
    case EV_COMMAND:
        switch (wParam) {
        case IDOK:
            hStatic = GetDlgItem (pDlg, IDC_LEVELINPUT);
            SendMessage (hStatic, EV_GETTEXT, (uint32_t)3, (uint32_t)strText);
            i = atoi(strText);
            EndDialog (pDlg, i);
            return 0;
            break;
            
        case IDCANCEL:
            EndDialog (pDlg, -1);
            return 0;
            break;
        }
        break;
        
    case EV_CLOSE:
        EndDialog (pDlg, -1);
        return 0;
        break;
    }
    
    return DefDialogProc (pDlg, message, wParam, lParam);
}

int32_t SokobanWndProc(WINDOW *hWnd, uint32_t message, uint32_t wParam, uint32_t lParam)
{
    DC *hdc;
    int32_t iRet;
    int32_t x,y;
    ptagLevelCollection pTempColl;
    ptagLevel pTempLev;
    int32_t iDir = 0;
    POINT pt;
    
    switch(message){
    case EV_CREATE:
        if(!InitializeApp()){
            return 1;
        }
        break;
        
    case EV_COMMAND:
        switch(LOuint16_t(wParam)){    
        case ID_GOTO:
            iRet = CreateDialog ("Choose level", WS_CAPTION | WS_BORDER, 200, 150, 
                hWnd, DialogInputLevelProc);
            if ((iRet > 0) && (iRet <= theMap->current->iNoOfLevels)) {
                pTempLev = theMap->current->head;
                if (pTempLev->iNo != iRet) {
                    pTempLev = pTempLev->next;
                    while(pTempLev != theMap->current->head) {
                        if(pTempLev->iNo == iRet) 
                            break;
                        pTempLev = pTempLev->next;
                    }
                }    
                    if ((pTempLev->iNo == iRet) && (pTempLev != theMap->currentLevel)) {
                    theMap->current->current = pTempLev;
                    PlayRestart();
                    InvalidateRect(hWnd, NULL, TRUE);
                }        
            }
            break;
            
        case ID_NEXT:
            theMap->current->current = theMap->current->current->next;
            PlayRestart();
            InvalidateRect(hWnd, NULL, TRUE);
            break;
            
        case ID_PREVIOUS:
            theMap->current->current = theMap->current->current->prev;
            PlayRestart();
            InvalidateRect(hWnd, NULL, TRUE);
            break;
            
        case ID_RESTART:
            PlayRestart();
            InvalidateRect(hWnd, NULL, TRUE);
            break;
            
        case ID_UNDO:
            PlayUndo(hWnd); 
            break;
            
        case ID_EXIT:
             OnClose(hWnd);
            break;
            
        case ID_MAP_A:
            pTempColl = theMap->head;
            if (GotoCollection(pTempColl)) InvalidateRect(hWnd, NULL, TRUE);
            break;
            
        case ID_MAP_B:
            pTempColl = theMap->head->next;
            if (GotoCollection(pTempColl)) InvalidateRect(hWnd, NULL, TRUE);
            break;
            
        case ID_MAP_C:
            pTempColl = theMap->head->next->next;
            if (GotoCollection(pTempColl)) InvalidateRect(hWnd, NULL, TRUE);
            break;
            
        case ID_MAP_D:
            pTempColl = theMap->head->next->next->next;
            if (GotoCollection(pTempColl)) InvalidateRect(hWnd, NULL, TRUE);
            break;
            
        case ID_MAP_E:
            pTempColl = theMap->tail->prev->prev->prev;
            if (GotoCollection(pTempColl)) InvalidateRect(hWnd, NULL, TRUE);
            break;
            
        case ID_MAP_F:
            pTempColl = theMap->tail->prev->prev;
            if (GotoCollection(pTempColl)) InvalidateRect(hWnd, NULL, TRUE);
            break;
            
        case ID_MAP_G:
            pTempColl = theMap->tail->prev;
            if (GotoCollection(pTempColl)) InvalidateRect(hWnd, NULL, TRUE);
            break;
            
        case ID_MAP_H:
            pTempColl = theMap->tail;
            if (GotoCollection(pTempColl)) InvalidateRect(hWnd, NULL, TRUE);
            break;
            
        case ID_ABOUT:
            MessageBox (hWnd, "Song Lixin(zjujoe@263.net)\nHouse Keeper Ver 0.1 (2001/03/06)", "About", MB_OK);
            break;
        }        
        return 0;
        
    case EV_LBUTTONDOWN:
        x = LOuint16_t(lParam);
        y = HIuint16_t(lParam);
        
        SetPoint (&pt, x, y);
        ScreenToClient (hWnd, &pt);
        x = pt.x;
        y = pt.y;
        
        if (!ptChoosePlace(hWnd, &x, &y))
            break;
            
        PlayMove(hWnd, x, y);
        break;
        
    case EV_RBUTTONDOWN:
        PlayUndo(hWnd); 
        break;
        
    case EV_ERASEBKGND:
    {
        DC* hdc;
        RECT ClientRect;
        
        hdc = BeginErase (hWnd, wParam, lParam);
        if (NULL == hdc) {
            break;
        }
        
        GetClientRect (hWnd, &ClientRect);
        
        SetFgColor (hdc, RGB (102, 102, 102));
        FillRect (hdc, &ClientRect);
        
        EndErase (hWnd, hdc, wParam, lParam);
        return 0;
        break;
    }
        
    case EV_PAINT:
        hdc = BeginPaint(hWnd, wParam, lParam);
        
        OnDraw(hWnd, hdc);
        
        EndPaint(hWnd, hdc, wParam, lParam);
        return 0;

    case EV_KEYDOWN:
        switch (LOuint16_t(wParam)) {
            case VK_UP:     
                iDir = DIR_UP;
                break;
                
            case VK_LEFT:        
                iDir = DIR_LEFT;
                break;
                
            case VK_RIGHT:      
                iDir = DIR_RIGHT;
                break;
                
            case VK_DOWN:        
                iDir = DIR_DOWN;
                break;
                
            default:
                iDir = -1;
        }
        
        if ((iDir != -1) && (PlayKeyboard(hWnd, iDir) == STEP_TYPE_PUSH)){
            if (CheckMissionComplete()) {
                if (IDOK == MessageBox(hWnd, "Misson Completed!", "Good", MB_OK)) {
                    theMap->current->current = theMap->current->current->next;
                }
                PlayRestart();
                InvalidateRect(hWnd, NULL, TRUE);
            }
            return 0;
        }
        
        if (LOuint16_t(wParam) == VK_U) 
            PlayUndo(hWnd);
        return 0;    
                
    case EV_TIMER:
        return 0;    
                
    case EV_CLOSE:
        OnClose(hWnd);
        return 0;
    }
    
    return DefWinProc(hWnd, message, wParam, lParam);
}

void OnDraw(WINDOW *hWnd,DC *hDC) 
{
    RECT rect;
    POINT poStart1;
    int32_t iSideLength;
    ptagLevel cur_level;
    int8_t strText[50];
    int32_t iRow;
    int32_t iCol;
    int32_t i, j;
    int32_t iBmp;

    GetClientRect(hWnd,&rect);
    SetBkColor(hDC, RGB(102, 102, 102));
    SetFgColor (hDC, RGB(0, 0, 0));
    poStart1.x = rect.left;
    poStart1.y = rect.top;
    
    cur_level = theMap->currentLevel;
    sprintf(strText,"Collection %s, Level %d ", theMap->current->strName, cur_level->iNo);    
    TextOut(hDC, rect.left + 40, rect.bottom - 20, strText);
    rect.bottom -= 20;    
    if (rect.bottom - rect.top < 100)
        return;
    iRow = cur_level->row;
    iCol = cur_level->col;

    i = RECTH(rect)/iRow;
    j = RECTW(rect)/iCol;
    if (i <= j) 
        iSideLength = i;
    else     
        iSideLength = j;    

    for (i = 0; i < iRow; i++) {
        for (j = 0; j < iCol; j++) {
            iBmp = cur_level->data[i * iCol + j];
            if (iBmp < B_NOTHING) {
                FillBoxWithBitmap(hDC, poStart1.x + iSideLength * j \
                        , poStart1.y + iSideLength * i, iSideLength, iSideLength, bitmapAll[iBmp]);
            }
        }
    }
}

static BITMAP *LoadDetectBitmap (const int8_t* filename)
{
    int8_t full_path [MAX_PATH + 1];

    strcpy (full_path, ROOT_PATH"/bmp/sokoban/");
    strcat (full_path, filename);
    
    return LoadBmpFile (full_path);
}

bool32_t InitializeApp(void)
{
    bitmapAll[B_GOAL]     = LoadDetectBitmap ("goal.bmp");
    if (NULL == bitmapAll[B_GOAL]) {
        return FALSE;
    }
    bitmapAll[B_MAN]      = LoadDetectBitmap ("man.bmp");
    if (NULL == bitmapAll[B_MAN]) {
        return FALSE;
    }
    bitmapAll[B_OBJECT]   = LoadDetectBitmap ("object.bmp");
    if (NULL == bitmapAll[B_OBJECT]) {
        return FALSE;
    }
    bitmapAll[B_SAVEMAN]  = LoadDetectBitmap ("saveman.bmp");
    if (NULL == bitmapAll[B_SAVEMAN]) {
        return FALSE;
    }
    bitmapAll[B_STONE]    = LoadDetectBitmap ("stone.bmp");
    if (NULL == bitmapAll[B_STONE]) {
        return FALSE;
    }
    bitmapAll[B_TREASURE] = LoadDetectBitmap ("treasure.bmp");
    if (NULL == bitmapAll[B_TREASURE]) {
        return FALSE;
    }
    
    InitMap();
    
    return TRUE;
}

void OnClose(WINDOW *hWnd)
{
    DestroyMap();
    UnloadBitmap(bitmapAll[B_GOAL]);
    UnloadBitmap(bitmapAll[B_MAN]);
    UnloadBitmap(bitmapAll[B_OBJECT]);
    UnloadBitmap(bitmapAll[B_SAVEMAN]);
    UnloadBitmap(bitmapAll[B_STONE]);
    UnloadBitmap(bitmapAll[B_TREASURE]);
}

void CovertCoord(WINDOW *hWnd,int32_t *px, int32_t *py)
{
    RECT rect;
    int32_t iSideLength;
    ptagLevel cur_level;
    int32_t iRow;
    int32_t iCol;
    int32_t i, j;

    GetClientRect(hWnd,&rect);
    rect.bottom -= 20;    
    if (rect.bottom - rect.top < 100)
        return;
    cur_level = theMap->currentLevel;
    iRow = cur_level->row;
    iCol = cur_level->col;
    i = RECTH(rect)/iRow;
    j = RECTW(rect)/iCol;
    if (i <= j) 
        iSideLength = i;
    else     
        iSideLength = j;    

    i = *px;
    j = *py;

    *px = rect.left + i * iSideLength;
    *py = rect.top  + j * iSideLength;
}

bool32_t ptChoosePlace(WINDOW *hWnd, int32_t *px, int32_t *py) 
{
    RECT rect;
    int32_t iSideLength;
    ptagLevel cur_level;
    int32_t iRow;
    int32_t iCol;
    int32_t i, j;

    GetClientRect(hWnd,&rect);
    rect.bottom -= 20;    
    if (rect.bottom - rect.top < 100)
        return FALSE;
    cur_level = theMap->currentLevel;
    iRow = cur_level->row;
    iCol = cur_level->col;
    i = RECTH(rect)/iRow;
    j = RECTW(rect)/iCol;
    if (i <= j) 
        iSideLength = i;
    else     
        iSideLength = j;    

    i = *px;
    j = *py;

    if ( (i > rect.left) && (i < rect.left + iSideLength * iCol) &&
         (j > rect.top ) && (j < rect.top  + iSideLength * iRow) ) {
         *px = (i - rect.left) / iSideLength;
         *py = (j - rect.top ) / iSideLength;
         return TRUE;
    }
    return FALSE;
}

void DrawALittleBlock(WINDOW *hWnd, int32_t x, int32_t y, int32_t itype)    
{
    DC *hdc;
    RECT rect;
    int32_t iSideLength;
    ptagLevel cur_level;
    int32_t iRow;
    int32_t iCol;
    int32_t i, j;

    GetClientRect(hWnd,&rect);
    rect.bottom -= 20;    
    if (rect.bottom - rect.top < 100)
        return;
    cur_level = theMap->currentLevel;
    iRow = cur_level->row;
    iCol = cur_level->col;
    i = RECTH(rect)/iRow;
    j = RECTW(rect)/iCol;
    if (i <= j) 
        iSideLength = i;
    else     
        iSideLength = j;    

    i = x;
    j = y;

    x = rect.left + i * iSideLength;
    y = rect.top  + j * iSideLength;

    hdc = GetDC(hWnd);
    
    if(itype <= B_NOTHING) {
        if(itype == B_NOTHING) {
            RECT rect;
            
            SetFgColor (hdc, RGB(102, 102, 102));
            SetRect (&rect, x, y, x + iSideLength, y + iSideLength);
            FillRect (hdc, &rect);
        }
        else {
            FillBoxWithBitmap (hdc, x, y, iSideLength, iSideLength, bitmapAll[itype]);
        }

    }
    
    ReleaseDC(hWnd, hdc);
}

static MENU *SokobanCreateMainMenu(void)
{
    MENUItem Item, *pItem;
    MENU *pMenu, *pPlayMenu, *pLevelMenu, *pAboutMenu;
    
    pMenu = CreateMenu ("主菜单");
    
    Item.id = 2000;
    strcpy (Item.spName, "游戏");
    InsertMenuItem (pMenu, &Item);
    
    Item.id = 2001;
    strcpy (Item.spName, "地图");
    InsertMenuItem (pMenu, &Item);
    
    Item.id = 2002;
    strcpy (Item.spName, "关于");
    InsertMenuItem (pMenu, &Item);
    
    pItem = GetMenuItem (pMenu, 2000);
    pPlayMenu = CreateSubMenu (pItem);
    
    Item.id = ID_GOTO;
    sprintf (Item.spName, "设定");
    InsertMenuItem (pPlayMenu, &Item);
    
    Item.id = ID_NEXT;
    sprintf (Item.spName, "下一关");
    InsertMenuItem (pPlayMenu, &Item);
    
    Item.id = ID_PREVIOUS;
    sprintf (Item.spName, "上一关");
    InsertMenuItem (pPlayMenu, &Item);
    
    Item.id = ID_RESTART;
    sprintf (Item.spName, "重新开始");
    InsertMenuItem (pPlayMenu, &Item);
    
    Item.id = ID_UNDO;
    sprintf (Item.spName, "撤消");
    InsertMenuItem (pPlayMenu, &Item);
    
    Item.id = ID_EXIT;
    sprintf (Item.spName, "退出");
    InsertMenuItem (pPlayMenu, &Item);
    
    pItem = GetMenuItem (pMenu, 2001);
    pLevelMenu = CreateSubMenu (pItem);
    
    Item.id = ID_MAP_A;
    sprintf (Item.spName, "地图A");
    InsertMenuItem (pLevelMenu, &Item);
    
    Item.id = ID_MAP_B;
    sprintf (Item.spName, "地图B");
    InsertMenuItem (pLevelMenu, &Item);
    
    Item.id = ID_MAP_C;
    sprintf (Item.spName, "地图C");
    InsertMenuItem (pLevelMenu, &Item);
    
    Item.id = ID_MAP_D;
    sprintf (Item.spName, "地图D");
    InsertMenuItem (pLevelMenu, &Item);
    
    Item.id = ID_MAP_E;
    sprintf (Item.spName, "地图E");
    InsertMenuItem (pLevelMenu, &Item);
    
    Item.id = ID_MAP_F;
    sprintf (Item.spName, "地图F");
    InsertMenuItem (pLevelMenu, &Item);
    
    Item.id = ID_MAP_G;
    sprintf (Item.spName, "地图G");
    InsertMenuItem (pLevelMenu, &Item);
    
    Item.id = ID_MAP_H;
    sprintf (Item.spName, "地图H");
    InsertMenuItem (pLevelMenu, &Item);
    
    pItem = GetMenuItem (pMenu, 2002);
    pAboutMenu = CreateSubMenu (pItem);
    
    Item.id = ID_ABOUT;
    sprintf (Item.spName, "关于推箱子");
    InsertMenuItem (pAboutMenu, &Item);
    
    return pMenu;
}

bool32_t RegisterSokobanWndClass (void)
{
    WNDCLASS DetectWndClass;
    
    memset (&DetectWndClass, 0, sizeof (WNDCLASS));
    DetectWndClass.pClassName = "SokobanWnd";
    DetectWndClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    DetectWndClass.WinProc    = SokobanWndProc;
    
    return RegisterClass (&DetectWndClass);
}

WINDOW *CreateSokobanWnd(void)
{
    MENU *pMainMenu;
    WINDOW *hSokobanWnd;
    
    RegisterSokobanWndClass ();
    
    pMainMenu = SokobanCreateMainMenu ();
    
    hSokobanWnd = CreateWindow ("SokobanWnd", "推箱子", WS_MAINWND | WS_CAPTION | WS_BORDER, 
        150, 150, WIDTH_LARGEWIN, HEIGHT_LARGEWIN, NULL, pMainMenu, NULL);
    if (NULL == hSokobanWnd) {
        return NULL;
    }
    
    ShowWindow (hSokobanWnd, SW_SHOW);
    
    return hSokobanWnd;
}
