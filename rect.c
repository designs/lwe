/*
  Copyright (C), 2008, zhanbin
  File name:    rect.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081206
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081206    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

bool32_t SetPoint(LPPOINT lppt, int32_t x, int32_t y)
{
    if (NULL == lppt) {
        return FALSE;
    }
    
    lppt->x = x;
    lppt->y = y;
    
    return TRUE;
}

bool32_t SetRect(LPRECT lpRect, int32_t left, int32_t top, int32_t right, int32_t bottom)
{
    if (NULL == lpRect) {
        return FALSE;
    }
    
    lpRect->left   = left;
    lpRect->top    = top;
    lpRect->right  = right;
    lpRect->bottom = bottom;
    
    return TRUE;
}

bool32_t CopyRect(LPRECT lpdrc, LPRECT lpsrc)
{
    if (NULL == lpsrc) {
        return FALSE;
    }
    
    if (NULL == lpdrc) {
        return FALSE;
    }
    
    memcpy (lpdrc, lpsrc, sizeof (RECT));
    
    return TRUE;
}

bool32_t OffsetRect(LPRECT lpRect, LPPOINT lppt)
{
    if (NULL == lppt) {
        return FALSE;
    }
    
    if (NULL == lpRect) {
        return FALSE;
    }
    
    lpRect->left   += lppt->x;
    lpRect->top    += lppt->y;
    lpRect->right  += lppt->x;
    lpRect->bottom += lppt->y;
    
    return TRUE;
}

bool32_t PtInRect(const LPRECT lpRect, LPPOINT lppt)
{
    if (NULL == lppt) {
        return FALSE;
    }
    
    if (NULL == lpRect) {
        return FALSE;
    }
    
    if (   lppt->x >= lpRect->left 
        && lppt->x < lpRect->right 
        && lppt->y >= lpRect->top 
        && lppt->y < lpRect->bottom) {
        return TRUE;
    }
    
    return FALSE;
}

bool32_t NormalizeRect(LPRECT lpRect)
{
    int32_t iTemp;
    
    if (NULL == lpRect) {
        return FALSE;
    }

    if(lpRect->left > lpRect->right)
    {
         iTemp          = lpRect->left;
         lpRect->left   = lpRect->right;
         lpRect->right  = iTemp;
    }

    if(lpRect->top > lpRect->bottom)
    {
         iTemp          = lpRect->top;
         lpRect->top    = lpRect->bottom;
         lpRect->bottom = iTemp;
    }
    
    return TRUE;
}

bool32_t IntersectRECT(LPRECT pdrc, const LPRECT psrc1, const LPRECT psrc2)
{
    if (NULL == psrc1) {
        return FALSE;
    }
    
    if (NULL == psrc2) {
        return FALSE;
    }
    
    if (NULL == pdrc) {
        return FALSE;
    }
    
    pdrc->left = (psrc1->left > psrc2->left) ? psrc1->left : psrc2->left;
    pdrc->top  = (psrc1->top > psrc2->top) ? psrc1->top : psrc2->top;
    pdrc->right = (psrc1->right < psrc2->right) ? psrc1->right : psrc2->right;
    pdrc->bottom = (psrc1->bottom < psrc2->bottom) 
                   ? psrc1->bottom : psrc2->bottom;

    if(pdrc->left >= pdrc->right || pdrc->top >= pdrc->bottom) {
        return FALSE;
    }

    return TRUE;
}

bool32_t DoesIntersect (const RECT* psrc1, const RECT* psrc2)
{
    int left, top, right, bottom;
    
    if (NULL == psrc1) {
        return FALSE;
    }
    
    if (NULL == psrc2) {
        return FALSE;
    }
    
    left = (psrc1->left > psrc2->left) ? psrc1->left : psrc2->left;
    top  = (psrc1->top > psrc2->top) ? psrc1->top : psrc2->top;
    right = (psrc1->right < psrc2->right) ? psrc1->right : psrc2->right;
    bottom = (psrc1->bottom < psrc2->bottom) 
                   ? psrc1->bottom : psrc2->bottom;

    if(left >= right || top >= bottom) {
        return FALSE;
    }

    return TRUE;
}

bool32_t InflateRect(LPRECT lpRect, int cx, int cy)
{
    if (NULL == lpRect) {
        return FALSE;
    }
    
    lpRect->left   -= cx;
    lpRect->top    -= cy;
    lpRect->right  += cx;
    lpRect->bottom += cy;
    
    return TRUE;
}
