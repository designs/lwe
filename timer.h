#ifndef __TIMER_H__
#define __TIMER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define CARET_TIMER_ID  0x88888888

typedef void            (*TimerFunction)(WINDOW *,uint32_t,uint32_t);

struct _TIMER_T
{
    uint32_t                 id;                 // 定时器ID
    uint32_t                 nInterval;          // 时间间隔
    uint32_t                 nElapse;            // 已经经过的时间
    struct _WINDOW*    Window;             // 定时器所属窗口
    TimerFunction     Function;           // 定时器函数
    struct list_head    WndListNode;        // 窗口定时器节点
    struct list_head    SysListNode;        // 系统定时器节点
};

struct _TIMER
{
    MUTEX            TimerMutex;
    
    struct list_head    TimerListHead;
    
#ifdef ECOS
    cyg_handle_t        AlarmHandle;
    cyg_alarm           AlarmObject;
    
    THREAD*          pTimerThread;
#elif defined(LINUX)
    timer_t             TimerHandle;
#endif
};

bool32_t  InitTimer(void *);
bool32_t  SetTimer(WINDOW *, uint32_t, uint32_t, TimerFunction);
bool32_t  KillTimer(WINDOW *, uint32_t);
bool32_t  KillAllTimer(WINDOW *);
void ReleaseTimer(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
