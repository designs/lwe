#ifndef __PHY_DC_H__
#define __PHY_DC_H__

#ifdef __cplusplus
extern "C"
{
#endif

typedef COLORREF        (*get_pixel)(PHYDC *pPhyDC, int32_t x, int32_t y);
typedef void            (*set_pixel)(PHYDC *, int32_t, int32_t, COLORREF);
typedef void            (*line)(PHYDC *, int32_t, int32_t, int32_t, int32_t, COLORREF);
typedef void            (*fill_rect)(PHYDC *, RECT *rect, COLORREF);
typedef void            (*copy_rect)(PHYDC *pPhyDC, RECT *lpRect, void *pDes);
typedef void            (*set_rect)(PHYDC *pPhyDC, RECT *lpRect, void *pSrc);

struct _PHYDC
{
    void*               fb;                 // �Դ��ַ
    int32_t                 w;                  // ��Ļ��
    int32_t                 h;                  // ��Ļ��
    int32_t                 bpp;                // ��ɫλ��
    int32_t                 linesize;           // ���ֽ���
    
    get_pixel           get_pixel;          // ����
    set_pixel           set_pixel;          // ����
    line                line;               // ����
    fill_rect           fill_rect;          // �������
    copy_rect           copy_rect;          // ���ο���������
    set_rect            set_rect;           // ���忽��������
};

bool32_t InitPhyDC(PHYDC *, void *);
void ReleasePhyDC(PHYDC *);


#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
