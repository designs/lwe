/*
  Copyright (C), 2008~2009, zhanbin
  File name:    dumcursor.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090106
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090106    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

#ifdef DUM_CURSOR
bool32_t InitHWCursor(void)
{
    return TRUE;
}

void ReleaseHWCursor(void)
{
}

void SetHWCursor(CURSOR *pCursor)
{
}

void SetHWCursorPos(int32_t xpos, int32_t ypos, int32_t xoff, int32_t yoff)
{
}

void SetHWCursorEnable(bool32_t bEnable)
{
}
#endif
