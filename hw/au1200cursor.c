/*
  Copyright (C), 2008~2009, zhanbin
  File name:    au1200cursor.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090106
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090106    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"
#ifdef AU1200_CURSOR
#include    "cyg/devs/au1200_fbctl.h"

static int32_t fd;
static uint8_t CursorPattern[256];

bool32_t InitHWCursor(void)
{
    fd = open("/dev/fb0", O_RDWR);
    if (fd < 0) {
        return FALSE;
    }
    
    return TRUE;
}

void ReleaseHWCursor(void)
{
    close (fd);
}

void SetHWCursor(CURSOR *pCursor)
{
    int32_t err;
    uint16_t i, j, k, l;
    uint16_t *pDes, *td;
    uint8_t *pAndMask, *pXorMask, *ta, *tx;
    
    pAndMask = (uint8_t *)pCursor->pAndMask;
    pXorMask = (uint8_t *)pCursor->pXorMask;
    pDes = (uint16_t *)CursorPattern;
    
    memset (CursorPattern, 0x00, 256);
    for (i = 0; i < pCursor->h; i++) {
        td = pDes;
        ta = pAndMask;
        tx = pXorMask;
        for (j = 0; j < pCursor->w; j++) {
            k = j / 8;
            l = j % 8;
            
            if (!(ta[k] & (0x1 << (7 - l)))) {
                if (tx[k] & (0x1 << (7 - l))) {
                    td[k] = td[k] | (0x2 << (l * 2));
                }
                else {
                    td[k] = td[k] | (0x1 << (l * 2));
                }
            }
        }
        
        pDes     += 4;
        pAndMask += pCursor->w / 8;
        pXorMask += pCursor->w / 8;
    }
    
    err = cyg_fs_fsetinfo (fd, VIDEO_CTL_SET_CURSOR_PATTERN, CursorPattern, 256);
}

void SetHWCursorPos(int32_t xpos, int32_t ypos, int32_t xoff, int32_t yoff)
{
    int32_t err;
    au1200_lcd_cursor_t pos;
    
    pos.xpos = xpos;
    pos.ypos = ypos;
    pos.xoff = xoff;
    pos.yoff = yoff;
    
    err = cyg_fs_fsetinfo (fd, VIDEO_CTL_SET_CURSOR_POS, &pos, sizeof (pos));
}

void SetHWCursorEnable(bool32_t bEnable)
{
    int32_t err;
    
    err = cyg_fs_fsetinfo (fd, VIDEO_CTL_SET_CURSOR_ENABLE, &bEnable, sizeof (bEnable));
}
#endif
