#ifndef __MENU_H__
#define __MENU_H__

#include "window.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define MENU_TYPE_ITEM          0x0000
#define MENU_TYPE_SUBMENU       0x0001

struct _MENU;

typedef struct _MENUItem//菜单项
{
    int32_t                 nType;//类型
    int32_t                 id;//ID
    int8_t                  spName[32];//名称
    RECT                ItemRect;//项目矩形
    
    struct list_head    ChildListNode;//子列表节点
    
    struct _MENU*      pParentMenu;//父菜单
    struct _MENU*      pSubMenu;//子菜单
}MENUItem;

struct _MENU//菜单
{
    int8_t                  spName[32];//名称
    
    int32_t                 nItemWidth;//项目宽
    int32_t                 nItemHeight;//项目高
    
    int32_t                 nItemsPerCol;//
    
    POINT               BindWndPos;//外框窗口位置
    
    struct _WINDOW*    pMenuWnd;//菜单窗口
    struct _WINDOW*    pOldCaptureWnd;//旧的标题窗口
    
    struct _MENU*      pUpMenu;//更新的菜单
    
    struct _MENUItem*  pOldSelItem;//旧的选择项目
    
    int32_t                 nChildCount;//子项计数
    struct list_head    ChildListHead;//子列表头
};

bool32_t  RegisterMenuClass (void);
MENU *CreateMenu(int8_t *spName);
MENU *CreateSubMenu(MENUItem *pItem);
MENUItem *GetMenuItem(MENU *pMenu, int32_t id);
bool32_t  InsertMenuItem(MENU *pMenu, MENUItem *pItem);
void ShowMenu(MENU *pMenu, int32_t X, int32_t Y, WINDOW *pParent);
void HideMenu(MENU *pMenu);
bool32_t  DestroyMenu(MENU *pMenu);
WINDOW *GetMenuActiveWindow(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
