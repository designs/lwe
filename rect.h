#ifndef __RECT_H__
#define __RECT_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define RECTWP(prc)     (prc->right - prc->left)
#define RECTHP(prc)     (prc->bottom - prc->top)
#define RECTW(rc)       (rc.right - rc.left)
#define RECTH(rc)       (rc.bottom - rc.top)

bool32_t SetPoint(LPPOINT lppt, int32_t, int32_t);
bool32_t SetRect(LPRECT, int32_t, int32_t, int32_t, int32_t);
bool32_t CopyRect(LPRECT, LPRECT);
bool32_t OffsetRect(LPRECT, LPPOINT);
bool32_t PtInRect(const LPRECT, LPPOINT);
bool32_t NormalizeRect(LPRECT);
bool32_t IntersectRECT(LPRECT, const LPRECT, const LPRECT);
bool32_t DoesIntersect (const RECT* psrc1, const RECT* psrc2);
bool32_t InflateRect(LPRECT, int, int);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
