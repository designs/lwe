/*
  Copyright (C), 2008, zhanbin
  File name:    timer.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081213
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081213    zhanbin         创建文件
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "window.h"

static void DefTimerFun(void)
{
    TIMER *pSysTimer = pGUI->pSysTimer;
    TIMER_T* _list_;
    struct list_head *_head_ = &pSysTimer->TimerListHead;
    #undef _item_
    #define _item_ SysListNode
    
    MutexLock (&pSysTimer->TimerMutex);
    
    for ((_list_) = list_entry((_head_)->next, TIMER_T, _item_); 
        &((_list_)->_item_) != (_head_);
        (_list_) = list_entry((_list_)->_item_.next, TIMER_T, _item_)) {
        _list_->nElapse--;
        
        if (0 == _list_->nElapse) {
            _list_->nElapse = _list_->nInterval;
            
            if (NULL != _list_->Function) {
                _list_->Function (_list_->Window, _list_->id, (uint32_t)GetTime ());
            }
            else {
                PostMessage (_list_->Window, EV_TIMER, (uint32_t)_list_->id, (uint32_t)GetTime ());
            }
        }
    }
    
    MutexUnlock (&pSysTimer->TimerMutex);
}

#ifdef ECOS
static void eCosTimerFun(cyg_handle_t AlarmHandle, cyg_addrword_t data)
{
    TIMER *pSysTimer = pGUI->pSysTimer;
    
    ThreadResume (pSysTimer->pTimerThread);
}

static int32_t EcosTimerThreadFun(THREAD *pThread, void *pData)
{
    while (TRUE) {
        ThreadSuspend (pThread);
        
        DefTimerFun ();
    }
}
#elif defined(LINUX)
static void LinuxTimerFun(sigval_t v)
{
    DefTimerFun ();
}
#endif

static TIMER_T* FindTimerByID(WINDOW *Window, uint32_t id)
{
    TIMER_T* _list_;
    struct list_head *_head_ = &Window->TimerListHead;
    #undef _item_
    #define _item_ WndListNode
    
    for ((_list_) = list_entry((_head_)->next, TIMER_T, _item_); 
        &((_list_)->_item_) != (_head_);
        (_list_) = list_entry((_list_)->_item_.next, TIMER_T, _item_)) {
        if (_list_->id == id) {
            return _list_;
        }
    }
    
    return NULL;
}

bool32_t SetTimer(WINDOW *Window, uint32_t id, uint32_t nTimeOut, TimerFunction Function)
{
    TIMER_T *Timer;
    TIMER *pSysTimer = pGUI->pSysTimer;
    
    MutexLock (&pSysTimer->TimerMutex);
    
    Timer = FindTimerByID (Window, id);
    if (NULL == Timer) {
        if((Timer = (TIMER_T*)Malloc (sizeof (TIMER_T))) == NULL){
            MutexUnlock (&pSysTimer->TimerMutex);
            
            return FALSE;
        }
        
        memset(Timer,0,sizeof(TIMER_T));
        
        list_add(&Timer->WndListNode, &Window->TimerListHead);
        list_add(&Timer->SysListNode, &pSysTimer->TimerListHead);
    }
    
    Timer->id        = id;
    Timer->nInterval = nTimeOut;
    Timer->nElapse   = nTimeOut;
    Timer->Function  = Function;
    Timer->Window    = Window;
    
    MutexUnlock (&pSysTimer->TimerMutex);
    
    return TRUE;
}

bool32_t KillTimer(WINDOW *Window, uint32_t id)
{
    TIMER_T *Timer;
    TIMER *pSysTimer = pGUI->pSysTimer;
    
    MutexLock (&pSysTimer->TimerMutex);
    
    Timer = FindTimerByID (Window, id);
    if(Timer == NULL) {
        MutexUnlock (&pSysTimer->TimerMutex);
        
        return FALSE;
    }
    
    list_del (&Timer->SysListNode);
    list_del (&Timer->WndListNode);
    
    Free (Timer);
    
    MutexUnlock (&pSysTimer->TimerMutex);
    
    return TRUE;
}

bool32_t KillAllTimer(WINDOW *Window)
{
    uint32_t id;
    bool32_t bUnlock = FALSE;
    TIMER *pSysTimer = pGUI->pSysTimer;
    TIMER_T* _list_;
    struct list_head *_head_ = &Window->TimerListHead;
    #undef _item_
    #define _item_ WndListNode
    
    MutexLock (&pSysTimer->TimerMutex);
    
    while (!list_empty (_head_)) {
        _list_ = list_entry((_head_)->next, TIMER_T, _item_);
        id = _list_->id;
        bUnlock = TRUE;
        
        MutexUnlock (&pSysTimer->TimerMutex);
        
        KillTimer (Window, id);
    }
    
    if (FALSE == bUnlock) {
        MutexUnlock (&pSysTimer->TimerMutex);
    }
    
    return TRUE;
}

bool32_t InitTimer(void *pParam)
{
    TIMER *pSysTimer;
/*#ifdef ECOS
    cyg_handle_t ClockHandle;
    cyg_handle_t CountHandle;
#elif  defined(LINUX)*/
    struct sigevent se;// 事件,定时器到期时的事件，包括到期执行函数
    struct itimerspec ts, ots;//定义时间间隔
//#endif
    
    pSysTimer = (TIMER *)Malloc (sizeof (TIMER));
    if (NULL == pSysTimer) {
        return FALSE;
    }
    
    if (FALSE == MutexCreate (&pSysTimer->TimerMutex)) {
        Free (pSysTimer);
        
        return FALSE;
    }
    
    LIST_HEAD_RESET(&pSysTimer->TimerListHead);
/*    
#ifdef ECOS
    pSysTimer->pTimerThread = ThreadCreate (EcosTimerThreadFun, NULL);
    if (NULL == pSysTimer->pTimerThread) {
        return FALSE;
    }
    ThreadSetPriority (pSysTimer->pTimerThread, 4);
    
    ClockHandle = cyg_real_time_clock ();
    cyg_clock_to_counter (ClockHandle, &CountHandle);
    
    cyg_alarm_create (CountHandle, eCosTimerFun, (cyg_addrword_t)NULL, 
        &pSysTimer->AlarmHandle, &pSysTimer->AlarmObject);
    cyg_alarm_initialize (pSysTimer->AlarmHandle, cyg_current_time() + 1, 1);
#elif defined(LINUX)*/
    memset (&se, 0, sizeof (se));
    se.sigev_notify            = SIGEV_THREAD;//定时器到期时将启动新的线程进行需要的处理
    se.sigev_notify_function   = LinuxTimerFun;//定时发送EV_TIME
    se.sigev_notify_attributes = NULL;
    se.sigev_value.sival_int   = 0;
    if (timer_create (CLOCK_REALTIME, &se, &pSysTimer->TimerHandle) < 0) {//创建定时器
        ReleaseMutex (&pSysTimer->TimerMutex);
        Free (pSysTimer);
        
        return FALSE;
    }
    
    ts.it_value.tv_sec     = 0;
    ts.it_value.tv_nsec    = 10000;
    ts.it_interval.tv_sec  = ts.it_value.tv_sec;
    ts.it_interval.tv_nsec = ts.it_value.tv_nsec;
    if (timer_settime (pSysTimer->TimerHandle, TIMER_ABSTIME, &ts, &ots) < 0) {
        ReleaseMutex (&pSysTimer->TimerMutex);
        timer_delete (pSysTimer->TimerHandle);
        Free (pSysTimer);
        
        return FALSE;
    }
//#endif
    
    pGUI->pSysTimer = pSysTimer;
    
    return TRUE;
}

void ReleaseTimer(void)
{
    TIMER *pSysTimer = pGUI->pSysTimer;
    
#ifdef ECOS
    cyg_alarm_delete (pSysTimer->AlarmHandle);
#elif defined(LINUX)
    timer_delete (pSysTimer->TimerHandle);
#endif
    
    ReleaseMutex (&pSysTimer->TimerMutex);
    
    pGUI->pSysTimer = NULL;
    
    Free (pSysTimer);
}
