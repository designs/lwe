#ifndef     __WINDEF_H__
#define     __WINDEF_H__

#ifdef      __cplusplus
extern "C" {
#endif

#ifdef ECOS
#include    <cyg/kernel/kapi.h>
#include    <cyg/infra/cyg_type.h>
#include    <cyg/infra/diag.h>
#include    <cyg/io/io.h>
#include    <sys/select.h>
#include    <sys/time.h>
#include    <sys/stat.h>                /* stat */
#include    <stdio.h>
#include    <stdlib.h>
#include    <string.h>
#include    <math.h>
#include    <unistd.h>
#include    <time.h>
#include    <fcntl.h>
#include    <ctype.h>                   /* toupper */
#include    <errno.h>
#elif defined(LINUX)
#include    <stdio.h>
#include    <stdlib.h>
#include    <string.h>
#include    <stddef.h>
#include    <errno.h>

#include    <ctype.h>
#include    <unistd.h>
#include    <fcntl.h>
#include    <string.h>
#include    <math.h>
#include    <semaphore.h>
#include    <pthread.h>
#include    <termios.h>
#include    <time.h>
#include    <signal.h>
#include    <stdarg.h>                  /* for va_list */

#include    <sys/time.h>
#include    <sys/timeb.h>
#include    <sys/ioctl.h>
#include    <sys/types.h>
#include    <sys/stat.h>
#include    <sys/mman.h>

#include    <linux/fb.h>
#include    <linux/kd.h>
#include    <linux/vt.h>
#endif
#ifndef LINUX
typedef          char           int8_t;

#else
typedef unsigned char           uint8_t;

typedef unsigned short          uint16_t;
typedef          short          int16_t;

typedef unsigned int            uint32_t;
typedef          int            int32_t;

typedef unsigned long long      uint64_t;
typedef          long long      int64_t;

typedef  int32_t                    bool32_t;    /* BOOL */
#endif

#ifndef FALSE
#define FALSE                   0
#endif

#ifndef TRUE
#define TRUE                    1
#endif

#ifndef NULL
#ifdef      __cplusplus
#define NULL                    0
#else
#define NULL                    ((void*)0)
#endif
#endif

typedef struct tagRECT
{
    int32_t                         left;
    int32_t                         top;
    int32_t                         right;
    int32_t                         bottom;
} RECT, *PRECT, *LPRECT;

typedef struct tagPOINT
{
    int32_t                         x;
    int32_t                         y;
} POINT, *PPOINT, *LPPOINT;

typedef struct tagSIZE
{
    int32_t                         cx;
    int32_t                         cy;
} SIZE, *PSIZE, *LPSIZE;


typedef uint32_t                     COLORREF;

#if defined(FB_16) || defined(FB_VNC16)
#define RGB(r,g,b)              ((uint16_t)(((b & 0xf8) >> 3) | ((g & 0xfc) << 3) | ((r & 0xf8) << 8)))

#define GetRValue(rgb)          ((uint8_t)((rgb & 0xf800) >> 8))
#define GetGValue(rgb)          ((uint8_t)((rgb & 0x07e0) >> 3))
#define GetBValue(rgb)          ((uint8_t)((rgb & 0x001f) <<3 ))
#elif defined(FB_24)
#define RGB(r,g,b)              ((uint32_t)((r << 16) | (g << 8) | (b)))

#define GetRValue(rgb)          ((uint8_t)(rgb >> 16))
#define GetGValue(rgb)          ((uint8_t)(rgb >> 8))
#define GetBValue(rgb)          ((uint8_t)(rgb))
#elif defined(FB_32)
#define RGB(r,g,b)              ((uint32_t)((r << 16) | (g << 8) | (b)))

#define GetRValue(rgb)          ((uint8_t)(rgb >> 16))
#define GetGValue(rgb)          ((uint8_t)(rgb >> 8))
#define GetBValue(rgb)          ((uint8_t)(rgb))
#endif

#ifdef ECOS
#define ROOT_PATH               "/gui"
#elif defined(LINUX)
#define ROOT_PATH               "./../gui"
#endif

#ifndef MAX_PATH
#define MAX_PATH                256
#endif

#ifndef ABS
#define ABS(x)                  (((x)<0) ? -(x) : (x))
#endif
#define SWAP(x,y)               {(x) ^= (y); (y) ^= (x); (x) ^= (y);}

#define MIN(a,b)              ((a) < (b) ? (a) : (b))
#define MAX(a,b)              ((a) > (b) ? (a) : (b))

#define LOint8_t(l)                 ((int8_t)(int16_t)(l))
#define HIint8_t(l)                 ((int8_t)((((int16_t)(l)) >> 8) & 0xFF))
#define LOuint8_t(l)                 ((uint8_t)(uint16_t)(l))
#define HIuint8_t(l)                 ((uint8_t)((((uint16_t)(l)) >> 8) & 0xFF))
#define MAKEuint16_t(low, high)      ((uint16_t)(((uint8_t)(low)) | (((uint16_t)((uint8_t)(high))) << 8)))

#define LOint16_t(l)                ((int16_t)(int32_t)(l))
#define HIint16_t(l)                ((int16_t)((((int32_t)(l)) >> 16) & 0xFFFF))
#define LOuint16_t(l)                ((uint16_t)(uint32_t)(l))
#define HIuint16_t(l)                ((uint16_t)((((uint32_t)(l)) >> 16) & 0xFFFF))
#define MAKEuint32_t(low, high)      ((uint32_t)(((uint16_t)(low)) | (((uint32_t)((uint16_t)(high))) << 16)))

#define GUIDEBUG             DebugPrint

#define VERSIONSTR              "C_OS-0.1"

#ifdef      __cplusplus
}
#endif

#endif
