/*
  Copyright (C), 2008, zhanbin
  File name:    winmisc.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081220
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081220    zhanbin         创建文件
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

bool32_t IsWindowEnabled(WINDOW *Window)
{
    return (0 == (Window->nStyle & WS_DISABLED));
}

bool32_t WindowIsVisible(WINDOW *Window)
{
    return (0 != (Window->nStyle & WS_VISIBLE));
}

bool32_t WindowIsTrans(WINDOW *Window)
{
    return (0 != (Window->nStyle & WS_TRANS));
}

bool32_t WindowIsTabStop(WINDOW *Window)
{
    return (0 != (Window->nStyle & WS_TABSTOP));
}

bool32_t WindowHaveNC(WINDOW *Window)//窗口有无效区
{
    return (WindowHaveCaption (Window) | WindowHaveBoard (Window) | 
        (WindowHaveHScrollBar (Window) & WindowHaveVScrollBar (Window)));
}

bool32_t WindowHaveCaption(WINDOW *Window)
{
    return (0 != (Window->nStyle & WS_CAPTION));
}

bool32_t WindowHaveBoard(WINDOW *Window)
{
    return (0 != (Window->nStyle & WS_BORDER));
}

bool32_t WndIsMainWnd(WINDOW *Window)
{
    if (FALSE == WindowIsMainWnd (Window)) {
        return FALSE;
    }
    
    return (NULL != Window->pMenu);
}

bool32_t WindowHaveHScrollBar(WINDOW *Window)
{
    return (0 != (Window->nStyle & WS_HSCROLL));
}

bool32_t WindowHaveVScrollBar(WINDOW *Window)
{
    return (0 != (Window->nStyle & WS_VSCROLL));
}

bool32_t WindowIsDesktop(WINDOW *Window)
{
    return (WS_DESKTOP == (Window->nStyle & WS_TYPE_MASK));
}

bool32_t WindowIsMainWnd(WINDOW *Window)
{
    return (WS_MAINWND == (Window->nStyle & WS_TYPE_MASK));
}

bool32_t WindowIsControl(WINDOW *Window)
{
    return (WS_CONTROL == (Window->nStyle & WS_TYPE_MASK));
}

bool32_t  WindowIsDialog(WINDOW *Window)
{
    return (WS_DIALOG == (Window->nStyle & WS_TYPE_MASK));
}

bool32_t  WindowIsMenu(WINDOW *Window)
{
    return (WS_MENU == (Window->nStyle & WS_TYPE_MASK));
}

bool32_t  WindowIsIme(WINDOW *Window)
{
    return (WS_IME == (Window->nStyle & WS_TYPE_MASK));
}

bool32_t  WindowIsTopmost(WINDOW *Window)
{
    return (0 != (Window->nStyle & WS_TOPMOST));
}

bool32_t  WindowNeedIme(WINDOW *Window)
{
    return SendMessage (Window, EV_NEEDIME, 0, 0);
}

bool32_t IsWindowNcActive(WINDOW* Window)
{
    return Window->bNcActive;
}

bool32_t WindowIsSysControl(WINDOW *Window)
{
    return (WS_SYSCONTROL == (Window->nStyle & WS_SYSCONTROL));
}

bool32_t GetWindowRect(WINDOW *Window, LPRECT lpRect)
{
    CopyRect (lpRect, &Window->WindowRect);
    
    return TRUE;
}
//把window的窗口矩形，改成以左上为0,0,去掉标题、边框、菜单、卷栏后的矩形给lpRect
bool32_t GetClientRect(WINDOW *Window, LPRECT lpRect)
{
    int32_t w, h, left, top, right, bottom;
    
    GetWindowRect (Window, lpRect);
    w = lpRect->right - lpRect->left;
    h = lpRect->bottom - lpRect->top;
    
    switch (Window->nStyle & (WS_CAPTION | WS_BORDER)) {
    case (WS_CAPTION | WS_BORDER):
        left   = 0;
        top    = 0;
        right  = w - 2 * GetSystemMetrics (SM_CXBORDER);
        bottom = h - GetSystemMetrics (SM_CYCAPTION) - GetSystemMetrics (SM_CYBORDER);
        break;
        
    case (WS_CAPTION):
        left   = 0;
        top    = 0;
        right  = w;
        bottom = h - GetSystemMetrics (SM_CYCAPTION);
        break;
        
    case (WS_BORDER):
        left   = 0;
        top    = 0;
        right  = w - 2 * GetSystemMetrics (SM_CXBORDER);
        bottom = h - 2 * GetSystemMetrics (SM_CYBORDER);
        break;
        
    default:
        left   = 0;
        top    = 0;
        right  = w;
        bottom = h;
        break;
    }
    
    if (WndIsMainWnd (Window)) {
        bottom -= GetSystemMetrics (SM_CYMENU);
    }
    
    if (WindowHaveHScrollBar (Window)) {
        bottom -= GetSystemMetrics (SM_CYHSCROLL);
    }
    
    if (WindowHaveVScrollBar (Window)) {
        right -= GetSystemMetrics (SM_CXVSCROLL);
    }
    
    SetRect (lpRect, left, top, right, bottom);
    
    return TRUE;
}

bool32_t  GetCloseBoxRect(WINDOW *Window, LPRECT lpRect)
{
    GetWindowRect (Window, lpRect);
	lpRect->left   = lpRect->right - GetSystemMetrics (SM_CXCLOSEBOX) - 2;
	lpRect->top    = lpRect->top   + 2;
	lpRect->right  = lpRect->left  + GetSystemMetrics (SM_CXCLOSEBOX);
	lpRect->bottom = lpRect->top   + GetSystemMetrics (SM_CYCLOSEBOX);
	
	return TRUE;
}
//点偏移到客户区
void ClientToWindow(WINDOW *Window, LPPOINT lpPoint)
{
    int32_t left, top;
    
    switch (Window->nStyle & (WS_CAPTION | WS_BORDER)) {
    case (WS_CAPTION | WS_BORDER):
        left   = GetSystemMetrics (SM_CXBORDER);
        top    = GetSystemMetrics (SM_CYCAPTION);
        break;
        
    case (WS_CAPTION):
        left   = 0;
        top    = GetSystemMetrics (SM_CYCAPTION);
        break;
        
    case (WS_BORDER):
        left   = GetSystemMetrics (SM_CXBORDER);
        top    = GetSystemMetrics (SM_CYBORDER);
        break;
        
    default:
        left   = 0;
        top    = 0;
        break;
    }
    
    if (WndIsMainWnd (Window)) {
        top += GetSystemMetrics (SM_CYMENU);
    }
    
    lpPoint->x += left;
    lpPoint->y += top;
}

void WindowToClient(WINDOW *Window, LPPOINT lpPoint)
{
    int32_t left, top;
    
    switch (Window->nStyle & (WS_CAPTION | WS_BORDER)) {
    case (WS_CAPTION | WS_BORDER):
        left   = GetSystemMetrics (SM_CXBORDER);
        top    = GetSystemMetrics (SM_CYCAPTION);
        break;
        
    case (WS_CAPTION):
        left   = 0;
        top    = GetSystemMetrics (SM_CYCAPTION);
        break;
        
    case (WS_BORDER):
        left   = GetSystemMetrics (SM_CXBORDER);
        top    = GetSystemMetrics (SM_CYBORDER);
        break;
        
    default:
        left   = 0;
        top    = 0;
        break;
    }
    
    if (WndIsMainWnd (Window)) {
        top += GetSystemMetrics (SM_CYMENU);
    }
    
    lpPoint->x -= left;
    lpPoint->y -= top;
}
//点偏移，恢复到窗口屏幕中起点位置
void WindowToScreen(WINDOW *Window, LPPOINT lpPoint)
{
    lpPoint->x += Window->WindowRect.left;
    lpPoint->y += Window->WindowRect.top;
}

void ScreenToWindow(WINDOW *Window, LPPOINT lpPoint)
{
    lpPoint->x -= Window->WindowRect.left;
    lpPoint->y -= Window->WindowRect.top;
}

//点偏移到窗口在屏幕的客户区起点
void ClientToScreen(WINDOW *Window, LPPOINT lpPoint)
{
//点偏移到客户区
    ClientToWindow (Window, lpPoint);
//点偏移到窗口屏幕中的起点位置
    WindowToScreen (Window, lpPoint);
}

void ScreenToClient(WINDOW *Window, LPPOINT lpPoint)
{
    ScreenToWindow (Window, lpPoint);//得到窗口的左上角
    WindowToClient (Window, lpPoint);//根据类型左上角，再次左上偏移
}

void ChildToParent(WINDOW *Window, LPPOINT lpPoint)
{
    lpPoint->x += Window->ParentPos.x;
    lpPoint->y += Window->ParentPos.y;
}

void ParentToChild(WINDOW *Window, LPPOINT lpPoint)
{
    lpPoint->x -= Window->ParentPos.x;
    lpPoint->y -= Window->ParentPos.y;
}

void ClientToWindowRect(WINDOW *Window, LPRECT lpRect)//有标题和边框，相应菜单的矩形下移
{
    LPPOINT lpPoint;
    
    lpPoint = (LPPOINT)lpRect;//只取left/top 到point的x/y
    ClientToWindow (Window, lpPoint);//移动左上角
    lpPoint++;//right/bottom
    ClientToWindow (Window, lpPoint);//移动右下角
}
//矩形参考点由窗口左上角，改为客户区左上角
void WindowToClientRect(WINDOW *Window, LPRECT lpRect)
{
    LPPOINT lpPoint;
    
    lpPoint = (LPPOINT)lpRect;
    WindowToClient (Window, lpPoint);
    lpPoint++;
    WindowToClient (Window, lpPoint);
}

void WindowToScreenRect(WINDOW *Window, LPRECT lpRect)
{
    LPPOINT lpPoint;
    
    lpPoint = (LPPOINT)lpRect;
    WindowToScreen (Window, lpPoint);
    lpPoint++;
    WindowToScreen (Window, lpPoint);
}
////矩形参考点由屏幕左上角，改为窗口左上角
void ScreenToWindowRect(WINDOW *Window, LPRECT lpRect)
{
    LPPOINT lpPoint;
    
    lpPoint = (LPPOINT)lpRect;
    ScreenToWindow (Window, lpPoint);
    lpPoint++;
    ScreenToWindow (Window, lpPoint);
}

void ClientToScreenRect(WINDOW *Window, LPRECT lpRect)
{
    LPPOINT lpPoint;
    //左上变更
    lpPoint = (LPPOINT)lpRect;
    ClientToScreen (Window, lpPoint);
	//右下变更
    lpPoint++;
    ClientToScreen (Window, lpPoint);
}

void ScreenToClientRect(WINDOW *Window, LPRECT lpRect)
{
    LPPOINT lpPoint;
    
    lpPoint = (LPPOINT)lpRect;
    ScreenToClient (Window, lpPoint);
    lpPoint++;
    ScreenToClient (Window, lpPoint);
}

void ChildToParentRect(WINDOW *Window, LPRECT lpRect)
{
    LPPOINT lpPoint;
    
    lpPoint = (LPPOINT)lpRect;
    ChildToParent (Window, lpPoint);
    lpPoint++;
    ChildToParent (Window, lpPoint);
}

void ParentToChildRect(WINDOW *Window, LPRECT lpRect)
{
    LPPOINT lpPoint;
    
    lpPoint = (LPPOINT)lpRect;
    ParentToChild (Window, lpPoint);
    lpPoint++;
    ParentToChild (Window, lpPoint);
}

bool32_t GetWindowMenuRect(WINDOW *Window, LPRECT lpRect)
{
    if (!WndIsMainWnd (Window)) {
        return FALSE;
    }
    
    GetClientRect (Window, lpRect);//把window的窗口矩形，改成以左上为0,0,去掉标题、边框、菜单、卷栏后的矩形给lpRect
    
    lpRect->bottom = lpRect->top;
    lpRect->top   -= GetSystemMetrics (SM_CYMENU);//在window窗口之上添加菜单,lpRect矩形在window窗口之上
    
    ClientToWindowRect (Window, lpRect);
    
    return TRUE;
}

bool32_t GetWindowScrollBarRect(WINDOW *Window, bool32_t bHorz, LPRECT lpRect)
{
    if (bHorz && !WindowHaveHScrollBar (Window)) {
        return FALSE;
    }
    
    if (!bHorz && !WindowHaveVScrollBar (Window)) {
        return FALSE;
    }
    
    GetClientRect (Window, lpRect);
    
    if (bHorz) {
        lpRect->top     = lpRect->bottom;
        lpRect->bottom += GetSystemMetrics (SM_CYHSCROLL);
    }
    else {
        lpRect->left    = lpRect->right;
        lpRect->right  += GetSystemMetrics (SM_CXVSCROLL);
    }
    
    ClientToWindowRect (Window, lpRect);
    
    return TRUE;
}

bool32_t SetScrollRange(WINDOW *Window, int32_t nBar, int32_t nMinPos, int32_t nMaxPos, bool32_t bRedraw)
{
    WINDOW *pScroll;
    
    if (NULL == Window) {
        return FALSE;
    }
    
    switch (nBar) {
    case SB_CTL:
        return -1;
        break;
        
    case SB_VERT:
        pScroll = Window->pVSrollBar;
        if (NULL == pScroll) {
            return FALSE;
        }
        break;
        
    case SB_HORZ:
        pScroll = Window->pHSrollBar;
        if (NULL == pScroll) {
            return FALSE;
        }
        break;
        
    default:
        return FALSE;
    }
    
    SendMessage (pScroll, SBM_SETRANGE, nMinPos, nMaxPos);
    if (bRedraw) {
        InvalidateRect(pScroll, NULL, FALSE);
    }
    
    return TRUE;
}

int32_t SetScrollPos(WINDOW *Window, int32_t nBar, int32_t nPos)
{
    WINDOW *pScroll;
    
    if (NULL == Window) {
        return -1;
    }
    
    switch (nBar) {
    case SB_CTL:
        return -1;
        break;
        
    case SB_VERT:
        pScroll = Window->pVSrollBar;
        if (NULL == pScroll) {
            return -1;
        }
        break;
        
    case SB_HORZ:
        pScroll = Window->pHSrollBar;
        if (NULL == pScroll) {
            return -1;
        }
        break;
        
    default:
        return -1;
    }
    
    return SendMessage (pScroll, SBM_SETPOS, nPos, TRUE);
}

int32_t GetScrollPos(WINDOW *Window, int32_t nBar)
{
    WINDOW *pScroll;
    
    if (NULL == Window) {
        return -1;
    }
    
    switch (nBar) {
    case SB_CTL:
        return -1;
        break;
        
    case SB_VERT:
        pScroll = Window->pVSrollBar;
        if (NULL == pScroll) {
            return -1;
        }
        break;
        
    case SB_HORZ:
        pScroll = Window->pHSrollBar;
        if (NULL == pScroll) {
            return -1;
        }
        break;
        
    default:
        return -1;
    }
    
    return SendMessage (pScroll, SBM_GETPOS, 0, 0);
}

bool32_t EnableScrollBar (WINDOW *Window, int32_t nBar, bool32_t bEnable)
{
    WINDOW *pScroll;
    
    if (NULL == Window) {
        return FALSE;
    }
    
    switch (nBar) {
    case SB_VERT:
        pScroll = Window->pVSrollBar;
        if (NULL == pScroll) {
            return FALSE;
        }
        break;
        
    case SB_HORZ:
        pScroll = Window->pHSrollBar;
        if (NULL == pScroll) {
            return FALSE;
        }
        break;
        
    default:
        return FALSE;
    }
    
    EnableWindow (pScroll, bEnable);
    
    return TRUE;
}

int32_t GetDeviceCaps(int32_t nIndex)
{
    int32_t Caps = -1;
    PHYDC *pDC = &pGUI->PhyDC;
    
    switch (nIndex) {
    case HORZRES:
        Caps = pDC->w;
        break;
        
    case VERTRES:
        Caps = pDC->h;
        break;
        
    case BITSPIXEL:
        Caps = pDC->bpp;
        break;
    }
    
    return Caps;
}

int32_t GetSystemMetrics(int32_t nIndex)
{
    int32_t nMetrics = -1;
    PHYDC *pDC = &pGUI->PhyDC;
    
    switch (nIndex) {
    case SM_CXSCREEN:
        nMetrics = pDC->w;
        break;
        
    case SM_CYSCREEN:
        nMetrics = pDC->h;
        break;
        
    case SM_CXVSCROLL:
        nMetrics = 16;
        break;
        
    case SM_CYHSCROLL:
        nMetrics = 16;
        break;
        
    case SM_CYCAPTION:
        nMetrics = 20;
        break;
        
    case SM_CXBORDER:
        nMetrics = 1;
        break;
        
    case SM_CYBORDER:
        nMetrics = 1;
        break;
        
    case SM_CYMENU:
        nMetrics = 20;
        break;
        
    case SM_CXCLOSEBOX:
    case SM_CYCLOSEBOX:
        nMetrics = 16;
        break;
    }
    
    return nMetrics;
}

WINDOW *GetRootWindow(WINDOW* Window)
{
    WINDOW *Find, *Parent, *Desktop;
    
    Desktop = GetDesktopWindow ();
    
    Find    = Window;
    do {
        Parent = GetParent (Find);
    } while (Parent != Desktop);
    
    return Find;
}

uint32_t GetWindowStyle (WINDOW *hWnd)
{
    return hWnd->nStyle;
}

bool32_t ExcludeWindowStyle (WINDOW *hWnd, uint32_t dwStyle)
{
    hWnd->nStyle &= ~dwStyle;
    
    return TRUE;
}

bool32_t IncludeWindowStyle (WINDOW *hWnd, uint32_t dwStyle)
{
    hWnd->nStyle |= dwStyle;
    
    return TRUE;
}

const int8_t *GetWindowCaption (WINDOW *hWnd)
{
    return hWnd->pCaption;
}

void NotifyParent(WINDOW *hWnd, int32_t id, int32_t code)
{
    SendMessage (GetParent (hWnd), EV_COMMAND, 
        (uint32_t)MAKEuint32_t (id, code), (uint32_t)hWnd);
}

bool32_t RegisterClass(WNDCLASS *pWndClass)
{
    WNDCLASS *pNewNode;
//用malloc 分配内存后，用memcpy 将字符串复制进去。若用字符数组指明长度就不会错。而用malloc则提示
    pNewNode = (WNDCLASS *)Malloc (sizeof (WNDCLASS));
    if (NULL == pNewNode) {
        return FALSE;
    }
    
    memcpy (pNewNode, pWndClass, sizeof (WNDCLASS));
    pNewNode->pClassName = (int8_t*)Malloc (strlen (pWndClass->pClassName) + 1);//申请名字（补上“\0”）空间，把名字空间地址给窗口结构中的窗口名指针
    if (NULL == pNewNode->pClassName) {
        Free (pNewNode);
        return FALSE;
    }
    strcpy (pNewNode->pClassName, pWndClass->pClassName);//把传入的（临时），给已在内存分配的窗口类指针
    list_add (&pNewNode->WCListNode, &pGUI->WCListHead);//链表挂接
    
    return TRUE;
}

void UnregisterClass(WNDCLASS *pWndClass)
{
}

WNDCLASS *GetClassInfo(const int8_t* pClassName)
{
    WNDCLASS* _list_;
    struct list_head *_head_ = &pGUI->WCListHead;
    #undef _item_
    #define _item_ WCListNode
    
    for ((_list_) = list_entry((_head_)->next, WNDCLASS, _item_); 
        &((_list_)->_item_) != (_head_);
        (_list_) = list_entry((_list_)->_item_.next, WNDCLASS, _item_)) {
        if (0 == strcmp (_list_->pClassName, pClassName)) {
            return _list_;
        }
    }
    
    return NULL;
}
