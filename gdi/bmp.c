/*
  Copyright (C), 2008, zhanbin
  File name:    bmp.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081206
  Description:
  Others:
    代码来自MINIGUI，在此表示感谢。
  Function List:
  History:
    Date        Author          Modification
    20081210    zhanbin         解决16位显示问题，解码后倒转图片。
    20081206    zhanbin         功能实现
*/
#include "window.h"

#define PIX2BYTES(n)    (((n)+7)/8)
/*
 * compute image line size and bytes per pixel
 * from bits per pixel and width
 */
static int32_t bmpComputePitch(int32_t bpp, uint32_t width, uint32_t *pitch, bool32_t does_round)
{
    int32_t linesize;
    int32_t bytespp = 1;

    if(bpp == 1)
        linesize = PIX2BYTES (width);
    else if(bpp <= 4)
        linesize = PIX2BYTES (width << 2);
    else if (bpp <= 8)
        linesize = width;
    else if(bpp <= 16) {
        linesize = width * 2;
        bytespp = 2;
    } else if(bpp <= 24) {
        linesize = width * 3;
        bytespp = 3;
    } else {
        linesize = width * 4;
        bytespp = 4;
    }

    /* rows are DWORD right aligned */
    if (does_round)
        *pitch = (linesize + 3) & -4;
    else
        *pitch = linesize;
    return bytespp;
}

static uint8_t fp_getc(FILE *fp)
{
    uint8_t ch;

    fread(&ch, 1, 1, fp);

    return ch;
}

static uint16_t fp_igetw(FILE *fp)
{
    uint16_t ch;

    fread(&ch, 2, 1, fp);

    return ch;
}

/* read_RLE8_compressed_image:
 *  For reading the 8 bit RLE compressed BMP image format.
 */
static void read_RLE8_compressed_image (FILE *f, uint8_t *bits, int32_t pitch, BITMAPINFOHEADER *infoheader)
{
    uint8_t count, val, val0;
    int32_t j, pos, line;
    int32_t eolflag, eopicflag;

    eopicflag = 0;
    line = infoheader->biHeight - 1;

    while (eopicflag == 0) {
        pos = 0;                               /* x position in bitmap */
        eolflag = 0;                           /* end of line flag */

        while ((eolflag == 0) && (eopicflag == 0)) {
            count = fp_getc(f);
            val = fp_getc(f);

            if (count > 0) {                    /* repeat pixel count times */
                for (j=0;j<count;j++) {
                    bits [pos] = val;
                    pos++;
                }
            }
            else {
                switch (val) {
                case 0:                       /* end of line flag */
                    eolflag=1;
                    break;
                
                case 1:                       /* end of picture flag */
                    eopicflag=1;
                    break;
                
                case 2:                       /* displace picture */
                    count = fp_getc(f);
                    val = fp_getc(f);
                    pos += count;
                    line -= val;
                    break;
                
                default:                      /* read in absolute mode */
                    for (j=0; j<val; j++) {
                        val0 = fp_getc(f);
                        bits [pos] = val0;
                        pos++;
                    }
                
                    if (j%2 == 1)
                        val0 = fp_getc(f);    /* align on word boundary */
                    break;
                }
            }

            if (pos-1 > (int)infoheader->biWidth)
                eolflag=1;
        }

        bits += pitch;
        line--;
        if (line < 0)
            eopicflag = 1;
    }
}

/* read_RLE4_compressed_image:
 *  For reading the 4 bit RLE compressed BMP image format.
 */
static void read_RLE4_compressed_image (FILE *f, uint8_t *bits, int32_t pitch, BITMAPINFOHEADER *infoheader)
{
   uint8_t b[8];
   uint8_t count;
   uint16_t val0, val;
   int32_t j, k, pos, line;
   int32_t eolflag, eopicflag;

   eopicflag = 0;                            /* end of picture flag */
   line = infoheader->biHeight - 1;

    while (eopicflag == 0) {
        pos = 0;
        eolflag = 0;                           /* end of line flag */

        while ((eolflag == 0) && (eopicflag == 0)) {
        count = fp_getc(f);
        val = fp_getc(f);

        if (count > 0) {                    /* repeat pixels count times */
            b[1] = val & 15;
            b[0] = (val >> 4) & 15;
            for (j=0; j<count; j++) {
                if (pos % 2 == 0)
                    bits [pos/2] = b[j%2] << 4;
                else
                    bits [pos/2] = bits [pos/2] | b[j%2];
                pos++;
            }
        }
        else {
            switch (val) {
            case 0:                       /* end of line */
                eolflag=1;
                break;
            
            case 1:                       /* end of picture */
                eopicflag=1;
                break;
            
            case 2:                       /* displace image */
                count = fp_getc(f);
                val = fp_getc(f);
                pos += count;
                line -= val;
                break;
            
            default:                      /* read in absolute mode */
                for (j=0; j<val; j++) {
                    if ((j%4) == 0) {
                        val0 = fp_igetw(f);
                        for (k=0; k<2; k++) {
                            b[2*k+1] = val0 & 15;
                            val0 = val0 >> 4;
                            b[2*k] = val0 & 15;
                            val0 = val0 >> 4;
                        }
                    }
                    if (pos % 2 == 0)
                        bits [pos/2] = b[j%4] << 4;
                    else
                        bits [pos/2] = bits [pos/2] | b[j%4];
                    pos++;
                }
                break;
            }
        }

        if (pos-1 > (int)infoheader->biWidth)
            eolflag=1;
        }
        
        bits += pitch;
        line--;
        if (line < 0)
            eopicflag = 1;
    }
}

BITMAP *LoadBmpFile(int8_t *pFileName)
{
    FILE *fp;
    uint16_t tmp;
    BITMAP *pBmp;
    uint8_t *bits, *spp, *sp;
    uint32_t i, j, k, l, m, ncol;
    uint32_t pitch, biSize;
    RGBQUAD pal[256], *bmBits, *dpp, *dp;
    BITMAPFILEHEADER fileheader;
    BITMAPINFOHEADER infoheader;
    OS2BMPINFOHEADER os2infoheader;
    
    pBmp = Malloc (sizeof (BITMAP));
    if ( NULL == pBmp ) {
        return NULL;
    }
    
    pBmp->bmType = -1;
    pBmp->bmBits = NULL;
    
    if ( (fp = fopen (pFileName, "rb")) == NULL ) {
        return NULL;
    }
    
    fread (&fileheader, sizeof(BITMAPFILEHEADER), 1, fp);
    if (fileheader.bfType != 19778) {
        fclose (fp);
        return NULL;
    }
    
    fread (&infoheader, sizeof(BITMAPINFOHEADER), 1, fp);
    if ( infoheader.biSize == 40 ) {
        ncol = (fileheader.bfOffBits - 54) >> 2;
        if ( ncol > 256 ) {
            fclose (fp);
            return NULL;
        }
        for ( i = 0; i < ncol; i++ ) {
            fread (&pal[i], sizeof(RGBQUAD), 1, fp);
        }
    }
    else {
        fseek (fp, 14, SEEK_SET);
        fread (&os2infoheader, sizeof(OS2BMPINFOHEADER), 1, fp);
        infoheader.biBitCount    = os2infoheader.biBitCount;
        infoheader.biHeight      = os2infoheader.biHeight;
        infoheader.biWidth       = os2infoheader.biWidth;
        infoheader.biPlanes      = os2infoheader.biPlanes;
        infoheader.biSize         = os2infoheader.biSize;
        infoheader.biCompression = 0;
        ncol = (fileheader.bfOffBits - 26) / 3;
        if ( ncol > 256 ) {
            fclose (fp);
            return NULL;
        }
        for ( i = 0; i < ncol; i++ ) {
            fread (&pal[i], sizeof(RGBQUAD) - 1, 1, fp);
        }
    }

    bmpComputePitch (infoheader.biBitCount, infoheader.biWidth, &pitch, TRUE);
    biSize = pitch * infoheader.biHeight;

    if( !(bits = Malloc (biSize)) ) {
        fclose (fp);
        return NULL;
    }

    fseek (fp, fileheader.bfOffBits, SEEK_SET);

    switch (infoheader.biCompression) {
    case BI_RGB:
        fread (bits, biSize, 1, fp);
        break;

    case BI_RLE8:
        read_RLE8_compressed_image (fp, bits, pitch, &infoheader);
        break;

    case BI_RLE4:
        read_RLE4_compressed_image (fp, bits, pitch, &infoheader);
        break;

    default:
        fclose (fp);
        Free (bits);
        return NULL;
    }

    bmBits = Malloc (infoheader.biWidth * infoheader.biHeight * sizeof(RGBQUAD));
    if ( NULL == bmBits ) {
        fclose (fp);
        Free (bits);
        return NULL;
    }

    for ( i = 0, k = (infoheader.biHeight - 1) * pitch, m = 0; i < (uint32_t)infoheader.biHeight; i++ ) {
        spp = &bits[k];
        dpp = &bmBits[m];
        for ( j = 0, l = 0; j < (uint32_t)infoheader.biWidth; j++ ) {
            switch ( infoheader.biBitCount ) {
            case 1:
                sp = &spp[j >> 3];
                dp = &dpp[j];
                tmp = ( *((uint8_t *)sp) >> (7 - (j & 0x7)) ) & 0x1;
                memcpy (dp, &pal[tmp], sizeof(RGBQUAD));
                break;
                
            case 4:
                sp = &spp[j >> 1];
                dp = &dpp[j];
                tmp = ( *((uint8_t *)sp) >> (4 - ((j & 0x1) << 2) ) ) & 0xf;
                memcpy (dp, &pal[tmp], sizeof(RGBQUAD));
                break;
                
            case 8:
                sp = &spp[j];
                dp = &dpp[j];
                tmp = *((uint8_t *)sp);
                memcpy (dp, &pal[tmp], sizeof(RGBQUAD));
                break;
                
            case 16:
                sp = &spp[l];
                dp = &dpp[j];
                tmp = *((uint16_t *)sp);
                dp->rgbRed   = ((tmp >> 10) & 0x1f) << 3;/* RGB555 */
                dp->rgbGreen = ((tmp >>  5) & 0x1f) << 3;
                dp->rgbBlue  = ((tmp >>  0) & 0x1f) << 3;
                dp->rgbReserved = 0;
                l += 2;
                break;
                
            case 24:
                sp = &spp[l];
                dp = &dpp[j];
                memcpy (dp, sp, 3);
                dp->rgbReserved = 0;
                l += 3;
                break;
                
            case 32:
                sp = &spp[l];
                dp = &dpp[j];
                memcpy (dp, sp, 4);
                l += 4;
                break;
            }
        }
        k -= pitch;
        m += infoheader.biWidth;
    }
    
    pBmp->bmType       = 0;
    pBmp->bmWidth      = infoheader.biWidth;
    pBmp->bmHeight     = infoheader.biHeight;
    pBmp->bmBitsPixel  = 32;
    pBmp->bmWidthBytes = infoheader.biWidth * 4;
    pBmp->bmPlanes     = 1;
    pBmp->bmBits       = bmBits;
    
    Free (bits);
    fclose (fp);
    
    return pBmp;
}

void UnloadBitmap(BITMAP *pBmp)
{
    if ( pBmp && pBmp->bmBits ) {
        Free (pBmp->bmBits);
        pBmp->bmBits = NULL;
    }
}
