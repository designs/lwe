#ifndef __CLIP_H__
#define __CLIP_H__

#ifdef __cplusplus
extern "C"
{
#endif

bool32_t ClipLine(LPRECT, int32_t *, int32_t *, int32_t *, int32_t *);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
