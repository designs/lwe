/*
  Copyright (C), 2008~2009, zhanbin
  File name:    icon.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090218
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090218    zhanbin         ����ʵ��
*/
#include "window.h"

static ICON *CreateIcon(int32_t nWidth,int32_t nHeight,
    const void *pvANDPlane,const void *pvXORPlane)
{
    ICON *pIcon;
    
    pIcon = (ICON *)Malloc (sizeof (ICON));
    if (NULL == pIcon) {
        return NULL;
    }
    
    pIcon->w = nWidth;
    pIcon->h = nHeight;
    
    pIcon->pAndMask = (void *)pvANDPlane;
    pIcon->pXorMask = (void *)pvXORPlane;
    
    return pIcon;
}

static uint8_t *Icon2Glyph(uint8_t *pMask, int32_t nWidth, int32_t nHeight, int32_t nBit)
{
    switch (nBit) {
    case 1:
    {
        int32_t i;
        uint8_t *pGlyph, *pSrc;
        
        pGlyph = (uint8_t *)Malloc (nWidth * nHeight >> 3);
        if (NULL == pGlyph) {
            return NULL;
        }
        
        pSrc = pMask;
        pGlyph += nWidth * nHeight >> 3;
        for (i = 0; i < nHeight; i++) {
            pGlyph -= nWidth >> 3;
            memcpy (pGlyph, pSrc, nWidth >> 3);
            pSrc += nWidth >> 3;
        }
        return pGlyph;
        break;
    }
    
    default:
        break;
    }
    
    return NULL;
}

static uint8_t *Icon2Bmp(uint8_t *pMask, int32_t nWidth, int32_t nHeight, int32_t nBit, const RGBQUAD *pPal)
{
    switch (nBit) {
    case 4:
    {
        uint8_t *pSrc;
        int32_t i, j, t = 0;
        RGBQUAD *pGlyph, *pDes;
        
        pGlyph = (RGBQUAD *)Malloc (nWidth * nHeight * sizeof (RGBQUAD));
        if (NULL == pGlyph) {
            return NULL;
        }
        
        pSrc = pMask;
        pGlyph += nWidth * nHeight;
        for (i = 0; i < nHeight; i++) {
            pDes = pGlyph -= nWidth;
            for (j = 0; j < nWidth; j++) {
                if (j % 2 == 0) {
                    t = *pSrc++;
                }
                
                if (j % 2 == 0) {
                    *pDes = pPal[(t & 0xf0) >> 4];
                }
                else {
                    *pDes = pPal[t & 0x0f];
                }
                
                pDes++;
            }
        }
        
        return (uint8_t *)pGlyph;
        break;
    }
    
    case 8:
    {
        uint8_t *pSrc;
        int32_t i, j;
        RGBQUAD *pGlyph, *pDes;
        
        pGlyph = (RGBQUAD *)Malloc (nWidth * nHeight * sizeof (RGBQUAD));
        if (NULL == pGlyph) {
            return NULL;
        }
        
        pSrc = pMask;
        pGlyph += nWidth * nHeight;
        for (i = 0; i < nHeight; i++) {
            pDes = pGlyph -= nWidth;
            for (j = 0; j < nWidth; j++) {
                *pDes = pPal[*pSrc];
                
                pSrc++;
                pDes++;
            }
        }
        
        return (uint8_t *)pGlyph;
        break;
    }
    
    default:
        break;
    }
    
    return NULL;
}

ICON *LoadIconFile(int8_t *pFileName)
{
    FILE *fp;
    ICON *pIcon;
    ICONDIR icondir;
    ICONFILEHEADER fileheader;
    BITMAPINFOHEADER infoheader;
    uint8_t *pPal, *pData, *pAndMask, *pXorMask;
    uint32_t nBmpWidth, nGlyphWidth, nImageSize;
    
    if ( (fp = fopen (pFileName, "rb")) == NULL ) {
        return FALSE;
    }
    
    fread (&fileheader, sizeof(ICONFILEHEADER), 1, fp);
    if (fileheader.idType != 1) {
        fclose (fp);
        return NULL;
    }
    
    fread (&icondir, sizeof(ICONDIR), 1, fp);
    if (icondir.bWidth % 8 || icondir.bHeight % 8) {
        fclose (fp);
        return NULL;
    }
    
    fseek(fp, icondir.dwImageOffset, SEEK_SET);
    fread (&infoheader, sizeof(BITMAPINFOHEADER), 1, fp);
    if (infoheader.biPlanes != 1 || 
        infoheader.biBitCount < 4 || 
        infoheader.biBitCount > 8) {
        fclose (fp);
        return NULL;
    }
    
    pPal = Malloc (sizeof (RGBQUAD) << infoheader.biBitCount);
    if (NULL == pPal) {
        fclose (fp);
        return NULL;
    }
    fread (pPal, sizeof (RGBQUAD) << infoheader.biBitCount, 1, fp);
    
    nGlyphWidth = ((icondir.bWidth + 31) >> 5) << 5;
    nBmpWidth = (((infoheader.biBitCount * icondir.bWidth) + 31) >> 5) << 5;
    nImageSize = ((nBmpWidth + nGlyphWidth) >> 3) * icondir.bHeight;
    
    pData = (uint8_t *)Malloc (nImageSize);
    if (NULL == pData) {
        Free (pPal);
        fclose (fp);
        return NULL;
    }
    
    fread (pData, nImageSize, 1, fp);
    
    fclose (fp);
    
    pXorMask = Icon2Bmp (pData, icondir.bWidth, icondir.bHeight, infoheader.biBitCount, (const RGBQUAD *)pPal);
    if (NULL == pXorMask) {
        Free (pData);
        Free (pPal);
        return NULL;
    }
    
    pAndMask = Icon2Glyph (pData + nImageSize - (nGlyphWidth >> 3) * icondir.bHeight, 
        nGlyphWidth, icondir.bHeight, 1);
    if (NULL == pAndMask) {
        Free (pXorMask);
        Free (pData);
        Free (pPal);
        return NULL;
    }
    
    Free (pData);
    Free (pPal);
    
    pIcon = CreateIcon (icondir.bWidth, icondir.bHeight, pAndMask, pXorMask);
    if (NULL == pIcon) {
        Free (pXorMask);
        Free (pAndMask);
        return NULL;
    }
    
    return pIcon;
}

bool32_t DestroyIcon(ICON * pIcon)
{
    if (NULL == pIcon) {
        return FALSE;
    }
    
    Free (pIcon->pAndMask);
    Free (pIcon->pXorMask);
    Free (pIcon);
    
    return TRUE;
}

void DrawIcon(DC* pDC, int32_t x, int32_t y, int32_t w, int32_t h, ICON *pIcon)
{
    uint8_t *pAndMask, *ta;
    RGBQUAD *pXorMask, rgb;
    RECT BoundRect, ValidRect;
    CLIPREGION* ClipRegion = pDC->ClipRegion;
    int32_t i, j, k, l, m, ssx, ssy, dx, dy, cw, ch, nAndMaskLineSize;
    
    if (NULL == pIcon) {
        return;
    }
    
    if (w <= 0 || h <= 0) {
        w = pIcon->w;
        h = pIcon->h;
    }
    
    SetRect (&BoundRect, x, y, x + w, y + h);
    
    switch (pDC->nType) {
    case TYPE_WINDOW_DC:
        WindowToScreenRect (pDC->pWnd, &BoundRect);
        break;
        
    case TYPE_CLIENT_DC:
        ClientToScreenRect (pDC->pWnd, &BoundRect);
        break;
        
    default:
        return;
        break;
    }
    
    nAndMaskLineSize = ((pIcon->w + 31) >> 5) << 2;
    
    pAndMask = pIcon->pAndMask;
    pXorMask = pIcon->pXorMask;
    for (i = 0; i < ClipRegion->numRects; i++) {
        if (FALSE == IntersectRECT (&ValidRect, &BoundRect, &ClipRegion->rects[i])) {
            continue;
        }
        
#ifdef SOFT_CURSOR
        GdiCheckSoftCursor (&ValidRect, TRUE);
#endif
        
        ssx = ValidRect.left - BoundRect.left;
        ssy = ValidRect.top  - BoundRect.top;
        dx  = ValidRect.left;
        dy  = ValidRect.top;
        cw  = ValidRect.right  - ValidRect.left;
        ch  = ValidRect.bottom - ValidRect.top;
        for (j = 0; j < ch; j++) {
            ta = pAndMask + (ssy + j) * nAndMaskLineSize;
            for (k = 0; k < cw; k++) {
                l = (ssx + k) >> 3;
                m = (ssx + k) % 8;
                
                if (ta[l] & (0x1 << (7 - m))) {
                }
                else {
                    rgb = pXorMask[(ssy + j) * pIcon->w + (ssx + k)];
                    pDC->pPhyDC->set_pixel (pDC->pPhyDC, dx + k, dy + j, 
                        RGB (rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue));
                }
            }
        }
        
#ifdef SOFT_CURSOR
        GdiCheckSoftCursor (&ValidRect, FALSE);
#endif
    }
}

bool32_t GetIconSize (ICON *pIcon, int32_t* w, int32_t* h)
{
    if (NULL == pIcon) {
        return FALSE;
    }
    
    *w = pIcon->w;
    *h = pIcon->h;
    
    return TRUE;
}
