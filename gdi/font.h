#ifndef __FONT_H__
#define __FONT_H__

#include "window.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define FONT_CHINESS        ROOT_PATH"/pfont/GBK.dat"
#define FONT_ASCII          ROOT_PATH"/pfont/ASCII.dat"

#define MUL32(X)            (X<<5)
#define MUL192(X)           ((X<<7)+(X<<6))
#define MUL200(X)           ((X<<7)+(X<<6)+(X<<3))

#define FONTPOINT 16

#if     FONTPOINT == 16
    #define FONT_POINT      16
    #define SIZE_FONT       32L
#elif     FONTPOINT == 24
    #define FONT_POINT      24
    #define SIZE_FONT       72L
#elif     FONTPOINT == 32
    #define FONT_POINT      32
    #define SIZE_FONT       128L
#elif     FONTPOINT == 40
    #define FONT_POINT      40
    #define SIZE_FONT       200L
#elif     FONTPOINT == 48
    #define FONT_POINT      48
    #define SIZE_FONT       288L
#endif

void LoadASCII(FILE *fp, int32_t asc, uint8_t *outfont);
void LoadChiness(FILE *fp, int8_t *s, uint8_t *outfont);

void GetTextExtent (DC *hdc, const int8_t* text, int32_t len, SIZE* size);
int32_t GetTextExtentPoint(DC* hdc, const int8_t* text, int32_t len, int32_t max_extent, 
                int32_t* fit_chars, int32_t* pos_chars, int32_t* dx_chars, SIZE* size);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
