/*
  Copyright (C), 2008, zhanbin
  File name:    font.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081206
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081206    zhanbin         功能实现
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "font.h"

void LoadASCII(FILE *fp, int32_t asc, uint8_t *outfont)
{
    uint32_t location;
    
    location = asc * SIZE_FONT >> 1;
    fseek (fp, location, SEEK_SET);
    fread (outfont, SIZE_FONT >> 1, 1, fp); 
}

void LoadChiness(FILE *fp, int8_t *s, uint8_t *outfont)
{
    uint8_t qh,wh;
    uint32_t location;
    
    qh = *s - 0x81;
    wh = *(s + 1) - 0x40;
    
    location = (MUL192(qh) + (wh)) * SIZE_FONT; 
    fseek (fp, location, SEEK_SET);
    fread (outfont, SIZE_FONT, 1, fp);  
}
//获取文本的字数到text，文本的像素长度到size
void GetTextExtent (DC *hdc, const int8_t* text, int32_t len, SIZE* size)
{
    int32_t len_cur_char, width_cur_char;
    int32_t left_bytes = len;
    int32_t cx = 0;
    
    if (len == -1) {
        len = strlen (text);
    }
    
    size->cy = 0;
    size->cx = 0;
    while (left_bytes) {
        if (text[0] == '\0') {
            size->cx = (size->cx < cx) ? cx : size->cx;
            if (0 != cx) {
                size->cy += FONT_POINT;
            }
            break;
        }
        else if (text[0] == '\r') {
            if (text[1] == '\n') {
                len_cur_char = 2;
                width_cur_char = 0;
            }
            else {
                len_cur_char = 1;
                width_cur_char = 0;
            }
        }
        else if (text[0] == '\n') {
            len_cur_char = 1;
            width_cur_char = 0;
        }
        else if (text[0] & 0x80) {
            len_cur_char = 2;
            width_cur_char = FONT_POINT;
        }
        else {
            len_cur_char = 1;
            width_cur_char = FONT_POINT >> 1;
        }
        
        if (0 == width_cur_char) {
            size->cx = (size->cx < cx) ? cx : size->cx;
            cx  = 0;
            size->cy += FONT_POINT;
        }
        else {
            cx += width_cur_char;
        }
        
        left_bytes -= len_cur_char;
        text += len_cur_char;
    }
}

int32_t GetTextExtentPoint(DC* hdc, const int8_t* text, int32_t len, int32_t max_extent, 
                int32_t* fit_chars, int32_t* pos_chars, int32_t* dx_chars, SIZE* size)
{
    int32_t len_cur_char, width_cur_char;
    int32_t left_bytes = len;
    int32_t char_count = 0;
    
    if (len == -1) {
        len = strlen (text);
    }
    
    size->cy = FONT_POINT;
    size->cx = 0;
    while (left_bytes) {
        if (pos_chars)
            pos_chars [char_count] = len - left_bytes;
        if (dx_chars)
            dx_chars [char_count] = size->cx;
            
        if (text[0] == '\0') {
            break;
        }
        else if (text[0] & 0x80) {
            len_cur_char = 2;
            width_cur_char = FONT_POINT;
        }
        else {
            len_cur_char = 1;
            width_cur_char = FONT_POINT >> 1;
        }
        
        width_cur_char += 0;
        
        if (max_extent > 0 && (size->cx + width_cur_char) > max_extent) {
            goto ret;
        }
        
        char_count ++;
        size->cx += width_cur_char;
        left_bytes -= len_cur_char;
        text += len_cur_char;
    }

ret:
    if (fit_chars) *fit_chars = char_count;
    return len - left_bytes;
}
