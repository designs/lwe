/*
  Copyright (C), 2008~2009, zhanbin
  File name:    gdi.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.2
  Date:         20090110
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090220    zhanbin         新的画圆算法，代码来自GGI
    20090110    zhanbin         功能实现
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "window.h"

COLORREF GetFgColor(DC* pDC)
{
    return pDC->fgPixel;
}

COLORREF SetFgColor(DC* pDC, COLORREF crColor)
{
    COLORREF OldColor;
    
    OldColor = pDC->fgPixel;
    pDC->fgPixel = crColor;
    
    return OldColor;
}

COLORREF GetBkColor(DC* pDC)
{
    return pDC->bkPixel;
}

COLORREF SetBkColor(DC* pDC, COLORREF crColor)
{
    COLORREF OldColor;
    
    OldColor = pDC->bkPixel;
    pDC->bkPixel = crColor;
    
    return OldColor;
}

int32_t GetBkMode(DC* pDC)
{
    return pDC->bkMode;
}

int32_t SetBkMode(DC* pDC, uint32_t mode)
{
    uint32_t OldMode;
    
    if (mode < TRANSPARENT) {
        mode = TRANSPARENT;
    }
    else if (mode > OPAQUE) {
        mode = OPAQUE;
    }
    
    OldMode = pDC->bkMode;
    pDC->bkMode = mode;
    
    return OldMode;
}

int32_t GetPenType(DC* pDC)
{
    return pDC->lineStyle;
}

int32_t SetPenType(DC* pDC, int32_t type)
{
    int32_t OldStyle;
    
    OldStyle = pDC->lineStyle;
    pDC->lineStyle = type;
    
    return OldStyle;
}

int32_t GetBrushType(DC* pDC)
{
    return pDC->fillStyle;
}

int32_t SetBrushType(DC* pDC, int32_t type)
{
    int32_t OldStyle;
    
    OldStyle = pDC->fillStyle;
    pDC->fillStyle = type;
    
    return OldStyle;
}

COLORREF GetPixel(DC* pDC, int32_t x, int32_t y)
{
    POINT p;
    
    p.x = x;
    p.y = y;
    
    switch (pDC->nType) {
    case TYPE_WINDOW_DC:
        WindowToScreen (pDC->pWnd, &p);
        break;
        
    case TYPE_CLIENT_DC:
        ClientToScreen (pDC->pWnd, &p);
        break;
        
    default:
        return RGB (0, 0, 0);
        break;
    }
    
    if (PtInRegion (pDC->ClipRegion, p.x, p.y)) {
        return pDC->pPhyDC->get_pixel (pDC->pPhyDC, p.x, p.y);
    }
    
    return RGB (0, 0, 0);
}

void SetPixel(DC* pDC, int32_t x, int32_t y, COLORREF crColor)
{
    POINT p;
    
    p.x = x;
    p.y = y;
    
    switch (pDC->nType) {
    case TYPE_WINDOW_DC:
        WindowToScreen (pDC->pWnd, &p);
        break;
        
    case TYPE_CLIENT_DC:
        ClientToScreen (pDC->pWnd, &p);
        break;
        
    default:
        return;
        break;
    }
    
    MoveToEx (pDC, x, y, NULL);
    
    if (PtInRegion (pDC->ClipRegion, p.x, p.y)) {
#ifdef SOFT_CURSOR
        RECT BoundRect;
        
        SetRect (&BoundRect, p.x, p.y, p.x, p.y);
        InflateRect (&BoundRect, 1, 1);
        GdiCheckSoftCursor (&BoundRect, TRUE);
#endif
        
        pDC->pPhyDC->set_pixel (pDC->pPhyDC, p.x, p.y, pDC->fgPixel);
        
#ifdef SOFT_CURSOR
        GdiCheckSoftCursor (&BoundRect, FALSE);
#endif
    }
}

bool32_t MoveToEx(DC* pDC, int32_t x, int32_t y, POINT *pPoint)
{
    if (NULL != pPoint) {
        pPoint->x = pDC->CurPos.x;
        pPoint->y = pDC->CurPos.y;
    }
    
    pDC->CurPos.x = x;
    pDC->CurPos.y = y;
    
    return TRUE;
}

static void Line0(DC* pDC, POINT *p1, POINT *p2)
{
    RECT BoundRect, ValidRect;
    int32_t i, x00, y00, x11, y11;
    CLIPREGION* ClipRegion = pDC->ClipRegion;
    
    SetRect (&BoundRect, p1->x, p1->y, p2->x, p2->y);
    NormalizeRect (&BoundRect);
    InflateRect (&BoundRect, 1, 1);
    
    if (FALSE == IntersectRECT (&ValidRect, &BoundRect, &ClipRegion->extents)) {
        return;
    }
    
    for (i = 0; i < ClipRegion->numRects; i++) {
        if (FALSE == IntersectRECT (&ValidRect, &BoundRect, &ClipRegion->rects[i])) {
            continue;
        }
        
        x00 = p1->x;
        y00 = p1->y;
        x11 = p2->x;
        y11 = p2->y;
        if (0 == ClipLine (&ValidRect, &x00, &y00, &x11, &y11)) {
            continue;
        }
        
#ifdef SOFT_CURSOR
        GdiCheckSoftCursor (&ValidRect, TRUE);
#endif
        
        pDC->pPhyDC->line (pDC->pPhyDC, x00, y00, x11, y11, pDC->fgPixel);
        
#ifdef SOFT_CURSOR
        GdiCheckSoftCursor (&ValidRect, FALSE);
#endif
    }
}

bool32_t LineTo(DC* pDC, int32_t x, int32_t y)
{
    POINT p1, p2;
    
    p1.x = pDC->CurPos.x;
    p1.y = pDC->CurPos.y;
    p2.x = x;
    p2.y = y;
    
    switch (pDC->nType) {
    case TYPE_WINDOW_DC:
        WindowToScreen (pDC->pWnd, &p1);
        WindowToScreen (pDC->pWnd, &p2);
        break;
        
    case TYPE_CLIENT_DC:
        ClientToScreen (pDC->pWnd, &p1);
        ClientToScreen (pDC->pWnd, &p2);
        break;
        
    default:
        return FALSE;
        break;
    }
    
    Line0 (pDC, &p1, &p2);
    
    pDC->CurPos.x = x;
    pDC->CurPos.y = y;
    
    return TRUE;
}

void PolylineTo (DC* pDC, const POINT* pt, int32_t num)
{
    int32_t i;
    
    MoveToEx (pDC, pt[0].x, pt[0].y, NULL);
    
    for (i = 1; i < num; i++) {
        LineTo (pDC, pt[i].x, pt[i].y);
    }
}

bool32_t Rectangle(DC* pDC, int32_t x0, int32_t y0, int32_t x1, int32_t y1)
{
    MoveToEx (pDC, x0, y0, NULL);
    
    LineTo (pDC, x0, y1);
    LineTo (pDC, x1, y1);
    LineTo (pDC, x1, y0);
    LineTo (pDC, x0, y0);
    
    return TRUE;
}

void DrawRect(DC* pDC, RECT *lpRect)
{
    int32_t x0, y0, x1, y1;
    
    x0 = lpRect->left;
    x1 = lpRect->right - 1;
    y0 = lpRect->top;
    y1 = lpRect->bottom - 1;
    
    MoveToEx (pDC, x0, y0, NULL);
    
    LineTo (pDC, x0, y1);
    LineTo (pDC, x1, y1);
    LineTo (pDC, x1, y0);
    LineTo (pDC, x0, y0);
}

void FillRect(DC* pDC, RECT *lpRect)
{
    int32_t i;
    RECT BoundRect, ValidRect;
    CLIPREGION* ClipRegion = pDC->ClipRegion;
    
    CopyRect (&BoundRect, lpRect);
    NormalizeRect (&BoundRect);
    
    switch (pDC->nType) {
    case TYPE_WINDOW_DC:
        WindowToScreenRect (pDC->pWnd, &BoundRect);
        break;
        
    case TYPE_CLIENT_DC:
        ClientToScreenRect (pDC->pWnd, &BoundRect);
        break;
        
    default:
        return;
        break;
    }
    
    for (i = 0; i < ClipRegion->numRects; i++) {
        if (FALSE == IntersectRECT (&ValidRect, &BoundRect, &ClipRegion->rects[i])) {
            continue;
        }
        
#ifdef SOFT_CURSOR
        GdiCheckSoftCursor (&ValidRect, TRUE);
#endif
        
        pDC->pPhyDC->fill_rect (pDC->pPhyDC, &ValidRect, pDC->fgPixel);
        
#ifdef SOFT_CURSOR
        GdiCheckSoftCursor (&ValidRect, FALSE);
#endif
    }
}

void Circle(DC* pDC, int32_t xc, int32_t yc, int32_t r)
{
    int32_t x,y,od,md,sd;
    
    x = r;
    y = md = 0;
    while (x >= y) {
        SetPixel (pDC, xc-y, yc-x, pDC->fgPixel);
        SetPixel (pDC, xc+y, yc-x, pDC->fgPixel);
        SetPixel (pDC, xc+x, yc-y, pDC->fgPixel);
        SetPixel (pDC, xc-x, yc-y, pDC->fgPixel);
        SetPixel (pDC, xc-x, yc+y, pDC->fgPixel);
        SetPixel (pDC, xc+x, yc+y, pDC->fgPixel);
        SetPixel (pDC, xc+y, yc+x, pDC->fgPixel);
        SetPixel (pDC, xc-y, yc+x, pDC->fgPixel);
        od = md + y + y + 1;
        sd = od - x - x - 1;
        y++;
        md = od;
        if (ABS (sd) < ABS (od)) {
            x--;
            md = sd;
        }
    }
}

static void DrawTextGlyph(DC* pDC, int32_t x,int32_t y,uint8_t *pIcon,int32_t w,int32_t h)
{
    uint16_t i,j,k;
    uint16_t sx,sy;
    uint16_t size,pos;
    RECT BoundRect;
    
    SetRect (&BoundRect, x, y, x + w, y + h);
    
    if (RECT_OUT == RectInRegion (pDC->ClipRegion, &BoundRect)) {
        return;
    }
    
#ifdef SOFT_CURSOR
    GdiCheckSoftCursor (&BoundRect, TRUE);
#endif
    
    size = w >> 3;
    for (i = 0, pos = 0, sy = y; i < h; i++, sy++) {
        for (j = 0, sx = x; j < size; j++, pos++) {
            for (k = 0; k < 8; k++, sx++) {
                if (PtInRegion (pDC->ClipRegion, sx, sy)) {
                    if ((pIcon[pos] & (0x80 >> k)) != 0) {
                        pDC->pPhyDC->set_pixel (pDC->pPhyDC, sx, sy, pDC->fgPixel);
                    }
                    else {
                        if (OPAQUE == GetBkMode (pDC)) {
                            pDC->pPhyDC->set_pixel (pDC->pPhyDC, sx, sy, pDC->bkPixel);
                        }
                    }
                }
            }
        }
    }
    
#ifdef SOFT_CURSOR
    GdiCheckSoftCursor (&BoundRect, FALSE);
#endif
}

int32_t TextOutLen (DC* pDC, int32_t x, int32_t y, const int8_t *lpString, int32_t nCount)
{
    int32_t i;
    POINT p1;
    int8_t *pText;
    FILE *fpa, *fpc;
    uint8_t outfont[SIZE_FONT];
    
    if (nCount < 0) {
        nCount = strlen (lpString);
    }
    
    if (nCount == 0) {
        return 0;
    }
    
    if ((fpa = fopen (FONT_ASCII, "rb")) == NULL) {
        DebugPrint ("Can't open ASCII.dat!\n");
        return 0;
    }
    
    if ((fpc = fopen (FONT_CHINESS, "rb")) == NULL) {
        DebugPrint ("Can't open GBK.dat!\n");
        return 0;
    }
    
    p1.x = x;
    p1.y = y;
    switch (pDC->nType) {
    case TYPE_WINDOW_DC:
        WindowToScreen (pDC->pWnd, &p1);
        break;
        
    case TYPE_CLIENT_DC:
        ClientToScreen (pDC->pWnd, &p1);
        break;
        
    default:
        fclose (fpa);
        fclose (fpc);
        return 0;
        break;
    }
    
    x = p1.x;
    y = p1.y;
    
    pText = (int8_t *)lpString;
    for (i = 0; i < nCount; i++) {
        if (*pText & 0x80) {     /* HZ */
            LoadChiness (fpc, pText, outfont);
            DrawTextGlyph (pDC, x, y, outfont, FONT_POINT, FONT_POINT);
            x += FONT_POINT;
            pText++;
            i++;
        }
        else {                      /* ASCII */
            LoadASCII (fpa, *pText, outfont);
            DrawTextGlyph (pDC, x, y, outfont, FONT_POINT >> 1, FONT_POINT);
            x += FONT_POINT >> 1;
        }
        pText++;
    }
    
    fclose (fpa);
    fclose (fpc);
    
    return nCount;
}

void DrawText(DC* pDC, const int8_t *lpString, int32_t nCount, RECT *lpRect, uint32_t uFormat)
{
    int8_t *pText;
    int32_t i, x, y;
    RECT BoundRect;
    FILE *fpa, *fpc;
    uint8_t outfont[SIZE_FONT];
    CLIPREGION *OldRng, *NewRng;
    
    if (nCount < 0) {
        nCount = strlen (lpString);
    }
    
    if (nCount == 0) {
        return;
    }

    if ((fpa = fopen (FONT_ASCII, "rb")) == NULL) {
        DebugPrint ("Can't open ASCII.dat!\n");
        return;
    }
    
    if ((fpc = fopen (FONT_CHINESS, "rb")) == NULL) {
        DebugPrint ("Can't open GBK.dat!\n");
        return;
    }
    
    CopyRect (&BoundRect, lpRect);
    NormalizeRect (&BoundRect);
    
    switch (pDC->nType) {
    case TYPE_WINDOW_DC:
        WindowToScreenRect (pDC->pWnd, &BoundRect);
        break;
        
    case TYPE_CLIENT_DC:
        ClientToScreenRect (pDC->pWnd, &BoundRect);
        break;
        
    default:
        fclose (fpa);
        fclose (fpc);
        return;
        break;
    }
    
    OldRng = pDC->ClipRegion;
    
    NewRng = AllocRectRegionIndirect (&BoundRect);
    IntersectRegion (NewRng, NewRng, pDC->ClipRegion);
    pDC->ClipRegion = NewRng;
    
    switch (uFormat) {
    case DT_LEFT:
        x = BoundRect.left;
        break;
        
    case DT_RIGHT:
        x = BoundRect.left - (nCount * (FONT_POINT >> 1) - (BoundRect.right - BoundRect.left));
        break;
        
    case DT_CENTER:
    default:
        x = BoundRect.left + (BoundRect.right - BoundRect.left - nCount * (FONT_POINT >> 1)) / 2;
        break;
    }
    y = BoundRect.top + (BoundRect.bottom - BoundRect.top - FONT_POINT) / 2;
    
    pText = (int8_t *)lpString;
    for (i = 0; i < nCount; i++) {
        if (*pText & 0x80) {     /* HZ */
            LoadChiness (fpc, pText, outfont);
            DrawTextGlyph (pDC, x, y, outfont, FONT_POINT, FONT_POINT);
            x += FONT_POINT;
            pText++;
            i++;
        }
        else {                      /* ASCII */
            LoadASCII (fpa, *pText, outfont);
            DrawTextGlyph (pDC, x, y, outfont, FONT_POINT >> 1, FONT_POINT);
            x += FONT_POINT >> 1;
        }
        pText++;
    }
    
    pDC->ClipRegion = OldRng;
    FreeClipRegion (NewRng);
    
    fclose (fpa);
    fclose (fpc);
}

void DrawBitmap(DC* pDC, int32_t x, int32_t y, BITMAP *pBmp, int32_t sx, int32_t sy, int32_t w, int32_t h)
{
    RGBQUAD *pbit, rgb;
    RECT BoundRect, ValidRect;
    int32_t i, j, k, ssx, ssy, dx, dy, cw, ch;
    CLIPREGION* ClipRegion = pDC->ClipRegion;
    
    if (NULL == pBmp) {
        return;
    }
    
    if (w <= 0 || h <= 0) {
        w = pBmp->bmWidth;
        h = pBmp->bmHeight;
    }
    
    SetRect (&BoundRect, x, y, x + w, y + h);
    
    switch (pDC->nType) {
    case TYPE_WINDOW_DC:
        WindowToScreenRect (pDC->pWnd, &BoundRect);
        break;
        
    case TYPE_CLIENT_DC:
        ClientToScreenRect (pDC->pWnd, &BoundRect);
        break;
        
    default:
        return;
        break;
    }
    
    pbit = pBmp->bmBits;
    for (i = 0; i < ClipRegion->numRects; i++) {
        if (FALSE == IntersectRECT (&ValidRect, &BoundRect, &ClipRegion->rects[i])) {
            continue;
        }
        
#ifdef SOFT_CURSOR
        GdiCheckSoftCursor (&ValidRect, TRUE);
#endif
        
        ssx = ValidRect.left - BoundRect.left + sx;
        ssy = ValidRect.top  - BoundRect.top  + sy;
        dx  = ValidRect.left;
        dy  = ValidRect.top;
        cw  = ValidRect.right - ValidRect.left;
        ch  = ValidRect.bottom - ValidRect.top;
        for (j = 0; j < ch; j++) {
            for (k = 0; k < cw; k++) {
                rgb = pbit[(ssy + j) * pBmp->bmWidth + (ssx + k)];
                pDC->pPhyDC->set_pixel (pDC->pPhyDC, dx + k, dy + j, RGB (rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue));
            }
        }
        
#ifdef SOFT_CURSOR
        GdiCheckSoftCursor (&ValidRect, FALSE);
#endif
    }
}
