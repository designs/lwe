#ifndef __ICON_H__
#define __ICON_H__

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct tagICONFILEHEADER
{
   uint16_t      idReserved;         // reserved (must be 0) 
   uint16_t      idType;             // resource type (1 for icons)
   uint16_t      idCount;            // how many images
} __attribute__((packed))ICONFILEHEADER;

typedef struct tagICONDIR
{  
    uint8_t      bWidth;             // width,in pixels,of the image  
    uint8_t      bHeight;            // height, in pixels, of the image  
    uint8_t      bColorCount;        // number of colors in image (0 if >=8bpp)  
    uint8_t      bReserved;          // reserved (must be 0)  
    uint16_t     wPlanes;            // color planes  
    uint16_t     wBitCount;          // bits per pixel  
    uint32_t     dwBytesiInres;      // how many bytes in this resource?  
    uint32_t     dwImageOffset;      // where in the file is this image?  
} __attribute__((packed))ICONDIR;

typedef struct tagICON
{
    int32_t     w;
    int32_t     h;
    
    void*   pAndMask;
    void*   pXorMask;
} __attribute__((packed))ICON;

ICON *LoadIconFile(int8_t *);
bool32_t  DestroyIcon(ICON * pIcon);
bool32_t  GetIconSize (ICON *pIcon, int32_t* w, int32_t* h);
void DrawIcon(DC* pDC, int32_t x, int32_t y, int32_t w, int32_t h, ICON *pIcon);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
