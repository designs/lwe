/*
  Copyright (C), 2008~2009, zhanbin
  File name:    softcursor.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090108
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090108    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

#ifdef SOFT_CURSOR
static bool32_t bRedrawCursor;
static RECT SaveBmpRect;
static int32_t nOldSaveX;
static int32_t nOldSaveY;
static BITMAP *pSaveBmp;
static MUTEX MutexObject;

bool32_t InitSoftCursor(void)
{
    pSaveBmp = (BITMAP *)Malloc (sizeof (BITMAP));
    if (NULL == pSaveBmp) {
        return FALSE;
    }
    
    pSaveBmp->bmType       = 0;
    pSaveBmp->bmWidth      = 32;
    pSaveBmp->bmHeight     = 32;
    pSaveBmp->bmBitsPixel  = GetDeviceCaps (BITSPIXEL);
    pSaveBmp->bmWidthBytes = pSaveBmp->bmWidth * (pSaveBmp->bmBitsPixel / 8);
    pSaveBmp->bmPlanes     = 1;
    pSaveBmp->bmBits       = Malloc (pSaveBmp->bmWidth * pSaveBmp->bmHeight * (pSaveBmp->bmBitsPixel / 8));
    if (NULL == pSaveBmp->bmBits) {
        return FALSE;
    }
    
    bRedrawCursor = FALSE;
    SetRect (&SaveBmpRect, 0, 0, 0, 0);
    nOldSaveX     = -1;
    nOldSaveY     = -1;
    if (FALSE == MutexCreate (&MutexObject)) {
        Free (pSaveBmp->bmBits);
        Free (pSaveBmp);
        
        return FALSE;
    }
    
    return TRUE;
}

void LockSoftCursor(void)
{
    MutexLock (&MutexObject);
}

void UnlockSoftCursor(void)
{
    MutexUnlock (&MutexObject);
}

void ResumeCursorBmp(void)
{
    if (nOldSaveX < 0 || nOldSaveY < 0) {
        return;
    }
    
    pGUI->PhyDC.set_rect (&pGUI->PhyDC, &SaveBmpRect, pSaveBmp->bmBits);
}

void SaveCursorBmp(int32_t x, int32_t y)
{
    CURSOR *pCursor = pGUI->pSysCursor;
    
    if (NULL == pCursor) {
        nOldSaveX = -1;
        nOldSaveY = -1;
        return;
    }
    
    if (x < pCursor->x) {
        x = 0;
    }
    else if ((GetSystemMetrics (SM_CXSCREEN) - 1 - x) < pCursor->x) {
        x = GetSystemMetrics (SM_CXSCREEN) - 1 - pCursor->w;
    }
    else {
        x = x - pCursor->x;
    }
    
    if (y < pCursor->y) {
        y = 0;
    }
    else if ((GetSystemMetrics (SM_CYSCREEN) - 1 - y) < pCursor->h) {
        y = GetSystemMetrics (SM_CYSCREEN) - 1 - pCursor->h;
    }
    else {
        y = y - pCursor->y;
    }
    
    nOldSaveX = x;
    nOldSaveY = y;
    SetRect (&SaveBmpRect, nOldSaveX, nOldSaveY, nOldSaveX + 32, nOldSaveY + 32);
    pGUI->PhyDC.copy_rect (&pGUI->PhyDC, &SaveBmpRect, pSaveBmp->bmBits);
}

void DrawSoftCursor(int32_t x, int32_t y)
{
    CURSOR *pCursor = pGUI->pSysCursor;
    uint8_t *pAndMask, *pXorMask, *ta, *tx;
    uint16_t i, j, k, l, cx, cy, cw, ch, dx, dy, dxx, dyy;
    
    if (NULL == pCursor) {
        return;
    }
    
    pAndMask = (uint8_t *)pCursor->pAndMask;
    pXorMask = (uint8_t *)pCursor->pXorMask;
    
    if (x < pCursor->x) {
        cx = pCursor->x - x;
        dx = 0;
        cw = pCursor->w - cx;
    }
    else {
        cx = 0;
        dx = x - pCursor->x;
        cw = GetSystemMetrics (SM_CXSCREEN) - 1 - dx;
        if (cw > pCursor->w) {
            cw = pCursor->w;
        }
    }
    
    if (y < pCursor->y) {
        cy = pCursor->y - y;
        dy = 0;
        ch = pCursor->h - cy;
    }
    else {
        cy = 0;
        dy = y - pCursor->y;
        ch = GetSystemMetrics (SM_CYSCREEN) - 1 - dy;
        if (ch > pCursor->h) {
            ch = pCursor->h;
        }
    }
    
    dyy = dy;
    for (i = cy; i < cy + ch; i++) {
        ta = pAndMask + i * pCursor->w / 8;
        tx = pXorMask + i * pCursor->w / 8;
        dxx= dx;
        for (j = cx; j < cx + cw; j++) {
            k = j / 8;
            l = j % 8;
            
            if (ta[k] & (0x1 << (7 - l))) {
            }
            else {
                if (tx[k] & (0x1 << (7 - l))) {
                    pGUI->PhyDC.set_pixel (&pGUI->PhyDC, dxx, dyy, RGB (255, 255, 255));
                }
                else {
                    pGUI->PhyDC.set_pixel (&pGUI->PhyDC, dxx, dyy, RGB (000, 000, 000));
                }
            }
            
            dxx++;
        }
        
        dyy++ ;
    }
}

void GdiCheckSoftCursor(RECT *lpRect, bool32_t bBefore)
{
    if (TRUE == bBefore) {
        LockSoftCursor ();
    
        bRedrawCursor = DoesIntersect (lpRect, &SaveBmpRect);
        if (TRUE == bRedrawCursor) {
            ResumeCursorBmp ();
        }
    }
    else {
        if (TRUE == bRedrawCursor) {
            SaveCursorBmp (pGUI->CursorPos.x, pGUI->CursorPos.y);
            DrawSoftCursor (pGUI->CursorPos.x, pGUI->CursorPos.y);
        }
        
        UnlockSoftCursor ();
    }
}
#endif
