/*
  Copyright (C), 2008~2009, zhanbin
  File name:    draw3d.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090216
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090216    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

void Draw3DUpFrame(DC* hdc, int32_t l, int32_t t, int32_t r, int32_t b, COLORREF fillc)
{
    RECT rect;
    
    r--;
    b--;
    
    SetFgColor (hdc, RGB(192, 192, 192));
    Rectangle(hdc, l, t, r, b);
    SetFgColor (hdc, RGB(128, 128, 128));
    Rectangle(hdc, l, t, r - 1, b - 1);
    SetFgColor (hdc, RGB(0, 0, 0));
    MoveToEx(hdc, l, b, NULL);
    LineTo(hdc, r, b);
    LineTo(hdc, r, t);
    SetFgColor (hdc, RGB(255, 255, 255));
    MoveToEx(hdc, l + 1, b - 1, NULL);
    LineTo(hdc, l + 1, t + 1);
    LineTo(hdc, r - 1, t + 1); 
    
    if (fillc != 0) {
         SetFgColor (hdc, fillc);
         SetRect (&rect, l + 2, t + 2, r - 1, b - 1);
         FillRect (hdc, &rect);
    }
}

void Draw3DDownFrame(DC *hdc, int32_t l, int32_t t, int32_t r, int32_t b, COLORREF fillc)
{
    RECT rect;
    
    SetFgColor(hdc, RGB(0, 0, 0));
    MoveToEx(hdc, l, b, NULL);
    LineTo(hdc, l, t);
    LineTo(hdc, r, t);
    SetFgColor(hdc, RGB(128, 128, 128));
    MoveToEx(hdc, l + 1, b - 1, NULL);
    LineTo(hdc, l + 1, t + 1);
    LineTo(hdc, r - 1, t + 1);
    SetFgColor(hdc, RGB(192, 192, 192));
    MoveToEx(hdc, l + 1, b - 1, NULL);
    LineTo(hdc, r - 1, b - 1);
    LineTo(hdc, r - 1, t + 1);
    SetFgColor(hdc, RGB(255, 255, 255));
    MoveToEx(hdc, l, b, NULL);
    LineTo(hdc, r, b);
    LineTo(hdc, r, t);
    
    if (fillc != 0) {
        SetFgColor (hdc, fillc);
        SetRect (&rect, l + 2, t + 2, r - 1, b - 1);
        FillRect (hdc, &rect);
    }
}

void Draw3DControlFrame (DC *hdc, int32_t x0, int32_t y0, int32_t x1, int32_t y1, 
            COLORREF fillc, bool32_t updown)
{
    RECT rect;
    int32_t left, top, right, bottom;
    
    left = MIN (x0, x1);
    top  = MIN (y0, y1);
    right = MAX (x0, x1);
    bottom = MAX (y0, y1);
    
    if (fillc != 0) {
        SetFgColor (hdc, fillc);
        SetRect (&rect, left + 1, top + 1, right - 2, bottom - 2);
        FillRect (hdc, &rect);
    }

    if (updown) {
        SetFgColor (hdc, RGB (255, 255, 255));
        MoveToEx (hdc, left, bottom, NULL);
        LineTo (hdc, left, top);
        LineTo (hdc, right, top);
        SetFgColor (hdc, RGB (0, 0, 0));
        LineTo (hdc, right, bottom);
        LineTo (hdc, left, bottom);

        left++; top++; right--; bottom--;
        SetFgColor (hdc, RGB (128, 128, 128));
        MoveToEx (hdc, left, bottom, NULL);
        LineTo (hdc, left, top);
        LineTo (hdc, right, top);
        SetFgColor (hdc, RGB (128, 128, 128));
        LineTo (hdc, right, bottom);
        LineTo (hdc, left, bottom);
    }
    else {
        SetFgColor (hdc, RGB (128, 128, 128));
        MoveToEx (hdc, left, bottom, NULL);
        LineTo (hdc, left, top);
        LineTo (hdc, right, top);
        SetFgColor (hdc, RGB (255, 255, 255));
        MoveToEx (hdc, left, bottom, NULL);
        LineTo (hdc, right, bottom);
        LineTo (hdc, right, top);

        left++; top++; right--; bottom--;
        SetFgColor (hdc, RGB (0, 0, 0));
        MoveToEx (hdc, left, bottom, NULL);
        LineTo (hdc, left, top);
        LineTo (hdc, right, top);
        SetFgColor (hdc, RGB (128, 128, 128));
        MoveToEx (hdc, left, bottom, NULL);
        LineTo (hdc, right, bottom);
        LineTo (hdc, right, top);
    }
}
