#ifndef __GDI_H__
#define __GDI_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define DT_LEFT             0x00000000
#define DT_CENTER           0x00000001
#define DT_RIGHT            0x00000002

#define TRANSPARENT         0x00000000
#define OPAQUE              0x00000001

COLORREF GetFgColor(DC* pDC);
COLORREF SetFgColor(DC* pDC, COLORREF crColor);
COLORREF GetBkColor(DC* pDC);
COLORREF SetBkColor(DC* pDC, COLORREF crColor);
int32_t  GetBkMode(DC* pDC);
int32_t  SetBkMode(DC* pDC, uint32_t mode);
int32_t  GetPenType (DC* pDC);
int32_t  SetPenType (DC* pDC, int32_t type);
int32_t  GetBrushType(DC* pDC);
int32_t  SetBrushType(DC* pDC, int32_t type);
COLORREF GetPixel(DC* pDC, int32_t x, int32_t y);
void SetPixel(DC* pDC, int32_t x, int32_t y, COLORREF crColor);
bool32_t  MoveToEx(DC* pDC, int32_t x, int32_t y, POINT *pPoint);
bool32_t  LineTo(DC* pDC, int32_t x, int32_t y);
void PolylineTo (DC* pDC, const POINT* pt, int32_t num);
bool32_t  Rectangle(DC* pDC, int32_t x0, int32_t y0, int32_t x1, int32_t y1);
void DrawRect(DC* pDC, RECT *lpRect);
void FillRect(DC* pDC, RECT *lpRect);
void Circle(DC* pDC, int32_t xc, int32_t yc, int32_t r);
int32_t  TextOutLen (DC* pDC, int32_t x, int32_t y, const int8_t *lpString, int32_t nLen);
void DrawText(DC* pDC, const int8_t *lpString, int32_t nCount, RECT *lpRect, uint32_t uFormat);
void DrawBitmap(DC* pDC, int32_t x, int32_t y, BITMAP *pBmp, int32_t sx, int32_t sy, int32_t w, int32_t h);

#define TextOut(hdc, x, y, text)  TextOutLen (hdc, x, y, text, -1)

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
