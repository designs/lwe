/*
  Copyright (C), 2008~2009, zhanbin
  File name:    fb32.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090112
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090112    zhanbin         ����ʵ��
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "window.h"

#ifdef FB_32
COLORREF fb_get_pixel(PHYDC *pPhyDC, int32_t x, int32_t y)
{
    uint32_t *fb;
    uint32_t offset;
    
    fb = (uint32_t *)pPhyDC->fb;
    offset = y * pPhyDC->w + x;
    
    return (COLORREF)*(fb + offset);
}

void fb_set_pixel(PHYDC *pPhyDC, int32_t x, int32_t y, COLORREF color)
{
    uint32_t *fb;
    uint32_t offset;
    
    fb = (uint32_t *)pPhyDC->fb;
    offset = y * pPhyDC->w + x;
    
    *(fb + offset) = (uint32_t)color;
}

void fb_line(PHYDC *pPhyDC, int32_t x0, int32_t y0, int32_t x1, int32_t y1, COLORREF color)
{
    int32_t pos;
    
    if ((x0 == x1) && (y0 == y1)) {
        fb_set_pixel (pPhyDC, x0 , y0 , color);
    }
    else if ( x0 == x1 ) {
        if (y0 > y1) {
            SWAP (y0, y1);
        }
        
        for (pos = y0 ; pos <= y1 ; pos++) {
            fb_set_pixel (pPhyDC, x0 , pos , color);
        }
    }
    else if (y0 == y1) {
        if (x0 > x1) {
            SWAP (x0, x1);
        }
        
        for (pos = x0 ; pos <= x1; pos++) {
            fb_set_pixel (pPhyDC, pos , y0 , color);
        }
    }
    else {
        float k;
        float dx,dy;
        float fx,fy;
        int32_t x2, y2;
        
        x2 = x0;
        y2 = y0;
        dx=(float)(x2 - x1);
        dy=(float)(y2 - y1);
        
        k = dy / dx;
        if (fabs (k) <= 1){
            if (x1 > x2){
                SWAP (x1, x2);
                SWAP (y1, y2);
            }
            
            fy = (float)y1;
            for (x0 = x1; x0 <= x2; x0++){
                fb_set_pixel (pPhyDC, x0, (int32_t)(fy+0.5), color);
                fy = fy + k;
            }
        }
        else{
            if (y1 > y2){
                SWAP (x1, x2);
                SWAP (y1, y2);
            }
            
            fx = (float)x1;
            for(y0 = y1; y0 <= y2; y0++){
                fb_set_pixel (pPhyDC, (int32_t)(fx + 0.5), y0, color);
                fx = fx + 1 / k;
            }
        }
    }
}

void fb_fill_rect(PHYDC *pPhyDC, RECT *lpRect, COLORREF color)
{
    int32_t i;
    RECT Rect;
    
    CopyRect (&Rect, lpRect);
    NormalizeRect (&Rect);
    
    for (i = Rect.top; i < Rect.bottom; i++) {
        fb_line (pPhyDC, Rect.left, i, Rect.right - 1, i, color) ;
    }
}

void fb_copy_rect(PHYDC *pPhyDC, RECT *lpRect, void *pDes)
{
    uint32_t offset, i, w, h;
    uint32_t *fb, *psrc, *pdes;
    
    fb = (uint32_t *)pPhyDC->fb;
    offset = lpRect->top * pPhyDC->w + lpRect->left;
    
    w = lpRect->right - lpRect->left;
    h = lpRect->bottom - lpRect->top;
    psrc = fb + offset;
    pdes = (uint32_t *)pDes;
    for (i = 0; i < h; i++) {
        memcpy (pdes, psrc, w * 4);
        psrc += pPhyDC->w;
        pdes += w;
    }
}

void fb_set_rect(PHYDC *pPhyDC, RECT *lpRect, void *pSrc)
{
    uint32_t offset, i, w, h;
    uint32_t *fb, *psrc, *pdes;
    
    fb = (uint32_t *)pPhyDC->fb;
    offset = lpRect->top * pPhyDC->w + lpRect->left;
    
    w = lpRect->right - lpRect->left;
    h = lpRect->bottom - lpRect->top;
    psrc = (uint32_t *)pSrc;
    pdes = fb + offset;
    for (i = 0; i < h; i++) {
        memcpy (pdes, psrc, w * 4);
        psrc += w;
        pdes += pPhyDC->w;
    }
}
#endif
