#ifndef __FB_H__
#define __FB_H__

#ifdef __cplusplus
extern "C"
{
#endif

COLORREF fb_get_pixel(PHYDC *pPhyDC, int32_t x, int32_t y);
void fb_set_pixel(PHYDC *pPhyDC, int32_t x, int32_t y, COLORREF color);
void fb_line(PHYDC *pPhyDC, int32_t x0, int32_t y0, int32_t x1, int32_t y1, COLORREF color);
void fb_fill_rect(PHYDC *pPhyDC, RECT *lpRect, COLORREF color);
void fb_copy_rect(PHYDC *pPhyDC, RECT *lpRect, void *pDes);
void fb_set_rect(PHYDC *pPhyDC, RECT *lpRect, void *pSrc);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
