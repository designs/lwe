#ifndef __SOFT_CURSOR_H__
#define __SOFT_CURSOR_H__

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef SOFT_CURSOR
bool32_t  InitSoftCursor(void);
void LockSoftCursor(void);
void UnlockSoftCursor(void);
void ResumeCursorBmp(void);
void SaveCursorBmp(int32_t, int32_t);
void DrawSoftCursor(int32_t, int32_t);
void GdiCheckSoftCursor(RECT *, bool32_t);
#endif

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
