#ifndef __BMP_H__
#define __BMP_H__

#include "window.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define BI_RGB          0
#define BI_RLE8         1
#define BI_RLE4         2
#define BI_BITFIELDS    3

typedef struct tagBITMAPFILEHEADER
{
   uint16_t      bfType;
   uint32_t      bfSize;
   uint16_t      bfReserved1;
   uint16_t      bfReserved2;
   uint32_t      bfOffBits;
} __attribute__((packed))BITMAPFILEHEADER;

typedef struct tagBITMAPINFOHEADER
{
   uint32_t      biSize;
   uint32_t      biWidth;
   uint32_t      biHeight;
   uint16_t      biPlanes;
   uint16_t      biBitCount;
   uint32_t      biCompression;
   uint32_t      biSizeImage;
   uint32_t      biXPelsPerMeter;
   uint32_t      biYPelsPerMeter;
   uint32_t      biClrUsed;
   uint32_t      biClrImportant;
} __attribute__((packed))BITMAPINFOHEADER;

typedef struct tagBITMAP
{
    uint8_t      bmType;
    uint8_t      bmBitsPixel;
    uint8_t      bmWidthBytes;
    uint8_t      bmReserved;

    uint32_t     bmWidth;
    uint32_t     bmHeight;
    uint32_t     bmPlanes;

    void*   bmBits;
} __attribute__((packed))BITMAP;

typedef struct tagRGBQUAD
{
    uint8_t      rgbBlue;
    uint8_t      rgbGreen;
    uint8_t      rgbRed;
    uint8_t      rgbReserved;
} __attribute__((packed))RGBQUAD;

typedef struct tagOS2BMPINFOHEADER  /* size: 12 */
{
   uint32_t      biSize;
   uint16_t      biWidth;
   uint16_t      biHeight;
   uint16_t      biPlanes;
   uint16_t      biBitCount;
} __attribute__((packed))OS2BMPINFOHEADER;

BITMAP *LoadBmpFile(int8_t *pFileName);
void UnloadBitmap(BITMAP *pBmp);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
