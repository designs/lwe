#ifndef __DRAW_3D_H__
#define __DRAW_3D_H__

#ifdef __cplusplus
extern "C"
{
#endif

void Draw3DUpFrame(DC* hdc, int32_t l, int32_t t, int32_t r, int32_t b, COLORREF fillc);
void Draw3DDownFrame(DC *hDC, int32_t l, int32_t t, int32_t r, int32_t b, COLORREF fillc);
void Draw3DControlFrame (DC *hdc, int32_t x0, int32_t y0, int32_t x1, int32_t y1, COLORREF fillc, bool32_t updown);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
