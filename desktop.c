/*
  Copyright (C), 2008, zhanbin
  File name:    desktop.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081223
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081223    zhanbin         创建文件
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "window.h"

static BITMAP *pBackBmp = NULL;
static MENU *pPopupMenu = NULL;

static void DesktopCreatePopupMenu(void)
{
    MENUItem Item;
    
    if (NULL != pPopupMenu) {
        return;
    }
    
    pPopupMenu = CreateMenu ("桌面菜单");
    
    Item.id = 2000;
    strcpy (Item.spName, "关于");
    InsertMenuItem (pPopupMenu, &Item);
    
    Item.id = 2001;
    strcpy (Item.spName, "退出");
    InsertMenuItem (pPopupMenu, &Item);
}

static int32_t AboutDialogProc(WINDOW *pDlg,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    WINDOW *hStatic;
    BITMAP *pButtonBmp;
    
    switch(Message){
    case EV_CREATE:
        pButtonBmp = GetSystemBitmap (BMP_PUSH_BUTTON_ID);
        
        hStatic = CreateWindow ("static", "static", WS_CONTROL | WS_VISIBLE, 
            10, 10, 150, 20, pDlg, (MENU *)1000, NULL);
        SendMessage (hStatic, EV_SETTEXT, 0, (uint32_t)VERSIONSTR);
        
        hStatic = CreateWindow ("static", "static", WS_CONTROL | WS_VISIBLE, 
            10, 40, 250, 20, pDlg, (MENU *)1001, NULL);
        SendMessage (hStatic, EV_SETTEXT, 0, (uint32_t)"作者：流浪的狮子");
        
        hStatic = CreateWindow ("static", "static", WS_CONTROL | WS_VISIBLE, 
            10, 60, 250, 20, pDlg, (MENU *)1002, NULL);
        SendMessage (hStatic, EV_SETTEXT, 0, (uint32_t)"邮件：zhanbin98154@163.com");
        
        hStatic = CreateWindow ("static", "static", WS_CONTROL | WS_VISIBLE | SS_BITMAP, 
            200, 10, 60, 30, pDlg, (MENU *)1004, GetSystemBitmap (BMP_GUI_LOGO_ID));
        
        CreateWindow ("bmpbutton", "确定", WS_CONTROL | WS_VISIBLE | WS_TABSTOP, 
            112, 100, 75, 21, pDlg, (MENU *)IDOK, pButtonBmp);
        break;
        
    default:
        break;
    }
    
    return DefDialogProc (pDlg, Message, wParam, lParam);
}

static int32_t DesktopWndProc(WINDOW* Window,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    switch(Message){
    case EV_CREATE:
    {
        DesktopCreatePopupMenu ();
        
        pBackBmp = LoadBmpFile (ROOT_PATH"/bmp/wp.bmp");
        break;
    }
        
    case EV_ERASEBKGND:
    {
        DC* hdc;
        RECT WndRect;
        
        hdc = BeginErase (Window, wParam, lParam);
        if (NULL == hdc) {
            break;
        }
        
        if (pBackBmp) {
            DrawBitmap (hdc, 0, 0, pBackBmp, 0, 0, 0, 0);
        }
        else {
            GetClientRect (Window, &WndRect);
            
            SetFgColor (hdc, RGB (0, 78, 152));
            FillRect (hdc, &WndRect);
        }
        
        EndErase (Window, hdc, wParam, lParam);
        return 0;
        break;
    }
        
    case EV_PAINT:
    {
        DC* hdc;
        RECT WndRect;
        int8_t strMessage[50];
        
        hdc = BeginPaint (Window, wParam, lParam);
        if (NULL == hdc) {
            break;
        }
        
        GetClientRect (Window, &WndRect);
        
        SetFgColor (hdc, RGB (255, 0, 0));
        sprintf (strMessage, "%s", Window->pCaption);
        DrawText (hdc, strMessage, strlen (strMessage), &WndRect, DT_CENTER);
        
        EndPaint (Window, hdc, wParam, lParam);
        return 0;
        break;
    }
        
    case EV_RBUTTONUP:
    {
        POINT p;
        
        p.x = LOuint16_t (lParam);
        p.y = HIuint16_t (lParam);
        ScreenToWindow (Window, &p);
        ShowMenu (pPopupMenu, p.x, p.y, Window);
        break;
    }
        
    case EV_COMMAND:
    {
        switch (LOuint16_t (wParam)) {
        case 2000:
            CreateDialog ("关于GUI", WS_CAPTION | WS_BORDER, 300, 150, Window, AboutDialogProc);
            break;
            
        case 2001:
            PostMessage (Window, EV_QUIT, (uint32_t)0, (uint32_t)0);
            break;
            
        default:
            break;
        }
        
        break;
    }
        
    case EV_DESTROY:
    {
        UnloadBitmap (pBackBmp);
        
        DestroyMenu (pPopupMenu);
        break;
    }
        
    default:
        break;
    }
    
    return DefWinProc(Window, Message, wParam, lParam);
}

static bool32_t RegisterDesktopWndClass (void)
{
    WNDCLASS DesktopClass;

    memset (&DesktopClass, 0, sizeof (WNDCLASS));
    DesktopClass.pClassName = "Desktop";
    DesktopClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    DesktopClass.WinProc    = DesktopWndProc;
    
    return RegisterClass (&DesktopClass);
}

WINDOW *CreateDesktopWnd()
{
    WINDOW *hDeskWnd;
    
    RegisterDesktopWndClass ();
    
    hDeskWnd = CreateWindow ("Desktop", VERSIONSTR, WS_DESKTOP, 
        0, 0, GetSystemMetrics (SM_CXSCREEN), GetSystemMetrics (SM_CYSCREEN), NULL, NULL, NULL);
    if (NULL == hDeskWnd) {
        return NULL;
    }
    
    ShowWindow (hDeskWnd, SW_SHOW);
    
    SetCursorPos (GetSystemMetrics (SM_CXSCREEN) / 2, GetSystemMetrics (SM_CYSCREEN) / 2);
    
    return hDeskWnd;
}
