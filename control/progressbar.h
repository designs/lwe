/*
** $Id: progressbar.h,v 1.6 2003/09/04 03:40:35 weiym Exp $
**
** prograssbar.h: the head file of PrograssBar control.
**
** Copyright (c) 2003 Feynman Software.
** Copyright (c) 1999 ~ 2002 Wei Yongming.
**
** Create date: 1999/8/29
*/

#ifndef __PROGRESSBAR_H__
#define __PROGRESSBAR_H__

#ifdef  __cplusplus
extern  "C" {
#endif

#define PB_OKAY                 0
#define PB_ERR                  -1

#define PBS_NOTIFY              0x00010000L
#define PBS_VERTICAL            0x00020000L

#define PBM_SETRANGE            0xF0A0
#define PBM_SETSTEP             0xF0A1
#define PBM_SETPOS              0xF0A2
#define PBM_DELTAPOS            0xF0A3
#define PBM_STEPIT              0xF0A4
#define PBM_GETPOS              0xF0A5

#define PBN_REACHMAX            1
#define PBN_REACHMIN            2

typedef  struct tagPROGRESSDATA
{
    uint32_t nMin;
    uint32_t nMax;
    uint32_t nPos;
    uint32_t nStepInc;
}PROGRESSDATA;
typedef PROGRESSDATA* PPROGRESSDATA;

bool32_t RegisterProgressBarControl (void);

#ifdef  __cplusplus
}
#endif

#endif // __PROGRESSBAR_H__

