#ifndef __BMP_SCROLL_BAR_H__
#define __BMP_SCROLL_BAR_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define SB_HORZ           0
#define SB_VERT           1
#define SB_CTL            2
#define SB_BOTH           3

#define SBS_FOCUS         0x80000000
                            
#define SBS_TYPEMASK      0x00010000
#define SBS_HORZ          0x00000000
#define SBS_VERT          0x00010000
                            
#define SST_ULMASK          0x00040000
#define SST_UL_NORMAL       0x00000000
#define SST_UL_PRESSED      0x00040000
                            
#define SST_DRMASK          0x00080000
#define SST_DR_NORMAL       0x00000000
#define SST_DR_PRESSED      0x00080000
                            
#define SST_SLMASK          0x00100000
#define SST_SL_NORMAL       0x00000000
#define SST_SL_PRESSED      0x00100000

#define SB_NONE           0x0000
#define SB_LINEUP         0x0001
#define SB_LINEDOWN       0x0002
#define SB_PAGEUP         0x0003
#define SB_PAGEDOWN       0x0004
#define SB_THUMBTRACK     0x0005
#define SB_THUMBPOSITION  0x0006
#define SB_LINELEFT       SB_LINEUP
#define SB_LINERIGHT      SB_LINEDOWN
#define SB_PAGELEFT       SB_PAGEUP
#define SB_PAGERIGHT      SB_PAGEDOWN

typedef struct _TBmpScrollBarData
{
    int32_t                     nStatus;
    
    int16_t                     nMinPos;
    int16_t                     nMaxPos;
    int16_t                     nCurPos;
    int16_t                     nPageStep;
    
    BITMAP*                 pBmp;           // ͼƬ����
    
    RECT                    Button0Rect;    // ����
    RECT                    Button1Rect;    // �ң���
    RECT                    BarRect;        // BAR
    RECT                    SliderRect;     // SLIDER
    
    WINDOW *               pCapWnd;
}TBmpScrollBarData;

bool32_t RegisterBmpScrollBarClass(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
