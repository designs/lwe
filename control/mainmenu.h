#ifndef __MAIN_MENU_H__
#define __MAIN_MENU_H__

#ifdef __cplusplus
extern "C"
{
#endif

bool32_t RegisterMainMenuClass(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
