/*
** $Id: medit.c,v 1.69 2003/11/22 05:00:55 weiym Exp $
**
** medit.c: the Multi-Line Edit control
**
** Copyright (C) 2003 Feynman Software.
** Copyright (C) 1999 ~ 2002 Chen Lei and Wei Yongming.
** 
** Current maintainer: Chen Lei (leon@minigui.org).
**
** Create date: 1999/8/26
*/

/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
** TODO:
**    * Selection.
**    * Undo.
*/
/*
  Copyright (C), 2008, zhanbin
  File name:    medit.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081205
  Description:
    从MINIGUI移值。
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081205    zhanbin         创建文件
*/
#include    "window.h"

static int32_t MLEditCtrlProc (WINDOW * hWnd, uint32_t message, uint32_t wParam, uint32_t lParam);

bool32_t RegisterMLEditControl (void)
{
    WNDCLASS EditClass;

    memset (&EditClass, 0, sizeof (WNDCLASS));
    EditClass.pClassName = "medit";
    EditClass.pCursor    = GetSystemCursor (CURSOR_BEAM_ID);
    EditClass.WinProc    = MLEditCtrlProc;
    
    return RegisterClass (&EditClass);
}

#define SetTextColor  SetFgColor
#define SetPenColor   SetFgColor
#define SetBrushColor SetFgColor
#define SetBkColor    SetBkColor

static bool32_t EditIsReadOnly(uint32_t nStatus)
{
    return (0 != (nStatus & EST_READONLY));
}

static void Ping(void)
{
}

static void SetWindowAdditionalData2(WINDOW *hWnd, uint32_t nPriv)
{
    hWnd->pPrivData = (void *)nPriv;
}

static void *GetWindowAdditionalData2(WINDOW *hWnd)
{
    return hWnd->pPrivData;
}

bool32_t ActiveCaret (WINDOW *hWnd)
{
    return TRUE;
}

int32_t GetMaxFontWidth(DC* hdc)
{
    return 16;
}

#if 0
void MLEditControlCleanup (void)
{
    // do nothing
    return;
}
#endif

static int32_t edtGetOutWidth (WINDOW * hWnd)
{
    RECT rc;
    PMLEDITDATA pMLEditData =(PMLEDITDATA) GetWindowAdditionalData2(hWnd);
    
    GetClientRect(hWnd,&rc);

    return RECTW(rc) - pMLEditData->leftMargin - pMLEditData->rightMargin;
}

static int32_t edtGetLineNO (const MLEDITDATA* pMLEditData, int32_t x)
{
    int32_t nline = 0;
    if(x>=0)
    {
        nline = x / pMLEditData->lineHeight;
        if (nline <= pMLEditData->linesDisp)
               return nline;
    }
    return -1;
}

int32_t GetRETURNPos(char *str, int32_t len)
{
    int32_t i;

    for(i=0;i<len;i++)
    {    
        if(str[i]==10)
            return i;
    }
    return -1;
}

void MLEditEmptyBuffer(PMLEDITDATA pMLEditData)
{
    PLINEDATA temp;
    PLINEDATA  pLineData;
    pLineData = pMLEditData->head;
    while(pLineData)
    {
        temp = pLineData->next;
        Free (pLineData);
        pLineData = temp;
    }
    pMLEditData->head = NULL;
}

// get next wrap line start address according to startPos
// > 0 : next start position
// -1  : end of current line
static int32_t edtGetnextStartPos (WINDOW * hWnd, PMLEDITDATA pMLEditData, int32_t startPos)
{
    int32_t        i = 0;
    
    if (!pMLEditData->dx_chars) return -1;

    if (pMLEditData->sz.cx - pMLEditData->dx_chars[startPos] 
                                            <= edtGetOutWidth (hWnd))
        return -1;
    
    for (i = startPos; i < pMLEditData->fit_chars+1; i++) {
        if (pMLEditData->dx_chars[i]-pMLEditData->dx_chars[startPos] >= edtGetOutWidth (hWnd))
            return i-1;
    }
    return -1;
}

static void edtGetLineInfoEx (WINDOW * hWnd, PMLEDITDATA pMLEditData, PLINEDATA pLineData)
{
    DC*            hdc = GetDC (hWnd);
    if (!pMLEditData) pMLEditData = (PMLEDITDATA) GetWindowAdditionalData2(hWnd);

#if 0
    if (pMLEditData->logfont)
        SelectFont (hdc, pMLEditData->logfont);
#endif

    GetTextExtentPoint (hdc,
                        pLineData->buffer, 
                        pLineData->dataEnd,  
                        GetMaxFontWidth (hdc)*LEN_MLEDIT_BUFFER,
                        &(pMLEditData->fit_chars), 
                        pMLEditData->pos_chars, 
                        pMLEditData->dx_chars, 
                        &(pMLEditData->sz));
                        
    ReleaseDC (hWnd, hdc);

    if (pMLEditData->pos_chars)
        pMLEditData->pos_chars[pMLEditData->fit_chars] = pLineData->dataEnd;
    if (pMLEditData->dx_chars)
        pMLEditData->dx_chars[pMLEditData->fit_chars] = pMLEditData->sz.cx;
}

static void edtGetLineInfo (WINDOW * hWnd, PLINEDATA pLineData)
{
    edtGetLineInfoEx (hWnd, NULL, pLineData);
}

static int32_t edtGetStartDispPosAtEnd (WINDOW * hWnd,
            PLINEDATA pLineData)
{
    PMLEDITDATA pMLEditData =(PMLEDITDATA) GetWindowAdditionalData2(hWnd);
    int32_t         i = 0;
    int32_t         dist = MAX_IMPOSSIBLE;
    int32_t         newdist = 0;
    
    edtGetLineInfo (hWnd, pLineData);

    if (pMLEditData->sz.cx <= edtGetOutWidth (hWnd)) 
        return 0;
    
    for (i = 0; i < pMLEditData->fit_chars-1; i++) {
        newdist = (pMLEditData->sz.cx - edtGetOutWidth (hWnd)) - pMLEditData->dx_chars[i];
        if (newdist >= 0 && newdist < dist) {
            dist = newdist;
            if (dist == 0) { return i; } 
        }else { return i; }
    }
    return 0;
}

void calcLineInfo (WINDOW * hWnd, PMLEDITDATA pMLEditData, PLINEDATA pLineData)
{
    int32_t i;

    edtGetLineInfoEx (hWnd, pMLEditData, pLineData);

    for ( i=0,pLineData->nwrapline=1,pLineData->wrapStartPos[0]=0; 
                    (i = edtGetnextStartPos(hWnd, pMLEditData,i)) > 0; 
                    pLineData->nwrapline++ )
    {
        pLineData->wrapStartPos[pLineData->nwrapline] = i;
    }
    pLineData->wrapStartPos[pLineData->nwrapline] = pMLEditData->fit_chars; 
}

void MLEditInitBuffer (WINDOW * hWnd, PMLEDITDATA pMLEditData, const char *spcaption, uint32_t dwStyle)
{
    char *caption=(char*)spcaption; 
    int32_t off1;
    int32_t lineNO=0;
	int32_t pace=0;
	int32_t totallen=0;

    PLINEDATA  pLineData;

    if (!(pMLEditData->head = Malloc (sizeof (LINEDATA)))) {
        return ;
    }
    pMLEditData->head->previous = NULL;
    pLineData = pMLEditData->head;    
    pMLEditData->wraplines = 0;

	totallen = strlen(caption);

    while ( (off1 = GetRETURNPos(caption, totallen)) != -1)
    {
        int32_t off;
        off1 = MIN (off1, LEN_MLEDIT_BUFFER);
        if ((char)caption[off1-1] == '\r')
            off = off1-1;
        else
            off = off1;
        if ((pMLEditData->curtotalLen + MIN (off,LEN_MLEDIT_BUFFER) >= pMLEditData->totalLimit)
            && pMLEditData->totalLimit != -1)
        {
            pLineData->dataEnd = pMLEditData->totalLimit-pMLEditData->curtotalLen;

            pace = MIN (pLineData->dataEnd, LEN_MLEDIT_BUFFER) + 1;
            caption += pace;
			totallen -= pace;

            if (pLineData->dataEnd-2 > 0 && (char)caption[pLineData->dataEnd-2] == '\r')
                pLineData->dataEnd -= 2;
            else if (pLineData->dataEnd-1 > 0 && (char)caption[pLineData->dataEnd-1] == '\n')
                pLineData->dataEnd --;

            memcpy(pLineData->buffer,caption, pLineData->dataEnd);
            pLineData->buffer[pLineData->dataEnd] = '\0';
            pLineData->lineNO  = lineNO++;
            pMLEditData->curtotalLen += pLineData->dataEnd;
            
            if (!pLineData->dataEnd)
            {
                pLineData->nwrapline = 1;
                pLineData->wrapStartPos[0] = 0;
                pLineData->wrapStartPos[1] = 0;
            } else
                calcLineInfo (hWnd, pMLEditData, pLineData);

            pMLEditData->wraplines += pLineData->nwrapline;
            pLineData->lineNO  = lineNO++;    

            if (pMLEditData->curtotalLen == pMLEditData->totalLimit)
            {
                pLineData->next = NULL;
                break;
            } else if (pMLEditData->curtotalLen + 1 < pMLEditData->totalLimit)
                pMLEditData->curtotalLen ++;
            pLineData->next    = Malloc (sizeof (LINEDATA));
            pLineData->next->previous = pLineData; 
            pLineData->next->next = NULL;
            pLineData = pLineData->next;
            break;
        }
        memcpy(pLineData->buffer,caption, off1);
        if (pLineData->buffer[off1-1] == '\r')
            pLineData->buffer[off1-1] = '\0';
        else
            pLineData->buffer[off1] = '\0';
        
        pace = MIN (off1,LEN_MLEDIT_BUFFER)+1;
        caption += pace;
        
        pLineData->lineNO  = lineNO++;    
        pLineData->dataEnd = strlen(pLineData->buffer); 
        if (!pLineData->dataEnd)
        {
            pLineData->nwrapline = 1;
            pLineData->wrapStartPos[0] = 0;
            pLineData->wrapStartPos[1] = 0;
        } else
            calcLineInfo (hWnd, pMLEditData, pLineData);

        pMLEditData->wraplines += pLineData->nwrapline;

        pMLEditData->curtotalLen += pLineData->dataEnd;
        if (pMLEditData->curtotalLen == pMLEditData->totalLimit)
        {
            pLineData->next = NULL;
            break;
        } else if (pMLEditData->curtotalLen + 1 < pMLEditData->totalLimit)
            pMLEditData->curtotalLen ++;

        pLineData->next    = Malloc (sizeof (LINEDATA));
        pLineData->next->previous = pLineData; 
        pLineData->next->next = NULL;
        pLineData = pLineData->next;
    }
    if (pMLEditData->totalLimit > 0 && pMLEditData->curtotalLen < pMLEditData->totalLimit)
    {
        int32_t off=0;
        off1 = MIN (strlen(caption),LEN_MLEDIT_BUFFER);
        if (off1 && (char)caption[off1-1] == '\r')
            off = off1-1;
        else
            off = off1;
        if (pMLEditData->curtotalLen + MIN (off,LEN_MLEDIT_BUFFER) >= pMLEditData->totalLimit)
        {
            pLineData->dataEnd = pMLEditData->totalLimit-pMLEditData->curtotalLen;
            if (pLineData->dataEnd-2 > 0 && (char)caption[pLineData->dataEnd-2] == '\r')
                pLineData->dataEnd -= 2;
            else if (pLineData->dataEnd-1 > 0 && (char)caption[pLineData->dataEnd-1] == '\n')
                pLineData->dataEnd --;

            memcpy(pLineData->buffer,caption, pLineData->dataEnd);
            pLineData->buffer[pLineData->dataEnd] = '\0';

        }else {
            memcpy(pLineData->buffer,caption,off1);
            if (off1 && pLineData->buffer[off1-1] == '\r')
                pLineData->buffer[off1-1] = '\0';
            else
                pLineData->buffer[off1] = '\0';
            pLineData->dataEnd         = strlen(pLineData->buffer); 
            pLineData->next            = NULL; 

        }
        pMLEditData->curtotalLen += pLineData->dataEnd;
        if (pMLEditData->curtotalLen == pMLEditData->totalLimit-1)
            pMLEditData->curtotalLen = pMLEditData->totalLimit;
        if (!pLineData->dataEnd)
        {
            pLineData->nwrapline = 1;
            pLineData->wrapStartPos[0] = 0;
            pLineData->wrapStartPos[1] = 0;
        } else
              calcLineInfo (hWnd, pMLEditData, pLineData);

        pMLEditData->wraplines += pLineData->nwrapline;
        pLineData->lineNO = lineNO++;
    }

    pMLEditData->lines      = lineNO; 

    if (dwStyle & ES_AUTOWRAP)
        pMLEditData->linesDisp     =  MIN (pMLEditData->wraplines, pMLEditData->MaxlinesDisp);
    else
        pMLEditData->linesDisp     =  MIN (lineNO , pMLEditData->MaxlinesDisp);
    pMLEditData->EndlineDisp = pMLEditData->StartlineDisp + pMLEditData->linesDisp -1;
}

PLINEDATA GetLineData(PMLEDITDATA pMLEditData,int32_t lineNO)
{
    PLINEDATA pLineData=pMLEditData->head;
    for (; pLineData; pLineData=pLineData->next)
    {
        if(pLineData->lineNO==lineNO)
            return pLineData;
    }
    return NULL;
}

int32_t GetMaxLen (WINDOW * hWnd, PMLEDITDATA pMLEditData)
{
    int32_t i;
    PLINEDATA pLineData = pMLEditData->head;
    
    i = 0;
    while (pLineData)
    {
        edtGetLineInfo (hWnd, pLineData);
        if (pMLEditData->sz.cx > i) 
            i = pMLEditData->sz.cx;
        pLineData = pLineData->next;
    }
    return i;
}

// get the valid unit position in the array 
// according to the pos 
// which is relative to the original point of current line (0)  
static int32_t  edtGetNewvPos (WINDOW * hWnd, PLINEDATA pLineData , int32_t pos)
{
    int32_t i = 0;
    int32_t dist = MAX_IMPOSSIBLE;
    int32_t newdist = 0;
    
    PMLEDITDATA pMLEditData = (PMLEDITDATA) GetWindowAdditionalData2(hWnd);
    edtGetLineInfo (hWnd, pLineData);

    if (pos < 0) return -1;
    if (pos > pMLEditData->sz.cx) return -2;
    for (i=0; i<pMLEditData->fit_chars + 1; i++) {
        newdist = pos - pMLEditData->dx_chars[i];
        if (newdist > 0 && newdist < dist)
            dist = newdist;
        else 
            return i;
    }
    return 0;
}

static void edtSetScrollInfo (WINDOW * hWnd, PMLEDITDATA pMLEditData, bool32_t fRedraw)
{
    PLINEDATA       pLineData;
    RECT            rc;
    int32_t             viLn, nMax, nMin, nPos;
    
    // Vscroll
    GetClientRect (hWnd, &rc);
    viLn = (rc.bottom - rc.top - pMLEditData->topMargin - pMLEditData->bottomMargin) / pMLEditData->lineHeight;
    
    if ((GetWindowStyle(hWnd)) & ES_AUTOWRAP) 
        nMax = pMLEditData->wraplines - 1;
    else
        nMax = pMLEditData->lines - 1;
        
    nMin = 0;
    nPos = pMLEditData->StartlineDisp;
    
    if ((nMax < viLn) && (!nPos)) {
        nMax = 0;
        nMin = 0;
        nPos = 0;
        
        EnableScrollBar (hWnd, SB_VERT, FALSE);
    }
    else {
        nMax -= (viLn - 1);
        
        EnableScrollBar (hWnd, SB_VERT, TRUE);
    }
    
    SetScrollRange (hWnd, SB_VERT, nMin, nMax, fRedraw);
    SetScrollPos (hWnd, SB_VERT, nPos);
    
    // Hscroll
    pLineData = GetLineData(pMLEditData, pMLEditData->editLine);
    nMax = GetMaxLen(hWnd, pMLEditData);
    nMin = 0;
    edtGetLineInfo (hWnd, pLineData);
    nPos = pMLEditData->dispPos;
    
    if (nMax < edtGetOutWidth (hWnd) && (!nPos)) {
        nMax = 0;
        nMin = 0;
        nPos = 0;
        
        EnableScrollBar (hWnd, SB_HORZ, FALSE);
    }
    else {
        if (nPos >= (nMax - edtGetOutWidth (hWnd))) {
            nPos /= 8;
            nMax = nPos;
        }
        else {
            nPos /= 8;
            nMax -= edtGetOutWidth (hWnd);
            nMax += 7;
            nMax /= 8;
        }
        
        EnableScrollBar (hWnd, SB_HORZ, TRUE);
    }
    
    SetScrollRange (hWnd, SB_HORZ, nMin, nMax, fRedraw);
    SetScrollPos (hWnd, SB_HORZ, nPos);
}

int32_t edtGetLineNums (PMLEDITDATA pMLEditData, int32_t curline, int32_t* line, int32_t* wrapLine)
{
    PLINEDATA pLineData = pMLEditData->head;
    int32_t startline = 0;

    if (!curline)
    {
        *line = 0;
        *wrapLine = 0;
        return 0;
    }

    for (; pLineData; pLineData=pLineData->next)
    {
        if (startline+pLineData->nwrapline-1 >= curline)
        {
            *line = pLineData->lineNO;
            *wrapLine = curline - startline;
            return 0;
        }
        startline +=pLineData->nwrapline;
    }
    return -1;
}

// to set the Caret to a valid pos
bool32_t edtGetCaretValid (WINDOW * hWnd, int32_t lineNO, int32_t olddispPos, int32_t newOff/*of caret*/)
{
    int32_t line, wrapline;
    int32_t dist = MAX_IMPOSSIBLE;
    int32_t i, newdist = 0;
    PLINEDATA temp;
    PMLEDITDATA pMLEditData =(PMLEDITDATA) GetWindowAdditionalData2(hWnd);
        
    if (GetWindowStyle(hWnd) & ES_AUTOWRAP)
    {
        edtGetLineNums (pMLEditData, lineNO, &line, &wrapline);
        temp = GetLineData (pMLEditData, line);
        edtGetLineInfo (hWnd, temp);

        if( newOff >= pMLEditData->dx_chars[temp->wrapStartPos[wrapline+1]]
                            - pMLEditData->dx_chars[temp->wrapStartPos[wrapline]])
        {
            if (wrapline < temp->nwrapline-1)
                pMLEditData->veditPos = temp->wrapStartPos[wrapline+1]-1;
            else
                pMLEditData->veditPos = temp->wrapStartPos[wrapline+1];
        }else {
            newOff += pMLEditData->dx_chars[temp->wrapStartPos[wrapline]];
            for (i=temp->wrapStartPos[wrapline]; i<temp->wrapStartPos[wrapline+1]+1; i++) {
                newdist = newOff - pMLEditData->dx_chars[i];
                if (newdist > 0 && newdist < dist)
                    dist = newdist;
                else { 
                      pMLEditData->veditPos =i;
                    return FALSE;
                }
            }
        }
    }else {        
        temp = GetLineData (pMLEditData, lineNO);
        edtGetLineInfo (hWnd, temp);

        if( olddispPos > pMLEditData->sz.cx ) 
        {
            pMLEditData->veditPos = pMLEditData->fit_chars;
            pMLEditData->vdispPos = edtGetStartDispPosAtEnd (hWnd, temp);
            return TRUE;
        }else {
            pMLEditData->veditPos = edtGetNewvPos (hWnd, temp, newOff);
            if (pMLEditData->veditPos == -2)
                pMLEditData->veditPos = pMLEditData->fit_chars;
#if 0   // zhanbin
            pMLEditData->vdispPos = edtGetNewvPos (hWnd, temp, olddispPos);
            if (olddispPos != pMLEditData->dx_chars[pMLEditData->vdispPos])
                return TRUE;
#else
            if (pMLEditData->dx_chars[pMLEditData->veditPos] - 
                pMLEditData->dx_chars[pMLEditData->vdispPos] > edtGetOutWidth (hWnd)) {
                if (edtGetNewvPos (hWnd,
                        GetLineData(pMLEditData,pMLEditData->editLine),
                        pMLEditData->dx_chars[pMLEditData->veditPos] 
                        - edtGetOutWidth (hWnd)/4) == -1)
                    pMLEditData->vdispPos = 0;
                else
                    pMLEditData->vdispPos = edtGetNewvPos (hWnd,
                        GetLineData(pMLEditData,pMLEditData->editLine),
                        pMLEditData->dx_chars[pMLEditData->veditPos] 
                        - edtGetOutWidth (hWnd)/4);
                
                if (pMLEditData->dx_chars[pMLEditData->vdispPos] + edtGetOutWidth (hWnd) > pMLEditData->sz.cx) 
                    pMLEditData->vdispPos = edtGetStartDispPosAtEnd (hWnd, temp);
            }
            else {
                pMLEditData->vdispPos = edtGetNewvPos (hWnd, temp, olddispPos);
            }
            
            if (olddispPos != pMLEditData->dx_chars[pMLEditData->vdispPos])
                return TRUE;
#endif
        }
    }
    return FALSE;
}

void edtSetCaretPos (WINDOW * hWnd)
{
    int32_t line = 0, wrapline = 0;
    PMLEDITDATA pMLEditData =(PMLEDITDATA) GetWindowAdditionalData2(hWnd);
    PLINEDATA temp;

    if (GetWindowStyle(hWnd) & ES_AUTOWRAP)
    {
        edtGetLineNums (pMLEditData, pMLEditData->editLine, &line, &wrapline);
        temp = GetLineData (pMLEditData, line);
        edtGetLineInfo (hWnd, temp);
        SetCaretPos (hWnd, 
                pMLEditData->dx_chars[pMLEditData->veditPos] 
                    - pMLEditData->dx_chars[temp->wrapStartPos[wrapline]]
                    + pMLEditData->leftMargin, 
                (pMLEditData->editLine-pMLEditData->StartlineDisp) * pMLEditData->lineHeight
                    + pMLEditData->topMargin);
        
    }else {
        temp = GetLineData (pMLEditData, pMLEditData->editLine);
        edtGetLineInfo (hWnd, temp);
#if 0   // zhanbin
        SetCaretPos (hWnd, 
                pMLEditData->dx_chars[pMLEditData->veditPos] 
                    - pMLEditData->dx_chars[pMLEditData->vdispPos]
                    + pMLEditData->leftMargin, 
                (pMLEditData->editLine-pMLEditData->StartlineDisp) * pMLEditData->lineHeight
                    + pMLEditData->topMargin);
#else
        SetCaretPos (hWnd, 
                pMLEditData->dx_chars[pMLEditData->veditPos] 
                    - pMLEditData->dispPos
                    + pMLEditData->leftMargin, 
                (pMLEditData->editLine-pMLEditData->StartlineDisp) * pMLEditData->lineHeight
                    + pMLEditData->topMargin);
#endif
    }
}

void edtPosProc (WINDOW * hWnd)
{
    int32_t line, wrapline;
    PMLEDITDATA pMLEditData =(PMLEDITDATA) GetWindowAdditionalData2(hWnd);
    PLINEDATA pLineData;

    if (pMLEditData->diff) {
        pMLEditData->editLine = pMLEditData->realeditLine;
        pMLEditData->vdispPos = pMLEditData->realdispPos;
        pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
        pMLEditData->StartlineDisp = pMLEditData->realStartLine;
        pMLEditData->EndlineDisp   = pMLEditData->realEndLine;
        pMLEditData->diff = FALSE;
        if (GetWindowStyle(hWnd) & ES_AUTOWRAP)
        {
            edtGetLineNums (pMLEditData, pMLEditData->editLine, &line, &wrapline);
            pLineData = GetLineData(pMLEditData, line);
        }else
            pLineData= GetLineData(pMLEditData, pMLEditData->editLine);
           edtGetLineInfo (hWnd, pLineData);
        ActiveCaret (hWnd);
        pMLEditData->bSCROLL = TRUE;
    }else
        pMLEditData->bSCROLL = FALSE;
}

/* add end*/

static void edtOnEraseBackground (WINDOW * hWnd, uint32_t dwStyle, DC* hdc, const RECT* pClipRect)
{
    RECT rcTemp;
    //PMLEDITDATA pMLEditData = (PMLEDITDATA) GetWindowAdditionalData2(hWnd);
    
    GetClientRect (hWnd, &rcTemp);
    
    if (dwStyle & WS_DISABLED) {
        SetBrushColor (hdc, RGB (192, 192, 192));
    }
    else {
        SetBrushColor (hdc, RGB (255, 255, 255));
    }
    
    FillRect (hdc, &rcTemp);
}

static int32_t MLEditCtrlProc (WINDOW * hWnd, uint32_t message, uint32_t wParam, uint32_t lParam)
{   
    uint32_t       dwStyle;
    DC*        hdc;
    PLINEDATA   pLineData;
    PMLEDITDATA pMLEditData = NULL;
    dwStyle     = GetWindowStyle(hWnd);

    switch (message)
    {
        case EV_CREATE:
        {
            RECT ClientRect;
            
            if (!(pMLEditData = Malloc (sizeof (MLEDITDATA)))) {
                return -1;
            }
            
            GetClientRect (hWnd, &ClientRect);
            
            pMLEditData->lineHeight     = 16;
            
            if (!CreateCaret (hWnd, NULL, 1, pMLEditData->lineHeight))
            {
                Free (pMLEditData);
                return -1;
            }
            
            pMLEditData->status         = 0;
            pMLEditData->curtotalLen    = 0;
            pMLEditData->totalLimit     = 100*LEN_MLEDIT_BUFFER;
            pMLEditData->lineLimit      = LEN_MLEDIT_BUFFER;
            pMLEditData->editLine       = 0;
            pMLEditData->veditPos       = 0;
            pMLEditData->dispPos        = 0;
            pMLEditData->vdispPos       = 0;

            pMLEditData->MaxlinesDisp   = 0;
            pMLEditData->linesDisp      = 0;
            pMLEditData->StartlineDisp  = 0;
            pMLEditData->EndlineDisp    = 0;

#if 0
            pMLEditData->selStartPos    = 0;
            pMLEditData->selEndPos      = 0;
#endif

            pMLEditData->passwdChar     = '*';

            pMLEditData->leftMargin     = MARGIN_MEDIT_LEFT;
            pMLEditData->topMargin      = MARGIN_MEDIT_TOP;
            pMLEditData->rightMargin    = MARGIN_MEDIT_RIGHT;
            pMLEditData->bottomMargin   = MARGIN_MEDIT_BOTTOM;

            pMLEditData->pos_chars = (int32_t*)Malloc(LEN_MLEDIT_BUFFER*sizeof(int32_t));
            pMLEditData->dx_chars = (int32_t*)Malloc(LEN_MLEDIT_BUFFER*sizeof(int32_t));

            memset (pMLEditData->pos_chars, 0, LEN_MLEDIT_BUFFER*sizeof(int32_t));
            memset (pMLEditData->dx_chars , 0, LEN_MLEDIT_BUFFER*sizeof(int32_t));

            pMLEditData->fit_chars      = 0;
            pMLEditData->realdispPos    = 0;
            pMLEditData->realStartLine  = 0;
            pMLEditData->realEndLine    = 0;
            pMLEditData->diff           = FALSE;
            pMLEditData->bSCROLL        = FALSE;

            pMLEditData->wraplines      = 0;
            
            SetWindowAdditionalData2 (hWnd,(uint32_t)pMLEditData);
            
            pMLEditData->MaxlinesDisp   = (ClientRect.bottom - ClientRect.top - pMLEditData->topMargin
                                            - pMLEditData->bottomMargin)
                                        / pMLEditData->lineHeight;
            if (pMLEditData->MaxlinesDisp < 0)
                pMLEditData->MaxlinesDisp = 0;
            
            MLEditInitBuffer (hWnd, pMLEditData, GetWindowCaption(hWnd), dwStyle);
            
            edtSetScrollInfo (hWnd, pMLEditData, TRUE);
            break;
        }

        case EV_DESTROY:
        {
            pMLEditData =(PMLEDITDATA) GetWindowAdditionalData2(hWnd);
            DestroyCaret (hWnd);
            MLEditEmptyBuffer(pMLEditData);
            Free (pMLEditData->pos_chars);
            Free (pMLEditData->dx_chars);
            pMLEditData->pos_chars = NULL;
            pMLEditData->dx_chars = NULL;
            Free (pMLEditData); 
        }
        break;
        
        case EV_SIZE:
        {
            int32_t nHeight = HIint16_t (lParam);
            pMLEditData =(PMLEDITDATA) GetWindowAdditionalData2(hWnd);
            pMLEditData->MaxlinesDisp   = (nHeight - pMLEditData->topMargin
                                            - pMLEditData->bottomMargin)
                                        / pMLEditData->lineHeight;
            if (pMLEditData->MaxlinesDisp < 0)
                pMLEditData->MaxlinesDisp = 0;
            if (dwStyle & ES_AUTOWRAP && pMLEditData->head)
            {
                   PLINEDATA  pLineData = pMLEditData->head;
                pMLEditData->wraplines = 0;
                for (; pLineData; pLineData = pLineData->next)
                {
                    calcLineInfo (hWnd, pMLEditData, pLineData);
                    pMLEditData->wraplines += pLineData->nwrapline;
                }
            }
            if (dwStyle & ES_AUTOWRAP)
                pMLEditData->linesDisp    = MIN (pMLEditData->MaxlinesDisp, pMLEditData->wraplines);
            else
                pMLEditData->linesDisp    = MIN (pMLEditData->MaxlinesDisp, pMLEditData->lines);
            pMLEditData->StartlineDisp  = 0;
            pMLEditData->EndlineDisp    = pMLEditData->StartlineDisp + pMLEditData->linesDisp - 1;
            pMLEditData->editLine       = 0;
            pMLEditData->dispPos        = 0;
            
            edtSetScrollInfo (hWnd, pMLEditData, TRUE);
        }
        break;

#if 0   // zhanbin
        case EV_FONTCHANGED:
        {
            pMLEditData =(PMLEDITDATA) GetWindowAdditionalData2(hWnd);
            pMLEditData->vdispPos = 0;
            pMLEditData->veditPos = 0;
            edtSetCaretPos (hWnd);
            edtSetScrollInfo (hWnd, pMLEditData, TRUE);
            InvalidateRect (hWnd, NULL, TRUE);
            return 0;
        }
        
        case EV_SETCURSOR:
            if (dwStyle & WS_DISABLED) {
                SetCursor (GetSystemCursor (IDC_ARROW));
                return 0;
            }
        break;
#endif

        case EV_KILLFOCUS:
        {
            pMLEditData = (PMLEDITDATA) GetWindowAdditionalData2 (hWnd);
            pMLEditData->status &= ~EST_FOCUSED;
            HideCaret (hWnd);
            NotifyParent (hWnd, GetDlgCtrlID(hWnd), EN_KILLFOCUS);
        }
        break;
        
        case EV_SETFOCUS:
        {
            pMLEditData = (PMLEDITDATA) GetWindowAdditionalData2 (hWnd);
            if (pMLEditData->status & EST_FOCUSED)
                break;
            
            pMLEditData->status |= EST_FOCUSED;

            /* only implemented for ES_LEFT align format. */
            edtSetCaretPos (hWnd);

            ShowCaret (hWnd);
            ActiveCaret (hWnd);

            NotifyParent (hWnd, GetDlgCtrlID(hWnd), EN_SETFOCUS);
        }
        break;
        
        case EV_ENABLE:
            if ( (!(dwStyle & WS_DISABLED) && !wParam)
                    || ((dwStyle & WS_DISABLED) && wParam) ) {
                if (wParam)
                    ExcludeWindowStyle(hWnd,WS_DISABLED);
                else
                    IncludeWindowStyle(hWnd,WS_DISABLED);

                InvalidateRect (hWnd, NULL, TRUE);
            }
        return 0;

        case EV_ERASEBKGND:
        {
            bool32_t hidden = HideCaret (hWnd);
            hdc = BeginErase (hWnd, wParam, lParam);
            
            edtOnEraseBackground (hWnd, dwStyle, hdc, (const RECT*)lParam);
            if (hidden)
                ShowCaret (hWnd);
                
            EndErase (hWnd, hdc, wParam, lParam);
            return 0;
        }

        case EV_PAINT:
        {
            int32_t     i;
            char    dispBuffer [LEN_MLEDIT_BUFFER+1];
            bool32_t hidden = HideCaret (hWnd);            //zhanbin
            
            pMLEditData =(PMLEDITDATA) GetWindowAdditionalData2(hWnd);
            
            hdc = BeginPaint (hWnd, wParam, lParam);
    
            if (dwStyle & WS_DISABLED) {
                SetBrushColor (hdc, RGB (192, 192, 192));
                SetBkColor (hdc, RGB (192, 192, 192));
            }
            else {
                SetBrushColor (hdc, RGB (255, 255, 255));
                SetBkColor (hdc, RGB (255, 255, 255));
            }

            SetTextColor (hdc, RGB (0, 0, 0));

            for (i = pMLEditData->StartlineDisp; i <= pMLEditData->EndlineDisp; i++) {
                int32_t y, line=i, wrapline=0;
                if (dwStyle & ES_AUTOWRAP)
                    edtGetLineNums (pMLEditData, i, &line, &wrapline);
            
                pLineData= GetLineData(pMLEditData, line);
                if (pLineData == NULL)
                    break;

                edtGetLineInfo (hWnd, pLineData);
                if (dwStyle & ES_AUTOWRAP) {
                    if (pMLEditData->fit_chars == 0 && 
                        pMLEditData->EndlineDisp >= pMLEditData->wraplines) {
                        //FIXME
                        continue;
                    }
                }
                else {
                    if (pMLEditData->fit_chars == 0 && 
                        pMLEditData->EndlineDisp >= pMLEditData->lines) {
                        //FIXME
                        continue;
                    }
                }

                if (dwStyle & ES_PASSWORD)
                    memset (dispBuffer, pMLEditData->passwdChar, pLineData->dataEnd);
                    memcpy (dispBuffer, 
                        pLineData->buffer,
                        pLineData->dataEnd);
                        dispBuffer[pLineData->dataEnd] = '\0';

                y = pMLEditData->lineHeight*
                        (i - pMLEditData->StartlineDisp)
                        + pMLEditData->topMargin;
                //fprint (stderr, "textOut-buffer = %s\n", dispBuffer+pLineData->wrapStartPos[wrapline]);
                if (dwStyle & ES_AUTOWRAP)
                    TextOutLen (hdc, 
                        pMLEditData->leftMargin - pMLEditData->dispPos, 
                        y,
                        dispBuffer+pMLEditData->pos_chars[pLineData->wrapStartPos[wrapline]],
                        pMLEditData->pos_chars[pLineData->wrapStartPos[wrapline+1]]
                            -pMLEditData->pos_chars[pLineData->wrapStartPos[wrapline]]);
                else
                    TextOut(hdc, 
                        pMLEditData->leftMargin - pMLEditData->dispPos, y,
                        dispBuffer);
            }
            
            EndPaint (hWnd, hdc, wParam, lParam);
            
            if (hidden)                     // zhanbin
                ShowCaret (hWnd);
        }
        return 0;

        case EV_KEYDOWN:
        {
            bool32_t     bChange = FALSE;
            int32_t     i;
            int32_t     line, wrapline;
            RECT    InvRect; 
            int32_t     deleted;
            PLINEDATA temp = NULL;
            char *  tempP = NULL;

            pMLEditData =(PMLEDITDATA) GetWindowAdditionalData2(hWnd);
            GetClientRect (hWnd, &InvRect);
            InvRect.left = pMLEditData->leftMargin - 1;
        
            switch (LOuint16_t (wParam)) {
                case VK_RETURN:
                {
                    if (dwStyle & ES_READONLY) {
                        Ping();
                        return 0;
                    }
                       edtPosProc (hWnd); 

                    if ((pMLEditData->totalLimit >= 0) 
                            && ((pMLEditData->curtotalLen + 1) > pMLEditData->totalLimit)) {
                        Ping ();
                        NotifyParent (hWnd, GetDlgCtrlID(hWnd), EN_MAXTEXT);
                        return 0;
                    }else
                        pMLEditData->curtotalLen++;

                    if (dwStyle & ES_AUTOWRAP)
                    {
                        edtGetLineNums (pMLEditData, pMLEditData->editLine, &line, &wrapline);
                        pLineData = GetLineData(pMLEditData, line);
                    }else
                        pLineData = GetLineData(pMLEditData,pMLEditData->editLine);
                    

                    if (dwStyle & ES_AUTOWRAP)
                    {
                        edtGetLineInfo (hWnd, pLineData);

                        if (pMLEditData->veditPos < pMLEditData->fit_chars)
                            tempP = pLineData->buffer + 
                                    pMLEditData->pos_chars[pMLEditData->veditPos];
                        temp = pLineData->next;
                        pLineData->next = Malloc( sizeof(LINEDATA) );
                        pLineData->next->previous = pLineData;
                        pLineData->next->next = temp;
                    
                        if(temp)
                            temp->previous = pLineData->next;
                    
                        temp = pLineData->next;
                        temp->lineNO = line + 1;
                        if(tempP) {
                            memcpy(temp->buffer,tempP,strlen(tempP));
                            temp->dataEnd = strlen(tempP);
                        }
                        else
                            temp->dataEnd = 0;
                        temp->buffer[temp->dataEnd] = '\0'; 
                        pLineData->dataEnd = pMLEditData->pos_chars[pMLEditData->veditPos];
                        pLineData->buffer[pLineData->dataEnd]='\0';
                        temp = temp->next;
                        while (temp)
                        {
                            temp->lineNO++;
                            temp = temp->next;
                        }
                        if (!((pMLEditData->veditPos == pLineData->wrapStartPos[wrapline])
                               && (wrapline)))
                            pMLEditData->editLine++;
                        pMLEditData->wraplines -= pLineData->nwrapline;
                        calcLineInfo (hWnd, pMLEditData, pLineData);
                        calcLineInfo (hWnd, pMLEditData, pLineData->next);
                        edtGetLineInfo (hWnd, pLineData->next);
                        pMLEditData->lines++;
                        pMLEditData->wraplines += pLineData->nwrapline + pLineData->next->nwrapline;
                        pMLEditData->veditPos=0;

                        if (pMLEditData->editLine > pMLEditData->EndlineDisp)
                        {
                            pMLEditData->EndlineDisp = pMLEditData->editLine;
                            if (pMLEditData->EndlineDisp > 
                                            pMLEditData->StartlineDisp+pMLEditData->MaxlinesDisp-1)
                            {
                                pMLEditData->StartlineDisp = pMLEditData->EndlineDisp
                                                            -pMLEditData->MaxlinesDisp + 1;
                            }else {
                                InvRect.top  = (pMLEditData->editLine 
                                                - pMLEditData->StartlineDisp -1) 
                                                    * pMLEditData->lineHeight
                                                  + pMLEditData->topMargin;
                                InvRect.bottom  -=  pMLEditData->lineHeight;
                            }
                        }else {
                            pMLEditData->EndlineDisp = pMLEditData->StartlineDisp + MIN (pMLEditData->wraplines 
                                                                                        - pMLEditData->StartlineDisp, 
                                                                                    pMLEditData->MaxlinesDisp) - 1;
                        }
                        pMLEditData->linesDisp = pMLEditData->EndlineDisp 
                                                - pMLEditData->StartlineDisp + 1;
                        NotifyParent (hWnd, GetDlgCtrlID(hWnd), EN_CHANGE);
                        InvalidateRect (hWnd, &InvRect, TRUE);
                        edtSetCaretPos (hWnd);
                        edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                        if (pMLEditData->bSCROLL)
                            InvalidateRect (hWnd, NULL, TRUE);
                        return 0;
                    }

                    edtGetLineInfo (hWnd, pLineData);
                    
                    if (pMLEditData->veditPos < pMLEditData->fit_chars)
                        tempP = pLineData->buffer + pMLEditData->pos_chars[pMLEditData->veditPos];
                    temp = pLineData->next;
                    pLineData->next = Malloc( sizeof(LINEDATA) );
                    pLineData->next->previous = pLineData;
                    pLineData->next->next = temp;
                    
                    if(temp)
                        temp->previous = pLineData->next;
                    
                    temp = pLineData->next;
                    temp->lineNO  = pMLEditData->editLine + 1;
                    if(tempP) {
                        memcpy(temp->buffer,tempP,strlen(tempP));
                        temp->dataEnd = strlen(tempP);
                    }
                    else
                        temp->dataEnd = 0;
                    temp->buffer[temp->dataEnd] = '\0'; 
                    pLineData->dataEnd = pMLEditData->pos_chars[pMLEditData->veditPos];
                    pLineData->buffer[pLineData->dataEnd]='\0';
                    temp = temp->next;
                    while (temp)
                    {
                        temp->lineNO++;
                        temp = temp->next;
                    }
                    // added by leon to optimize display
                    if ((pMLEditData->editLine - pMLEditData->StartlineDisp + 1) 
                                    < pMLEditData->MaxlinesDisp) {
                        if (!(pMLEditData->dx_chars[pMLEditData->veditPos] - pMLEditData->leftMargin
                                 > edtGetOutWidth (hWnd))) {
                            InvRect.top  = (pMLEditData->editLine - pMLEditData->StartlineDisp -1) 
                                                * pMLEditData->lineHeight
                                                  + pMLEditData->topMargin;
                        }
                                
                     }else {
                        if ((pMLEditData->vdispPos == 0) 
                                && (pMLEditData->vdispPos == pMLEditData->veditPos))         
                            InvRect.bottom  -=  pMLEditData->lineHeight;
                     }
                    // added by leon to optimize display
                    pMLEditData->veditPos = 0;
                    pMLEditData->vdispPos = 0;
                
                    if(pMLEditData->linesDisp < pMLEditData->MaxlinesDisp)
                    {
                        pMLEditData->EndlineDisp++;
                        pMLEditData->linesDisp++;
                    }
                    else if(pMLEditData->editLine == pMLEditData->EndlineDisp) 
                    {
                        pMLEditData->StartlineDisp++;
                        pMLEditData->EndlineDisp++;
                    }
                    pMLEditData->editLine++;
                    edtGetLineInfo (hWnd, GetLineData(pMLEditData,pMLEditData->editLine));    
                    pMLEditData->lines++;
                    
                    pMLEditData->dispPos = 0;

                    pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    edtSetCaretPos (hWnd);
                    edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                    InvalidateRect (hWnd, &InvRect, TRUE); // modified by leon

                    NotifyParent (hWnd, GetDlgCtrlID(hWnd), EN_CHANGE);
                    if (pMLEditData->bSCROLL)
                        InvalidateRect (hWnd, NULL, TRUE);

                    return 0;
                }
                case VK_HOME:
                {
                    int32_t         line = 0, wrapline = 0;
                    bool32_t         bScroll = FALSE;

                    pMLEditData =(PMLEDITDATA) GetWindowAdditionalData2(hWnd);

                    if (pMLEditData->veditPos == 0)
                        return 0;

                    if (dwStyle & ES_AUTOWRAP)
                    {
                        edtGetLineNums (pMLEditData, pMLEditData->editLine, &line, &wrapline);
                        pLineData = GetLineData(pMLEditData, line);

                        pMLEditData->editLine -= wrapline;
                        if (pMLEditData->editLine < pMLEditData->StartlineDisp)
                        {
                            pMLEditData->StartlineDisp = pMLEditData->editLine;
                            pMLEditData->linesDisp =  MIN (pMLEditData->wraplines 
                                                            - pMLEditData->StartlineDisp, 
                                                        pMLEditData->MaxlinesDisp);
                            pMLEditData->EndlineDisp = 
                                    pMLEditData->StartlineDisp + pMLEditData->linesDisp - 1;
                            bScroll = TRUE;
                        }
                        edtGetCaretValid (hWnd, pMLEditData->editLine, 0, 0);

                        edtSetCaretPos (hWnd);
                        if (bScroll)
                        {
                            InvalidateRect(hWnd,NULL,TRUE);    
                            edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                        }
                        if (pMLEditData->bSCROLL)
                            InvalidateRect (hWnd, NULL, TRUE);
                        return 0;
                    }
                    
                    pMLEditData->veditPos = 0;

                    if (pMLEditData->vdispPos != 0)
                    {
                        pMLEditData->vdispPos = 0;
                        InvalidateRect (hWnd, NULL, TRUE);
                    }
                    pMLEditData->dispPos = 0;
                    edtSetCaretPos (hWnd);
                    edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                    if (pMLEditData->bSCROLL)
                        InvalidateRect (hWnd, NULL, TRUE);
                       return 0;
                }
                case VK_END:
                {
                    int32_t      newStartPos, oldeditPos, line, wrapline;
                    bool32_t     bScroll = FALSE;

                    pMLEditData =(PMLEDITDATA) GetWindowAdditionalData2(hWnd);

                    if (dwStyle & ES_AUTOWRAP)
                    {
                        edtGetLineNums (pMLEditData, pMLEditData->editLine, &line, &wrapline);
                        pLineData = GetLineData(pMLEditData, line);
                    }else 
                        pLineData = GetLineData(pMLEditData,pMLEditData->editLine);
                    
                    edtGetLineInfo (hWnd, pLineData);
                    if (pMLEditData->veditPos == pMLEditData->fit_chars)
                        return 0;
                    
                    if (dwStyle & ES_AUTOWRAP)
                    {
                        pMLEditData->veditPos = pMLEditData->fit_chars;
                        pMLEditData->editLine += pLineData->nwrapline - 1 - wrapline;
                        if (pMLEditData->editLine > pMLEditData->EndlineDisp)
                        {
                            pMLEditData->EndlineDisp = pMLEditData->editLine;
                            pMLEditData->StartlineDisp = 
                                    pMLEditData->EndlineDisp - pMLEditData->MaxlinesDisp + 1;
                            bScroll = TRUE;
                        }

                        oldeditPos = pMLEditData->dx_chars[pMLEditData->veditPos] 
                            - pMLEditData->dx_chars[pLineData->wrapStartPos[pLineData->nwrapline-1]];
                        
                        edtGetCaretValid (hWnd, pMLEditData->editLine, 0, oldeditPos);

                        edtSetCaretPos (hWnd);
                        if (bScroll)
                            InvalidateRect(hWnd,NULL,TRUE);    
                        edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                        if (pMLEditData->bSCROLL)
                            InvalidateRect (hWnd, NULL, TRUE);
                        return 0;
                    }

                    
                    newStartPos = edtGetStartDispPosAtEnd (hWnd, pLineData);
                    pMLEditData->veditPos = pMLEditData->fit_chars;

                    pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    if (pMLEditData->vdispPos != newStartPos) {
                        pMLEditData->vdispPos = newStartPos;
                        pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                        InvalidateRect (hWnd, NULL, TRUE);
                    }
                    edtSetCaretPos (hWnd);
                    edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                }
                    if (pMLEditData->bSCROLL)
                        InvalidateRect (hWnd, NULL, TRUE);
                return 0;

                case VK_LEFT: 
                {
                    bool32_t         bScroll = FALSE;
                    int32_t          newStartPos, line, wrapline;
                    PLINEDATA     temp;

                    edtPosProc (hWnd); 
                    if (dwStyle & ES_AUTOWRAP)
                    {
                        edtGetLineNums (pMLEditData, pMLEditData->editLine, &line, &wrapline);
                        pLineData = GetLineData(pMLEditData, line);
                    }
                    else
                        pLineData = GetLineData(pMLEditData,pMLEditData->editLine);

                    if (dwStyle & ES_AUTOWRAP)
                    {
                        if (!pMLEditData->veditPos && !pMLEditData->editLine) return 0;
                        edtGetLineInfo (hWnd, pLineData);
                        if (!pMLEditData->veditPos)
                        {
                            if (pMLEditData->editLine == pMLEditData->StartlineDisp) {
                                bScroll = TRUE;
                                pMLEditData->StartlineDisp--;
                                if (pMLEditData->EndlineDisp >= pMLEditData->MaxlinesDisp)
                                    pMLEditData->EndlineDisp--;
                            }
                            pMLEditData->editLine--;
                            temp = pLineData->previous;
                            edtGetLineInfo (hWnd, temp);
                            pMLEditData->veditPos = temp->wrapStartPos[temp->nwrapline];
                        }else if (pMLEditData->veditPos == pLineData->wrapStartPos[wrapline]) {
                            if (pMLEditData->editLine == pMLEditData->StartlineDisp) {
                                bScroll = TRUE;
                                pMLEditData->StartlineDisp--;
                                if (pMLEditData->EndlineDisp >= pMLEditData->MaxlinesDisp)
                                    pMLEditData->EndlineDisp--;
                            }
                            pMLEditData->editLine--;
                            pMLEditData->veditPos--; 
                        }else 
                            pMLEditData->veditPos--;

                        pMLEditData->linesDisp = pMLEditData->EndlineDisp 
                                                - pMLEditData->StartlineDisp + 1;
                        edtSetCaretPos (hWnd);
                        if (bScroll)
                        {
                            InvalidateRect(hWnd,NULL,TRUE);    
                            edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                        }
                        if (pMLEditData->bSCROLL)
                            InvalidateRect (hWnd, NULL, TRUE);
                        return 0;
                    }

                    edtGetLineInfo (hWnd, pLineData);   // zhanbin
                    if (!pMLEditData->veditPos)
                    {
                        temp = pLineData->previous;
                        if (!temp) return 0;
                        if(pMLEditData->editLine > 0 )
                        {
                            if (pMLEditData->editLine == pMLEditData->StartlineDisp) {
                                bScroll = TRUE;
                                pMLEditData->StartlineDisp--;
                                if (pMLEditData->EndlineDisp >= pMLEditData->MaxlinesDisp)
                                    pMLEditData->EndlineDisp--;
                            }
                            pMLEditData->editLine --;
                            edtGetLineInfo (hWnd, GetLineData (pMLEditData, pMLEditData->editLine));
                            pMLEditData->veditPos = pMLEditData->fit_chars; 
                            newStartPos = edtGetStartDispPosAtEnd (hWnd, temp);
                            if (pMLEditData->vdispPos != newStartPos)
                            {
                                pMLEditData->vdispPos = newStartPos;
                                bScroll = TRUE;
                            }
                        }
                    }
                    else
                    {    
                        if (pMLEditData->veditPos == pMLEditData->vdispPos){
                            pMLEditData->veditPos--;
                            if (edtGetNewvPos (hWnd,
                                                GetLineData(pMLEditData,pMLEditData->editLine),
                                                pMLEditData->dx_chars[pMLEditData->veditPos] 
                                                - edtGetOutWidth (hWnd)/4) == -1)
                                pMLEditData->vdispPos = 0;
                            else {
                                pMLEditData->vdispPos = edtGetNewvPos (hWnd,
                                                GetLineData(pMLEditData,pMLEditData->editLine),
                                                pMLEditData->dx_chars[pMLEditData->veditPos] 
                                                - edtGetOutWidth (hWnd)/4);
                            }
                            bScroll = TRUE;
                        }else {
                            pMLEditData->veditPos--;
                        }
                            
                    }    
                    pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    edtSetCaretPos (hWnd);
                    if (bScroll)
                        InvalidateRect (hWnd, NULL, TRUE);
                    edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                    if (pMLEditData->bSCROLL)
                        InvalidateRect (hWnd, NULL, TRUE);
                }
                return 0;
                
                case VK_RIGHT:
                {
                    bool32_t         bScroll = FALSE;
                    int32_t          line, wrapline;
                    PLINEDATA     temp;

                    if (dwStyle & ES_AUTOWRAP)
                    {
                        edtGetLineNums (pMLEditData, pMLEditData->editLine, &line, &wrapline);
                        pLineData = GetLineData(pMLEditData, line);
                    }
                    else
                        pLineData = GetLineData(pMLEditData,pMLEditData->editLine);

                    if (dwStyle & ES_AUTOWRAP)
                    {
                        if (!(temp = pLineData->next) 
                                    && pMLEditData->veditPos == pMLEditData->fit_chars) return 0;
                        edtGetLineInfo (hWnd, pLineData);
                        if (pMLEditData->veditPos == pMLEditData->fit_chars)
                        {
                            if (pMLEditData->editLine == pMLEditData->EndlineDisp) {
                                bScroll = TRUE;
                                pMLEditData->StartlineDisp++;
                                pMLEditData->EndlineDisp++;
                            }
                            edtGetLineInfo (hWnd, temp);
                            pMLEditData->editLine++;
                            pMLEditData->veditPos = 0;
                        }else if (pMLEditData->veditPos == pLineData->wrapStartPos[wrapline+1]-1)
                        {
                            if (pMLEditData->veditPos < 
                                            pLineData->wrapStartPos[pLineData->nwrapline]-1)
                            {
                                if (pMLEditData->editLine == pMLEditData->EndlineDisp) {
                                    bScroll = TRUE;
                                    pMLEditData->StartlineDisp++;
                                    pMLEditData->EndlineDisp++;
                                }
                                pMLEditData->editLine++;
                            }
                            pMLEditData->veditPos++;
                        }
                        else
                            pMLEditData->veditPos++;

                        pMLEditData->linesDisp = pMLEditData->EndlineDisp 
                                                - pMLEditData->StartlineDisp + 1;

                        edtSetCaretPos (hWnd);
                        if (bScroll)
                        {
                            InvalidateRect(hWnd,NULL,TRUE);    
                            edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                        }
                        if (pMLEditData->bSCROLL)
                            InvalidateRect (hWnd, NULL, TRUE);
                        return 0;
                    }
                            
                    edtGetLineInfo (hWnd, pLineData);   // zhanbin
                    if (pMLEditData->veditPos == pMLEditData->fit_chars) 
                    {
                        temp = pLineData->next;
                        if (temp && pMLEditData->editLine == pMLEditData->EndlineDisp) {
                            bScroll = TRUE;
                            pMLEditData->StartlineDisp++;
                            pMLEditData->EndlineDisp++;
                        }
                        if(temp)
                        {
                            pMLEditData->editLine++;
                            edtGetLineInfo (hWnd, GetLineData (pMLEditData, pMLEditData->editLine));
                            pMLEditData->veditPos  = 0;
                            if(pMLEditData->vdispPos !=0)
                            {
                                pMLEditData->vdispPos  = 0;
                                bScroll = TRUE;
                            }
                        } else
                            return 0;
                    } else {    
                        // caret is at the end of the display rect
                        if ((pMLEditData->dx_chars[pMLEditData->veditPos] 
                                    - pMLEditData->dx_chars[pMLEditData->vdispPos] 
                                    <= edtGetOutWidth (hWnd)) 
                                && (pMLEditData->dx_chars[pMLEditData->veditPos + 1] 
                                    - pMLEditData->dx_chars[pMLEditData->vdispPos] 
                                    > edtGetOutWidth (hWnd))) {
                            bScroll = TRUE;
                            pMLEditData->veditPos++;
                            if (edtGetNewvPos (hWnd,
                                                GetLineData(pMLEditData,pMLEditData->editLine),
                                                pMLEditData->dx_chars[pMLEditData->veditPos] 
                                                - edtGetOutWidth (hWnd)/4) == -1)
                                pMLEditData->vdispPos = 0;
                            else
                                pMLEditData->vdispPos = edtGetNewvPos (hWnd,
                                                GetLineData(pMLEditData,pMLEditData->editLine),
                                                pMLEditData->dx_chars[pMLEditData->veditPos] 
                                                - edtGetOutWidth (hWnd)/4);

                            if (pMLEditData->dx_chars[pMLEditData->vdispPos] 
                                        + edtGetOutWidth (hWnd) 
                                    > pMLEditData->sz.cx) 
                                pMLEditData->vdispPos = 
                                            edtGetStartDispPosAtEnd (hWnd, pLineData);
                        }else {
                            pMLEditData->veditPos++;
                            if (pMLEditData->dx_chars[pMLEditData->veditPos] < 
                                pMLEditData->dx_chars[pMLEditData->vdispPos]) {
                                bScroll = TRUE;
                                pMLEditData->vdispPos = pMLEditData->veditPos;
                            }
                        }
                    }
                    pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    edtSetCaretPos (hWnd);
                    if (bScroll)
                        InvalidateRect (hWnd, NULL, TRUE);
                        edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                    if (pMLEditData->bSCROLL)
                        InvalidateRect (hWnd, NULL, TRUE);
                }
                return 0;
                   
                case VK_UP:
                {
                    bool32_t bScroll = FALSE;
                    int32_t  newStartPos, olddispPos, oldeditPos, line, wrapline;
                    PLINEDATA temp;

                       edtPosProc (hWnd); 
                    olddispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    oldeditPos = pMLEditData->dx_chars[pMLEditData->veditPos];
                    if (dwStyle & ES_AUTOWRAP)
                    {
                        edtGetLineNums (pMLEditData, pMLEditData->editLine, &line, &wrapline);
                        pLineData = GetLineData(pMLEditData, line);
                    }
                    else
                        pLineData = GetLineData(pMLEditData,pMLEditData->editLine);

                    temp = pLineData->previous; 
                    if(pMLEditData->editLine == 0) return 0;
                    if (pMLEditData->editLine == pMLEditData->StartlineDisp)
                    {
                        bScroll = TRUE;
                        pMLEditData->StartlineDisp--;
                        if (dwStyle & ES_AUTOWRAP)
                            pMLEditData->EndlineDisp = MIN (pMLEditData->wraplines - 1,
                                                pMLEditData->StartlineDisp + 
                                                pMLEditData->MaxlinesDisp - 1);
                        else
                            pMLEditData->EndlineDisp--;    
                    }
                    pMLEditData->editLine--;

                    if (dwStyle & ES_AUTOWRAP)
                    {
                        oldeditPos = pMLEditData->dx_chars[pMLEditData->veditPos] 
                                        - pMLEditData->dx_chars[pLineData->wrapStartPos[wrapline]];
                        edtGetCaretValid (hWnd, pMLEditData->editLine,
                                                           olddispPos, oldeditPos);

                        pMLEditData->linesDisp = pMLEditData->EndlineDisp 
                                                - pMLEditData->StartlineDisp + 1;
                        edtSetCaretPos (hWnd);
                        if (bScroll)
                            InvalidateRect(hWnd,NULL,TRUE);    
                        edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                        if (pMLEditData->bSCROLL)
                            InvalidateRect (hWnd, NULL, TRUE);
                        return 0;
                    }    
                    
                    edtGetLineInfo (hWnd, temp);

                    if( olddispPos > pMLEditData->sz.cx ) 
                    {
                        pMLEditData->veditPos = pMLEditData->fit_chars;
                           newStartPos = edtGetStartDispPosAtEnd (hWnd, temp);
                           pMLEditData->vdispPos =  newStartPos;
                        bScroll = TRUE;
                    }                
                    else 
                    {
                        pMLEditData->veditPos = edtGetNewvPos (hWnd, temp, oldeditPos);
                        if (pMLEditData->veditPos == -2)
                            pMLEditData->veditPos = pMLEditData->fit_chars;
                        pMLEditData->vdispPos = edtGetNewvPos (hWnd, temp, olddispPos);
                        if (olddispPos != pMLEditData->dx_chars[pMLEditData->vdispPos])
                            bScroll = TRUE;
                    }
                    
                    pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    SetCaretPos (hWnd, 
                        pMLEditData->dx_chars[pMLEditData->veditPos] 
                            - pMLEditData->dx_chars[pMLEditData->vdispPos] 
                            + pMLEditData->leftMargin, 
                        (pMLEditData->editLine - pMLEditData->StartlineDisp) * pMLEditData->lineHeight
                            + pMLEditData->topMargin);
                    if (bScroll)
                        InvalidateRect(hWnd,NULL,TRUE);    
                    edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                    if (pMLEditData->bSCROLL)
                        InvalidateRect (hWnd, NULL, TRUE);
                }
                break;
                case VK_DOWN:
                {
                    bool32_t bScroll = FALSE;
                    int32_t  newStartPos, olddispPos, oldeditPos, line, wrapline;
                    PLINEDATA temp;

                       edtPosProc (hWnd); 
                    oldeditPos = pMLEditData->dx_chars[pMLEditData->veditPos];
                    olddispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    if (dwStyle & ES_AUTOWRAP)
                    {
                        edtGetLineNums (pMLEditData, pMLEditData->editLine, &line, &wrapline);
                        pLineData = GetLineData(pMLEditData, line);
                    }
                    else
                        pLineData = GetLineData(pMLEditData,pMLEditData->editLine);

                    temp = pLineData->next; 
                    if (dwStyle & ES_AUTOWRAP) {
                        if(pMLEditData->editLine == pMLEditData->wraplines-1) return 0;
                    }else {
                        if(pMLEditData->editLine == pMLEditData->lines-1) return 0;
                    }
                    if (pMLEditData->editLine == pMLEditData->EndlineDisp)
                    {
                        bScroll = TRUE;
                        pMLEditData->StartlineDisp++;
                        pMLEditData->EndlineDisp++;    
                    }
                    pMLEditData->editLine++;

                    if (dwStyle & ES_AUTOWRAP)
                    {
                        oldeditPos = pMLEditData->dx_chars[pMLEditData->veditPos] 
                                        - pMLEditData->dx_chars[pLineData->wrapStartPos[wrapline]];
                        edtGetCaretValid (hWnd, pMLEditData->editLine,
                                                           olddispPos, oldeditPos);

                        edtSetCaretPos (hWnd);
                        if (bScroll)
                            InvalidateRect(hWnd,NULL,TRUE);    
                        edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                        if (pMLEditData->bSCROLL)
                            InvalidateRect (hWnd, NULL, TRUE);
                        return 0;
                    }    

                    edtGetLineInfo (hWnd, temp);

                    if( olddispPos > pMLEditData->sz.cx ) 
                    {
                        pMLEditData->veditPos = pMLEditData->fit_chars;
                           newStartPos = edtGetStartDispPosAtEnd (hWnd, temp);
                           pMLEditData->vdispPos =  newStartPos;
                        bScroll = TRUE;
                    }                
                    else 
                    {
                        pMLEditData->veditPos = edtGetNewvPos (hWnd, temp, oldeditPos);
                        if (pMLEditData->veditPos == -2)
                            pMLEditData->veditPos = pMLEditData->fit_chars;
                        pMLEditData->vdispPos = edtGetNewvPos (hWnd, temp, olddispPos);
                        if (olddispPos != pMLEditData->dx_chars[pMLEditData->vdispPos])
                            bScroll = TRUE;
                    }
                    
                    pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    edtSetCaretPos (hWnd);
                    if (bScroll)
                        InvalidateRect(hWnd,NULL,TRUE);    
                    edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                    if (pMLEditData->bSCROLL)
                        InvalidateRect (hWnd, NULL, TRUE);
                }
                break;
                case VK_INSERT:
                    pMLEditData->status ^= EST_REPLACE;
                break;

                case VK_DELETE:
                {
                    PLINEDATA temp;
                    int32_t leftLen;
                    int32_t line, wrapline;
                    int32_t oldnwrapline;
                    bool32_t bScroll=FALSE;

                    if (dwStyle & ES_READONLY) {
                        Ping();
                        return 0;
                    }
                       edtPosProc (hWnd); 
                    if (dwStyle & ES_AUTOWRAP)
                    {
                        edtGetLineNums (pMLEditData, pMLEditData->editLine, &line, &wrapline);
                        pLineData = GetLineData(pMLEditData, line);
                    } else
                        pLineData = GetLineData(pMLEditData, pMLEditData->editLine);
                    
                    edtGetLineInfo (hWnd,pLineData); 

                    if (dwStyle & ES_AUTOWRAP)
                    {
                        oldnwrapline = pLineData->nwrapline;
                        InvRect.top = (pMLEditData->editLine - pMLEditData->StartlineDisp) 
                                        * pMLEditData->lineHeight
                                        + pMLEditData->topMargin;
                        if (pMLEditData->veditPos == pMLEditData->fit_chars) // end of the paragraph
                        {
                            bScroll = TRUE;
                            if (pMLEditData->editLine == pMLEditData->wraplines-1) return 0;

                               temp = pLineData->next; 

                            pMLEditData->wraplines -= pLineData->nwrapline;
                            pMLEditData->curtotalLen --;
                            if (temp)
                                pMLEditData->wraplines -= pLineData->next->nwrapline;

                            if(pLineData->dataEnd + temp->dataEnd <= LEN_MLEDIT_BUFFER)
                            {
                                memcpy(pLineData->buffer+pLineData->dataEnd,temp->buffer,temp->dataEnd);    
                                pLineData->dataEnd += temp->dataEnd;
                                pLineData->buffer[pLineData->dataEnd] = '\0';
                                if(temp->next)
                                {
                                    pLineData->next = temp->next;
                                    temp->next->previous = pLineData;    
                                }
                                else
                                    pLineData->next = NULL;
                                Free (temp);
                                temp = pLineData->next;
                                while (temp)
                                {
                                    temp->lineNO--;
                                    temp = temp->next;
                                }
                                edtGetLineInfo (hWnd, pLineData);
                    
                            }
                            else if (temp->dataEnd > 0)
                            {
                                leftLen = LEN_MLEDIT_BUFFER - pLineData->dataEnd;
                                memcpy(pLineData->buffer+pLineData->dataEnd,temp->buffer,leftLen);
                                pLineData->dataEnd +=leftLen;
                                pLineData->buffer[pLineData->dataEnd] = '\0';
                                memcpy(temp->buffer,temp->buffer+leftLen,temp->dataEnd-leftLen);  
                                temp->dataEnd -= leftLen;
                                temp->buffer[temp->dataEnd] = '\0';
                            }
                            calcLineInfo (hWnd, pMLEditData, pLineData);
                            edtGetLineInfo (hWnd, pLineData);
                            pMLEditData->lines--;
                            pMLEditData->wraplines += pLineData->nwrapline;
                            
                        }else {
                            deleted = pMLEditData->pos_chars[pMLEditData->veditPos + 1]
                                        - pMLEditData->pos_chars[pMLEditData->veditPos];
                            pMLEditData->curtotalLen -= deleted;

                            for (i = pMLEditData->pos_chars[pMLEditData->veditPos]; 
                                    i < pLineData->dataEnd - deleted;
                                    i++)
                                pLineData->buffer [i] 
                                    = pLineData->buffer [i + deleted];

                            pLineData->dataEnd -= deleted;
                            if (pLineData->dataEnd == 0) {
                                pMLEditData->fit_chars = 0;
                                pMLEditData->pos_chars[0] = 0;
                                pMLEditData->dx_chars[0] = 0;
                            }
                            pLineData->buffer[pLineData->dataEnd] = '\0';

                            pMLEditData->wraplines -= pLineData->nwrapline;
                            calcLineInfo (hWnd, pMLEditData, pLineData);
                            pMLEditData->wraplines += pLineData->nwrapline;
                            edtGetLineInfo (hWnd, pLineData);

                            if (wrapline == oldnwrapline - 1)
                                InvRect.left = pMLEditData->dx_chars[pMLEditData->veditPos]
                                    - pMLEditData->dx_chars[pLineData->wrapStartPos[wrapline]]
                                    + pMLEditData->leftMargin - 1; 
                            if (oldnwrapline == pLineData->nwrapline)
                                InvRect.bottom = InvRect.top + pMLEditData->lineHeight
                                                                *(pLineData->nwrapline-wrapline);
                        }

                        pMLEditData->EndlineDisp = MIN (pMLEditData->StartlineDisp
                                                        + pMLEditData->MaxlinesDisp - 1,
                                                       pMLEditData->wraplines - 1);
                        pMLEditData->linesDisp = pMLEditData->EndlineDisp 
                                                    - pMLEditData->StartlineDisp + 1;

                        if (wrapline == pLineData->nwrapline /*&& pLineData->nwrapline != 1*/
                                        && pMLEditData->veditPos == pMLEditData->fit_chars)
                        {
                            pMLEditData->editLine--;
                            if (pMLEditData->editLine < pMLEditData->StartlineDisp)
                            {
                                bScroll = TRUE;
                                pMLEditData->StartlineDisp--;
                                pMLEditData->EndlineDisp = MIN (pMLEditData->StartlineDisp +
                                                                    pMLEditData->MaxlinesDisp -1, 
                                                                pMLEditData->EndlineDisp);
                                pMLEditData->linesDisp = pMLEditData->EndlineDisp 
                                                            - pMLEditData->StartlineDisp + 1;
                            }
                        }
                        if (oldnwrapline != pLineData->nwrapline)
                            bScroll = TRUE;
                        edtSetCaretPos (hWnd);
                        if (bScroll)
                            edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                        InvalidateRect (hWnd, &InvRect,TRUE); 
                        if (pMLEditData->bSCROLL)
                            InvalidateRect (hWnd, NULL, TRUE);
                        NotifyParent (hWnd, GetDlgCtrlID(hWnd), EN_CHANGE);
                        return 0;
                    }

                       temp = pLineData->next; 
                    if (pMLEditData->veditPos == pMLEditData->fit_chars && temp)
                    {
                        if(pLineData->dataEnd + temp->dataEnd <= LEN_MLEDIT_BUFFER)
                        {
                            pMLEditData->curtotalLen --;
                            memcpy(pLineData->buffer+pLineData->dataEnd,temp->buffer,temp->dataEnd);    
                            pLineData->dataEnd += temp->dataEnd;
                            pLineData->buffer[pLineData->dataEnd] = '\0';
                            if(temp->next)
                            {
                                pLineData->next = temp->next;
                                temp->next->previous = pLineData;    
                            }
                            else
                                pLineData->next = NULL;
                            Free (temp);
                            temp = pLineData->next;
                            while (temp)
                            {
                                temp->lineNO--;
                                temp = temp->next;
                            }
                            edtGetLineInfo (hWnd, pLineData);
                    
                            if(pMLEditData->EndlineDisp >= pMLEditData->lines-1)        
                            {
                            // added by leon to optimize display
                            if ((pMLEditData->EndlineDisp == pMLEditData->lines -1) 
                                        && (pMLEditData->StartlineDisp != 0)) {
                                InvRect.bottom = (pMLEditData->editLine - pMLEditData->StartlineDisp + 1) 
                                                    * pMLEditData->lineHeight + pMLEditData->topMargin;
                            }else {
                                InvRect.top = (pMLEditData->editLine - pMLEditData->StartlineDisp) 
                                        * pMLEditData->lineHeight + pMLEditData->topMargin;
                            }
                            // added by leon to optimize display
                                if(pMLEditData->StartlineDisp !=0)
                                {
                                    pMLEditData->StartlineDisp--;
                                    pMLEditData->EndlineDisp = MIN ( pMLEditData->lines-1,
                                        pMLEditData->StartlineDisp+pMLEditData->MaxlinesDisp -1);
                                } else
                                    pMLEditData->EndlineDisp--;
                                
                                pMLEditData->linesDisp = pMLEditData->EndlineDisp -
                                                pMLEditData->StartlineDisp+1;
                            }
                            else if (pMLEditData->lines <= pMLEditData->MaxlinesDisp)
                            {
                                pMLEditData->EndlineDisp--;
                                pMLEditData->linesDisp = pMLEditData->EndlineDisp -
                                                pMLEditData->StartlineDisp+1;
                            }
                            pMLEditData->lines--;
                        } else if (temp->dataEnd > 0)
                        {
                            leftLen = LEN_MLEDIT_BUFFER - pLineData->dataEnd;
                            memcpy(pLineData->buffer+pLineData->dataEnd,temp->buffer,leftLen);
                            pLineData->dataEnd +=leftLen;
                            pLineData->buffer[pLineData->dataEnd] = '\0';
                            memcpy(temp->buffer,temp->buffer+leftLen,temp->dataEnd-leftLen);  
                            temp->dataEnd -=leftLen;
                            temp->buffer[temp->dataEnd] = '\0';
                            // added by leon to optimize display
                            InvRect.top = (pMLEditData->editLine - pMLEditData->StartlineDisp) 
                                    * pMLEditData->lineHeight + pMLEditData->topMargin;
                            // added by leon to optimize display
                        }
                    }
                    else if (pMLEditData->veditPos != pMLEditData->fit_chars)
                    {    
                        edtGetLineInfo (hWnd, GetLineData (pMLEditData, pMLEditData->editLine));

                        deleted = pMLEditData->pos_chars[pMLEditData->veditPos + 1]
                                    - pMLEditData->pos_chars[pMLEditData->veditPos];
                        pMLEditData->curtotalLen -= deleted;

                        for (i = pMLEditData->pos_chars[pMLEditData->veditPos]; 
                                i < pLineData->dataEnd - deleted;
                                i++)
                            pLineData->buffer [i] 
                                = pLineData->buffer [i + deleted];

                        pLineData->dataEnd -= deleted;
                        if (pLineData->dataEnd == 0) {
                            pMLEditData->fit_chars = 0;
                            pMLEditData->pos_chars[0] = 0;
                            pMLEditData->dx_chars[0] = 0;
                        }
                        pLineData->buffer[pLineData->dataEnd] = '\0';
                        // only current line to redraw
                        if (pMLEditData->EndlineDisp <= pMLEditData->lines - 1) {
                            InvRect.left = pMLEditData->dx_chars[pMLEditData->veditPos] 
                                            - pMLEditData->dx_chars[pMLEditData->vdispPos]
                                                + pMLEditData->leftMargin - 1; 
                            InvRect.top = (pMLEditData->editLine - pMLEditData->StartlineDisp) 
                                            * pMLEditData->lineHeight
                                            + pMLEditData->topMargin;
                            InvRect.bottom = InvRect.top + pMLEditData->lineHeight * 2;
                        }
                    }
                    else {
                        InvRect.left = InvRect.top = InvRect.right = InvRect.bottom = 0;
                        Ping ();
                    }
                    
                    edtGetLineInfo (hWnd, GetLineData (pMLEditData, pMLEditData->editLine));
                    pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    bChange = TRUE;
                    edtSetCaretPos (hWnd);
                    InvalidateRect (hWnd, &InvRect,TRUE); 
                    edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                    if (pMLEditData->bSCROLL)
                        InvalidateRect (hWnd, NULL, TRUE);
                }
                break;

                case VK_BACK:
                {
                    PLINEDATA temp;
                    int32_t leftLen,tempEnd;
                    int32_t oldnwrapline;
                    bool32_t bScroll = FALSE;

                    if (dwStyle & ES_READONLY) {
                        Ping ();
                        return 0;
                    }
                       edtPosProc (hWnd); 
                    GetClientRect (hWnd, &InvRect);
                    
                    if (dwStyle & ES_AUTOWRAP)
                    {
                        edtGetLineNums (pMLEditData, pMLEditData->editLine, &line, &wrapline);
                        pLineData = GetLineData(pMLEditData, line);
                    } else
                        pLineData = GetLineData(pMLEditData, pMLEditData->editLine);
                    
                    edtGetLineInfo (hWnd,pLineData); 

                    temp = pLineData->previous;    

                    if (!temp && !(pMLEditData->veditPos)) return 0;
                    
                    if (dwStyle & ES_AUTOWRAP)
                    {
                        oldnwrapline = pLineData->nwrapline;
/*-------------------------------------------*/                            
                        if (pMLEditData->veditPos == 0 && temp)
                        {
                            pMLEditData->wraplines -= pLineData->nwrapline + temp->nwrapline;
                            pMLEditData->curtotalLen --;
                            
                            tempEnd = temp->dataEnd;
                            edtGetLineInfo (hWnd, temp);
                                
                            if(pLineData->dataEnd + temp->dataEnd <= LEN_MLEDIT_BUFFER)    
                            {
                                pMLEditData->veditPos = pMLEditData->fit_chars;
                                memcpy(temp->buffer+temp->dataEnd,pLineData->buffer,pLineData->dataEnd);    
                                temp->dataEnd +=pLineData->dataEnd;
                                temp->buffer[temp->dataEnd] = '\0';
                                if(pLineData->next)
                                {
                                    temp->next = pLineData->next;
                                    pLineData->next->previous = temp;
                                } else
                                    temp->next = NULL;
                                Free (pLineData);
                                pLineData = temp;
                                temp = temp->next;
                                while(temp)
                                {
                                    temp->lineNO--;
                                    temp = temp->next;
                                }
                                pMLEditData->lines--;
                                if (pMLEditData->editLine == pMLEditData->StartlineDisp)
                                {
                                    pMLEditData->StartlineDisp--;
                                    bScroll = TRUE;
                                } /*else if ( pMLEditData->EndlineDisp == pMLEditData->wraplines-1) {
                                    if( pMLEditData->StartlineDisp != 0)
                                        pMLEditData->StartlineDisp--;    
                                }*/
                                   calcLineInfo (hWnd, pMLEditData, pLineData);
                                pMLEditData->wraplines += pLineData->nwrapline;
                                   pMLEditData->EndlineDisp = MIN (pMLEditData->wraplines - 1,
                                                               pMLEditData->StartlineDisp 
                                                            + pMLEditData->MaxlinesDisp - 1 );
                                pMLEditData->linesDisp = pMLEditData->EndlineDisp
                                                - pMLEditData->StartlineDisp + 1;    
                                pMLEditData->editLine--;
                            }
                            else if (pLineData->dataEnd > 0)
                            {
                                pMLEditData->wraplines -= pLineData->nwrapline + temp->nwrapline;
                                pMLEditData->veditPos = pMLEditData->fit_chars;
                                leftLen = LEN_MLEDIT_BUFFER - temp->dataEnd;
                                memcpy(temp->buffer+temp->dataEnd,pLineData->buffer,leftLen);
                                temp->dataEnd +=leftLen;
                                temp->buffer[temp->dataEnd] = '\0';
                                memcpy(pLineData->buffer,pLineData->buffer+leftLen,pLineData->dataEnd-leftLen);  
                                pLineData->dataEnd -=leftLen;
                                pLineData->buffer[pLineData->dataEnd] = '\0';
                                pMLEditData->editLine--;
                                   calcLineInfo (hWnd, pMLEditData, pLineData);
                                   calcLineInfo (hWnd, pMLEditData, temp);
                                pMLEditData->wraplines += pLineData->nwrapline + temp->nwrapline;
                            }
                            InvRect.top = (pMLEditData->editLine - pMLEditData->StartlineDisp)
                                               * pMLEditData->lineHeight
                                            + pMLEditData->topMargin;
                        } else if (pMLEditData->veditPos != 0 ) {// not the head 
                            pMLEditData->wraplines -= pLineData->nwrapline;
                            deleted = pMLEditData->pos_chars[pMLEditData->veditPos] 
                                        - pMLEditData->pos_chars[pMLEditData->veditPos - 1];
                            pMLEditData->curtotalLen -= deleted;
                            for (i = pMLEditData->pos_chars[pMLEditData->veditPos]; 
                                    i < pLineData->dataEnd;
                                    i++)
                                pLineData->buffer[i - deleted] = pLineData->buffer[i];

                              pLineData->dataEnd -= deleted;
                            pMLEditData->veditPos -= 1;
                            pLineData->buffer[pLineData->dataEnd] = '\0';
                               calcLineInfo (hWnd, pMLEditData, pLineData);

                            InvRect.top = (pMLEditData->editLine - pMLEditData->StartlineDisp)
                                               * pMLEditData->lineHeight
                                            + pMLEditData->topMargin;
                            if (oldnwrapline == pLineData->nwrapline)
                            {
                                if (wrapline == oldnwrapline - 1)
                                    InvRect.left = pMLEditData->dx_chars[pMLEditData->veditPos]
                                           - pMLEditData->dx_chars[pLineData->wrapStartPos[wrapline]]
                                             + pMLEditData->leftMargin - 1; 
                                InvRect.bottom = InvRect.top + pMLEditData->lineHeight
                                                                *(pLineData->nwrapline-wrapline);
                            }
                            if ((pMLEditData->veditPos == pLineData->wrapStartPos[wrapline]
                                    || pMLEditData->veditPos == pLineData->wrapStartPos[wrapline]-1)
                                    && pMLEditData->editLine == pMLEditData->StartlineDisp
                                    && oldnwrapline > 1 && wrapline != 0)
                            {
                                pMLEditData->StartlineDisp--;
                                pMLEditData->editLine = pMLEditData->StartlineDisp;
                                bScroll = TRUE;
                            }else if ((pMLEditData->veditPos == pLineData->wrapStartPos[wrapline]
                                || pMLEditData->veditPos == pLineData->wrapStartPos[wrapline]-1) 
                                    && oldnwrapline > 1 && wrapline != 0)
                            {
                                //FIXME
                                InvRect.top -= pMLEditData->lineHeight;
                                pMLEditData->editLine--;
                            }
                               calcLineInfo (hWnd, pMLEditData, pLineData);
                            pMLEditData->wraplines += pLineData->nwrapline;
                            pMLEditData->EndlineDisp = MIN (pMLEditData->wraplines - 1,
                                                           pMLEditData->StartlineDisp 
                                                        + pMLEditData->MaxlinesDisp - 1 );
                            pMLEditData->linesDisp = pMLEditData->EndlineDisp
                                                    - pMLEditData->StartlineDisp + 1;    
                        }
                        else {
                            InvRect.left = InvRect.top = InvRect.right = InvRect.bottom = 0;
                            Ping ();
                        }
                        edtSetCaretPos (hWnd);
                        if (bScroll)
                        {
                            InvalidateRect (hWnd, NULL, TRUE);
                            edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                        }
                        else
                            InvalidateRect (hWnd, &InvRect, TRUE);
                        if (pMLEditData->bSCROLL)
                        {
                            InvalidateRect (hWnd, NULL, TRUE);
                            edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                        }
                        NotifyParent (hWnd, GetDlgCtrlID(hWnd), EN_CHANGE);
/*-------------------------------------------*/                            
                        return 0;
                    }
                            
                    //the caret is at the head of current line
                    if (pMLEditData->veditPos == 0 && temp)
                    {
                        tempEnd = temp->dataEnd;
                        edtGetLineInfo (hWnd, temp);
                        pMLEditData->curtotalLen --;
                        if ( pMLEditData->sz.cx < edtGetOutWidth (hWnd)) { 
                            if (( pMLEditData->lines - 1 == pMLEditData->EndlineDisp) 
                                    && (pMLEditData->StartlineDisp != 0)) {
                                InvRect.bottom =(pMLEditData->editLine - 
                                                pMLEditData->StartlineDisp + 1)
                                               * pMLEditData->lineHeight 
                                               + pMLEditData->topMargin;
                            }else {
                                //partly redraw
                                InvRect.top = (pMLEditData->editLine - 
                                                pMLEditData->StartlineDisp - 1 )
                                               * pMLEditData->lineHeight 
                                               + pMLEditData->topMargin;
                            }
                        }
                                
                        if(pLineData->dataEnd + temp->dataEnd <= LEN_MLEDIT_BUFFER)    
                        {
                            edtGetLineInfo (hWnd, temp);
                            pMLEditData->veditPos = pMLEditData->fit_chars;
                            memcpy(temp->buffer+temp->dataEnd,pLineData->buffer,pLineData->dataEnd);    
                            temp->dataEnd +=pLineData->dataEnd;
                            temp->buffer[temp->dataEnd] = '\0';
                            if(pLineData->next)
                            {
                                temp->next = pLineData->next;
                                pLineData->next->previous = temp;
                            }
                            else
                                temp->next = NULL;
                            Free (pLineData);
                            pLineData = temp;
                            temp = temp->next;
                            while(temp)
                            {
                                temp->lineNO--;
                                temp = temp->next;
                            }
                            pMLEditData->lines--;
                            if (pMLEditData->StartlineDisp == pMLEditData->editLine
                                    && pMLEditData->StartlineDisp != 0)
                            {
                                pMLEditData->StartlineDisp--;
                                if(pMLEditData->EndlineDisp == pMLEditData->lines)
                                {
                                    pMLEditData->EndlineDisp = MIN (pMLEditData->lines - 1,
                                       pMLEditData->StartlineDisp + pMLEditData->MaxlinesDisp - 1 );
                                }
                                else 
                                {
                                    pMLEditData->EndlineDisp--;
                                }
                                    pMLEditData->linesDisp = pMLEditData->EndlineDisp
                                                    - pMLEditData->StartlineDisp + 1;    
                            }
                            else if ( pMLEditData->EndlineDisp == pMLEditData->lines
                                    && pMLEditData->editLine != pMLEditData->StartlineDisp )
                            {
                                if( pMLEditData->StartlineDisp != 0)
                                    pMLEditData->StartlineDisp--;    

                                pMLEditData->EndlineDisp--;
                                pMLEditData->linesDisp = pMLEditData->EndlineDisp -
                                                pMLEditData->StartlineDisp + 1;
                            }
                            pMLEditData->editLine--;
                        }
                        else if (pLineData->dataEnd > 0)
                        {
                            edtGetLineInfo (hWnd, temp);
                            pMLEditData->veditPos = pMLEditData->fit_chars;
                            leftLen = LEN_MLEDIT_BUFFER - temp->dataEnd;
                            memcpy(temp->buffer+temp->dataEnd,pLineData->buffer,leftLen);
                            temp->dataEnd +=leftLen;
                            temp->buffer[temp->dataEnd] = '\0';
                            memcpy(pLineData->buffer,pLineData->buffer+leftLen,pLineData->dataEnd-leftLen);  
                            pLineData->dataEnd -=leftLen;
                            pLineData->buffer[pLineData->dataEnd] = '\0';
                        }
                        /* added by leon to fix the 'roll window' bug */
                        if (pMLEditData->veditPos > edtGetStartDispPosAtEnd(hWnd,pLineData))
                            pMLEditData->vdispPos = edtGetStartDispPosAtEnd(hWnd,pLineData);
                        else {
                            if (edtGetNewvPos (hWnd,
                                                GetLineData(pMLEditData,pMLEditData->editLine),
                                                pMLEditData->dx_chars[pMLEditData->veditPos] 
                                                - edtGetOutWidth (hWnd)/4) == -1)
                                pMLEditData->vdispPos = 0;
                            else
                                pMLEditData->vdispPos = edtGetNewvPos (hWnd,
                                                GetLineData(pMLEditData,pMLEditData->editLine),
                                                pMLEditData->dx_chars[pMLEditData->veditPos] 
                                                - edtGetOutWidth (hWnd)/4);
                        }
                        /*add end*/
                        
                           if (pMLEditData->vdispPos == pMLEditData->veditPos 
                                && pMLEditData->veditPos != 0) {
                            if (edtGetNewvPos (hWnd,
                                                GetLineData(pMLEditData,pMLEditData->editLine),
                                                pMLEditData->dx_chars[pMLEditData->veditPos] 
                                                - edtGetOutWidth (hWnd)/4) == -1)
                                pMLEditData->vdispPos = 0;
                            else
                                pMLEditData->vdispPos = edtGetNewvPos (hWnd,
                                                GetLineData(pMLEditData,pMLEditData->editLine),
                                                pMLEditData->dx_chars[pMLEditData->veditPos] 
                                                - edtGetOutWidth (hWnd)/4);
                            }
                    }
                    // not the head
                    else if (pMLEditData->veditPos != 0 )
                    {
                        deleted = pMLEditData->pos_chars[pMLEditData->veditPos] 
                                    - pMLEditData->pos_chars[pMLEditData->veditPos - 1];
                        pMLEditData->curtotalLen -= deleted;
                        for (i = pMLEditData->pos_chars[pMLEditData->veditPos]; 
                                i < pLineData->dataEnd;
                                i++)
                            pLineData->buffer [i - deleted] 
                                = pLineData->buffer [i];

                          pLineData->dataEnd -= deleted;
                        pMLEditData->veditPos -= 1;
                        pLineData->buffer[pLineData->dataEnd] = '\0';
                        if (pMLEditData->vdispPos == pMLEditData->veditPos) {
                            if (edtGetNewvPos (hWnd,
                                                GetLineData(pMLEditData,pMLEditData->editLine),
                                                pMLEditData->dx_chars[pMLEditData->veditPos] 
                                                - edtGetOutWidth (hWnd)/4) == -1)
                                pMLEditData->vdispPos = 0;
                            else
                                pMLEditData->vdispPos = edtGetNewvPos (hWnd,
                                                GetLineData(pMLEditData,pMLEditData->editLine),
                                                pMLEditData->dx_chars[pMLEditData->veditPos] 
                                                - edtGetOutWidth (hWnd)/4);
                        }else {
                            InvRect.left = pMLEditData->dx_chars[pMLEditData->veditPos]
                                               - pMLEditData->dx_chars[pMLEditData->vdispPos]    
                                                + pMLEditData->leftMargin - 1; 
                            InvRect.top = (pMLEditData->editLine - pMLEditData->StartlineDisp)
                                               * pMLEditData->lineHeight
                                            + pMLEditData->topMargin;
                            InvRect.bottom = InvRect.top + pMLEditData->lineHeight*2;

                        }
                    }
                    else {
                        InvRect.left = InvRect.top = InvRect.right = InvRect.bottom = 0;
                        Ping ();
                    }
                    
                    bChange = TRUE;
                    pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    edtSetCaretPos (hWnd);
                    InvalidateRect (hWnd, &InvRect, TRUE);// modified by leon
                    edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                    if (pMLEditData->bSCROLL)
                        InvalidateRect (hWnd, NULL, TRUE);
                }
                break;
                case VK_PRIOR:
                {
                    int32_t olddispPos, line, wrapline;
                    int32_t newOff;
                    int32_t LinesDisp = pMLEditData->MaxlinesDisp;
                    
                    olddispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    newOff = pMLEditData->dx_chars[pMLEditData->veditPos];
                    
                    if (pMLEditData->StartlineDisp == 0)
                        return 0;

                    if (dwStyle & ES_AUTOWRAP)
                    {
                        edtGetLineNums (pMLEditData, pMLEditData->editLine, &line, &wrapline);
                        pLineData = GetLineData(pMLEditData, line);
                        edtGetLineInfo (hWnd, pLineData);
                        newOff = pMLEditData->dx_chars[pMLEditData->veditPos] 
                                    - pMLEditData->dx_chars[pLineData->wrapStartPos[wrapline]];
                    }

                    if (pMLEditData->StartlineDisp > LinesDisp - 1) {
                        pMLEditData->EndlineDisp = pMLEditData->StartlineDisp;
                        pMLEditData->StartlineDisp -= LinesDisp - 1;
                        pMLEditData->editLine -= LinesDisp - 1;
                    }else {
                        pMLEditData->editLine -= pMLEditData->StartlineDisp; 
                        if (pMLEditData->EndlineDisp > LinesDisp -1)
                            pMLEditData->EndlineDisp = LinesDisp -1;
                        pMLEditData->StartlineDisp = 0;
                    }
                    edtGetCaretValid (hWnd, pMLEditData->editLine, olddispPos, newOff);
                    pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                    InvalidateRect (hWnd, NULL, TRUE);
                    edtSetCaretPos (hWnd);
                    if (pMLEditData->bSCROLL)
                        InvalidateRect (hWnd, NULL, TRUE);
                }
                break;
                case VK_NEXT:
                {
                    int32_t olddispPos, line, wrapline;
                    int32_t newOff;
                    int32_t LinesDisp = pMLEditData->MaxlinesDisp;
                    
                    olddispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    newOff = pMLEditData->dx_chars[pMLEditData->veditPos];
                    
                    if (dwStyle & ES_AUTOWRAP)
                    {
                        if (pMLEditData->EndlineDisp == pMLEditData->wraplines -1) return 0;
                        edtGetLineNums (pMLEditData, pMLEditData->editLine, &line, &wrapline);
                        pLineData = GetLineData(pMLEditData, line);
                        edtGetLineInfo (hWnd, pLineData);
                        newOff = pMLEditData->dx_chars[pMLEditData->veditPos] 
                                    - pMLEditData->dx_chars[pLineData->wrapStartPos[wrapline]];
                        if (pMLEditData->wraplines - pMLEditData->EndlineDisp > LinesDisp - 1) {
                            pMLEditData->StartlineDisp = pMLEditData->EndlineDisp;
                            pMLEditData->editLine += LinesDisp - 1;
                            pMLEditData->EndlineDisp += LinesDisp -1;
                        }else {
                            pMLEditData->EndlineDisp = pMLEditData->wraplines - 1;
                            pMLEditData->editLine += (pMLEditData->EndlineDisp - (LinesDisp - 1))
                                                       - pMLEditData->StartlineDisp;
                            pMLEditData->StartlineDisp = pMLEditData->EndlineDisp - (LinesDisp - 1);
                        }
                    }else {
                        if (pMLEditData->EndlineDisp == pMLEditData->lines -1) return 0;
                    
                        if (pMLEditData->lines - pMLEditData->EndlineDisp > LinesDisp - 1) {
                            pMLEditData->StartlineDisp = pMLEditData->EndlineDisp;
                            pMLEditData->editLine += LinesDisp - 1;
                            pMLEditData->EndlineDisp += LinesDisp -1;
                        }else {
                            pMLEditData->EndlineDisp = pMLEditData->lines - 1;
                            pMLEditData->editLine += (pMLEditData->EndlineDisp - (LinesDisp - 1))
                                                       - pMLEditData->StartlineDisp;
                            pMLEditData->StartlineDisp = pMLEditData->EndlineDisp - (LinesDisp - 1);
                        }
                    }
                    edtGetCaretValid (hWnd, pMLEditData->editLine, olddispPos, newOff);
                    pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                       InvalidateRect (hWnd, NULL, TRUE);
                    edtSetCaretPos (hWnd);
                    if (pMLEditData->bSCROLL)
                        InvalidateRect (hWnd, NULL, TRUE);
                }
                break;
                default:
                break;
            }
       
            if (bChange)
                NotifyParent (hWnd, GetDlgCtrlID(hWnd), EN_CHANGE);

            
            return 0;
        }

        case EV_CHAR:
        {
            unsigned char charBuffer [2];
            int32_t  i, chars, inserting, oldeditPos;
            int32_t  line, wrapline, oldnwrapline=0;
            RECT InvRect; 
            bool32_t bScroll = FALSE;

            if (dwStyle & ES_READONLY) {
                Ping();
                return 0;
            }

            if (HIuint8_t (wParam)) {
                charBuffer [0] = LOuint8_t (wParam);
                charBuffer [1] = HIuint8_t (wParam);
                chars = 2;
            }
            else {
                chars = 1;

                if (dwStyle & ES_UPPERCASE) {
                    charBuffer [0] = toupper (LOuint8_t (wParam));
                }
                else if (dwStyle & ES_LOWERCASE) {
                    charBuffer [0] = tolower (LOuint8_t (wParam));
                }
                else
                    charBuffer [0] = LOuint8_t (wParam);
            }
            
            if (chars == 1) {
#if 0
                switch (charBuffer [0])
                {
                    case 0x00:  // NULL
                    case 0x07:  // BEL
                    case 0x08:  // BS
                    case 0x09:  // HT
                    case 0x0A:  // LF
                    case 0x0B:  // VT
                    case 0x0C:  // FF
                    case 0x0D:  // CR
                    case 0x1B:  // Escape
                    return 0;
                }
#else
                if (charBuffer [0] < 0x20 || charBuffer [0] == 0x7F)
                    return 0;
#endif
            }

            pMLEditData = (PMLEDITDATA)GetWindowAdditionalData2(hWnd); 
            
            GetClientRect (hWnd, &InvRect);
            InvRect.left = pMLEditData->leftMargin - 1;

            edtPosProc (hWnd); 

            if (dwStyle & ES_AUTOWRAP)
            {
                edtGetLineNums (pMLEditData, pMLEditData->editLine, &line, &wrapline);
                pLineData = GetLineData(pMLEditData, line);
            }else
                pLineData = GetLineData(pMLEditData,pMLEditData->editLine);

            if (pMLEditData->status & EST_REPLACE) {
                if (pMLEditData->veditPos == pMLEditData->fit_chars)
                    inserting = chars;
                else 
                    inserting = chars - ( pMLEditData->pos_chars[pMLEditData->veditPos + 1]
                                        - pMLEditData->pos_chars[pMLEditData->veditPos] );
            }
            else
                inserting = chars;
            // check space
            if (pLineData->dataEnd + inserting > pMLEditData->lineLimit) {
                Ping ();
                //NotifyParent (hWnd, GetDlgCtrlID(hWnd), EN_MAXTEXT);
                return 0;
            } 
            if ((pMLEditData->totalLimit >= 0) 
                        && ((pMLEditData->curtotalLen + inserting) 
                            > pMLEditData->totalLimit)) {
                Ping ();
                NotifyParent (hWnd, GetDlgCtrlID(hWnd), EN_MAXTEXT);
                return 0;
            }else
                pMLEditData->curtotalLen += inserting;

            if (dwStyle & ES_AUTOWRAP)
            {
                pMLEditData->wraplines -= pLineData->nwrapline;
                oldnwrapline = pLineData->nwrapline;
            }

            edtGetLineInfo (hWnd, pLineData);
            oldeditPos = pMLEditData->dx_chars[pMLEditData->veditPos]
                            - pMLEditData->dx_chars[pMLEditData->vdispPos];

            if (inserting == -1) {
                for (i = pMLEditData->pos_chars[pMLEditData->veditPos];
                        i < pLineData->dataEnd-1; i++)
                        pLineData->buffer [i] = pLineData->buffer [i + 1];
            }
            else if (inserting > 0) {
                for (i = pLineData->dataEnd + inserting - 1; 
                        i > pMLEditData->pos_chars[pMLEditData->veditPos] + inserting - 1; 
                        i--)
                    pLineData->buffer [i] 
                            = pLineData->buffer [i - inserting];
            }
            for (i = 0; i < chars; i++)
                    pLineData->buffer [pMLEditData->pos_chars[pMLEditData->veditPos] + i] 
                        = charBuffer [i];
            
            pMLEditData->veditPos += 1;
            pLineData->dataEnd += inserting;
            pLineData->buffer[pLineData->dataEnd] = '\0';
            
            edtGetLineInfo (hWnd, pLineData);

            if (dwStyle & ES_AUTOWRAP)
            {
                   calcLineInfo (hWnd, pMLEditData, pLineData);
                pMLEditData->wraplines += pLineData->nwrapline;

                if ( (wrapline == pLineData->nwrapline-1
                        && pMLEditData->veditPos > pLineData->wrapStartPos[wrapline+1])
                    || (wrapline < pLineData->nwrapline-1 
                            && pMLEditData->veditPos >= pLineData->wrapStartPos[wrapline+1]) )
                {
                    if (pMLEditData->editLine 
                                == pMLEditData->StartlineDisp+pMLEditData->MaxlinesDisp-1)
                    {
                        pMLEditData->StartlineDisp ++;
                        bScroll = TRUE;
                    }
                    else
                        InvRect.top = (pMLEditData->editLine - pMLEditData->StartlineDisp) 
                                            * pMLEditData->lineHeight
                                        + pMLEditData->topMargin;
                    pMLEditData->editLine++;
                    pMLEditData->linesDisp =  MIN (pMLEditData->wraplines 
                                                    - pMLEditData->StartlineDisp, 
                                                pMLEditData->MaxlinesDisp);
                    pMLEditData->EndlineDisp = 
                                    pMLEditData->StartlineDisp + pMLEditData->linesDisp - 1;

                }else {
                    if (wrapline == pLineData->nwrapline-1)
                        InvRect.left = pMLEditData->leftMargin
                                        + pMLEditData->dx_chars[pMLEditData->veditPos-1] 
                                        - pMLEditData->dx_chars[pLineData->wrapStartPos[wrapline]]
                                        - 1;
                    InvRect.top = (pMLEditData->editLine - pMLEditData->StartlineDisp) 
                                        * pMLEditData->lineHeight
                                    + pMLEditData->topMargin;
                    if (oldnwrapline == pLineData->nwrapline)
                    {
                        InvRect.bottom = InvRect.top
                                        + (pLineData->nwrapline-wrapline)*pMLEditData->lineHeight;
                    }
                }

                edtSetCaretPos (hWnd);
                InvalidateRect (hWnd, &InvRect,TRUE);
                NotifyParent (hWnd, GetDlgCtrlID(hWnd), EN_CHANGE);
                if (bScroll)
                    edtSetScrollInfo (hWnd, pMLEditData, TRUE);
                if (pMLEditData->bSCROLL)
                    InvalidateRect (hWnd, NULL, TRUE);
                return 0;
            }

            
            //for display
            if (((pMLEditData->dx_chars[pMLEditData->veditPos] 
                            - pMLEditData->dx_chars[pMLEditData->vdispPos])
                        > edtGetOutWidth (hWnd))
                    && (pMLEditData->dx_chars[pMLEditData->veditPos - 1] 
                            - pMLEditData->dx_chars[pMLEditData->vdispPos])
                        <= edtGetOutWidth (hWnd)) {

                bScroll = TRUE;
                pMLEditData->vdispPos = edtGetNewvPos (hWnd, pLineData, 
                                            pMLEditData->dx_chars[pMLEditData->veditPos]
                                            - edtGetOutWidth (hWnd)*3/4);
            }
            pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
            edtSetCaretPos (hWnd);
            if (!bScroll) {
                // only current line to redraw
                InvRect.left = pMLEditData->leftMargin + oldeditPos - 1;
                InvRect.top = (pMLEditData->editLine - pMLEditData->StartlineDisp) 
                                * pMLEditData->lineHeight
                                   + pMLEditData->topMargin;
                InvRect.bottom = InvRect.top + pMLEditData->lineHeight;
            }
            InvalidateRect (hWnd, &InvRect,TRUE);// modified by leon to display
            if (pMLEditData->bSCROLL)
                InvalidateRect (hWnd, NULL, TRUE);
            NotifyParent (hWnd, GetDlgCtrlID(hWnd), EN_CHANGE);
            edtSetScrollInfo (hWnd, pMLEditData, TRUE);
        }
        return 0;

        case EV_GETTEXTLEN:
        {
            pMLEditData = (PMLEDITDATA)GetWindowAdditionalData2(hWnd); 
            return pMLEditData->curtotalLen;
        }
        
        case EV_GETTEXT:
        {
            PLINEDATA temp;
            int32_t len,total = 0,dataLen=0;
            char * buffer = (char*)lParam;
            pMLEditData = (PMLEDITDATA)GetWindowAdditionalData2(hWnd); 
            len = (int32_t)wParam;
            temp = pMLEditData->head;
            // while (temp && total + temp->dataEnd <= len)  
            while (temp && total < len)  
            {
                if (temp != pMLEditData->head)
                {
                    buffer[total]='\012';
                    total++;
                }
                dataLen = MIN (temp->dataEnd, len-total);
                memcpy(buffer+total,temp->buffer,dataLen);
                total += dataLen;
                temp = temp->next;
            }    
            buffer[total]='\000';//finish flag.
        }
        return 0;

        case EV_SETTEXT:
        {
            pMLEditData =(PMLEDITDATA)GetWindowAdditionalData2(hWnd);
            MLEditEmptyBuffer(pMLEditData);
            pMLEditData->veditPos        = 0;
            pMLEditData->editLine       = 0;
            pMLEditData->vdispPos         = 0;
            pMLEditData->dispPos        = 0;
            pMLEditData->vdispPos        = 0;
            pMLEditData->StartlineDisp  = 0;
            pMLEditData->wraplines        = 0;
            pMLEditData->curtotalLen    = 0;

            MLEditInitBuffer(hWnd, pMLEditData, (char *)lParam, dwStyle);

            pMLEditData->realdispPos = 0;
            pMLEditData->realStartLine = 0;
            pMLEditData->realEndLine = 0;
            pMLEditData->diff = FALSE;
            pMLEditData->bSCROLL = FALSE;

            edtGetLineInfo (hWnd, pMLEditData->head);
            SetCaretPos (hWnd, pMLEditData->leftMargin,pMLEditData->topMargin);
            InvalidateRect (hWnd, NULL,TRUE);
            edtSetScrollInfo (hWnd, pMLEditData, TRUE);
        }
        return 0;

// can i add it to message defined?
/*
        case EV_GETLINETEXT:
        {
            PLINEDATA temp;
            char*   buffer = (char*)lParam;
            int32_t     lineNO,len;

            pMLEditData = (PMLEDITDATA)GetWindowAdditionalData2(hWnd); 
            lineNO = (int32_t)wParam;
            temp = GetLineData(pMLEditData,lineNO);
            if(temp)
            {
                len = MIN ((int32_t)wParam, temp->dataEnd);
                memcpy (buffer, temp->buffer,len);
                buffer [len] = '\0';
                return 0;
            }
            return -1;
        }
        break;

        case EV_SETLINETEXT:
        {
            int32_t len,lineNO;
            PLINEDATA temp;

            if (dwStyle & ES_READONLY)
                return 0;

            pMLEditData = (PMLEDITDATA)GetWindowAdditionalData2(hWnd); 
            
            len = strlen ((char*)lParam);
            lineNO = (int32_t)wParam;
            temp = pMLEditData->head;
            len = MIN (len, pMLEditData->totalLen);
            
            if (pMLEditData->totalLimit >= 0)
                len = MIN (len, pMLEditData->totalLimit);
              while (temp)
            {
                if(temp->lineNO == lineNO)
                {
                     temp->dataEnd = len;
                    memcpy (temp->buffer, (char*)lParam, len);
                }
                temp = temp->next;
            }
            pMLEditData->editPos        = 0;
            pMLEditData->caretPos       = 0;
            pMLEditData->dispPos        = 0;
            InvalidateRect (hWnd, NULL, TRUE);
        }
        return 0;
*/
#if 0   // zhanbin
        case EV_LBUTTONDBLCLK:
            NotifyParent (hWnd, GetDlgCtrlID (hWnd), EN_DBLCLK);
        break;
#endif
        
        case EV_LBUTTONDOWN:
        {
            POINT pt;
            bool32_t bScroll = FALSE;
            int32_t newOff, lineNO, olddispPos;
            
            pt.x = LOuint16_t (lParam);
            pt.y = HIuint16_t (lParam);
            ScreenToClient (hWnd, &pt);
            
            pMLEditData = (PMLEDITDATA)GetWindowAdditionalData2(hWnd);

            pMLEditData->diff = FALSE;
           
            olddispPos = pMLEditData->dispPos;
            lineNO = edtGetLineNO (pMLEditData, pt.y);
            if (lineNO < 0) {
                break;
            }
            
            lineNO += pMLEditData->StartlineDisp;
            if (dwStyle & ES_AUTOWRAP) {
                if (lineNO > pMLEditData->wraplines - 1) break;
            }else {
                if (lineNO > pMLEditData->lines - 1) break;
            }
            pMLEditData->editLine = lineNO;
            
            newOff = pt.x + olddispPos-pMLEditData->leftMargin;

            bScroll = edtGetCaretValid (hWnd,lineNO, olddispPos, newOff);

            pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
            
            edtSetCaretPos (hWnd);
            if (bScroll)
                InvalidateRect(hWnd,NULL,TRUE);    
            edtSetScrollInfo (hWnd, pMLEditData, TRUE);
            break;
        }

#if 0
        case EV_LBUTTONUP:
        case EV_MOUSEMOVE:
        break;
#endif
#if 0   // zhanbin
        case EV_GETDLGCODE:
            return DLGC_WANTCHARS | DLGC_HASSETSEL | DLGC_WANTARROWS | DLGC_WANTENTER;
        
        case EV_DOESNEEDIME:
            if (dwStyle & ES_READONLY)
                return FALSE;
            return TRUE;
        
        case EM_GETCARETPOS:
        {
            int32_t* line_pos = (int32_t *)wParam;
            int32_t* char_pos = (int32_t *)lParam;

            pMLEditData = (PMLEDITDATA)GetWindowAdditionalData2(hWnd); 

            if (line_pos) *line_pos = pMLEditData->editLine;
            if (char_pos) *char_pos = pMLEditData->veditPos;

            return pMLEditData->veditPos; 
        }

        case EM_SETREADONLY:
            if (wParam)
                IncludeWindowStyle(hWnd,ES_READONLY);
            else
                ExcludeWindowStyle(hWnd,ES_READONLY);
            return 0;
        
        case EM_SETPASSWORDCHAR:
            pMLEditData = (PMLEDITDATA)GetWindowAdditionalData2(hWnd); 

            if (pMLEditData->passwdChar != (int32_t)wParam) {
                if (dwStyle & ES_PASSWORD) {
                    pMLEditData->passwdChar = (int32_t)wParam;
                    InvalidateRect (hWnd, NULL, TRUE);
                }
            }
            return 0;
    
        case EM_GETPASSWORDCHAR:
        {
            int32_t* passwdchar;
            
            pMLEditData = (PMLEDITDATA)GetWindowAdditionalData2(hWnd); 
            passwdchar = (int32_t*) lParam;

            *passwdchar = pMLEditData->passwdChar;
            return 0;
        }
    
        case EM_LIMITTEXT:
        {
            int32_t newLimit = (int32_t)wParam;
            
            if (newLimit >= 0) {
                pMLEditData = (PMLEDITDATA)GetWindowAdditionalData2(hWnd); 
                if (newLimit < pMLEditData->lineLimit)
                {
                    pMLEditData->totalLimit = pMLEditData->lineLimit = newLimit;
                }else {
                    pMLEditData->totalLimit = newLimit;
                }
            }
            return 0;
        }
#endif
        
        case EV_VSCROLL:
        {
            int32_t     LinesDisp,newTop;
            RECT    rc;
            int32_t     olddispPos;
            int32_t     newOff;

            pMLEditData = (PMLEDITDATA)GetWindowAdditionalData2(hWnd); 

            if (!(pMLEditData->diff)) {
                pMLEditData->realeditLine = pMLEditData->editLine;
                pMLEditData->realdispPos = pMLEditData->vdispPos;
                pMLEditData->realStartLine = pMLEditData->StartlineDisp;
                pMLEditData->realEndLine   = pMLEditData->EndlineDisp;
                pMLEditData->diff = TRUE;
            }

            GetClientRect (hWnd, &rc);

            LinesDisp = (rc.bottom - rc.top - pMLEditData->topMargin - pMLEditData->bottomMargin)
                           / pMLEditData->lineHeight;
            olddispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
            newOff = pMLEditData->dx_chars[pMLEditData->veditPos];
               
            switch (LOuint16_t (wParam))
            {
                case SB_LINEDOWN:
                {
                    if (dwStyle & ES_AUTOWRAP) {
                        if (pMLEditData->EndlineDisp == pMLEditData->wraplines -1) return 0;
                        if (pMLEditData->EndlineDisp < pMLEditData->wraplines - 1) {
                            pMLEditData->EndlineDisp++;
                            pMLEditData->StartlineDisp++;
                            pMLEditData->editLine++;
                        }
                    }else {
                        if (pMLEditData->EndlineDisp == pMLEditData->lines -1) return 0;
                        if (pMLEditData->EndlineDisp < pMLEditData->lines - 1) {
                            pMLEditData->EndlineDisp++;
                            pMLEditData->StartlineDisp++;
                            pMLEditData->editLine++;
                        }
                    }
                }
                break;
                case SB_LINEUP:
                {
                    if (pMLEditData->StartlineDisp == 0)
                        return 0;
                    if (pMLEditData->StartlineDisp >  0) {
                        pMLEditData->StartlineDisp--;
                        pMLEditData->EndlineDisp--;
                        pMLEditData->editLine--;
                    }
                }
                break;
                case SB_PAGEDOWN:
                {
                    if (dwStyle & ES_AUTOWRAP) {
                        if (pMLEditData->EndlineDisp == pMLEditData->wraplines -1) return 0;
                        if (pMLEditData->wraplines - pMLEditData->EndlineDisp > LinesDisp - 1) {
                            pMLEditData->StartlineDisp = pMLEditData->EndlineDisp;
                            pMLEditData->editLine += LinesDisp - 1;
                            pMLEditData->EndlineDisp += LinesDisp -1;
                        }else {
                            pMLEditData->EndlineDisp = pMLEditData->wraplines - 1;
                            pMLEditData->editLine += (pMLEditData->EndlineDisp - (LinesDisp - 1))
                                                       - pMLEditData->StartlineDisp;
                            pMLEditData->StartlineDisp = pMLEditData->EndlineDisp - (LinesDisp - 1);
                        }
                    }else {
                        if (pMLEditData->EndlineDisp == pMLEditData->lines -1) return 0;
                        if (pMLEditData->lines - pMLEditData->EndlineDisp > LinesDisp - 1) {
                            pMLEditData->StartlineDisp = pMLEditData->EndlineDisp;
                            pMLEditData->editLine += LinesDisp - 1;
                            pMLEditData->EndlineDisp += LinesDisp -1;
                        }else {
                            pMLEditData->EndlineDisp = pMLEditData->lines - 1;
                            pMLEditData->editLine += (pMLEditData->EndlineDisp - (LinesDisp - 1))
                                                       - pMLEditData->StartlineDisp;
                            pMLEditData->StartlineDisp = pMLEditData->EndlineDisp - (LinesDisp - 1);
                        }
                    }
                }
                break;
                case SB_PAGEUP:
                {
                    
                    if (pMLEditData->StartlineDisp == 0)
                        return 0;
                    if (pMLEditData->StartlineDisp > LinesDisp - 1) {
                        pMLEditData->EndlineDisp = pMLEditData->StartlineDisp;
                        pMLEditData->StartlineDisp -= LinesDisp - 1;
                        pMLEditData->editLine -= LinesDisp - 1;
                    }else {
                        pMLEditData->editLine -= pMLEditData->StartlineDisp; 
                        pMLEditData->EndlineDisp = LinesDisp -1;
                        pMLEditData->StartlineDisp = 0;
                    }
                }
                break;
                case SB_THUMBTRACK:
                {
                    newTop = (int32_t) HIuint16_t (wParam);
                    if (dwStyle & ES_AUTOWRAP) {
                        if ((newTop > pMLEditData->StartlineDisp  
                                        && pMLEditData->EndlineDisp == pMLEditData->wraplines -1)
                                || (newTop == pMLEditData->StartlineDisp))
                            return 0;
                        if (newTop + LinesDisp - 1 < pMLEditData->wraplines) {
                            pMLEditData->EndlineDisp += - (pMLEditData->StartlineDisp - newTop);
                            pMLEditData->editLine += - (pMLEditData->StartlineDisp - newTop);
                            pMLEditData->StartlineDisp = newTop;
                        }
                    }else {
                        if ((newTop > pMLEditData->StartlineDisp  
                                            && pMLEditData->EndlineDisp == pMLEditData->lines -1)
                                || (newTop == pMLEditData->StartlineDisp))
                            return 0;
                        if (newTop + LinesDisp - 1 < pMLEditData->lines) {
                            pMLEditData->EndlineDisp += - (pMLEditData->StartlineDisp - newTop);
                            pMLEditData->editLine += - (pMLEditData->StartlineDisp - newTop);
                            pMLEditData->StartlineDisp = newTop;
                        }
                    }
                }
                break;
            }


            if ((pMLEditData->StartlineDisp <= pMLEditData->realeditLine) 
                            && (pMLEditData->EndlineDisp >= pMLEditData->realeditLine)) {
                pMLEditData->diff = FALSE;
                pMLEditData->editLine = pMLEditData->realeditLine;
                edtSetCaretPos (hWnd);

            } else 
                edtSetCaretPos (hWnd);

            edtSetScrollInfo (hWnd, pMLEditData, TRUE);
            InvalidateRect (hWnd, NULL, TRUE);
                
        }
        break;
        case EV_HSCROLL:
        {
            int32_t      newPos, width;
            int32_t     olddispPos;
            PLINEDATA temp;
            RECT     rc;

            GetClientRect (hWnd, &rc);

            pMLEditData = (PMLEDITDATA)GetWindowAdditionalData2(hWnd); 
        
            
            temp = GetLineData(pMLEditData, pMLEditData->realeditLine);

            if (!(pMLEditData->diff)) {
                pMLEditData->realeditLine = pMLEditData->editLine;
                pMLEditData->realdispPos = pMLEditData->vdispPos;
                pMLEditData->realStartLine = pMLEditData->StartlineDisp;
                pMLEditData->realEndLine   = pMLEditData->EndlineDisp;
                pMLEditData->diff = TRUE;
            }

            width = rc.right - rc.left - pMLEditData->leftMargin - pMLEditData->rightMargin; 
            
            
            olddispPos = pMLEditData->dispPos;

            switch (LOuint16_t (wParam))
            {
                case SB_LINERIGHT:
                {
                    if (olddispPos + width  >= GetMaxLen (hWnd, pMLEditData))
                        return 0;
                    pMLEditData->vdispPos ++;
                    if (pMLEditData->vdispPos > pMLEditData->fit_chars) {
                        pMLEditData->vdispPos = pMLEditData->fit_chars;
                        pMLEditData->dispPos += width/10;
                        if (pMLEditData->dispPos > GetMaxLen (hWnd, pMLEditData))
                            pMLEditData->dispPos = GetMaxLen (hWnd, pMLEditData);
                    }else {
                        pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    }

                }
                break;
                case SB_LINELEFT:
                {
                    PLINEDATA pLineData;
                    
                    if (pMLEditData->dispPos == 0)
                        return 0;
                        
                    pLineData = GetLineData(pMLEditData,pMLEditData->editLine);
                    edtGetLineInfo (hWnd, pLineData);
                    
                    if (pMLEditData->dispPos > pMLEditData->dx_chars[pMLEditData->fit_chars]) {
                        pMLEditData->dispPos -= width/10;
                        if (pMLEditData->dispPos < 0)
                            pMLEditData->dispPos = 0;
                    }else {
                        pMLEditData->vdispPos --;
                        if (pMLEditData->vdispPos < 0)
                            pMLEditData->vdispPos = 0;
                        pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    }
                }
                break;
                case SB_PAGERIGHT:
                {
                    if (olddispPos + width  >= GetMaxLen (hWnd, pMLEditData))
                        return 0;
                    if (olddispPos + width + width/4 <= GetMaxLen (hWnd, pMLEditData)) {
                        pMLEditData->vdispPos = edtGetNewvPos (hWnd, temp, olddispPos + width/4);
                        if (pMLEditData->vdispPos == -2) {
                            pMLEditData->vdispPos = pMLEditData->fit_chars;
                            pMLEditData->dispPos += width/4;
                        } else
                            pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
        
                    }
                    else {
                        pMLEditData->vdispPos = 
                                edtGetNewvPos (hWnd, temp, GetMaxLen(hWnd, pMLEditData) - width);
                        if (pMLEditData->vdispPos == -2) {
                            pMLEditData->vdispPos = pMLEditData->fit_chars;
                            pMLEditData->dispPos = GetMaxLen (hWnd, pMLEditData) - width;
                            if (pMLEditData->dispPos < 0)
                                pMLEditData->dispPos = 0;
                        } else
                            pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    }
                }
                break;
                case SB_PAGELEFT:
                {
                    if (pMLEditData->dispPos == 0)
                        return 0;
                    if (olddispPos >= width/4) {
                        if (pMLEditData->sz.cx < olddispPos - width/4)
                            pMLEditData->dispPos -= width/4;
                        else {
                            pMLEditData->vdispPos = edtGetNewvPos (hWnd, temp, olddispPos - width/4);
                            pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                        }
                    } else {
                        pMLEditData->vdispPos = 0;
                        pMLEditData->dispPos = 0;
                    }    
                }
                break;
                case SB_THUMBTRACK:
                {
                    newPos = (int32_t) HIuint16_t (wParam);
                    
                    if (newPos + width > GetMaxLen (hWnd, pMLEditData) 
                                        || (newPos < 0))
                        return 0;
                    if (newPos + width <= GetMaxLen (hWnd, pMLEditData)){
                        pMLEditData->vdispPos = 
                                edtGetNewvPos (hWnd, temp, newPos);
                        if (pMLEditData->vdispPos == -2) {
                            pMLEditData->vdispPos = pMLEditData->fit_chars;
                            pMLEditData->dispPos = newPos;
                        } else
                            pMLEditData->dispPos = pMLEditData->dx_chars[pMLEditData->vdispPos];
                    }
                    
                }
                break;
            }

            
            if ((pMLEditData->dx_chars[pMLEditData->veditPos] >= pMLEditData->dispPos) 
                    && (pMLEditData->dx_chars[pMLEditData->veditPos] 
                            <= pMLEditData->dispPos + width)) {
                pMLEditData->diff = FALSE;
                pMLEditData->realdispPos = pMLEditData->vdispPos;
                edtSetCaretPos (hWnd);
            } else 
                edtSetCaretPos (hWnd);
                
            edtSetScrollInfo (hWnd, pMLEditData, TRUE);
            InvalidateRect (hWnd, NULL, TRUE);
        }
        break;
        
        case EV_NEEDIME:
            if (FALSE == EditIsReadOnly (hWnd->nStyle)) {
                return 1;
            }
            else {
                return 0;
            }
            break;
    
        default:
        break;
    } 

    return DefWinProc (hWnd, message, wParam, lParam);
}
