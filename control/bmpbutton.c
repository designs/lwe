/*
  Copyright (C), 2008, zhanbin
  File name:    bmpbutton.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081219
  Description:
  简单的BMP按钮实现。
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081219    zhanbin         创建文件
*/
#include    "window.h"

static int32_t BmpButtonProc(WINDOW *Window,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    RECT WndRect;
    TBmpButtonData *pData;
    
    pData = (TBmpButtonData *)Window->pPrivData;
    
    switch(Message){
    case EV_CREATE:
    {
        BITMAP *pBmp;
        
        pBmp = (BITMAP *)lParam;
        if (NULL == pBmp) {
            return -1;
        }
        
        pData = (TBmpButtonData *) Malloc (sizeof(TBmpButtonData));
        if (NULL == pData) {
            return -2;
        }
        
        pData->nStatus        = Window->nStyle;
        pData->pBmp           = pBmp;
        pData->pOldCaptureWnd = NULL;
        
        Window->pPrivData     = pData;
        break;
    }
    
    case EV_ERASEBKGND:
        break;
        
    case EV_PAINT:
    {
        DC* hdc;
        
        pData = (TBmpButtonData *)Window->pPrivData;
        if (NULL == pData) {
            break;
        }
        
        hdc = BeginPaint (Window, wParam, lParam);
        if (NULL == hdc) {
            break;
        }
        
        GetClientRect (Window, &WndRect);
        
        if (FALSE == IsWindowEnabled (Window)) {
            DrawBitmap (hdc, WndRect.left, WndRect.top, pData->pBmp, 150, 0, 75, 21);
        }
        else if (pData->nStatus & BUTTON_PUSHED) {
            DrawBitmap (hdc, WndRect.left, WndRect.top, pData->pBmp, 0, 0, 75, 21);
        }
        else {
            DrawBitmap (hdc, WndRect.left, WndRect.top, pData->pBmp, 225, 0, 75, 21);
        }
        
        if (Window->pCaption != NULL) {
            if (FALSE == IsWindowEnabled (Window)) {
                SetFgColor (hdc, RGB(255, 255, 255));
            }
            else {
                SetFgColor (hdc, RGB(0, 0, 0));
            }
            
            DrawText (hdc, Window->pCaption, strlen (Window->pCaption), &WndRect, DT_CENTER);
        }
        
        EndPaint (Window, hdc, wParam, lParam);
        return 0;
        break;
    }
        
    case EV_LBUTTONDOWN:
    {
        DefWinProc(Window, Message, wParam, lParam);
        
        if (GetCapture () != Window) {
            pData->pOldCaptureWnd = SetCapture (Window);
            pData->nStatus |= BUTTON_PUSHED;
        }
        
        InvalidateRect(Window, NULL, FALSE);
        return 0;
        break;
    }
        
    case EV_LBUTTONUP:
    {
        POINT p;
        
        if (GetCapture () == Window) {
            SetCapture (pData->pOldCaptureWnd);
        }
        else {
            break;
        }
        
        p.x = LOuint16_t (lParam);
        p.y = HIuint16_t (lParam);
        
        GetClientRect (Window, &WndRect);
        ScreenToClient (Window, &p);
        if (TRUE == PtInRect (&WndRect, &p)) {
            int32_t nResult;
            
            pData->nStatus &= ~BUTTON_PUSHED;
            InvalidateRect(Window, NULL, FALSE);
            
            nResult = DefWinProc(Window, Message, wParam, lParam);
            
            if (0 == nResult) {
                NotifyParent (Window, GetDlgCtrlID(Window), BN_CLICKED);
            }
            
            return nResult;
        }
        break;
    }
        
    case EV_MOUSEMOVE:
    {
        POINT p;
        
        if (GetCapture () != Window) {
            break;
        }
        
        p.x = LOuint16_t (lParam);
        p.y = HIuint16_t (lParam);
        
        GetClientRect (Window, &WndRect);
        ScreenToClient (Window, &p);
        if (TRUE == PtInRect (&WndRect, &p)) {
            if (pData->nStatus & BUTTON_PUSHED) {
                break;
            }
            
            pData->nStatus |= BUTTON_PUSHED;
            
            InvalidateRect(Window, NULL, FALSE);
        }
        else {
            if (pData->nStatus & BUTTON_PUSHED) {
                pData->nStatus &= ~BUTTON_PUSHED;
                
                InvalidateRect(Window, NULL, FALSE);
                break;
            }
        }
        break;
    }
    case EV_CHOUSE:
		pData->nStatus |= BUTTON_PUSHED;
		InvalidateRect(Window, NULL, FALSE);
		break;
    case EV_UNCHOUSE:
		pData->nStatus &= ~BUTTON_PUSHED;
		InvalidateRect(Window, NULL, FALSE);
		break;
    case EV_KEYDOWN:
        if (LOuint16_t (wParam) != VK_RETURN) {
            break;
        }
                
        if (GetCapture () != Window) {
            pData->pOldCaptureWnd = SetCapture (Window);
            pData->nStatus |= BUTTON_PUSHED;
        }
        
        InvalidateRect(Window, NULL, FALSE);
        return 0;
        break;
        
    case EV_KEYUP:
        if (LOuint16_t (wParam) != VK_RETURN) {
            break;
        }
        
        if (GetCapture () == Window) {
            SetCapture (pData->pOldCaptureWnd);
        }
        else {
            break;
        }
        
        pData->nStatus &= ~BUTTON_PUSHED;
        InvalidateRect(Window, NULL, FALSE);
        
        NotifyParent (Window, GetDlgCtrlID(Window), BN_CLICKED);
        return 0;
        break;
        
    case EV_SETFOCUS:
        pData->nStatus |= BUTTON_FOCUS;
        InvalidateRect(Window, NULL, FALSE);
        break;
        
    case EV_KILLFOCUS:
        pData->nStatus &= (~BUTTON_FOCUS);
        InvalidateRect(Window, NULL, FALSE);
        break;
        
    case EV_ENABLE:
        InvalidateRect(Window, NULL, FALSE);
        break;
        
    case EV_DESTROY:
        pData = (TBmpButtonData *)Window->pPrivData;
        Free (pData);
        break;
    }
    
	return DefWinProc(Window, Message, wParam, lParam);
}

bool32_t RegisterBmpButtonClass(void)
{
    WNDCLASS BmpButtonClass;

    memset (&BmpButtonClass, 0, sizeof (WNDCLASS));
    BmpButtonClass.pClassName = "bmpbutton";
    BmpButtonClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    BmpButtonClass.WinProc    = BmpButtonProc;
    
    return RegisterClass (&BmpButtonClass);
}
