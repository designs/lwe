#ifndef __EDIT_H__
#define __EDIT_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define MAX_TEXT_LEN    256

#define EDIT_NORMAL     0x00010000
#define EDIT_FOCUS      0x00040000
#define EDIT_READONLY   0x00080000
#define EDIT_PASSWORD   0x00100000

#define CHAR_ASC        0x01
#define CHAR_HZ_0       0x02
#define CHAR_HZ_1       0x04

#define ETF_MASK        0x00000001
#define ETF_LEFT        0x00000000
#define ETF_RIGHT       0x00000001

typedef struct _TEditData
{
    int32_t                 nStatus;
    
    int16_t                 nMaxLen;                        // 最大可显示字符数
    int16_t                 nCanShowLen;                    // 编辑框可显示字符数
    int16_t                 nTrueShowLen;                   // 显示时实际使用字符数
    int16_t                 nInputLen;                      // 已经输入字符数
    int16_t                 nCaretPos;                      // 光标位置(0~nCanShowLen)
    int16_t                 nEditPos;                       // 编辑位置(整个输入字符串位置)
    int16_t                 nShowPos;                       // 显示开始位置(整个输入字符串位置)
    int8_t                  nTextOut;                       // 显示对齐方式(左／右)
    int8_t                  nTextLeftOff;                   // 显示使用字符串左移位(相对于nShowPos)
    int8_t                  nTextRightOff;                  // 显示使用字符串右移位(相对于nShowPos + nCanShowLen + 1)
    int8_t                  PswChar;                        // 密码字符
    int8_t                  EditBuf[MAX_TEXT_LEN + 1];      // 编辑框输入缓存
    int8_t                  CharState[MAX_TEXT_LEN + 1];    // 输入字符信息缓存
    int8_t*                 pShowBuf;                       // 显示缓存
}TEditData;

bool32_t RegisterEditClass(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
