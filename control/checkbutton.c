/*
  Copyright (C), 2008, zhanbin
  File name:    checkbutton.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081225
  Description:
  简单的CHECK按钮实现。
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081225    zhanbin         创建文件
*/
#include    "window.h"

static int32_t CheckButtonProc(WINDOW *Window,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    RECT WndRect;
    TCheckButtonData *pData;
    int32_t nOffsetY, nBmpHeight;
    
    pData = (TCheckButtonData *)Window->pPrivData;
    
    switch(Message){
    case EV_CREATE:
    {
        BITMAP *pBmp;
        
        pBmp = (BITMAP *)lParam;
        if (NULL == pBmp) {
            return -1;
        }
        
        pData = (TCheckButtonData *) Malloc (sizeof(TCheckButtonData));
        if (pData == NULL) {
            return -2;
        }
        
        pData->nStatus        = Window->nStyle;
        pData->pBmp           = pBmp;
        pData->pOldCaptureWnd = NULL;
        
        GetClientRect (Window, &WndRect);
        nBmpHeight = pBmp->bmHeight;
        nOffsetY = (WndRect.bottom - WndRect.top - nBmpHeight) / 2;
        SetRect (&pData->BmpRect, WndRect.left, nOffsetY, WndRect.left + nBmpHeight, nOffsetY + nBmpHeight);
        
        Window->pPrivData     = pData;
        break;
    }
    
    case EV_ERASEBKGND:
        break;
        
    case EV_PAINT:
    {
        DC* hdc;
        
        pData = (TCheckButtonData *)Window->pPrivData;
        if (NULL == pData) {
            break;
        }
        
        hdc = BeginPaint (Window, wParam, lParam);
        if (NULL == hdc) {
            break;
        }
        
        GetClientRect (Window, &WndRect);
        
        nBmpHeight = pData->pBmp->bmHeight;
        nOffsetY = (WndRect.bottom - WndRect.top - nBmpHeight) / 2;
        switch (pData->nStatus & (BUTTON_PUSHED | BUTTON_CHECK)) {
        case (BUTTON_PUSHED | BUTTON_CHECK):
            DrawBitmap (hdc, WndRect.left, nOffsetY, pData->pBmp, nBmpHeight, 0, nBmpHeight, nBmpHeight);
            break;
            
        case (BUTTON_PUSHED):
            DrawBitmap (hdc, WndRect.left, nOffsetY, pData->pBmp, 0, 0, nBmpHeight, nBmpHeight);
            break;
            
        case (BUTTON_CHECK):
            DrawBitmap (hdc, WndRect.left, nOffsetY, pData->pBmp, nBmpHeight, 0, nBmpHeight, nBmpHeight);
            break;
            
        default:
            DrawBitmap (hdc, WndRect.left, nOffsetY, pData->pBmp, 0, 0, nBmpHeight, nBmpHeight);
            break;
        }
        
        if (Window->pCaption != NULL) {
            WndRect.left += nBmpHeight;
            DrawText (hdc, Window->pCaption, strlen (Window->pCaption), &WndRect, DT_LEFT);
        }
        
        EndPaint (Window, hdc, wParam, lParam);
        return 0;
        break;
    }
        
    case EV_LBUTTONDOWN:
    {
        if (GetCapture () != Window) {
            pData->pOldCaptureWnd = SetCapture (Window);
        }
        
        pData->nStatus |= BUTTON_PUSHED;
        break;
    }
        
    case EV_LBUTTONUP:
    {
        POINT p;
        
        if (GetCapture () == Window) {
            SetCapture (pData->pOldCaptureWnd);
        }
        else {
            break;
        }
        
        p.x = LOuint16_t (lParam);
        p.y = HIuint16_t (lParam);
        
        GetClientRect (Window, &WndRect);
        ScreenToClient (Window, &p);
        if (TRUE == PtInRect (&WndRect, &p)) {
            int32_t nResult;
            
            pData->nStatus ^= BUTTON_CHECK;
            pData->nStatus &= ~BUTTON_PUSHED;
            InvalidateRect(Window, &pData->BmpRect, TRUE);
            
            nResult = DefWinProc(Window, Message, wParam, lParam);
            
            if (0 == nResult) {
                NotifyParent (Window, GetDlgCtrlID(Window), BN_CLICKED);
            }
            
            return nResult;
        }
        break;
    }
        
    case EV_MOUSEMOVE:
    {
        POINT p;
        
        if (GetCapture () != Window) {
            break;
        }
        
        p.x = LOuint16_t (lParam);
        p.y = HIuint16_t (lParam);
        
        GetClientRect (Window, &WndRect);
        ScreenToClient (Window, &p);
        if (TRUE == PtInRect (&WndRect, &p)) {
            if (pData->nStatus & BUTTON_PUSHED) {
                break;
            }
            
            pData->nStatus |= BUTTON_PUSHED;
            
            InvalidateRect(Window, &pData->BmpRect, FALSE);
        }
        else {
            if (pData->nStatus & BUTTON_PUSHED) {
                pData->nStatus &= ~BUTTON_PUSHED;
                
                InvalidateRect(Window, &pData->BmpRect, FALSE);
                break;
            }
        }
        break;
    }
    
    case BM_SETCHECK:
        pData->nStatus |= BUTTON_CHECK;
        break;
        
    case BM_GETCHECK:
        return (BUTTON_CHECK == (pData->nStatus & BUTTON_CHECK));
        break;
        
    case EV_SETFOCUS:
        pData->nStatus |= BUTTON_FOCUS;
        InvalidateRect(Window, NULL, TRUE);
        break;
        
    case EV_KILLFOCUS:
        pData->nStatus &= (~BUTTON_FOCUS);
        InvalidateRect(Window, NULL, TRUE);
        break;
        
    case EV_ENABLE:
        InvalidateRect(Window, NULL, FALSE);
        break;
        
    case EV_DESTROY:
        Free (pData);
        break;
        
    default:
        break;
    }
    
	return DefWinProc(Window, Message, wParam, lParam);
}

bool32_t RegisterCheckButtonClass(void)
{
    WNDCLASS CheckButtonClass;

    memset (&CheckButtonClass, 0, sizeof (WNDCLASS));
    CheckButtonClass.pClassName = "chedkbutton";
    CheckButtonClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    CheckButtonClass.WinProc    = CheckButtonProc;
    
    return RegisterClass (&CheckButtonClass);
}
