#ifndef __STATIC_H__
#define __STATIC_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define SS_TYPEMASK   0x000f0000
#define SS_LEFT       0x00000000
#define SS_CENTER     0x00010000
#define SS_RIGHT      0x00020000
#define SS_BITMAP     0x00030000
#define SS_ICON       0x00040000

bool32_t RegisterStaticClass(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
