/*
** $Id: progressbar.c,v 1.27 2003/09/04 02:40:36 weiym Exp $
**
** progressbar.c: the Progress Bar Control module.
**
** Copyright (C) 2003 Feynman Software.
** Copyright (C) 1999 ~ 2002 Wei Yongming.
**
** Current maintainer: Wei Yonming.
**
** Note:
**   Originally by Zhao Jianghua. 
**
** Create date: 1999/8/29
**
*/

/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
** Modify records:
**
**  Who             When        Where       For What                Status
**-----------------------------------------------------------------------------
**  zhanbin  2000/02/15       ��ֵ��GUI                          done
**  zhanbin  2000/02/15       Add PBM_GETPOS                        done
**-----------------------------------------------------------------------------
** TODO:
*/
#include    "window.h"

#define WIDTH_PBAR_BORDER  2

static void pbarOnDraw (WINDOW *hwnd, DC *hdc, PROGRESSDATA* pData, bool32_t fVertical)
{
    RECT    rcClient, rect;
    int32_t     x, y, w, h;
    ldiv_t  ndiv_progress;
    uint32_t     nAllPart;
    uint32_t     nNowPart;
    int32_t     whOne, nRem;
    int32_t     ix, iy;
    int32_t     i;
#ifndef _GRAY_SCREEN
    int8_t      szText[8] = "\0";
#endif
    int32_t     step;
    
    if (pData->nMax == pData->nMin)
        return;
    
    if ((pData->nMax - pData->nMin) > 5)
        step = 5;
    else
        step = 1;

    GetClientRect (hwnd, &rcClient);

    x = rcClient.left + WIDTH_PBAR_BORDER;
    y = rcClient.top + WIDTH_PBAR_BORDER;
    w = RECTW (rcClient) - (WIDTH_PBAR_BORDER << 1);
    h = RECTH (rcClient) - (WIDTH_PBAR_BORDER << 1);

    ndiv_progress = ldiv (pData->nMax - pData->nMin, step);
    nAllPart = ndiv_progress.quot;
    
    ndiv_progress = ldiv (pData->nPos - pData->nMin, step);
    nNowPart = ndiv_progress.quot;
    if (fVertical)
        ndiv_progress = ldiv (h, nAllPart);
    else
        ndiv_progress = ldiv (w, nAllPart);
        
    whOne = ndiv_progress.quot;
    nRem = ndiv_progress.rem;

    SetFgColor (hdc, RGB(192, 192, 192));
    SetRect (&rect, x, y, x + w, y + h);
    FillRect (hdc, &rect);

#ifdef _GRAY_SCREEN
    SetFgColor (hdc, RGB(0, 0, 0));
#else
    SetFgColor (hdc, RGB(0, 0, 255));
#endif
 
    if(whOne >= 4) {
        if (fVertical) {
            for (i = 0, iy = y + h - 1; i < (int32_t)nNowPart; ++i) {
                if ((iy - whOne) < y) 
                    whOne = iy - y;

                SetRect (&rect, x + 1, iy - whOne, x + w - 1, iy - 1);
                FillRect (hdc, &rect);
                iy -= whOne;
                if(nRem > 0) {
                    iy --;
                    nRem --;
                }
            }
        }
        else {
            for (i = 0, ix = x + 1; i < (int32_t)nNowPart; ++i) {
                if ((ix + whOne) > (x + w)) 
                    whOne = x + w - ix;

                SetRect (&rect, ix, y + 1, ix + whOne - 1, y + h - 1);
                FillRect (hdc, &rect);
                ix += whOne;
                if(nRem > 0) {
                    ix ++;
                    nRem --;
                }
            }
        }
    }
    else {
        // no vertical support
        double d = (nNowPart*1.0)/nAllPart;
        
        SetRect (&rect, x, y + 1, x + (int32_t)(w*d), y + h - 1);
        FillRect (hdc, &rect);

#ifndef _GRAY_SCREEN
        {
            SIZE text_ext;
            
            SetFgColor (hdc, RGB(192, 192, 192));
            SetBkColor (hdc, RGB(0, 0, 255));
            SetBkMode (hdc, TRANSPARENT);
            sprintf (szText, "%d%%", (int32_t)(d*100));
            GetTextExtent (hdc, szText, -1, &text_ext);
            TextOut (hdc, x + ((w - text_ext.cx) >> 1), 
                      y + ((h - text_ext.cy) >> 1), szText);
        }
#endif
    }
}

static void pbarNormalizeParams (WINDOW* hwnd, 
                PROGRESSDATA* pData, bool32_t fNotify)
{
    if (pData->nPos > pData->nMax) {
        pData->nPos = pData->nMax;
        if (fNotify)
            NotifyParent (hwnd, GetDlgCtrlID(hwnd), PBN_REACHMAX);
    }

    if (pData->nPos < pData->nMin) {
        pData->nPos = pData->nMin;
        if (fNotify)
            NotifyParent (hwnd, GetDlgCtrlID(hwnd), PBN_REACHMIN);
    }
}

static int32_t ProgressBarCtrlProc (WINDOW *hwnd, uint32_t message, uint32_t wParam, uint32_t lParam)
{
    PROGRESSDATA* pData;
    
    switch (message)
    {
        case EV_CREATE:
            if (!(pData = Malloc (sizeof (PROGRESSDATA)))) {
                fprintf(stderr, "Create progress bar control failure!\n");
                return -1;
            }
            
            pData->nMax     = 100;
            pData->nMin     = 0;
            pData->nPos     = 0;
            pData->nStepInc = 10;
            
            hwnd->pPrivData = (void *)pData;
            break;
        
        case EV_DESTROY:
            Free ((void *)(hwnd->pPrivData));
            break;
            
        case EV_ERASEBKGND:
        {
            DC* hdc;
            RECT rcClient;
            
            hdc = BeginErase (hwnd, wParam, lParam);
            if (NULL == hdc) {
                break;
            }
            
            GetClientRect (hwnd, &rcClient);
            
            Draw3DDownFrame (hdc, rcClient.left, rcClient.top, 
                             rcClient.right - 1, rcClient.bottom - 1, 
                             RGB(0, 0,0));
            
            EndErase (hwnd, hdc, wParam, lParam);
            return 0;
            break;
        }
        
        case EV_PAINT:
        {
            DC *hdc;
            RECT rcClient;
            
            GetClientRect (hwnd, &rcClient);

            hdc = BeginPaint (hwnd, wParam, lParam);
                             
            pbarOnDraw (hwnd, hdc, (PROGRESSDATA *)hwnd->pPrivData, 
                            hwnd->nStyle & PBS_VERTICAL);
            
            EndPaint (hwnd, hdc, wParam, lParam);
            return 0;
        }
        
        case PBM_SETRANGE:
            pData = (PROGRESSDATA *)hwnd->pPrivData;
            if (wParam == lParam)
                return PB_ERR;

            pData->nMin = MIN (wParam, lParam);
            pData->nMax = MAX (wParam, lParam);
            if (pData->nPos > pData->nMax)
                pData->nPos = pData->nMax;
            if (pData->nPos < pData->nMin)
                pData->nPos = pData->nMin;

            if (pData->nStepInc > (pData->nMax - pData->nMin))
                pData->nStepInc = pData->nMax - pData->nMin;
            return PB_OKAY;
        
        case PBM_SETSTEP:
            pData = (PROGRESSDATA *)hwnd->pPrivData;
            if (wParam > (pData->nMax - pData->nMin))
                return PB_ERR;

            pData->nStepInc = wParam;
            return PB_OKAY;
        
        case PBM_SETPOS:
            pData = (PROGRESSDATA *)hwnd->pPrivData;
            
            if (pData->nPos == wParam)
                return PB_OKAY;

            pData->nPos = wParam;
            pbarNormalizeParams (hwnd, pData, hwnd->nStyle & PBS_NOTIFY);

            InvalidateRect (hwnd, NULL, FALSE);
            return PB_OKAY;
        
        case PBM_DELTAPOS:
            pData = (PROGRESSDATA *)hwnd->pPrivData;

            if (wParam == 0)
                return PB_OKAY;
            
            pData->nPos += wParam;
            pbarNormalizeParams (hwnd, pData, hwnd->nStyle & PBS_NOTIFY);
            
            InvalidateRect (hwnd, NULL, FALSE);
            return PB_OKAY;
        
        case PBM_STEPIT:
            pData = (PROGRESSDATA *)hwnd->pPrivData;
            
            if (pData->nStepInc == 0)
                return PB_OKAY;

            pData->nPos += pData->nStepInc;
            pbarNormalizeParams (hwnd, pData, hwnd->nStyle & PBS_NOTIFY);

            InvalidateRect (hwnd, NULL, FALSE);
            return PB_OKAY;
            
        case PBM_GETPOS:
            pData = (PROGRESSDATA *)hwnd->pPrivData;
            return pData->nPos;
            break;
    }
    
    return DefWinProc (hwnd, message, wParam, lParam);
}

bool32_t RegisterProgressBarControl (void)
{
    WNDCLASS ProgressBarClass;

    memset (&ProgressBarClass, 0, sizeof (WNDCLASS));
    ProgressBarClass.pClassName = "progressbar";
    ProgressBarClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    ProgressBarClass.WinProc    = ProgressBarCtrlProc;
    
    return RegisterClass (&ProgressBarClass);
}
