#ifndef __LIST_BOX_H__
#define __LIST_BOX_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define LB_OKAY         0
#define LB_ERR          (-1)
#define LB_ERRSPACE     (-2)

#define LBS_NOTIFY  0x00010000
#define LBS_SORT    0x00020000
#define LBS_FOCUS   0x00040000

typedef struct _TListBoxItem
{
    int8_t*                 spName;
    struct list_head    ItemListNode;
}TListBoxItem;

typedef struct _TListBoxData
{
    uint32_t                 nStatus;
    
    int32_t                 nCanShow;
    int32_t                 nItemHeight;
    
    int32_t                 nFirstShowItem;
    TListBoxItem*       pFirstShowItem;
    
    int32_t                 nCurSelItem;
    TListBoxItem*       pCurSelItem;
    
    int32_t                 nItemCount;
    struct list_head    ItemListHead;
    
    COLORREF            SelColor;
}TListBoxData;

bool32_t RegisterListBoxClass(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
