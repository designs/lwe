#ifndef __CHECK_BUTTON_H__
#define __CHECK_BUTTON_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define BUTTON_CHECK        0x00800000

typedef struct _TCheckButtonData
{
    int32_t                     nStatus;
    
    BITMAP*                 pBmp;
    WINDOW *               pOldCaptureWnd;
    RECT                    BmpRect;
}TCheckButtonData;

bool32_t RegisterCheckButtonClass(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
