/*
  Copyright (C), 2008~2009, zhanbin
  File name:    mainmenu.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20090119
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20090222    zhanbin         Bug fix!!
    20090119    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

static int32_t DefMainMenuProc(WINDOW *Window,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    RECT ClientRect;
    MENU *pSubMenu;
    MENUItem *pNewSelItem = NULL;
    MENU *pMenu = (MENU *)Window->pPrivData;
    MENUItem* _list_;
    struct list_head *_head_ = &pMenu->ChildListHead;
    #undef _item_
    #define _item_ ChildListNode
    
    switch(Message){
    case EV_CREATE:
    {
        int32_t IX, IY;
        
        GetClientRect (Window, &ClientRect);
        
        IX = ClientRect.left;
        IY = ClientRect.top;
        for ((_list_) = list_entry((_head_)->next, MENUItem, _item_); 
            &((_list_)->_item_) != (_head_);
            (_list_) = list_entry((_list_)->_item_.next, MENUItem, _item_)) { 
            SetRect (&_list_->ItemRect, IX, IY, 
                IX + (strlen (_list_->spName) + 1) * (FONT_POINT >> 1), 
                IY + pMenu->nItemHeight);
            IX += (strlen (_list_->spName) + 1) * (FONT_POINT >> 1);
        }
        
        pMenu->pMenuWnd = Window;
        break;
    }
        
    case EV_PAINT:
    {
        DC* hdc;
        
        hdc = BeginPaint (Window, wParam, lParam);
        if (NULL == hdc) {
            break;
        }
        
        for ((_list_) = list_entry((_head_)->next, MENUItem, _item_); 
            &((_list_)->_item_) != (_head_);
            (_list_) = list_entry((_list_)->_item_.next, MENUItem, _item_)) {
            if (pMenu->pOldSelItem == _list_) {
                SetFgColor (hdc, RGB (49, 106, 197));
                FillRect (hdc, &_list_->ItemRect);
            }
            else {
                SetFgColor (hdc, RGB (239, 238, 226));
                FillRect (hdc, &_list_->ItemRect);
            }
            
            SetFgColor (hdc, RGB (0, 0, 0));
            DrawText (hdc, _list_->spName, strlen (_list_->spName), &_list_->ItemRect, DT_CENTER);
        }
        
        EndPaint (Window, hdc, wParam, lParam);
        return 0;
        break;
    }
        
    case EV_LBUTTONDOWN:
    {
        POINT p;
        
        p.x = LOuint16_t (lParam);
        p.y = HIuint16_t (lParam);
        ScreenToClient (Window, &p);
        
        GetClientRect (Window, &ClientRect);
        if (FALSE == PtInRect (&ClientRect, &p)) {
            if (GetCapture () == Window) {
                SetCapture (pMenu->pOldCaptureWnd);
            }
            
            PostMouseMessage (Message, wParam, lParam);
            
            return 0;
        }
        
        DefWinProc(Window, Message, wParam, lParam);
        
        for ((_list_) = list_entry((_head_)->next, MENUItem, _item_); 
            &((_list_)->_item_) != (_head_);
            (_list_) = list_entry((_list_)->_item_.next, MENUItem, _item_)) { 
            if (TRUE == PtInRect (&_list_->ItemRect, &p)) {
                pNewSelItem = _list_;
                break;
            }
        }
        
        if (NULL == pNewSelItem) {
            pGUI->pActiveMenu = NULL;
            
            if (NULL != pMenu->pOldSelItem) {
                pSubMenu = pMenu->pOldSelItem->pSubMenu;
                
                if (NULL != pSubMenu->pMenuWnd) {
                    HideMenu (pSubMenu);
                }
                
                InvalidateRect (Window, &pMenu->pOldSelItem->ItemRect, FALSE);
                
                pMenu->pOldSelItem = NULL;
            }
        }
        else {
            pGUI->pActiveMenu = pMenu;
            
            if (pMenu->pOldSelItem != pNewSelItem) {
                if (NULL != pMenu->pOldSelItem) {
                    pSubMenu = pMenu->pOldSelItem->pSubMenu;
                    
                    if (NULL != pSubMenu->pMenuWnd) {
                        HideMenu (pSubMenu);
                    }
                    
                    InvalidateRect (Window, &pMenu->pOldSelItem->ItemRect, FALSE);
                }
                
                if (MENU_TYPE_SUBMENU == pNewSelItem->nType) {
                    ShowMenu (pNewSelItem->pSubMenu, pNewSelItem->ItemRect.left, 
                        pNewSelItem->ItemRect.bottom, Window);
                    InvalidateRect (Window, &pNewSelItem->ItemRect, FALSE);
                }
                
                pMenu->pOldSelItem = pNewSelItem;
            }
            else {
                pSubMenu = pMenu->pOldSelItem->pSubMenu;
                    
                if (NULL != pSubMenu->pMenuWnd) {
                    HideMenu (pSubMenu);
                }
                else {
                    ShowMenu (pNewSelItem->pSubMenu, pNewSelItem->ItemRect.left, 
                        pNewSelItem->ItemRect.bottom, Window);
                }
            }
        }
        return 0;
        break;
    }
        
    case EV_LBUTTONUP:
    case EV_RBUTTONDOWN:
    case EV_RBUTTONUP:
        break;
        
    case EV_MOUSEMOVE:
    {
        POINT p;
        WINDOW *pParentWnd;
        
        pParentWnd = GetParent (Window);
        if (FALSE == IsWindowNcActive (pParentWnd)) {
            break;
        }
        
        p.x = LOuint16_t (lParam);
        p.y = HIuint16_t (lParam);
        ScreenToClient (Window, &p);
        
        for ((_list_) = list_entry((_head_)->next, MENUItem, _item_); 
            &((_list_)->_item_) != (_head_);
            (_list_) = list_entry((_list_)->_item_.next, MENUItem, _item_)) { 
            if (TRUE == PtInRect (&_list_->ItemRect, &p)) {
                pNewSelItem = _list_;
                break;
            }
        }
        
        if (NULL == pNewSelItem) {
            if (NULL != pMenu->pOldSelItem) {
                pSubMenu = pMenu->pOldSelItem->pSubMenu;
                
                if (NULL != pSubMenu->pMenuWnd) {
                }
                else {
                    InvalidateRect (Window, &pMenu->pOldSelItem->ItemRect, FALSE);
                    
                    pMenu->pOldSelItem = NULL;
                }
            }
        }
        else {
            if (pMenu->pOldSelItem != pNewSelItem) {
                if (NULL != pMenu->pOldSelItem) {
                    InvalidateRect (Window, &pMenu->pOldSelItem->ItemRect, FALSE);
                    
                    pSubMenu = pMenu->pOldSelItem->pSubMenu;
                    
                    if (NULL != pSubMenu->pMenuWnd) {
                        HideMenu (pSubMenu);
                        
                        if (MENU_TYPE_SUBMENU == pNewSelItem->nType) {
                            ShowMenu (pNewSelItem->pSubMenu, pNewSelItem->ItemRect.left, 
                                pNewSelItem->ItemRect.bottom, Window);
                        }
                    }
                }
                
                InvalidateRect (Window, &pNewSelItem->ItemRect, FALSE);
                
                pMenu->pOldSelItem = pNewSelItem;
            }
        }
        
        GetClientRect (Window, &ClientRect);
        if (TRUE == PtInRect (&ClientRect, &p)) {
            if (wParam == 0 && GetCapture () != Window) {
                pMenu->pOldCaptureWnd = SetCapture (Window);
            }
        }
        else {
            if (wParam == 0 && GetCapture () == Window) {
                SetCapture (pMenu->pOldCaptureWnd);
                pMenu->pOldCaptureWnd = NULL;
                pGUI->pActiveMenu = NULL;
            }
        }
        break;
    }
        
    case EV_KEYDOWN:
    {
        switch (wParam) {
        case VK_UP:
            if (NULL != pMenu->pOldSelItem) {
                pSubMenu = pMenu->pOldSelItem->pSubMenu;
                
                if (NULL != pSubMenu->pMenuWnd) {
                    HideMenu (pSubMenu);
                }
            }
            break;
            
        case VK_DOWN:
            if (NULL != pMenu->pOldSelItem) {
                pSubMenu = pMenu->pOldSelItem->pSubMenu;
                
                if (NULL == pSubMenu->pMenuWnd) {
                    ShowMenu (pSubMenu, pMenu->pOldSelItem->ItemRect.left, 
                        pMenu->pOldSelItem->ItemRect.bottom, Window);
                }
            }
            break;
            
        case VK_RETURN:
            if (NULL != pMenu->pOldSelItem) {
                pSubMenu = pMenu->pOldSelItem->pSubMenu;
                
                if (NULL != pSubMenu->pMenuWnd) {
                    HideMenu (pSubMenu);
                }
                else {
                    ShowMenu (pSubMenu, pMenu->pOldSelItem->ItemRect.left, 
                        pMenu->pOldSelItem->ItemRect.bottom, Window);
                }
            }
            break;
            
        case VK_LEFT:
            if (NULL != pMenu->pOldSelItem) {
                pNewSelItem = list_entry((pMenu->pOldSelItem->_item_).prev, MENUItem, _item_);
                
                if (&((pNewSelItem)->_item_) == (_head_)) {
                    pNewSelItem = list_entry((_head_)->prev, MENUItem, _item_);
                }
                
                InvalidateRect (Window, &pMenu->pOldSelItem->ItemRect, FALSE);
                InvalidateRect (Window, &pNewSelItem->ItemRect, FALSE);
                
                pSubMenu = pMenu->pOldSelItem->pSubMenu;
                if (NULL != pSubMenu->pMenuWnd) {
                    HideMenu (pSubMenu);
                    
                    ShowMenu (pNewSelItem->pSubMenu, pNewSelItem->ItemRect.left, 
                        pNewSelItem->ItemRect.bottom, Window);
                }
                
                pMenu->pOldSelItem = pNewSelItem;
            }
            break;
            
        case VK_RIGHT:
            if (NULL != pMenu->pOldSelItem) {
                pNewSelItem = list_entry((pMenu->pOldSelItem->_item_).next, MENUItem, _item_);
                
                if (&((pNewSelItem)->_item_) == (_head_)) {
                    pNewSelItem = list_entry((_head_)->next, MENUItem, _item_);
                }
                
                InvalidateRect (Window, &pMenu->pOldSelItem->ItemRect, FALSE);
                InvalidateRect (Window, &pNewSelItem->ItemRect, FALSE);
                
                pSubMenu = pMenu->pOldSelItem->pSubMenu;
                if (NULL != pSubMenu->pMenuWnd) {
                    HideMenu (pSubMenu);
                    
                    ShowMenu (pNewSelItem->pSubMenu, pNewSelItem->ItemRect.left, 
                        pNewSelItem->ItemRect.bottom, Window);
                }
                
                pMenu->pOldSelItem = pNewSelItem;
            }
            break;
            
        case VK_ESCAPE:
        {
            POINT p;
            
            if (NULL != pMenu->pOldSelItem) {
                GetCursorPos (&p);
                ScreenToClient (Window, &p);
        
                for ((_list_) = list_entry((_head_)->next, MENUItem, _item_); 
                    &((_list_)->_item_) != (_head_);
                    (_list_) = list_entry((_list_)->_item_.next, MENUItem, _item_)) { 
                    if (TRUE == PtInRect (&_list_->ItemRect, &p)) {
                        pNewSelItem = _list_;
                        break;
                    }
                }
                
                InvalidateRect (Window, &pMenu->pOldSelItem->ItemRect, FALSE);
                if (NULL != pNewSelItem) {
                    InvalidateRect (Window, &pNewSelItem->ItemRect, FALSE);
                }
            }
            
            pMenu->pOldSelItem   = pNewSelItem;
            pGUI->pActiveMenu = NULL;
            break;
        }
            
        default:
            break;
        }
        break;
    }
        
    case EV_HIDEMENU:
        if (NULL != pMenu->pOldSelItem) {
            InvalidateRect (Window, &pMenu->pOldSelItem->ItemRect, FALSE);
            
            pMenu->pOldSelItem = NULL;
        }
        
        if (GetCapture () == Window) {
            SetCapture (pMenu->pOldCaptureWnd);
        }
        return 0;
        break;
        
    case EV_COMMAND:
    {
        PostMessage (GetParent (Window), EV_COMMAND, wParam, lParam);
        return 0;
        break;
    }
    
    case EV_RESIZE:
    {
        RECT MenuRect;
        
        GetWindowMenuRect (GetParent (Window), &MenuRect);
        MoveWindow (Window, MenuRect.left, MenuRect.top, 
            MenuRect.right - MenuRect.left, MenuRect.bottom -MenuRect.top, TRUE);
        break;
    }
        
    default:
        break;
    }
    
	return DefWinProc(Window, Message, wParam, lParam);
}

bool32_t RegisterMainMenuClass (void)
{
    WNDCLASS MenuClass;

    memset (&MenuClass, 0, sizeof (WNDCLASS));
    MenuClass.pClassName = "mainmenu";
    MenuClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    MenuClass.WinProc    = DefMainMenuProc;
    
    return RegisterClass (&MenuClass);
}
