/*
** $Id: medit.h,v 1.12 2003/09/04 03:40:35 weiym Exp $
**
** medit.h: the head file of Multi-Line Edit control.
**
** Copyright (C) 2003 Feynman Software.
** Copyright (C) 1999, 2000 Chen Lei
** Copyright (C) 1999 ~ 2002 Wei Yongming.
**
** Create date: 1999/8/26
*/

#ifndef __MEDIT_H__
#define __MEDIT_H__

#ifdef  __cplusplus
extern  "C" {
#endif

#define WIDTH_MEDIT_BORDER       2
#define MARGIN_MEDIT_LEFT        1
#define MARGIN_MEDIT_TOP         1
#define MARGIN_MEDIT_RIGHT       2
#define MARGIN_MEDIT_BOTTOM      1

#define LEN_MLEDIT_BUFFER       256
#define LEN_MLEDIT_UNDOBUFFER   1024
#define MAX_IMPOSSIBLE			10000

#define EST_FOCUSED             0x00010000L
#define EST_MODIFY              0x00020000L
#define EST_READONLY            0x00040000L
#define EST_REPLACE             0x00080000L

#define ES_AUTOWRAP           0x00100000L
#define ES_READONLY           0x00200000L
#define ES_UPPERCASE          0x00400000L
#define ES_LOWERCASE          0x00800000L
#define ES_PASSWORD           0x01000000L

#define MEDIT_OP_NONE           0x00
#define MEDIT_OP_DELETE         0x01
#define MEDIT_OP_INSERT         0x02
#define MEDIT_OP_REPLACE        0x03

#define MAX_WRAP_NUM	        100
		
typedef struct tagLINEDATA
{
	int32_t     lineNO;	 	             	// 行号
	int32_t     dataEnd; 
	struct  tagLINEDATA *previous;    	// 前一行
	struct  tagLINEDATA *next;        	// 后一行 
	int8_t      buffer[LEN_MLEDIT_BUFFER+1];
	int32_t		nwrapline;					// wrap line support, wrap line number in current line
	int32_t  	wrapStartPos[MAX_WRAP_NUM];	// starting address of each wrap line in current line
}LINEDATA;

typedef    LINEDATA*     PLINEDATA;

typedef struct tagMLEDITDATA
{
    uint32_t     status;         // status of box
	int32_t     editLine;		// current eidt line
	int32_t     dispPos;        // 开始显示的位置
	int32_t     StartlineDisp;  // start line displayed
    int32_t     EndlineDisp;    // end line displayed
    int32_t     linesDisp;      // 需要显示的行数
	int32_t     lines;		    // 总的行数`
	int32_t     MaxlinesDisp;   // 最大显示的行数.
							
#if 0
    int32_t     selStartPos;    // selection start position
	int32_t     selStartLine;   // selection start line
    int32_t     selEndPos;      // selection end position
	int32_t     selEndLine;     // selection end line
#endif
    
    int32_t     passwdChar;     // password character
    
    int32_t     leftMargin;     // left margin
    int32_t     topMargin;      // top margin
    int32_t     rightMargin;    // right margin
    int32_t     bottomMargin;   // bottom margin
    
    int32_t     lineHeight;     // height of line.
    
    int32_t     totalLimit;    	// total text Len hard limit
	int32_t		curtotalLen;	// current total text len
	int32_t 	lineLimit;		// len limit per line

   	PLINEDATA   head;       // buffer
	PLINEDATA   tail;       // 可能不需要
						    // added by leon for charset support
	int32_t		fit_chars;		// number of valid units to display
	int32_t*	pos_chars;		// postion of each valid unit in buffer
	int32_t*	dx_chars;		// display postion of each valid unit
	SIZE	sz;				// display rect size
	int32_t 	veditPos;		// editPos position of valid unit in both arrays
	int32_t 	vdispPos;		// dispPos position of valid unit in both arrays
	int32_t 	realeditLine;	// the 'real' line number that the cursor should be on if visible
	int32_t 	realdispPos;	// the 'real' valid unit edit position that the cursor should be on if visible
	int32_t 	realStartLine;	// the 'real' valid start line number
	int32_t 	realEndLine;	// the 'real' valid end line number
	int32_t		diff;			// 0: no difference; 1: difference
	int32_t     bSCROLL;		// 0: not scroll; 1: scroll
							// add end 
	int32_t 	wraplines;		// total wrapline number
	
#if 0
    int32_t     lastOp;         // last operation
    int32_t     lastPos;        // last operation position
	int32_t     lastLine;       // last operation line
    int32_t     affectedLen;    // affected len of last operation
    int32_t     undoBufferLen;  // undo buffer len
    uint8_t      undoBuffer [LEN_MLEDIT_UNDOBUFFER];
                            // Undo buffer;
    PLOGFONT logfont;
#endif
}MLEDITDATA;
typedef MLEDITDATA* PMLEDITDATA;

bool32_t RegisterMLEditControl (void);

#ifdef __cplusplus
}
#endif

#endif /* GUI_MEDIT_H_ */

