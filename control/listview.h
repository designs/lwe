/*
** $Id: listview.h,v 1.28 2003/09/04 06:12:04 weiym Exp $ 
**
** listview.h: header file of ListView control.
**
** Copyright (C) 2003 Feynman Software.
** Copyright (C) 2002 Amokaqi chenjm kevin.
** Copyright (C) 2001, 2002 Shu Ming.
**
*/

#ifndef __LISTVIEW_H__
#define __LISTVIEW_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define LV_ROW_MAX          100000              // maximum item number

#define LV_CO_DEF	        62                  // default column width

//default header height
#define LV_HDRH_DEF(hwnd)   (16 + 4)
//default item height
#define LV_ITEMH_DEF(hwnd)  (16 + 4)

#define LV_HDR_TOP		    0                   // top of the header

#define COLUME_MIN_WIDTH         10                  // minimum column width
#define HSCROLL             5                   // h scroll value
#define VSCROLL             15                  // v scroll value

#define LIGHTBLUE	        ( RGB(0, 0, 180) )
#define GRAYBLUE	        ( RGB(156, 166, 189) )

typedef enum sorttype
{
    NOTSORTED = 0, 
    HI_SORTED, 
    LO_SORTED
} SORTTYPE;

#define LVIF_NORMAL	        0x0000L
#define LVIF_BITMAP         0x0001L
#define LVIF_ICON           0x0002L

typedef struct _subitemdata
{
    struct _subitemdata     *pNext;             // points to the next subitem
    uint32_t                     dwFlags;            // subitem flags
    int8_t                      *pszInfo;           // text of the subitem
    int32_t                     nTextColor;         // text color of the subitem
    uint32_t                     Image;              // image of the subitem
} SUBITEMDATA;
typedef SUBITEMDATA *PSUBITEMDATA;

typedef struct _itemdata
{
    struct _itemdata        *pNext;             // points to the next item
    bool32_t                     bSelected;          // item is selected
    PSUBITEMDATA            pSubItemHead;       // points to the subitem list
    int32_t                     addData;
} ITEMDATA;
typedef ITEMDATA *PITEMDATA;

/* column header struct */
typedef struct _lsthdr
{
    struct _lsthdr          *pNext;             // points to the next header
    int32_t                     x;                  // x position of the header
    int32_t                     width;              // width of the header/column/subitem
    SORTTYPE                sort;               // sort status
    int8_t                      *pTitle;            // title text of the column header
    PFNLVCOMPARE            pfnCmp;             // pointer to the application-defined or default
                                                // comparision function
    uint32_t                     Image;              // image of the header
    uint32_t                     flags;              // header and column flags
} LSTHDR;
typedef LSTHDR *PLSTHDR;

#define LVST_NORMAL	        0x0000              //normal status
#define LVST_BDDRAG	        0x0001              //the border is being dragged
#define LVST_HEADCLICK	    0x0002              //the header is being clicked
#define LVST_INHEAD	        0x0004              //mouse move in header

/* this macro doesn't check the pointer, so, be careful */
#define LVSTATUS(hwnd)      ( ((PLVDATA)GetWindowAdditionalData2(hwnd))->status )

typedef struct _listviewData
{
    int32_t                     nItemHeight;        // item height
    int32_t                     nHeadHeight;        // header height
    int32_t                     bkc_selected;       // background color of the selected item

    int32_t                     nCols;              // current column number
    int32_t                     nRows;              // current item number
    int32_t                     nOriginalX;         // scroll x pos
    int32_t                     nOriginalY;         // scroll y pos

    int32_t                     nItemSelected;      // index of the selected item
    int32_t                     nColCurSel;	        // current column selected.

    int32_t                     nItemDraged;        // the header beging dragged
    uint32_t                     status;             // list view status: dragged, clicked
    PLSTHDR                 pHdrClicked;        // the header being clicked

    PLSTHDR                 pLstHead;           // points to the header list
    PITEMDATA               pItemHead;          // points to the item list
    WINDOW *               hWnd;               // the control handle

    STRCMP                  str_cmp;            // default strcmp function
} LVDATA;
typedef LVDATA *PLVDATA;

bool32_t  RegisterListViewControl (void);
void ListViewControlCleanup (void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif  /* __LISTVIEW_H__ */

