/*
  Copyright (C), 2008, zhanbin
  File name:    edit.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.2
  Date:         20081226
  Description:
  简单的编辑框实现。
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081228    zhanbin         重写中文支持
    20081226    zhanbin         创建文件
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

static void EditLeftMove(WINDOW *Window, TEditData *pData, int32_t nChar);
static void EditRightMove(WINDOW *Window, TEditData *pData, int32_t nChar);

static bool32_t EditIsReadOnly(uint32_t nStatus)
{
    return (0 != (nStatus & EDIT_READONLY));
}

static void SetEditTextOutFmt(TEditData *pData, int8_t nNewFmt)
{
    pData->nTextOut &= ~ETF_MASK;
    
    pData->nTextOut |= nNewFmt;
}

static int8_t GetEditTextOutFmt(TEditData *pData)
{
    return (pData->nTextOut & ETF_MASK);
}

static void SetEditTextOff(TEditData *pData, bool32_t bLeft, int32_t nChar)
{
    if (TRUE == bLeft) {
        pData->nTextLeftOff = nChar;
    }
    else {
        pData->nTextRightOff = nChar;
    }
}

static int32_t GetEditTextOff(TEditData *pData, bool32_t bLeft)
{
    if (TRUE == bLeft) {
        return pData->nTextLeftOff;
    }
    else {
        return pData->nTextRightOff;
    }
}

static bool32_t EditCanInsert(TEditData *pData, int32_t nCharState)
{
    int32_t nSpace;
    bool32_t bCanIns = FALSE;
    
    nSpace = pData->nMaxLen - pData->nInputLen;
    if (nSpace <= 0) {
        return FALSE;
    }
    
    switch (nCharState) {
    case CHAR_ASC:
        bCanIns = TRUE;
        break;
        
    case CHAR_HZ_0:
        if (nSpace >= 2) {
            bCanIns = TRUE;
        }
        else {
            bCanIns = FALSE;
        }
        break;
        
    case CHAR_HZ_1:
        bCanIns = TRUE;
        break;
    }
    
    return bCanIns;
}

static void EditBakespaceChar(WINDOW *Window,uint32_t wParam,uint32_t lParam)
{
    int32_t nDelChar;
    int8_t nPreCharState;
    TEditData *pData;
    
    pData = (TEditData *)Window->pPrivData;
    
    if (pData->nEditPos == 0) {
        return;
    }
    
    nPreCharState = pData->CharState[pData->nEditPos - 1];
    switch (nPreCharState) {
    case CHAR_HZ_1:
        nDelChar = 2;
        break;
        
    case CHAR_ASC:
    case CHAR_HZ_0:
    default:
        nDelChar = 1;
        break;
    }
    
    memcpy (&pData->EditBuf[pData->nEditPos - nDelChar], &pData->EditBuf[pData->nEditPos], 
        pData->nInputLen - pData->nEditPos);
    memcpy (&pData->CharState[pData->nEditPos - nDelChar], &pData->CharState[pData->nEditPos], 
        pData->nInputLen - pData->nEditPos);
    pData->nInputLen -= nDelChar;
    pData->EditBuf[pData->nInputLen] = '\0';
    pData->nEditPos -= nDelChar;
    
    EditLeftMove (Window, pData, nDelChar);
    
    InvalidateRect(Window, NULL, TRUE);
}

static void EditSetCaretPos (WINDOW *Window)
{
    RECT WndRect;
    TEditData *pData;
    int32_t X, Y, nTextOutFmt;
    
    pData = (TEditData *)Window->pPrivData;
    
    GetClientRect (Window, &WndRect);
    
    Y = (WndRect.bottom - WndRect.top - FONTPOINT) / 2;
    nTextOutFmt = GetEditTextOutFmt (pData);
    if (nTextOutFmt == ETF_LEFT) {
        X = pData->nCaretPos * (FONTPOINT >> 1) + 2;
    }
    else {
        X = WndRect.right - (pData->nCanShowLen - pData->nCaretPos) * (FONTPOINT >> 1) - 2;
    }
    
    SetCaretPos (Window, X, Y);
}

static void EditLeftMove(WINDOW *Window, TEditData *pData, int32_t nChar)
{
    int8_t nCharState;
    int16_t nNewCaretPos;
    
    nNewCaretPos = pData->nCaretPos - nChar;
    if (nNewCaretPos <= 0) {
        pData->nShowPos = pData->nEditPos;
        pData->nCaretPos = 0;
        
        if (pData->nInputLen > pData->nEditPos + pData->nCanShowLen + 1) {
            pData->nTrueShowLen = pData->nCanShowLen + 1;
            
            nCharState = pData->CharState[pData->nEditPos + pData->nTrueShowLen - 1];
            switch (nCharState) {
            case CHAR_ASC:
            case CHAR_HZ_1:
                SetEditTextOff (pData, 1, 0);
                break;
                
            case CHAR_HZ_0:
                pData->nTrueShowLen++;
                
                SetEditTextOff (pData, 1, 1);
                break;
            }
        }
        else {
            pData->nTrueShowLen = pData->nInputLen - pData->nEditPos;
            
            SetEditTextOff (pData, 1, 0);
        }
        
        SetEditTextOff (pData, 0, 0);
        
        SetEditTextOutFmt (pData, ETF_LEFT);
    }
    else {
        if (ETF_LEFT == GetEditTextOutFmt (pData)) {
            pData->nShowPos = pData->nShowPos;
            
            if (pData->nInputLen - pData->nEditPos > (pData->nCanShowLen + 1) - nNewCaretPos) {
                pData->nTrueShowLen = pData->nCanShowLen + 1;
                nCharState = pData->CharState[pData->nTrueShowLen - 1];
                switch (nCharState) {
                case CHAR_ASC:
                case CHAR_HZ_1:
                    SetEditTextOff (pData, 1, 0);
                    break;
                    
                case CHAR_HZ_0:
                    pData->nTrueShowLen++;
                    
                    SetEditTextOff (pData, 1, 1);
                    break;
                }
            }
            else {
                pData->nTrueShowLen = pData->nInputLen - pData->nShowPos;
            }
            
            pData->nCaretPos = nNewCaretPos;
        }
        else {
            // 先删除左边字符，然后左移右边字符
            if (pData->nEditPos >= pData->nCaretPos + 1) {
                pData->nCaretPos = pData->nCaretPos;
                
                pData->nTrueShowLen = pData->nCanShowLen + 1;
                
                pData->nShowPos = pData->nEditPos - (pData->nCaretPos + 1);
                nCharState = pData->CharState[pData->nShowPos];
                switch (nCharState) {
                case CHAR_ASC:
                case CHAR_HZ_0:
                    SetEditTextOff (pData, 0, 0);
                    break;
                    
                case CHAR_HZ_1:
                    pData->nShowPos--;
                    pData->nTrueShowLen++;
                    
                    SetEditTextOff (pData, 0, 1);
                    break;
                }
            }
            else {
                pData->nShowPos = 0;
                
                pData->nCaretPos = pData->nEditPos;
                
                pData->nTrueShowLen = pData->nInputLen;
                if (pData->nTrueShowLen > pData->nCanShowLen) {
                    pData->nTrueShowLen = pData->nCanShowLen + 1;
                    
                    nCharState = pData->CharState[pData->nTrueShowLen - 1];
                    switch (nCharState) {
                    case CHAR_ASC:
                    case CHAR_HZ_1:
                        break;
                        
                    case CHAR_HZ_0:
                        pData->nTrueShowLen++;
                        break;
                    }
                }
                
                SetEditTextOff (pData, 0, 0);
                
                SetEditTextOff (pData, 1, 0);
                
                SetEditTextOutFmt (pData, ETF_LEFT);
            }
        }
    }
    
    EditSetCaretPos (Window);
}

static void EditRightMove(WINDOW *Window, TEditData *pData, int32_t nChar)
{
    int8_t nCharState;
    int16_t nNewCaretPos;
    
    nNewCaretPos = pData->nCaretPos + nChar;
    
    // 光标超出
    if (nNewCaretPos > pData->nCanShowLen) {
        SetEditTextOutFmt (pData, ETF_RIGHT);
        
        pData->nShowPos = pData->nEditPos - (pData->nCanShowLen + 1);
        nCharState = pData->CharState[pData->nShowPos];
        switch (nCharState) {
        case CHAR_ASC:
        case CHAR_HZ_0:
            pData->nTrueShowLen = pData->nCanShowLen + 1;
            
            SetEditTextOff (pData, 0, 0);
            break;
            
        case CHAR_HZ_1:
            pData->nShowPos--;
            pData->nTrueShowLen = pData->nCanShowLen + 2;
            
            SetEditTextOff (pData, 0, 1);
            break;
        }
        
        SetEditTextOff (pData, 1, 0);
        
        pData->nCaretPos = pData->nCanShowLen;
    }
    // 光标未超出
    else {
        if (ETF_LEFT == GetEditTextOutFmt (pData)) {
            pData->nShowPos = pData->nShowPos;
            
            if (pData->nInputLen - pData->nShowPos > pData->nCanShowLen + 1) {
                pData->nTrueShowLen = pData->nCanShowLen + 1;
            }
            else {
                pData->nTrueShowLen = pData->nInputLen - pData->nShowPos;
            }
            
            nCharState = pData->CharState[pData->nShowPos + pData->nTrueShowLen - 1];
            switch (nCharState) {
            case CHAR_ASC:
            case CHAR_HZ_1:
                break;
                
            case CHAR_HZ_0:
                pData->nTrueShowLen++;
                break;
            }
            
            pData->nCaretPos = nNewCaretPos;
        }
        else {
            pData->nShowPos = pData->nEditPos - (nNewCaretPos + 1);
            
            nCharState = pData->CharState[pData->nShowPos];
            switch (nCharState) {
            case CHAR_ASC:
            case CHAR_HZ_0:
                pData->nTrueShowLen = pData->nCanShowLen + 1;
                
                SetEditTextOff (pData, 0, 1);
                break;
                
            case CHAR_HZ_1:
                pData->nShowPos--;
                pData->nTrueShowLen = pData->nCanShowLen + 2;
                
                SetEditTextOff (pData, 0, 1);
                break;
            }
            
            SetEditTextOff (pData, 1, 0);
        
            pData->nCaretPos = nNewCaretPos;
        }
    }
    
    EditSetCaretPos (Window);
}

static void EditKeyDown(WINDOW *Window,uint32_t wParam,uint32_t lParam)
{
    TEditData *pData;
    int8_t nPreCharState, nNextCharState, nChar = 0;
    
    pData = (TEditData *)Window->pPrivData;
    
    switch (LOuint16_t (wParam)) {
    case VK_LEFT:
        if (pData->nEditPos <= 0) {
            break;
        }
        
        nPreCharState = pData->CharState[pData->nEditPos - 1];
        switch (nPreCharState) {
        case CHAR_ASC:
            nChar = 1;
            break;
            
        case CHAR_HZ_0:
            nChar = 1;
            break;
            
        case CHAR_HZ_1:
            nChar = 2;
            break;
        }
        pData->nEditPos -= nChar;
        EditLeftMove (Window, pData, nChar);
        
        InvalidateRect(Window, NULL, TRUE);
        break;
        
    case VK_RIGHT:
        if (pData->nEditPos >= pData->nInputLen) {
            break;
        }
        
        nNextCharState = pData->CharState[pData->nEditPos];
        switch (nNextCharState) {
        case CHAR_ASC:
            nChar = 1;
            break;
            
        case CHAR_HZ_0:
            nChar = 2;
            break;
            
        case CHAR_HZ_1:
            nChar = 1;
            break;
        }
        pData->nEditPos += nChar;
        EditRightMove (Window, pData, nChar);
        
        InvalidateRect(Window, NULL, TRUE);
        break;
        
    case VK_BACK:
        if (EditIsReadOnly (pData->nStatus)) {
            return;
        }
        
        EditBakespaceChar (Window, wParam, lParam);
        break;
        
    default:
        break;
    }
}

static int32_t EditChar(WINDOW *Window,uint32_t wParam,uint32_t lParam)
{
    int16_t i;
    int8_t NewChar;
    TEditData *pData;
    int8_t nPreCharState, nNewCharState;
    
    pData = (TEditData *)Window->pPrivData;
    
    if (EditIsReadOnly (pData->nStatus)) {
        return 0;
    }
    
    switch (LOuint16_t (wParam)) {
    case 0x00:  // NULL
    case 0x07:  // BEL
    case 0x08:  // BS
    case 0x09:  // HT
    case 0x0A:  // LF
    case 0x0B:  // VT
    case 0x0C:  // FF
    case 0x0D:  // CR
    case 0x1B:  // Escape
        return 0;
        break;
    }
    
    NewChar = LOuint8_t(LOuint16_t(wParam));
    
    nNewCharState = CHAR_ASC;
    if (pData->nEditPos > 0) {
        nPreCharState = pData->CharState[pData->nEditPos - 1];
        switch (nPreCharState) {
        case CHAR_ASC:
            if (NewChar & 0x80) {
                nNewCharState = CHAR_HZ_0;
            }
            else {
                nNewCharState = CHAR_ASC;
            }
            break;
            
        case CHAR_HZ_0:
            nNewCharState = CHAR_HZ_1;
            break;
            
        case CHAR_HZ_1:
            if (NewChar & 0x80) {
                nNewCharState = CHAR_HZ_0;
            }
            else {
                nNewCharState = CHAR_ASC;
            }
            break;
        }
        
        if (FALSE == EditCanInsert (pData, nNewCharState)) {
            return -1;
        }
    }
    else {
        if (NewChar & 0x80) {
            nNewCharState = CHAR_HZ_0;
        }
        else {
            nNewCharState = CHAR_ASC;
        }
        
        if (FALSE == EditCanInsert (pData, nNewCharState)) {
            return -1;
        }
    }
    
    for (i = pData->nInputLen; i > pData->nEditPos; i--) {
        pData->EditBuf[i] = pData->EditBuf[i - 1];
        pData->CharState[i] = pData->CharState[i - 1];
    }
    pData->nInputLen++;
    pData->EditBuf[pData->nInputLen] = '\0';
    
    pData->EditBuf[pData->nEditPos] = NewChar;
    pData->CharState[pData->nEditPos] = nNewCharState;
    pData->nEditPos++;
    
    switch (nNewCharState) {
    case CHAR_ASC:
        EditRightMove (Window, pData, 1);
        
        InvalidateRect(Window, NULL, TRUE);
        break;
        
    case CHAR_HZ_0:
        break;
        
    case CHAR_HZ_1:
        EditRightMove (Window, pData, 2);
        
        InvalidateRect(Window, NULL, TRUE);
        break;
    }
    
    return 0;
}

static void EditPaint(WINDOW *Window, DC* hdc)
{
    RECT TextRect;
    int32_t nTextOutFmt;
    TEditData *pData;
    
    pData = (TEditData *)Window->pPrivData;
    
    GetClientRect (Window, &TextRect);
    
    nTextOutFmt = GetEditTextOutFmt (pData);
    if (nTextOutFmt == ETF_LEFT) {
        nTextOutFmt = DT_LEFT;
    }
    else {
        nTextOutFmt = DT_RIGHT;
    }
    
    TextRect.left   += 2;
    TextRect.right  -= 2;
    TextRect.top    += 2;
    TextRect.bottom -= 2;
    TextRect.left   -= GetEditTextOff (pData, 0) * (FONTPOINT >> 1);
    TextRect.right  += GetEditTextOff (pData, 1) * (FONTPOINT >> 1);
    
    strncpy (pData->pShowBuf, pData->EditBuf + pData->nShowPos, pData->nTrueShowLen);
    pData->pShowBuf[pData->nTrueShowLen] = '\0';
    if (pData->nStatus & EDIT_PASSWORD) {
        memset (pData->pShowBuf, pData->PswChar, pData->nTrueShowLen);
    }
    DrawText (hdc, pData->pShowBuf, strlen (pData->pShowBuf), &TextRect, nTextOutFmt);
}

static void SetEditPosByMouse(WINDOW *Window, uint32_t wParam, uint32_t lParam)
{
    POINT p;
    RECT WndRect;
    int32_t nCharState;
    int32_t nPos, nETFmt;
    TEditData *pData;
    
    pData = (TEditData *)Window->pPrivData;
    
    p.x = LOuint16_t (lParam);
    p.y = HIuint16_t (lParam);
    
    GetClientRect (Window, &WndRect);
    ScreenToClient (Window, &p);
    
    nETFmt = GetEditTextOutFmt (pData);
    if (nETFmt == ETF_LEFT) {
        nPos = p.x / (FONTPOINT >> 1);
        if (pData->nShowPos + nPos > pData->nInputLen) {
            pData->nEditPos = pData->nInputLen;
            pData->nCaretPos = pData->nEditPos - pData->nShowPos;
        }
        else {
            pData->nEditPos = pData->nShowPos + nPos;
            
            nCharState = pData->CharState[pData->nEditPos];
            switch (nCharState) {
            case CHAR_ASC:
                break;
                
            case CHAR_HZ_0:
                break;
                
            case CHAR_HZ_1:
                pData->nEditPos--;
                break;
            }
            
            pData->nCaretPos = pData->nEditPos - pData->nShowPos;
        }
    }
    else {
        int16_t nEnd;
        
        nPos = (WndRect.right - p.x) / (FONTPOINT >> 1);
        if (nPos > pData->nCanShowLen) {
            nPos = pData->nCanShowLen;
        }
        
        nEnd = pData->nCanShowLen - pData->nCaretPos;
        pData->nCaretPos = pData->nCanShowLen - nPos;
        pData->nEditPos = pData->nEditPos + nEnd - nPos;
        
        nCharState = pData->CharState[pData->nEditPos - 1];
        switch (nCharState) {
        case CHAR_ASC:
        case CHAR_HZ_1:
            break;
            
        case CHAR_HZ_0:
            pData->nEditPos++;
            pData->nCaretPos++;
            break;
        }
    }
    
    EditSetCaretPos (Window);
    
    InvalidateRect (Window, NULL, TRUE);
}

static void ResetEdit(TEditData *pData)
{
    pData->nTrueShowLen  = 0;
    pData->nInputLen     = 0;
    pData->nMaxLen       = MAX_TEXT_LEN;
    pData->nCaretPos     = 0;
    pData->nEditPos      = 0;
    pData->nShowPos      = 0;
    pData->nTextOut      = ETF_LEFT;
    pData->nTextLeftOff  = 0;
    pData->nTextRightOff = 0;
    pData->PswChar       = '*';
    pData->EditBuf[0]    = '\0';
    memset (pData->CharState, 0, MAX_TEXT_LEN + 1);
}

static int32_t EditProc(WINDOW *Window, uint32_t Message, uint32_t wParam, uint32_t lParam)
{
    RECT WndRect;
    TEditData *pData;
    
    pData = (TEditData *)Window->pPrivData;
    
    switch(Message){
    case EV_CREATE:
    {
        pData = (TEditData *) Malloc (sizeof(TEditData));
        if (pData == NULL) {
            return -1;
        }
        
        GetClientRect (Window, &WndRect);
        
        pData->nStatus     = Window->nStyle | EDIT_NORMAL;
        pData->nCanShowLen = (int16_t)(WndRect.right - WndRect.left - 4) / (FONTPOINT >> 1);
        
        pData->pShowBuf  = Malloc (pData->nCanShowLen + 2);
        if (NULL == pData->pShowBuf) {
            Free (pData);
            return -1;
        }
        memset (pData->pShowBuf, '\0', pData->nCanShowLen + 2);
        
        if (!CreateCaret (Window, NULL, 1, 16)) {
            Free (pData);
            return -2;
        }
        
        Window->pPrivData = pData;
        
        ResetEdit (pData);
        EditSetCaretPos (Window);
        break;
    }
        
    case EV_ERASEBKGND:
    {
        DC* hdc;
        RECT WndRect;
        
        hdc = BeginErase (Window, wParam, lParam);
        if (NULL == hdc) {
            break;
        }
        
        GetClientRect (Window, &WndRect);
        
        Draw3DDownFrame (hdc, WndRect.left, WndRect.top, 
            WndRect.right, WndRect.bottom, RGB (255, 255, 255));
        
        EndErase (Window, hdc, wParam, lParam);
        return 0;
        break;
    }
        
    case EV_PAINT:
    {
        DC* hdc;
        bool32_t bShow = HideCaret (Window);
        
        hdc = BeginPaint (Window, wParam, lParam);
        if (NULL == hdc) {
            if (bShow) {
                ShowCaret (Window);
            }
            break;
        }
        
        EditPaint (Window, hdc);
        
        EndPaint (Window, hdc, wParam, lParam);
        
        if (bShow) {
            ShowCaret (Window);
        }
        return 0;
        break;
    }
        
    case EV_KEYDOWN:
    {
        EditKeyDown (Window, wParam, lParam);
        return 0;
        break;
    }
        
    case EV_CHAR:
    {
        if (HIuint8_t (wParam)) {
            if (EditChar (Window, LOuint8_t (wParam), lParam) < 0) {
                return -1;
            }
            
            return EditChar (Window, HIuint8_t (wParam), lParam);
        }
        
        return EditChar (Window, LOuint8_t (wParam), lParam);
        break;
    }
        
    case EV_LBUTTONDOWN:
    {
        DefWinProc(Window, Message, wParam, lParam);
        
        SetEditPosByMouse (Window, wParam, lParam);
        return 0;
        break;
    }
        
    case EV_GETTEXTLEN:
    {
        return pData->nInputLen;
        break;
    }
        
    case EV_SETTEXTLEN:
    {
        int32_t nLen = (int32_t)wParam;
        
        nLen = MIN (nLen, MAX_TEXT_LEN);
        
        pData->nMaxLen = nLen;
        break;
    }
        
    case EV_GETTEXT:
    {
        int32_t nLen;
        int8_t* pOutBuf = (int8_t*)lParam;
        
        nLen = MIN ((int32_t)wParam, pData->nInputLen);
        
        memcpy (pOutBuf, pData->EditBuf, nLen);
        pOutBuf [nLen] = '\0';
        
        return nLen;
        break;
    }
        
    case EV_SETTEXT:
    {
        int32_t i, nLen;
        int8_t* pInBuf = (int8_t*)lParam;
        
        if (EditIsReadOnly (pData->nStatus)) {
            return -1;
        }
        
        nLen = MIN ((int32_t)wParam, pData->nMaxLen);
        
        ResetEdit (pData);
        EditSetCaretPos (Window);
        
        for (i = 0; i < nLen; i++) {
            if (EditChar (Window, MAKEuint32_t(pInBuf[i], 0), 0) < 0) {
                break;
            }
        }
        break;
    }
        
    case EV_SETTEXTPSW:
    {
        if (TRUE == (bool32_t)wParam) {
            pData->nStatus |= EDIT_PASSWORD;
        }
        else {
            pData->nStatus &= (~EDIT_PASSWORD);
        }
        InvalidateRect(Window, NULL, TRUE);
        return 0;
        break;
    }
        
    case EV_SETFOCUS:
        ShowCaret (Window);
        pData->nStatus |= EDIT_FOCUS;
        InvalidateRect(Window, NULL, TRUE);
        break;
        
    case EV_KILLFOCUS:
        HideCaret (Window);
        pData->nStatus &= (~EDIT_FOCUS);
        InvalidateRect(Window, NULL, TRUE);
        break;
        
    case EV_NEEDIME:
        if (FALSE == EditIsReadOnly (Window->nStyle)) {
            return 1;
        }
        else {
            return 0;
        }
        break;
        
    case EV_DESTROY:
        DestroyCaret (Window);
        
        if (NULL != pData->pShowBuf) {
            Free (pData->pShowBuf);
        }
        Free (pData);
        break;
    }
    
    return DefWinProc(Window, Message, wParam, lParam);
}

bool32_t RegisterEditClass(void)
{
    WNDCLASS EditClass;

    memset (&EditClass, 0, sizeof (WNDCLASS));
    EditClass.pClassName = "edit";
    EditClass.pCursor    = GetSystemCursor (CURSOR_BEAM_ID);
    EditClass.WinProc    = EditProc;
    
    return RegisterClass (&EditClass);
}
