#ifndef __CLOCK_H__
#define __CLOCK_H__

#ifdef __cplusplus
extern "C"
{
#endif

bool32_t RegisterClockClass(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
