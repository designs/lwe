/*
  Copyright (C), 2008, zhanbin
  File name:    clock.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081219
  Description:
    演示控件定时器功能。
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081219    zhanbin         创建文件
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

static int32_t DefClockProc(WINDOW *Window,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    switch(Message){
    case EV_CREATE:
    {
        SetTimer (Window, 1000, 100, NULL);
        break;
    }
        
    case EV_TIMER:
    {
        InvalidateRect(Window, NULL, TRUE);
        break;
    }
        
    case EV_PAINT:
    {
        DC* hdc;
        RECT WndRect;
        int8_t strSysTime[30];
        
        hdc = BeginPaint (Window, wParam, lParam);
        if (NULL == hdc) {
            break;
        }
        
        sprintf (strSysTime, "系统运行时间：%d秒", (uint32_t)GetTime () / 1000);
        
        GetClientRect (Window, &WndRect);
        
        SetFgColor (hdc, RGB (255, 0, 0));
        DrawText (hdc, strSysTime, strlen (strSysTime), &WndRect, DT_LEFT);
        
        EndPaint (Window, hdc, wParam, lParam);
        return 0;
        break;
    }
    
    default:
        break;
    }

	return DefWinProc(Window, Message, wParam, lParam);
}

bool32_t RegisterClockClass(void)
{
    WNDCLASS ClockClass;

    memset (&ClockClass, 0, sizeof (WNDCLASS));
    ClockClass.pClassName = "clock";
    ClockClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    ClockClass.WinProc    = DefClockProc;
    
    return RegisterClass (&ClockClass);
}
