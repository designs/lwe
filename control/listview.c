/*
** $Id: listview.c,v 1.76.8.1 2004/04/30 01:21:48 snig Exp $
**
** listview.c: the ListView control
**
** Copyright (C) 2003 Feynman Software.
** 
** 2003/05/17: Rewritten by Zhong Shuyi.
**
** Create date: 2001/12/03
**
** Original authors: Shu Ming, Amokaqi, chenjm, kevin.
*/

/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
** Modify records:
**
**  Who             When        Where       For What                Status
**-----------------------------------------------------------------------------
**  amokaqi  2002/9/26        add CURSORBLOCKDOWN and UP            finish
**  amokaqi  2002/10/8        add KILLFOCUS and SETFOCUS            finish
**  amokaqi  2002/10/8        add GETDLGCODE for dialog             finish
**  chenjm   2002/10/24       add LVM_SELECTITEM and LVM_SHOWITEM   finish
**  kevin    2003/01/03       modify KILLFOCUS and SETFOCUS         finish
**  snig     2003/05/06       add SB_PAGELEFT, SB_PAGERIGHT         finish
**                                SB_PAGEUP and SB_PAGEDOWN;
**  snig     2003/05/13       code cleanup, rewrite ...             finish
**
**  snig     2003/05/15       entirely new listview interfaces      done
**  zhanbin  2009/02/14       移值到GUI                          done
**-----------------------------------------------------------------------------
**
** TODO:
*/
#include    "window.h"

/********************** for GUI by zhanbin **********************/
#define SetTextColor      SetFgColor
#define SetPenColor       SetFgColor
#define SetBrushColor     SetFgColor
#define SetBkColor        SetBkColor

#define NotifyParentEx(a,b,c,d)    NotifyParent(a,b,c)

static void *GetWindowAdditionalData2(WINDOW *hWnd)
{
    return hWnd->pPrivData;
}

static void SetWindowAdditionalData2(WINDOW *hWnd, uint32_t nPriv)
{
    hWnd->pPrivData = (void *)nPriv;
}

COLORREF GetWindowBkColor (WINDOW *hWnd)
{
    return RGB (255, 255, 255);
}

static void FillBox (DC *hdc, int32_t x, int32_t y, int32_t w, int32_t h)
{
    RECT rect;
    
    SetRect (&rect, x, y, x + w, y + h);
    FillRect (hdc, &rect);
}

static bool32_t FillBoxWithBitmap (DC *hdc, int32_t x, int32_t y, int32_t w, int32_t h, BITMAP* pBitmap)
{
    DrawBitmap (hdc, x, y, pBitmap, 0, 0, w, h);
    
    return TRUE;
}

/********************** internals functions declaration **********************/

static int32_t itemDelete (PITEMDATA pHead);

#define sGetItemFromList(nSeq, plvdata)  GetItemByRow(plvdata, nSeq)

inline static int32_t sGetFrontSubItemsWidth (int32_t end, PLVDATA plvdata);
static int32_t sGetSubItemWidth (int32_t nCols, PLVDATA plvdata);
static int32_t sAddOffsetToTailSubItem (int32_t nCols, int32_t offset,
                                    PLVDATA plvdata);

static int32_t sGetItemWidth (PLVDATA plvdata);
static PITEMDATA GetItemByRow (PLVDATA plvdata, int32_t nRows);
static int32_t lstSetVScrollInfo (PLVDATA plvdata);
static int32_t lstSetHScrollInfo (PLVDATA plvdata);
static PSUBITEMDATA lvGetSubItemByCol (PITEMDATA pItem, int32_t nCols);
static PLSTHDR GetColsListPoint (PLVDATA plvdata, int32_t nCols);

/* ========================================================================= 
 * size and position operation section of the listview control.
   ========================================================================= */

#define LV_HDR_HEIGHT           (plvdata->nHeadHeight)

#define LV_ITEM_Y(nRows)        ((nRows-1) * plvdata->nItemHeight)
#define LV_ITEM_YY(nRows)       (LV_ITEM_Y(nRows) - plvdata->nOriginalY)
#define LV_ITEM_YYH(nRows)      (LV_ITEM_YY(nRows) + plvdata->nHeadHeight)

#define sGetSubItemX(nCols)     ((GetColsListPoint(plvdata, nCols))->x)

/* Gets the rect of an item */
#define LV_GET_ITEM_RECT(nRow, rect)                                \
        GetClientRect(plvdata->hWnd, &rect);                      \
        rect.top = LV_ITEM_YYH(nRow);                               \
        rect.bottom = rect.top + plvdata->nItemHeight;              \
        if (rect.top < (LV_HDR_TOP + LV_HDR_HEIGHT)) {              \
            rect.top = (LV_HDR_TOP + LV_HDR_HEIGHT);                \
        }

/* Gets the rect of a subitem */
#define LV_GET_SUBITEM_RECT(nRows, nCols, rect)                     \
        LV_GET_ITEM_RECT(nRows, rect);                              \
        rect.left = sGetSubItemX(nCols) - plvdata->nOriginalX;      \
        rect.right = rect.left + sGetSubItemWidth(nCols, plvdata); 

/* Gets the text rect of a subitem */
#define LV_GET_SUBITEM_TEXTRECT(rect, textrect)                     \
        textrect.left = rect.left + 2;                              \
        textrect.top = rect.top + 2;                                \
        textrect.right = rect.right - 2;                            \
        textrect.bottom = rect.bottom - 2;

#define LV_GET_HDR_RECT(p1, rect)                                   \
        rect.left = p1->x - plvdata->nOriginalX + 1;                \
        rect.right = p1->x + p1->width - plvdata->nOriginalX - 1;   \
        rect.top = LV_HDR_TOP;                                      \
        rect.bottom = plvdata->nHeadHeight;

#define LV_BE_VALID_COL(nPosition)    (nPosition <= plvdata->nCols && nPosition >= 1)
#define LV_BE_VALID_ROW(nPosition)    (nPosition <= plvdata->nRows && nPosition >= 1)

#define LV_H_OUTWND(plvdata, rcClient)                              \
               ( sGetItemWidth (plvdata) - 2 > rcClient.right - rcClient.left)
               
inline static void Invalidate_ItemRect(WINDOW *hwnd)
{
    PLVDATA plvdata;
    RECT rect, rcClient;
    
    plvdata = (PLVDATA) GetWindowAdditionalData2 (hwnd);
    
    GetClientRect (hwnd, &rcClient);
    SetRect (&rect, rcClient.left, rcClient.top + LV_HDR_TOP + LV_HDR_HEIGHT, 
        rcClient.right, rcClient.bottom);
    InvalidateRect (hwnd, &rect, FALSE);
}

inline static bool32_t 
BeInHeadBorder (int32_t mouseX, int32_t mouseY, PLSTHDR p1, PLVDATA plvdata)
{
    if ( (mouseX >= (p1->x + p1->width - plvdata->nOriginalX - 1))
        && (mouseX <= (p1->x + p1->width - plvdata->nOriginalX))
        && (mouseY >= 0) && (mouseY <= plvdata->nHeadHeight) )
        return TRUE;
        
    return FALSE;
}

static int32_t 
InWhichHeadBorder (int32_t mouseX, int32_t mouseY, PLSTHDR * pRet, PLVDATA plvdata)
{
    int32_t nPosition = 0;
    PLSTHDR p1 = plvdata->pLstHead;

    while (p1 != NULL)
    {
        nPosition++;

        if (BeInHeadBorder(mouseX, mouseY, p1, plvdata))
            break;
    
        p1 = p1->pNext;
    }
    
    if (!p1) {
        if (pRet)
            *pRet = NULL;
        return -1;
    }

    if (pRet)
        *pRet = p1;
        
    return nPosition;
}

static int32_t isInListViewHead (int32_t mouseX, int32_t mouseY, PLSTHDR * pRet, PLVDATA plvdata)
{
    POINT pt;
    int32_t nPosition = 0;
    PLSTHDR p1 = plvdata->pLstHead;
    RECT rect;

    while (p1)
    {
        nPosition++;

    LV_GET_HDR_RECT(p1, rect);
    SetPoint (&pt, mouseX, mouseY);
    if (PtInRect (&rect, &pt))
            break;
        
        p1 = p1->pNext;
    }

    //not in head
    if (!p1 || (nPosition > plvdata->nCols) || (nPosition == 0)) {
        if (pRet)
        *pRet = NULL;
        return -1;
    }

    //in head
    if (pRet)
        *pRet = p1;
    return nPosition;
}

static int32_t isInListViewItem (int32_t mouseX, int32_t mouseY, PITEMDATA * pRet, PLVDATA plvdata)
{
    int32_t ret, j;
    PITEMDATA p1;

    if ((mouseY < plvdata->nHeadHeight))
        return -1;

    ret = (mouseY + plvdata->nOriginalY - plvdata->nHeadHeight) / plvdata->nItemHeight;
    ret++;
    *pRet = NULL;

    p1 = plvdata->pItemHead;
    j = 0;

    while ((p1 != NULL) && (j < ret))
    {
        *pRet = p1;
        p1 = p1->pNext;
        j++;
    }
    
    if (ret > j)
        return -1;
    
    return ret;
}

/* ========================================================================= 
 * Drawing section of the listview control.
   ========================================================================= */

static void sDrawText (DC *hdc, int32_t x, int32_t y, int32_t width, int32_t height, 
        const int8_t *pszText, uint32_t format)
{
    RECT rect;
    //SIZE size;

    if (pszText != NULL)
    {
        SetRect (&rect, x+2, y+2, x+width, y+height);
        DrawText (hdc, pszText, -1, &rect, format);
    }
}

static void DrawSubItem (DC *hdc, int32_t nRows, int32_t nCols, PLVDATA plvdata)
{
    RECT rect, rect_text;
    PITEMDATA pItem;
    PSUBITEMDATA psub;
    PLSTHDR ph;
    uint32_t text_format;

    if ( !(pItem = GetItemByRow(plvdata, nRows)) )
        return;
    if ( !(psub = lvGetSubItemByCol(pItem, nCols)) )
        return;

    ph = GetColsListPoint (plvdata, nCols);
    LV_GET_SUBITEM_RECT(nRows, nCols, rect);

    if (!pItem->bSelected) {
        SetBrushColor (hdc, GetWindowBkColor(plvdata->hWnd));
        SetBkColor (hdc, GetWindowBkColor(plvdata->hWnd));
        //SetTextColor (hdc, psub->nTextColor);       // zhanbin
    }
    else {
        SetBkColor (hdc, plvdata->bkc_selected);
        SetBrushColor (hdc, plvdata->bkc_selected);
        //SetTextColor (hdc, RGB (255, 255, 255));    // zhanbin
    }

    FillBox (hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top);

    LV_GET_SUBITEM_TEXTRECT(rect, rect_text);

    if (psub->Image) {
       int32_t width, height;
       if (psub->dwFlags & LVIF_BITMAP) {
            BITMAP *bmp = (BITMAP *)psub->Image;
            height = (bmp->bmHeight < plvdata->nItemHeight)? bmp->bmHeight : plvdata->nItemHeight;
            rect.top += (plvdata->nItemHeight - height) / 2;
            rect.left += 2;
            FillBoxWithBitmap (hdc, rect.left, rect.top, 0, height, bmp);
            rect_text.left = rect.left + bmp->bmWidth + 2;
       }
       else if (psub->dwFlags & LVIF_ICON) {
            GetIconSize ((ICON *)psub->Image, &width, &height);
            height = (height < plvdata->nItemHeight) ? height : plvdata->nItemHeight;
            rect.top += (plvdata->nItemHeight - height) / 2;
            rect.left += 2;
            DrawIcon (hdc, rect.left, rect.top, 0, height,
                (ICON *)psub->Image);
            rect_text.left = rect.left + width + 2;
       }
    }

    if (!psub->pszInfo)
        return;

#if 0   // zhanbin
    if (ph->flags & COLUME_TXT_RIGHTALIGN)
        text_format = DT_SINGLELINE | DT_RIGHT | DT_VCENTER;
    else if (ph->flags & COLUME_TXT_CENTERALIGN)
        text_format = DT_SINGLELINE | DT_CENTER | DT_VCENTER;
    else
        text_format = DT_SINGLELINE | DT_LEFT | DT_VCENTER;
#else
    if (ph->flags & COLUME_TXT_RIGHTALIGN)
        text_format = DT_RIGHT;
    else if (ph->flags & COLUME_TXT_CENTERALIGN)
        text_format = DT_CENTER;
    else
        text_format = DT_LEFT;
#endif

    if (!pItem->bSelected) {        // zhanbin
        SetBkColor (hdc, GetWindowBkColor(plvdata->hWnd));
        SetTextColor (hdc, psub->nTextColor);
    }
    else {
        SetBkColor (hdc, plvdata->bkc_selected);
        SetTextColor (hdc, RGB (255, 255, 255));
    }
    
    DrawText (hdc, psub->pszInfo, -1, &rect_text, text_format);
}

//Draws listview header
static void DrawHeader (WINDOW *hwnd, DC *hdc)
{
    PLSTHDR p1 = NULL;
    PLVDATA plvdata;
    RECT rcClient;
    bool32_t up = TRUE;
    uint32_t format;

    GetClientRect (hwnd, &rcClient);
    plvdata = (PLVDATA) GetWindowAdditionalData2 (hwnd);
    p1 = plvdata->pLstHead;

    if (LVSTATUS(hwnd) & LVST_HEADCLICK && LVSTATUS(hwnd) & LVST_INHEAD)
        up = FALSE;

    SetBkColor (hdc, RGB (192, 192, 192));
    SetBrushColor (hdc, RGB (192, 192, 192));
    FillBox (hdc, rcClient.left, rcClient.top, rcClient.right - rcClient.left,
        plvdata->nHeadHeight);

    SetTextColor (hdc, RGB (0, 0, 0));

    while (p1)
    {
        Draw3DControlFrame (hdc, p1->x - plvdata->nOriginalX + 1,
                          LV_HDR_TOP,
                          p1->x - plvdata->nOriginalX + p1->width - 1,
                          LV_HDR_TOP + LV_HDR_HEIGHT, RGB (192, 192, 192), up);
#if 0   // zhanbin
    if (p1->flags & HEADER_TXT_CENTERALIGN)
        format = DT_SINGLELINE | DT_CENTER | DT_VCENTER;
    else if (p1->flags & HEADER_TXT_RIGHTALIGN)
        format = DT_SINGLELINE | DT_RIGHT | DT_VCENTER;
    else
        format = DT_SINGLELINE | DT_LEFT | DT_VCENTER;
#else
    if (p1->flags & HEADER_TXT_CENTERALIGN)
        format = DT_CENTER;
    else if (p1->flags & HEADER_TXT_RIGHTALIGN)
        format = DT_RIGHT;
    else
        format = DT_LEFT;
#endif

        sDrawText (hdc, p1->x - plvdata->nOriginalX + 2, LV_HDR_TOP,
                 p1->width - 4, LV_HDR_HEIGHT, p1->pTitle, format);
        p1 = p1->pNext;
    }
    //draws the right most unused header
    if ( !LV_H_OUTWND(plvdata, rcClient) ) {
      Draw3DControlFrame (hdc, sGetItemWidth (plvdata),
                          LV_HDR_TOP,
                          rcClient.right+2,
                          LV_HDR_TOP + LV_HDR_HEIGHT, RGB (192, 192, 192), up);
    }
}

static void DrawItem (WINDOW *hwnd, DC *hdc, int32_t nRows)
{
    int32_t i;
    //RECT rcClient;
    PLVDATA plvdata;
    //PITEMDATA p3 = NULL;

    plvdata = (PLVDATA) GetWindowAdditionalData2 (hwnd);
    //p3 = GetItemByRow(plvdata, nRows);

    //GetClientRect (hwnd, &rcClient);

    /*
    if (p3->bSelected) {
        SetBrushColor (hdc,plvdata->bkc_selected);
    }
    else
        SetBrushColor (hdc, RGB (255, 255, 255));

    FillBox (hdc, rcClient.left,
             (nRows - 1) * plvdata->nItemHeight +
             plvdata->nHeadHeight,
             sGetItemWidth(plvdata)-1,
             plvdata->nItemHeight);
    */

    for (i = 1; i <= plvdata->nCols; i++)
    {
        DrawSubItem (hdc, nRows, i, plvdata);
    }
}

static void ListView_Draw (WINDOW *hwnd, DC *hdc)
{
    int32_t j;
    RECT rcClient;
    PLVDATA plvdata;

    plvdata = (PLVDATA) GetWindowAdditionalData2 (hwnd);
    GetClientRect (hwnd, &rcClient);

    SetBkColor (hdc, RGB (255, 255, 255));
    SetBrushColor (hdc, RGB (255, 255, 255));
    FillBox (hdc, rcClient.left, rcClient.top, rcClient.right - rcClient.left,
             rcClient.bottom - rcClient.top);

    //draws item area
    for (j = 1; j <= plvdata->nRows; j++)
    {
        DrawItem (hwnd, hdc, j);
    }

    DrawHeader (hwnd, hdc);
}

/*************************************  Listview Move/Scroll Action ***********************/


/* Makes an item to be visible */
static void
MakeItemVisible (WINDOW *hwnd, PLVDATA plvdata, int32_t nRows)
{
    int32_t scrollHeight = 0;
    RECT rect;
    int32_t area_height;

    if (!LV_BE_VALID_ROW(nRows))
        nRows = plvdata->nItemSelected;
    if (!LV_BE_VALID_ROW(nRows))
        return;

    GetClientRect (hwnd, &rect);
    area_height = rect.bottom - rect.top - plvdata->nHeadHeight;

    if ( LV_ITEM_Y(nRows) < plvdata->nOriginalY) {
        scrollHeight = plvdata->nOriginalY - LV_ITEM_Y(nRows);
    }
    else if ( LV_ITEM_Y(nRows+1) - plvdata->nOriginalY > area_height ) {
        scrollHeight = plvdata->nOriginalY + area_height - LV_ITEM_Y(nRows+1);
    }

    if (scrollHeight != 0) {
        plvdata->nOriginalY -= scrollHeight;
        lstSetVScrollInfo (plvdata);
        //InvalidateRect(hwnd, NULL, FALSE);
        Invalidate_ItemRect (hwnd);
    }
}


static void lvVScroll (WINDOW *hwnd, uint32_t wParam, uint32_t lParam)
{
    int32_t scrollHeight = 0;
    RECT rect;
    int32_t scrollBoundMax;
    int32_t scrollBoundMin;
    int32_t vscroll = 0;
    PLVDATA plvdata;

    plvdata = (PLVDATA) GetWindowAdditionalData2 (hwnd);

    GetClientRect (hwnd, &rect);
    scrollBoundMax = plvdata->nRows * plvdata->nItemHeight -
             (rect.bottom - rect.top - plvdata->nHeadHeight);
    scrollBoundMin = 0;
    
    //decides the desired value to scroll
    if (LOuint16_t (wParam) == SB_LINEUP || LOuint16_t (wParam) == SB_LINEDOWN)
        vscroll = VSCROLL;
    else if (LOuint16_t (wParam) == SB_PAGEUP || LOuint16_t (wParam) == SB_PAGEDOWN)
        vscroll = rect.bottom - rect.top - plvdata->nHeadHeight -
          plvdata->nItemHeight;

    //scroll down
    if ( (LOuint16_t (wParam) == SB_LINEDOWN || LOuint16_t (wParam) == SB_PAGEDOWN) &&
                    plvdata->nOriginalY < scrollBoundMax )
    {
    if ((plvdata->nOriginalY + vscroll) > scrollBoundMax)
    {
        scrollHeight = plvdata->nOriginalY - scrollBoundMax;
        plvdata->nOriginalY = scrollBoundMax;
    }
    else
    {
        plvdata->nOriginalY += vscroll;
        scrollHeight = -vscroll;
    }
    }
    //scroll up
    else if ( (LOuint16_t (wParam) == SB_LINEUP || LOuint16_t (wParam) == SB_PAGEUP) &&
                plvdata->nOriginalY > scrollBoundMin )
    {
    if ((plvdata->nOriginalY - vscroll) > scrollBoundMin)
    {
        plvdata->nOriginalY -= vscroll;
        scrollHeight = vscroll;
    }
    else
    {
        scrollHeight = plvdata->nOriginalY - scrollBoundMin;
        plvdata->nOriginalY = scrollBoundMin;
    }
    }
    //dragging
    else if ( LOuint16_t (wParam) == SB_THUMBTRACK )
    {
        int32_t scrollNewPos = (int32_t) HIuint16_t (wParam);
        
        if (((scrollNewPos - plvdata->nOriginalY) < 5) &&
          ((scrollNewPos - plvdata->nOriginalY) > -5) &&
          (scrollNewPos > 5) && ((scrollBoundMax - scrollNewPos) > 5))
            return;

        if ((scrollNewPos < plvdata->nOriginalY) && (scrollNewPos <= VSCROLL))
        {
            scrollHeight = plvdata->nOriginalY - 0;
            plvdata->nOriginalY = 0;
        }
        else
        {
            if ((scrollNewPos > plvdata->nOriginalY) && ((scrollBoundMax - scrollNewPos) < VSCROLL))
            {
                scrollHeight = plvdata->nOriginalY - scrollBoundMax;
                plvdata->nOriginalY = scrollBoundMax;
            }
            else
            {
                scrollHeight = plvdata->nOriginalY - scrollNewPos;
                plvdata->nOriginalY = scrollNewPos;
            }
        }
    }

    if (scrollHeight != 0) {
        //InvalidateRect (hwnd, NULL, FALSE);
        Invalidate_ItemRect (hwnd);
        lstSetVScrollInfo (plvdata);
    }
}


static void lvHScroll (WINDOW *hwnd, uint32_t wParam, uint32_t lParam)
{
    int32_t scrollWidth = 0;
    int32_t scrollBoundMax;
    int32_t scrollBoundMin;
    RECT rect;
    int32_t hscroll = 0;
    PLVDATA plvdata;

    plvdata = (PLVDATA) GetWindowAdditionalData2 (hwnd);

    GetClientRect (hwnd, &rect);
    scrollBoundMax = sGetItemWidth (plvdata) - (rect.right - rect.left);
    scrollBoundMin = 0;

    //decides the desired value to scroll
    if (LOuint16_t (wParam) == SB_LINERIGHT || LOuint16_t (wParam) == SB_LINELEFT)
        hscroll = HSCROLL;
    else if (LOuint16_t (wParam) == SB_PAGERIGHT || LOuint16_t (wParam) == SB_PAGELEFT)
        hscroll = rect.right - rect.left;

    //scroll right 
    if ( (LOuint16_t (wParam) == SB_LINERIGHT || LOuint16_t (wParam) == SB_PAGERIGHT) && 
            plvdata->nOriginalX < scrollBoundMax )
    {
        if ((plvdata->nOriginalX + hscroll) > scrollBoundMax)
        {
            scrollWidth = plvdata->nOriginalX - scrollBoundMax;
            plvdata->nOriginalX = scrollBoundMax;
        }
        else
        {
            plvdata->nOriginalX += hscroll;
            scrollWidth = -hscroll;
        }
    }
    //scroll left 
    else if ( (LOuint16_t (wParam) == SB_LINELEFT || LOuint16_t (wParam) == SB_PAGELEFT) && 
            plvdata->nOriginalX > scrollBoundMin)
    {
        if ((plvdata->nOriginalX - hscroll) > scrollBoundMin)
        {
            plvdata->nOriginalX -= hscroll;
            scrollWidth = hscroll;
        }
        else
        {
            scrollWidth = plvdata->nOriginalX - scrollBoundMin;
            plvdata->nOriginalX = scrollBoundMin;
        }
    }
    //draging
    else if (LOuint16_t (wParam) == SB_THUMBTRACK)
    {
        int32_t scrollNewPos = (int32_t) HIuint16_t (wParam);
        
        if (((scrollNewPos - plvdata->nOriginalX) < HSCROLL) &&
            ((scrollNewPos - plvdata->nOriginalX) > -HSCROLL) && (scrollNewPos > HSCROLL)
            && ((scrollBoundMax - scrollNewPos) > HSCROLL))
            return;
        
        if ((scrollNewPos < plvdata->nOriginalX) && (scrollNewPos <= HSCROLL))
        {
            scrollWidth = plvdata->nOriginalX - 0;
            plvdata->nOriginalX = 0;
        }
        else
        {
            if ((scrollNewPos > plvdata->nOriginalX) && ((scrollBoundMax - scrollNewPos) < HSCROLL))
            {
                scrollWidth = plvdata->nOriginalX - scrollBoundMax;
                plvdata->nOriginalX = scrollBoundMax;
            }
            else
            {
                scrollWidth = plvdata->nOriginalX - scrollNewPos;
                plvdata->nOriginalX = scrollNewPos;
            }
        }
    }

    if (scrollWidth != 0) {
        InvalidateRect (hwnd, NULL, FALSE);
        lstSetHScrollInfo (plvdata);
    }
}

/*************************  header operations ********************************/
static PLSTHDR GetColsListPoint (PLVDATA plvdata, int32_t nCols)
{
    int32_t i;
    PLSTHDR p1 = plvdata->pLstHead;

    if ((nCols >  plvdata->nCols) || (nCols < 1 ))
        return NULL;
//从pLstHed向后找到nCols的链表指针
    for (i = 1; i < nCols; i++) {
    p1 = p1->pNext;
    }

    return p1;
}

/* creates a new header */
static PLSTHDR 
CreatNewHdr (PLVCOLUMN pcol, PLVDATA plvdata, int32_t *col)
{
    PLSTHDR p1;
    int32_t nCols = pcol->nCols;

    if (!LV_BE_VALID_COL(nCols)) {
       nCols = plvdata->nCols + 1;
       *col = nCols;
    }

    p1 = (PLSTHDR) Malloc (sizeof (LSTHDR));
    p1->sort = NOTSORTED;
    p1->pfnCmp = pcol->pfnCompare;
    p1->Image = pcol->image;
    p1->flags = pcol->colFlags;

    if (pcol->pszHeadText != NULL)
    {
        p1->pTitle = (int8_t *) Malloc (strlen (pcol->pszHeadText) + 1);
        strcpy (p1->pTitle, pcol->pszHeadText);
    }
    else
        p1->pTitle = NULL;
    
    p1->x = sGetFrontSubItemsWidth (nCols - 1, plvdata);
    if (pcol->width <= 0)
    p1->width = LV_CO_DEF;
    else if (pcol->width < COLUME_MIN_WIDTH)
    p1->width = COLUME_MIN_WIDTH;
    else
        p1->width = pcol->width;

    if (nCols == 1)
    {
        p1->pNext = plvdata->pLstHead;
        plvdata->pLstHead = p1;
    }
    else
    {//P1插入链表中
    PLSTHDR p2 = GetColsListPoint(plvdata, nCols-1);
        p1->pNext = p2->pNext;
        p2->pNext = p1;
    }

    return p1;
}

//free a header
static void FreePointFromList (PLSTHDR pLstHdr)
{
    if (pLstHdr != NULL)
    {
        if (pLstHdr->pTitle != NULL)
            Free (pLstHdr->pTitle);
        Free (pLstHdr);
    }
}

//deletes a header
static void DelHdrInList (int32_t nCols, PLVDATA plvdata)
{
    PLSTHDR p1 = plvdata->pLstHead;
    PLSTHDR pdel;

    if (nCols == 1)
    {
    pdel = p1;
        plvdata->pLstHead = p1->pNext;
    }
    else
    {
    p1 = GetColsListPoint(plvdata, nCols-1);
    if (!p1)
        return;
    pdel = p1->pNext;
    if (pdel) {
            p1->pNext = pdel->pNext;
        FreePointFromList(pdel);
    }
    }
}

/* ========================================================================= 
 * Model & Data section of the listview control.
   ========================================================================= */

static PITEMDATA GetItemByRow (PLVDATA plvdata, int32_t nRows)
{
    int32_t i;
    PITEMDATA pItem = plvdata->pItemHead;

    if ((nRows >  plvdata->nRows) || (nRows < 1 ))
        return NULL;

    for (i = 1; i < nRows; i++) {
    pItem = pItem->pNext;
    }

    return pItem;
}

static PSUBITEMDATA lvGetSubItemByCol (PITEMDATA pItem, int32_t nCols)
{
    int32_t i;
    PSUBITEMDATA pSubItem;

    if (!pItem)
    return NULL;
    pSubItem = pItem->pSubItemHead;

    for (i = 1; i < nCols; i++) {
    pSubItem = pSubItem->pNext;
    }

    return pSubItem;
}

#define lstSelectItem(hwnd, nRows, plvdata)    \
             lvSetItemSelect (hwnd, nRows, plvdata, TRUE)
#define lstUnSelectItem(hwnd, nRows, plvdata)    \
             lvSetItemSelect (hwnd, nRows, plvdata, FALSE)

/* select and make the original one unselected */
#define SelectItem(hwnd, nRows, plvdata)     \
         lstUnSelectItem(hwnd, plvdata->nItemSelected, plvdata);   \
         lstSelectItem(hwnd, nRows, plvdata);

/* Sets the item to be selcted or unselected status */
static int32_t
lvSetItemSelect (WINDOW *hwnd, int32_t nRows, PLVDATA plvdata, bool32_t bSel)
{
    PITEMDATA pSel;
    RECT rect;

    if ( !(pSel = GetItemByRow (plvdata, nRows)) )
    return LV_ERR;

    if (bSel == pSel->bSelected)
    return LV_ERR;
    
    if (bSel) {
        plvdata->nItemSelected = nRows;
    }
    else {
        plvdata->nItemSelected = 0;
    }
    pSel->bSelected = bSel;

    LV_GET_ITEM_RECT(nRows, rect);
    InvalidateRect (hwnd, &rect, FALSE);

    return LV_OKAY;
}

#if 0

#define lvSelectNextItem(hwnd, plvdata)    \
              lvStepItem(hwnd, plvdata, 1)
#define lvSelectPrevItem(hwnd, plvdata)    \
              lvStepItem(hwnd, plvdata, -1)
static void
lvStepItem(WINDOW *hwnd, PLVDATA plvdata, int32_t step)
{
    int32_t prev;
    prev = plvdata->nItemSelected;
    if (prev+step <= plvdata->nRows && prev+step > 0) {
        lstUnSelectItem (hwnd, prev, plvdata);
        lstSelectItem (hwnd, prev+step, plvdata);
    }
}

#endif

/****************************  data init and destroy  ************************/
static void InitListViewData (WINDOW *hwnd)
{
    PLVDATA plvdata = (PLVDATA) GetWindowAdditionalData2 (hwnd);

    plvdata->nCols = 0;
    plvdata->nRows = 0;
    plvdata->hWnd = hwnd;
    plvdata->pLstHead = NULL;
    plvdata->pItemHead = NULL;
    plvdata->nOriginalX = 0;
    plvdata->nOriginalY = 0;
    LVSTATUS(hwnd) = LVST_NORMAL;

    plvdata->pHdrClicked = NULL;
    plvdata->nItemDraged = 0;
    plvdata->nItemSelected = 0;

    plvdata->bkc_selected = LIGHTBLUE;

    plvdata->nItemHeight = LV_ITEMH_DEF(hwnd);
    plvdata->nHeadHeight = LV_HDRH_DEF(hwnd);

    plvdata->str_cmp = strncmp;
}

//Destroies all the internal datas
static void lvDataDestory (PLVDATA plvdata)
{
    PITEMDATA p1, p2;

    p1 = plvdata->pItemHead;
    //FIXME
    FreePointFromList (plvdata->pLstHead);

    while (p1 != NULL)
    {
        p2 = p1;
        p1 = p1->pNext;
        itemDelete (p2);
    }
    Free (plvdata);
}

/******************** subitem operations ****************************/

static PSUBITEMDATA subitemNew (const int8_t *pszInfoText)
{
    PSUBITEMDATA p1;
    
    p1 = (PSUBITEMDATA) Malloc (sizeof (SUBITEMDATA));
    p1->pNext = NULL;
    
    if (pszInfoText != NULL)
    {
        p1->pszInfo = (int8_t *) Malloc (strlen (pszInfoText) + 1);
        strcpy (p1->pszInfo, pszInfoText);
    }
    else
        p1->pszInfo = NULL;
    p1->nTextColor = RGB (0, 0, 0);
    p1->Image = 0;
    p1->dwFlags = 0;

    return p1;
}

static void subitemFree (PSUBITEMDATA pSubItemData)
{
    if (pSubItemData != NULL)
    {
        if (pSubItemData->pszInfo != NULL)
            Free (pSubItemData->pszInfo);
        Free (pSubItemData);
    }
}

static void
lvAddSubitem (PLVDATA plvdata, int32_t nCols)
{
    int32_t i;
    PSUBITEMDATA p2, p3;
    PITEMDATA p1 = plvdata->pItemHead;

    while (p1)
    {
            p2 = subitemNew (NULL);
            if (nCols == 1)
            {
                p2->pNext = p1->pSubItemHead;
                p1->pSubItemHead = p2;
            }
            else
            {
                p3 = p1->pSubItemHead;
		        for (i = 1; i < nCols-1; i++) {
		                    p3 = p3->pNext;
		        }
		        p2->pNext = p3->pNext;
		        p3->pNext = p2;
            }
            p1 = p1->pNext;//换一列加子条目
    }
}

static void
lvDelSubitem (PLVDATA plvdata, int32_t nCols)
{
    int32_t i;
    PSUBITEMDATA p2, pdel;
    PITEMDATA p1 = plvdata->pItemHead;

    while (p1)
    {
            if (nCols == 1)
            {
                p1->pSubItemHead = p1->pSubItemHead->pNext;
            }
            else
            {
                p2 = p1->pSubItemHead;
        for (i = 1; i < nCols-1; i++) {
                    p2 = p2->pNext;
        }
        pdel = p2->pNext;
        p2->pNext = pdel->pNext;
        subitemFree(pdel);
            }
            p1 = p1->pNext;
    }
}

static bool32_t 
AddColumnToList (PLVCOLUMN pcol, PLVDATA plvdata)
{
    PLSTHDR p1 = NULL;
    int32_t nCols = pcol->nCols;

    p1 = CreatNewHdr (pcol, plvdata, &nCols);
    if (!p1)
    return FALSE;

    lvAddSubitem (plvdata, nCols);
    sAddOffsetToTailSubItem (nCols, p1->width, plvdata);
    plvdata->nCols ++;

    return TRUE;
}

static int32_t sGetItemSeq (PLVDATA plvdata)
{
    PITEMDATA p1;
    int32_t i = 0;

    p1 = plvdata->pItemHead;
    while (p1)
    {
        i++;

        if (p1->bSelected)
            return i;

        p1 = p1->pNext;
    }

    return -1;
}

//FIXME
static int32_t sModifyHeadText (int32_t nCols, const int8_t *pszHeadText, PLVDATA plvdata)
{
    PLSTHDR p1 = NULL;

    p1 = GetColsListPoint(plvdata, nCols);
    if (!p1 || !pszHeadText)
        return -1;
  
    if (p1->pTitle != NULL)
        Free (p1->pTitle);
  
    p1->pTitle = (int8_t *) Malloc (sizeof (pszHeadText) + 1);
    strcpy (p1->pTitle, pszHeadText);
  
    return 0;
}

inline static PSUBITEMDATA
lvGetSubItem (int32_t nItem, int32_t nSubItem, PLVDATA plvdata)
{
    PITEMDATA pItem = GetItemByRow(plvdata, nItem);
    return lvGetSubItemByCol(pItem, nSubItem);
}

/* Sets the properties of the subitem */

/* Fills the content of a subitem */
static int32_t 
lvFillSubItem (int32_t nItem, int32_t subItem, const int8_t *pszText, PLVDATA plvdata)
{
    PSUBITEMDATA p1;

    if ( !(p1 = lvGetSubItem (nItem, subItem, plvdata)) )
        return -1;

    if (pszText == NULL)
        return -1;

    if (p1->pszInfo != NULL)
        Free (p1->pszInfo);
    p1->pszInfo = (int8_t *) Malloc (strlen (pszText) + 1);
    strcpy (p1->pszInfo, pszText);

    return 0;
}

/* Sets the text color of the subitem */
static int32_t 
lvSetSubItemTextColor (int32_t nItem, int32_t subItem, int32_t color, PLVDATA plvdata)
{
    PSUBITEMDATA p1;

    if ( !(p1 = lvGetSubItem (nItem, subItem, plvdata)) )
        return -1;

    p1->nTextColor = color;

    return 0;
}

static int32_t 
lvSetSubItem (PLVSUBITEM pinfo, PLVDATA plvdata)
{
    PSUBITEMDATA p1;

    if ( !(p1 = lvGetSubItem (pinfo->nItem, pinfo->subItem, plvdata)) )
        return -1;

    if (p1->pszInfo != NULL)
        Free (p1->pszInfo);
    p1->pszInfo = (int8_t *) Malloc (strlen (pinfo->pszText) + 1);
    strcpy (p1->pszInfo, pinfo->pszText);

    p1->nTextColor = pinfo->nTextColor;
    if (pinfo->flags & LVFLAG_BITMAP)
        p1->dwFlags |= LVIF_BITMAP;
    if (pinfo->flags & LVFLAG_ICON)
        p1->dwFlags |= LVIF_ICON;
    p1->Image = pinfo->image;

    return 0;
}

static int32_t 
lvRemoveColumn (int32_t nCols, PLVDATA plvdata)
{
    int32_t offset;

    if (!LV_BE_VALID_COL(nCols))
    return -1;

    offset = -(sGetSubItemWidth (nCols, plvdata));
    sAddOffsetToTailSubItem (nCols + 1, offset, plvdata);

    DelHdrInList (nCols, plvdata);
    lvDelSubitem (plvdata, nCols);

    plvdata->nCols --;

    lstSetHScrollInfo (plvdata);
    //InvalidateRect (plvdata->hWnd, NULL, FALSE);
    Invalidate_ItemRect (plvdata->hWnd);

    return 0;
}

static PITEMDATA itemNew (int32_t nCols)
{
    PSUBITEMDATA pHead = NULL;
    PSUBITEMDATA p1 = NULL;
    PSUBITEMDATA p2 = NULL;
    PITEMDATA p3 = NULL;
    int32_t i;
    int32_t j;

    j = nCols;

    if (j >= 1)
    {
        pHead = subitemNew (NULL);
        p1 = pHead;
    }
    else
        return NULL;

    for (i = 1; i <= j - 1; i++)
    {
        p2 = subitemNew (NULL);
        p1->pNext = p2;
        p1 = p2;
    }
    p3 = (PITEMDATA) Malloc (sizeof (ITEMDATA));
    p3->pNext = NULL;
    p3->bSelected = FALSE;

    p3->pSubItemHead = pHead;

    return p3;
}

static int32_t itemDelete (PITEMDATA pItem)
{
    PSUBITEMDATA p1 = NULL;
    PSUBITEMDATA p2 = NULL;

    p1 = pItem->pSubItemHead;

    while (p1 != NULL)
    {
        p2 = p1;
        p1 = p1->pNext;
        subitemFree (p2);
    }
    pItem->pSubItemHead = NULL;
    Free (pItem);

    return 0;
}

static int32_t sAddItemToList (int32_t nItem, PITEMDATA pnew, PLVDATA plvdata)
{
    int32_t i;
    PITEMDATA p1 = pnew;
    PITEMDATA p2 = NULL;
    PITEMDATA p3 = NULL;

    if (plvdata->nRows > LV_ROW_MAX)
        return -1;

    if ((nItem < 1) || (nItem > plvdata->nRows))
        nItem = plvdata->nRows + 1;

    if (p1 == NULL)
        return -1;

    if (nItem == 1)
    {
        p2 = plvdata->pItemHead;
        plvdata->pItemHead = p1;
        p1->pNext = p2;
    }
    else
    {
        i = nItem;
        p2 = plvdata->pItemHead;
        while (i != 2)
        {
            i = i - 1;
            p2 = p2->pNext;
        }
        p3 = p2->pNext;
        p2->pNext = p1;
        p1->pNext = p3;
    }

    plvdata->nRows ++;

    if (nItem <= plvdata->nItemSelected)
        plvdata->nItemSelected++;

    return nItem;
}

static int32_t sRemoveItemFromList (int32_t nItem, PLVDATA plvdata)
{
    PITEMDATA p1 = NULL;
    PITEMDATA pp1 = NULL;

    if ((nItem < 1) || (nItem > plvdata->nRows) || (plvdata->nRows < 1))
        return -1;

    if (nItem == 1)
    {
        p1 = plvdata->pItemHead;
        plvdata->pItemHead = plvdata->pItemHead->pNext;
    }
    else
    {
    pp1 = GetItemByRow (plvdata, nItem-1);
        p1 = pp1->pNext;
        pp1->pNext = p1->pNext;
    }

    if (p1->bSelected)
    plvdata->nItemSelected = 0;

    itemDelete (p1);

    plvdata->nRows --;

    /*
    if () {
        plvdata->nOriginalY -= plvdata->nItemHeight;
    }
    */

    lstSetVScrollInfo (plvdata);
    //InvalidateRect (plvdata->hWnd, NULL, FALSE);
    Invalidate_ItemRect (plvdata->hWnd);

    return 0;
}

static int32_t sRemoveAllItem (PLVDATA plvdata)
{
    PITEMDATA p1 = NULL;
    PITEMDATA p2 = NULL;

    p1 = plvdata->pItemHead;

    while (p1 != NULL)
    {
        p2 = p1;
        p1 = p1->pNext;
        itemDelete (p2);
    }

    plvdata->nRows = 0;
    plvdata->pItemHead = NULL;
    //plvdata->nOriginalX = 0;

    lstSetVScrollInfo (plvdata);
    //lstSetHScrollInfo (plvdata);
    //InvalidateRect (plvdata->hWnd, NULL, FALSE);
    Invalidate_ItemRect (plvdata->hWnd);

    return 0;
}

static int32_t lvFindItem (PLVFINDINFO pFindInfo, PLVDATA plvdata)
{
    PITEMDATA p1;
    PSUBITEMDATA p2;
    int32_t i = 0, j = 0;

    if (pFindInfo == NULL)
        return -1;

    if ( !(p1 = GetItemByRow (plvdata, pFindInfo->iStart)) )
        p1 = plvdata->pItemHead;

    while (p1 != NULL)
    { 
        p2 = p1->pSubItemHead;
        i = pFindInfo->nCols;

    if (pFindInfo->flags & LVFF_ADDDATA) {
        if (pFindInfo->addData != p1->addData)
        continue;
    }

    if (pFindInfo->flags & LVFF_TEXT) {
            while ((p2 != NULL) && (i > 0))
            {
                if (plvdata->str_cmp (p2->pszInfo, pFindInfo->pszInfo[i - 1], (size_t)-1) != 0)
                    break;

                i--;
                p2 = p2->pNext;
            }
    }
        
        j++;
        p1 = p1->pNext;

        if (i == 0)
            return j;
    }

    return -1;
}

static int32_t sGetSubItemWidth (int32_t nCols, PLVDATA plvdata)
{
    PLSTHDR p;
    int32_t nPosition;

    nPosition = nCols;

    if ((nCols < 1) || (nCols > plvdata->nCols))
    {
        return -1;
    }

    p = plvdata->pLstHead;

    while (nPosition != 1)
    {
        nPosition--;
        p = p->pNext;
    }
    
    return p->width;
}

static int32_t sGetItemWidth (PLVDATA plvdata)
{
    PLSTHDR p;
    int32_t width;

    p = plvdata->pLstHead;
    width = 0;

    while (p != NULL)
    {
        width += p->width;
        p = p->pNext;
    }
    
    return width;
}

/*
 * gets the previous nClos items width
 */
inline static int32_t 
sGetFrontSubItemsWidth (int32_t nCols, PLVDATA plvdata)
{
    PLSTHDR p1 = GetColsListPoint(plvdata, nCols);
    if (p1)
        return p1->x + p1->width;
    return -1;
}

/* be care, doesn't check p and offset */
inline static int32_t 
sAddOffsetToSubItem (PLSTHDR p, int32_t offset)
{
    p->width += offset;
    return 0;
}

/* offset the tail subitems from nCols */
static int32_t sAddOffsetToTailSubItem (int32_t nCols, int32_t offset, PLVDATA plvdata)
{
    PLSTHDR p;

    p = GetColsListPoint (plvdata, nCols);
    if (!p) return -1;

    while (p)
    {
        p->x += offset;
        p = p->pNext;
    }

    return 0;
}

static void SetColumnWidth (int32_t nCols, int32_t width, PLVDATA plvdata)
{
    int32_t offset;
    PLSTHDR ph = GetColsListPoint (plvdata, nCols);

    if (!ph) return;

    if (width < COLUME_MIN_WIDTH)
    width = COLUME_MIN_WIDTH;

    offset = width - ph->width;
    sAddOffsetToSubItem(ph, offset);
    sAddOffsetToTailSubItem (nCols+1, offset, plvdata);
}

/* The default comparision function for compare two items */
static int32_t 
lvDefCompare (int32_t row1, int32_t row2, int32_t ncol, PLVDATA plvdata)
{
    PITEMDATA p1, p2;
    PSUBITEMDATA psub1, psub2;

    p1 = GetItemByRow (plvdata, row1);
    p2 = GetItemByRow (plvdata, row2);
    psub1 = lvGetSubItemByCol (p1, ncol);
    psub2 = lvGetSubItemByCol (p2, ncol);

    if (plvdata->str_cmp)
        return plvdata->str_cmp (psub1->pszInfo, psub2->pszInfo, (size_t)-1);
    else
        return StrCaseCmp (psub1->pszInfo, psub2->pszInfo);
}

static void lvMoveItem (int32_t old_row, int32_t new_row, PLVDATA plvdata)
{
    PITEMDATA p1, p2, pp1, pp2;

    if (old_row == new_row)
    return;

    p1 = GetItemByRow (plvdata, old_row);
    p2 = GetItemByRow (plvdata, new_row);

    if (!p1 || !p2)
    return;

    pp1 = GetItemByRow (plvdata, old_row-1);
    pp2 = GetItemByRow (plvdata, new_row-1);

    if (pp1)
    pp1->pNext = p1->pNext;
    else
    plvdata->pItemHead = p1->pNext;
    
    if (pp2)
        pp2->pNext = p1;
    else
    plvdata->pItemHead = p1;
    p1->pNext = p2;
}

/* sorting items using a comparision function */
static int32_t 
lvSortItem (PFNLVCOMPARE pfn_user, int32_t nCols, SORTTYPE sort, PLVDATA plvdata)
{
    int32_t i, j;
    PLSTHDR ph;
    PFNLVCOMPARE pcmp;
    LVSORTDATA sortData;

    /* If pfn_user is not NULL, use it as comparision function; otherwise, 
     * use the one associated with nCols column.
     */ 
    if (pfn_user)
    pcmp = pfn_user;
    else {
        if ( !(ph = GetColsListPoint (plvdata, nCols)) )
        return -1;
        pcmp = ph->pfnCmp;
    }

    //FIXME, sortData howto use?
    sortData.ncol = nCols;

    //FIXME, more efficient algorithm
    for (i = 1; i <= plvdata->nRows; i++)
    {
    for (j = 1; j < i; j++) {
        int32_t ret;

        if (pcmp) {
        ret = pcmp (i, j, &sortData);
            if (ret == 0)
            ret = lvDefCompare (i, j, 1, plvdata);
        }
        else {
                ret = lvDefCompare (i, j, nCols, plvdata);
        }

            if ( (sort == LO_SORTED && ret < 0) || (sort == HI_SORTED && ret > 0))
        break;
    }
    lvMoveItem (i, j, plvdata);
    }

    return 0;
}

/***************************  scroll info  ***********************************/
static int32_t lstSetVScrollInfo (PLVDATA plvdata)
{
    RECT rect;
    int32_t  nMax, nMin, nPos;
    
    GetClientRect (plvdata->hWnd, &rect);
    
    if ((rect.bottom - rect.top - plvdata->nHeadHeight) > ((plvdata->nRows) * plvdata->nItemHeight))
    {
        SetScrollRange (plvdata->hWnd, SB_VERT, 0, 0, TRUE);
        SetScrollPos (plvdata->hWnd, SB_VERT, 0);
        EnableScrollBar (plvdata->hWnd, SB_VERT, FALSE);
        
        return 0;
    }
    
    if (plvdata->nOriginalY < 0) {
        plvdata->nOriginalY = 0;
    }
    
    nMin = 0;
    nMax = plvdata->nRows * plvdata->nItemHeight - (rect.bottom - rect.top - plvdata->nHeadHeight);
    nPos = plvdata->nOriginalY;
    
    SetScrollRange (plvdata->hWnd, SB_VERT, nMin, nMax, TRUE);
    SetScrollPos (plvdata->hWnd, SB_VERT, nPos);
    EnableScrollBar (plvdata->hWnd, SB_VERT, TRUE);
    
    return 0;
}

static int32_t lstSetHScrollInfo (PLVDATA plvdata)
{
    RECT rect;
    int32_t  nMax, nMin, nPos;

    GetClientRect (plvdata->hWnd, &rect);
    
    if (!LV_H_OUTWND(plvdata, rect))
    {
        SetScrollRange (plvdata->hWnd, SB_HORZ, 0, 0, TRUE);
        SetScrollPos (plvdata->hWnd, SB_HORZ, 0);
        EnableScrollBar (plvdata->hWnd, SB_HORZ, FALSE);
        
        return 0;
    }
    
    nMin = 0;
    nMax = sGetItemWidth (plvdata) - (rect.right - rect.left);
    nPos = plvdata->nOriginalX;
    
    SetScrollRange (plvdata->hWnd, SB_HORZ, nMin, nMax, TRUE);
    SetScrollPos (plvdata->hWnd, SB_HORZ, nPos);
    EnableScrollBar (plvdata->hWnd, SB_HORZ, TRUE);
    
    return 0;
}

/*************************************  Listview drag border action ***********************/

static void lvBorderDrag (WINDOW *hwnd, int32_t x, int32_t y)
{
    int32_t mouseX = x, mouseY = y;
    PLVDATA plvdata;
    RECT rect, rcClient;
    int32_t offset;
    PLSTHDR pDrag;
    POINT pt;

    plvdata = (PLVDATA) GetWindowAdditionalData2 (hwnd);
    pDrag = GetColsListPoint (plvdata, plvdata->nItemDraged);

    GetClientRect (hwnd, &rcClient);
    
    pt.x = mouseX;
    pt.y = mouseY;
    ScreenToClient (hwnd, &pt);
    mouseX = pt.x;
    mouseY = pt.y;

    //the column width should not less than the min value
    if ((pDrag->x - plvdata->nOriginalX + COLUME_MIN_WIDTH) > mouseX - 1)
        return;

    offset = mouseX - (pDrag->x + pDrag->width-plvdata->nOriginalX);
    SetColumnWidth (plvdata->nItemDraged, pDrag->width+offset, plvdata);

    rect.left = rcClient.left;
    rect.right = rcClient.right;
    rect.top = rcClient.top;
    rect.bottom = rect.top + plvdata->nHeadHeight+1;

    InvalidateRect(hwnd, &rect, FALSE);

    if (offset < 0) {
        plvdata->nOriginalX += offset;
        if (plvdata->nOriginalX < 0)
            plvdata->nOriginalX = 0;
    }

    //lstSetHScrollInfo (plvdata);
}

static void lvToggleSortStatus (PLSTHDR p1)
{
    switch (p1->sort)
    {
    case NOTSORTED:
    p1->sort = LO_SORTED;
    break;
    case HI_SORTED:
    p1->sort = LO_SORTED;
    break;
    case LO_SORTED:
    p1->sort = HI_SORTED;
    break;
    }
}

/********************************************** List Report        **********************************************/
static int32_t sListViewProc (WINDOW *hwnd, uint32_t message, uint32_t wParam, uint32_t lParam)
{
    PLVDATA plvdata = NULL;
    uint32_t dwStyle;

    if (message != EV_CREATE)
        plvdata = (PLVDATA) GetWindowAdditionalData2 (hwnd);

    switch (message)
    {
        case EV_CREATE:
        {  
            dwStyle = GetWindowStyle (hwnd);

            if (!(plvdata = (PLVDATA) Malloc (sizeof (LVDATA))))
                return -1;

            SetWindowAdditionalData2 (hwnd, (uint32_t) plvdata);

            InitListViewData (hwnd);

            lstSetHScrollInfo (plvdata);
            lstSetVScrollInfo (plvdata);

            break;
        }
#if 0
        case EV_SETCURSOR:
        {
            int32_t mouseX = LOint16_t (lParam);
            int32_t mouseY = HIint16_t (lParam);
            
            if ((LVSTATUS(hwnd) & LVST_BDDRAG) || 
                (InWhichHeadBorder (mouseX, mouseY, NULL, plvdata) > 0))
            {
                SetCursor (GetSystemCursor (IDC_SPLIT_VERT));

                return 0;
            }
            break;
        }
#endif
        case EV_PAINT:
        {
            DC *hdc; /*, mem_dc*/;

            hdc = BeginPaint (hwnd, wParam, lParam);
            /* mem_dc = CreateCompatibleDC (hdc); */

            /* ListView_Draw (hwnd, mem_dc); */
            ListView_Draw (hwnd, hdc);
            /* BitBlt (mem_dc, 0, 0, 0, 0, hdc, 0, 0, 0); */

            /* DeleteCompatibleDC (mem_dc); */
            EndPaint (hwnd, hdc, wParam, lParam);
            return 0;
        }

        case EV_SETFOCUS:
        case EV_KILLFOCUS:
            if (message == EV_SETFOCUS)
                plvdata->bkc_selected = LIGHTBLUE;
            else
                plvdata->bkc_selected = GRAYBLUE;
            if (plvdata->nItemSelected > 0) {
                RECT rc;
                LV_GET_ITEM_RECT(plvdata->nItemSelected, rc);
                InvalidateRect(hwnd, &rc, FALSE);
            }
            break;
#if 0
        case EV_GETDLGCODE:
            return DLGC_WANTARROWS | DLGC_WANTCHARS;
#endif

        case EV_MOUSEMOVE://鼠标移动
        {
            POINT pt;
            int32_t mouseX = LOint16_t (lParam);
            int32_t mouseY = HIint16_t (lParam);

            //in head clicked status
            if (LVSTATUS(hwnd) & LVST_HEADCLICK) {
                RECT rc;
                PLSTHDR p1;
                
                if (GetCapture() != hwnd)
                    break;
                    
                pt.x = mouseX;
                pt.y = mouseY;
                ScreenToClient (hwnd, &pt);
                mouseX = pt.x;
                mouseY = pt.y;
            
                p1 = plvdata->pHdrClicked;
                LV_GET_HDR_RECT(p1, rc);
                
                SetPoint (&pt, mouseX, mouseY);
                if (PtInRect(&rc, &pt)) {
                    if (!(LVSTATUS(hwnd) & LVST_INHEAD)) {
                        LVSTATUS(hwnd) |= LVST_INHEAD;
                        InvalidateRect (hwnd, &rc, FALSE);
                    }
                }
                else if (LVSTATUS(hwnd) & LVST_INHEAD) {
                    rc.left -= 1;
                    rc.right += 1;
                    LVSTATUS(hwnd) &= ~LVST_INHEAD;
                    InvalidateRect (hwnd, &rc, FALSE);
                }
            }
            //in border dragged status
            else if (LVSTATUS(hwnd) & LVST_BDDRAG)
                lvBorderDrag (hwnd, mouseX, mouseY);

            break;
        }

        case EV_LBUTTONDOWN://左键按下
        {
            int32_t mouseX = LOint16_t (lParam);
            int32_t mouseY = HIint16_t (lParam);
            int32_t nCols, nRows;
            POINT pt;

            RECT rect, rcClient;
            PLSTHDR p1;
            PITEMDATA p2;
            
            pt.x = mouseX;
            pt.y = mouseY;
            ScreenToClient (hwnd, &pt);
            mouseX = pt.x;
            mouseY = pt.y;

            GetClientRect (hwnd, &rcClient);

            nCols = isInListViewHead (mouseX, mouseY, &p1, plvdata);

            if (nCols > 0)  // clicks on the header
            {
                if (GetCapture() == hwnd)
                    break;

                SetCapture (hwnd);
                LVSTATUS(hwnd) |= LVST_HEADCLICK;
                LVSTATUS(hwnd) |= LVST_INHEAD;
                plvdata->pHdrClicked = p1;

                SetRect (&rect, p1->x - plvdata->nOriginalX, LV_HDR_TOP, 
                p1->x - plvdata->nOriginalX+ p1->width, 
                LV_HDR_TOP + LV_HDR_HEIGHT);
                InvalidateRect (hwnd, &rect, FALSE);
            }
            else
            {
                if ((nCols = InWhichHeadBorder (mouseX, mouseY, &p1, plvdata)) > 0)
                {
                    LVSTATUS(hwnd) |= LVST_BDDRAG;
                    plvdata->nItemDraged = nCols;
                    SetCapture (hwnd);
                }
                else if ((nRows = isInListViewItem (mouseX, mouseY, &p2, plvdata)) > 0)
                {
                    SelectItem (hwnd, nRows, plvdata);
                    MakeItemVisible (hwnd, plvdata, nRows);
                    NotifyParent (hwnd, GetDlgCtrlID(hwnd), LVN_SELCHANGE);
                }
            }
            
            break;
        }
        
        case EV_LBUTTONUP://左键弹起
        {
            PLSTHDR p1;
            int32_t nCols;
            int32_t mouseX = LOint16_t (lParam);
            int32_t mouseY = HIint16_t (lParam);
            POINT pt;

            if (LVSTATUS(hwnd) & LVST_HEADCLICK)
            {
                if (GetCapture() != hwnd)
                    break;
                ReleaseCapture ();

                LVSTATUS(hwnd) &= ~LVST_HEADCLICK;
                LVSTATUS(hwnd) &= ~LVST_INHEAD;
                
                pt.x = mouseX;
                pt.y = mouseY;
                ScreenToClient (hwnd, &pt);
                mouseX = pt.x;
                mouseY = pt.y;

                nCols = isInListViewHead (mouseX, mouseY, NULL, plvdata);
                p1 = GetColsListPoint (plvdata, nCols);
                if (p1 != plvdata->pHdrClicked)
                    break;

                lvToggleSortStatus(p1);
                lvSortItem (NULL, nCols, p1->sort, plvdata);
                plvdata->nItemSelected = sGetItemSeq (plvdata);

                InvalidateRect (hwnd, NULL, FALSE);
            }
            else if (LVSTATUS(hwnd) & LVST_BDDRAG)
            {
                ReleaseCapture ();
                lstSetHScrollInfo (plvdata);
                LVSTATUS(hwnd) &= ~LVST_BDDRAG;
                InvalidateRect (hwnd, NULL, FALSE);
            }
            
            break;
        }

        /* listview defined messages */
        case LVM_COL_SORT://短列
        {
            int32_t nCols = (int32_t)wParam;

            if (lvSortItem (NULL, nCols, LO_SORTED, plvdata) == 0) {
                plvdata->nItemSelected = sGetItemSeq (plvdata);
                InvalidateRect (hwnd, NULL, FALSE);
                return LV_OKAY;
            }
            return LV_ERR;
        }

        case LVM_SORTITEMS://短条目
        {
            //PLVSORTDATA sortData = (PLVSORTDATA)wParam;
            PFNLVCOMPARE pfn = (PFNLVCOMPARE)lParam;
        
            if (lvSortItem (pfn, 0, LO_SORTED, plvdata) == 0) {
                plvdata->nItemSelected = sGetItemSeq (plvdata);
                InvalidateRect (hwnd, NULL, FALSE);
                return LV_OKAY;
            }
            return LV_ERR;
        }

        case LVM_SETITEMHEIGHT://设置条目高度
        {
            int32_t height = (int32_t)wParam; 
        
            plvdata->nItemHeight = height;
            if (height < LV_ITEMH_DEF(hwnd))
                plvdata->nItemHeight = LV_ITEMH_DEF(hwnd);
        
            lstSetVScrollInfo (plvdata);
            InvalidateRect (hwnd, NULL, FALSE);
        
            return TRUE;
        }

        case LVM_SETHEADHEIGHT://设置标头高度
        {
            int32_t height = (int32_t)wParam; 
        
            plvdata->nHeadHeight = height;
            if (height < LV_HDRH_DEF(hwnd))
                plvdata->nHeadHeight = LV_HDRH_DEF(hwnd);
        
            lstSetVScrollInfo (plvdata);
            InvalidateRect (hwnd, NULL, FALSE);
        
            return TRUE;
        }
    
        case LVM_ADDITEM://增加条目
        {
            PLVITEM p1;
            PITEMDATA pnew;
            int32_t index;

            p1 = (PLVITEM) lParam;

            pnew = itemNew (plvdata->nCols);//有几列nCols就new几个item条目（一行）
            pnew->addData = p1->itemData;

            index = sAddItemToList (p1->nItem, pnew, plvdata); 

            lstSetVScrollInfo (plvdata);
            //InvalidateRect (hwnd, NULL, FALSE);
            Invalidate_ItemRect (hwnd);
            return index;
        }
        
        case LVM_SETSUBITEMCOLOR://设置子条目颜色
        {
            PLVSUBITEM p1;
            RECT rect;
            
            p1 = (PLVSUBITEM) lParam;

            lvSetSubItemTextColor(p1->nItem, p1->subItem, p1->nTextColor, plvdata); 

            LV_GET_SUBITEM_RECT(p1->nItem, p1->subItem, rect);
            InvalidateRect (hwnd, &rect, FALSE);
            return 0;
        }
        
        case LVM_SETSUBITEMTEXT://设置子条目文字
        {
            PLVSUBITEM p1;
            RECT rect;
            
            p1 = (PLVSUBITEM) lParam;
            
            if (lvFillSubItem(p1->nItem, p1->subItem, p1->pszText, plvdata) < 0)
                return LV_ERR;

            LV_GET_SUBITEM_RECT(p1->nItem, p1->subItem, rect);
            InvalidateRect (hwnd, &rect, FALSE);
            return LV_OKAY;
        }

        case LVM_FILLSUBITEM://填充子条目
        case LVM_SETSUBITEM://设置子条目
        {
            PLVSUBITEM p1;
            RECT rect;
            
            p1 = (PLVSUBITEM) lParam;
            if ( (lvSetSubItem(p1, plvdata)) < 0 )
                return LV_ERR;

            LV_GET_SUBITEM_RECT(p1->nItem, p1->subItem, rect);
            InvalidateRect (hwnd, &rect, FALSE);
            return LV_OKAY;
        }

        case LVM_GETITEM://攻取子条目
        {
            PLVITEM p1;
            PITEMDATA pitem;
                
            p1 = (PLVITEM) lParam;
            pitem = GetItemByRow (plvdata, p1->nItem);
            if (!pitem)
                return LV_ERR;
        
            p1->itemData = pitem->addData;
            return LV_OKAY;
        }
        
        case LVM_GETITEMCOUNT://获取条目数量
        {
            return plvdata->nRows;
        }
        
        case LVM_GETSELECTEDITEM://取得选中的条目
        {
            return plvdata->nItemSelected;
        }
        
        case LVM_GETCOLUMN://取得列
        {
            int32_t col = (int32_t)wParam;
            PLVCOLUMN pcol = (PLVCOLUMN)lParam;
            PLSTHDR ph = GetColsListPoint(plvdata, col);
            if (!ph)
                return -1;
            pcol->width = ph->width;
            strncpy (pcol->pszHeadText, ph->pTitle, pcol->nTextMax);
            pcol->pszHeadText[pcol->nTextMax] = 0;
            pcol->image = ph->Image;
            pcol->pfnCompare = ph->pfnCmp;
            pcol->colFlags = ph->flags;
        }
        
        case LVM_GETCOLUMNWIDTH://取得列宽
        {
            int32_t col = (int32_t)wParam;
            PLSTHDR ph = GetColsListPoint(plvdata, col);
            if (ph)
                return ph->width;
            else
                return -1;
        }
        
        case LVM_GETCOLUMNCOUNT://取得列的数量
        {
            return plvdata->nCols;
        }
        
        case LVM_GETSUBITEMLEN://取得子条目长度
        {
            PLVSUBITEM p1;
            PSUBITEMDATA p2;
        
            p1 = (PLVSUBITEM) lParam;
            p2 = lvGetSubItem (p1->nItem, p1->subItem, plvdata);
        
            if (p2 == NULL || p2->pszInfo == NULL)
                return LV_ERR;
        
            return strlen (p2->pszInfo);
        }
        
        case LVM_GETSUBITEMTEXT://取得子条目文本
        {
            PLVSUBITEM p1;
            PSUBITEMDATA p2;
        
            p1 = (PLVSUBITEM) lParam;
            p2 = lvGetSubItem (p1->nItem, p1->subItem, plvdata);
            strcpy (p1->pszText, p2->pszInfo);//参数2到参数1;
            return 0;
        }
        
        case LVM_ADDCOLUMN://增加列
        {
            PLVCOLUMN p1;
        
            if ( !(p1 = (PLVCOLUMN)lParam) )
                return LV_ERR;
        
            if (!AddColumnToList (p1, plvdata))
                return LV_ERR;
        
            lstSetHScrollInfo (plvdata);
            InvalidateRect (hwnd, NULL, FALSE);
        
            return LV_OKAY;
        }
            
        case LVM_MODIFYHEAD://编辑列头
        {
            PLVCOLUMN p1 = (PLVCOLUMN) lParam;
            if (!p1)
                return LV_ERR;
            if (sModifyHeadText (p1->nCols, p1->pszHeadText, plvdata) >= 0) {
                InvalidateRect (hwnd, NULL, FALSE);
                return LV_OKAY;
            }
            return LV_ERR;
        }
        
        case LVM_SETCOLUMN://设置列
        {
            PLVCOLUMN p1 = (PLVCOLUMN) lParam;
            PLSTHDR ph;
        
            if (!p1)
                return LV_ERR;
                
            if ( !(ph = GetColsListPoint (plvdata, p1->nCols)) )
                return LV_ERR;
        
            sModifyHeadText (p1->nCols, p1->pszHeadText, plvdata);
            SetColumnWidth (p1->nCols, p1->width, plvdata);
            ph->Image = p1->image;
            ph->flags = p1->colFlags;
            
            InvalidateRect (hwnd, NULL, FALSE);
            return LV_OKAY;
        }
        case LVM_FINDITEM://查找条目
        {
            PLVFINDINFO p1;
            p1 = (PLVFINDINFO) lParam;
            return lvFindItem(p1, plvdata);
        }
        case LVM_DELITEM://删除条目
        {
            if (sRemoveItemFromList ((int32_t)wParam, plvdata)) 
                return LV_ERR;
            return LV_OKAY;
        }
        case LVM_DELALLITEM://删除全部条目
        {
            sRemoveAllItem (plvdata); 
            return LV_OKAY;
        }

        //TODO
        case LVM_CLEARSUBITEM://清除子条目
            break;

        case LVM_DELCOLUMN://删除列
        {
            if (lvRemoveColumn ((int32_t)wParam, plvdata))
                return LV_ERR;
            return LV_OKAY;
        }

        case LVM_SELECTITEM://选择条目
            SelectItem (hwnd, (int32_t)wParam, plvdata);
            return 0;
            
        case LVM_SHOWITEM://显示条目
            MakeItemVisible (hwnd, plvdata, (int32_t)wParam);
            return 0;
            
        case LVM_CHOOSEITEM://选中条目
            SelectItem(hwnd, (int32_t)wParam, plvdata); 
            MakeItemVisible (hwnd, plvdata, (int32_t)wParam);
            return 0;

        case LVM_GETITEMADDDATA://取得条目的增加数据
        {
            PITEMDATA pitem = GetItemByRow (plvdata, (int32_t)wParam);
            if (pitem)
                return pitem->addData;
            else
                return LV_ERR;
        }
        
        case LVM_SETITEMADDDATA://设置条目的附加数据
        {
            PITEMDATA pitem = GetItemByRow (plvdata, (int32_t)wParam);
            if (pitem) {
                pitem->addData = (int32_t)lParam;
                return LV_OKAY;
            }
            else
                return LV_ERR;
        }

        case LVM_SETSTRCMPFUNC://设置字串比较功能
            if (lParam) {
                plvdata->str_cmp = (STRCMP)lParam;
                return 0;
            }
            return -1;
            break;
        
        case EV_VSCROLL://垂直卷栏
        {
            lvVScroll (hwnd, wParam, lParam);
            return 0;
        }
        
        case EV_HSCROLL://水平卷栏
        {
            lvHScroll (hwnd, wParam, lParam);
            return 0;
        }
#if 0
        case EV_FONTCHANGED:
        {
            if (plvdata->nItemHeight < LV_ITEMH_DEF(hwnd)) {
                    plvdata->nItemHeight = LV_ITEMH_DEF(hwnd);
            }
            if (plvdata->nHeadHeight < LV_HDRH_DEF(hwnd)) {
                    plvdata->nHeadHeight = LV_HDRH_DEF(hwnd);
            }
        
            lstSetVScrollInfo (plvdata);
            InvalidateRect (hwnd, NULL, FALSE);
        
            return 0;
        }
#endif
        case EV_LBUTTONDBLCLK://左双击
        {
            int32_t mouseX = LOint16_t (lParam);
            int32_t mouseY = HIint16_t (lParam);
            PITEMDATA p2;
        
            if (isInListViewItem (mouseX, mouseY, &p2, plvdata) > 0)
                NotifyParent (hwnd, GetDlgCtrlID(hwnd), LVN_ITEMDBCLK);
            
            break;
        }

        case EV_RBUTTONUP://右键弹起
        case EV_RBUTTONDOWN://右键按下
        {
            int32_t mouseX = LOint16_t (lParam);
            int32_t mouseY = HIint16_t (lParam);
            int32_t nCols, nRows;
            POINT pt;
            
            RECT rcClient;
            PLSTHDR p1;
            PITEMDATA p2;
            
            LVNM_NORMAL lvnm;
            lvnm.wParam = wParam;
            lvnm.lParam = lParam;
        
            GetClientRect (hwnd, &rcClient);
            
            pt.x = mouseX;
            pt.y = mouseY;
            ScreenToClient (hwnd, &pt);
            mouseX = pt.x;
            mouseY = pt.y;
        
            // clicks on the header
            if ( (nCols = isInListViewHead (mouseX, mouseY, &p1, plvdata)) > 0 )
            {
                if (message == EV_RBUTTONDOWN)
                    NotifyParentEx (hwnd, GetDlgCtrlID(hwnd), LVN_HEADRDOWN, 
                        (uint32_t)&lvnm);
                else
                    NotifyParentEx (hwnd, GetDlgCtrlID(hwnd), LVN_HEADRUP, 
                        (uint32_t)&lvnm);
            }
            // clicks on an item
            else if ( (nRows = isInListViewItem (mouseX, mouseY, &p2, plvdata)) > 0) {
                if (message == EV_RBUTTONDOWN) {
                        SelectItem (hwnd, nRows, plvdata);
                    MakeItemVisible (hwnd, plvdata, nRows);
                        NotifyParent (hwnd, GetDlgCtrlID(hwnd), LVN_SELCHANGE);
                    NotifyParentEx (hwnd, GetDlgCtrlID(hwnd), LVN_ITEMRDOWN, (uint32_t)&lvnm);
                }
                else {
                    NotifyParentEx (hwnd, GetDlgCtrlID(hwnd), LVN_ITEMRUP, (uint32_t)&lvnm);
                }
            }
            break;
        }

        case EV_KEYDOWN://键盘
        {
            int32_t id = LOuint16_t (wParam);
            LVNM_NORMAL lvnm;
            int32_t nItem = plvdata->nItemSelected;
            switch (id)
            {	
				case VK_0:
                case VK_UP:
                    nItem = plvdata->nItemSelected-1;
                    break;
				case VK_1:
                case VK_DOWN:
                    nItem = plvdata->nItemSelected+1;
                    break;
                case VK_HOME:
                    nItem = 1;
                    break;
                case VK_END:
                    nItem = plvdata->nRows;
                    break;
				case VK_2:
				if (LV_BE_VALID_ROW(nItem))
				{
					SendMessage (hwnd->pParentWnd, EV_GETTEXT,nItem,lParam);
				}
				break;
				case VK_3:
					SendMessage (hwnd->pParentWnd, EV_CLOSE, 0, 0);
					break;

                case VK_LEFT:
                    break;
                case VK_RIGHT:
                    break;

                case VK_PRIOR:
                    SendMessage (hwnd, EV_VSCROLL, SB_PAGEUP, 0);
                    break;
                case VK_NEXT:
                    SendMessage (hwnd, EV_VSCROLL, SB_PAGEDOWN, 0);
                    break;
            }

            if (LV_BE_VALID_ROW(nItem)) {
                SelectItem (hwnd, nItem, plvdata);
                MakeItemVisible (hwnd, plvdata, nItem);
            }

            lvnm.wParam = wParam;
            lvnm.lParam = lParam;
            NotifyParentEx (hwnd, GetDlgCtrlID(hwnd), LVN_KEYDOWN, (uint32_t)&lvnm);
            
            break;
        }

        case EV_DESTROY:
        {
            lvDataDestory (plvdata);
            break;
        }
    }

    return DefWinProc (hwnd, message, wParam, lParam);
}

bool32_t RegisterListViewControl (void)
{
    WNDCLASS ListViewClass;

    memset (&ListViewClass, 0, sizeof (WNDCLASS));
    ListViewClass.pClassName = "listview";
    ListViewClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    ListViewClass.WinProc    = sListViewProc;
    
    return RegisterClass (&ListViewClass);
}

void ListViewControlCleanup (void)
{
}
