/*
  Copyright (C), 2008, zhanbin
  File name:    scrollbar.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081229
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081229    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

static bool32_t GetBmpScrollBarType(TBmpScrollBarData *pData)
{
    return (SBS_HORZ == (pData->nStatus & SBS_TYPEMASK));
}

static bool32_t GetBmpScrollBarButtonState(TBmpScrollBarData *pData, bool32_t nLeftUp)
{
    if (nLeftUp) {
        return (SST_UL_PRESSED == (pData->nStatus & SST_ULMASK));
    }
    else {
        return (SST_DR_PRESSED == (pData->nStatus & SST_DRMASK));
    }
}

static void SetBmpScrollBarButtonState(TBmpScrollBarData *pData, bool32_t nLeftUp, bool32_t nPressed)
{
    if (nLeftUp) {
        if (nPressed) {
            pData->nStatus |= SST_UL_PRESSED;
        }
        else {
            pData->nStatus &= ~SST_UL_PRESSED;
        }
    }
    else {
        if (nPressed) {
            pData->nStatus |= SST_DR_PRESSED;
        }
        else {
            pData->nStatus &= ~SST_DR_PRESSED;
        }
    }
}

static bool32_t GetBmpScrollBarSliderState(TBmpScrollBarData *pData)
{
    return (SST_SL_PRESSED == (pData->nStatus & SST_SLMASK));
}

static void SetBmpScrollBarSliderState(TBmpScrollBarData *pData, bool32_t nPressed)
{
    if (nPressed) {
        pData->nStatus |= SST_SL_PRESSED;
    }
    else {
        pData->nStatus &= ~SST_SL_PRESSED;
    }
}

static void DrawHBmpScrollBar(WINDOW *Window, DC* hdc, uint32_t wParam, uint32_t lParam)
{
    int32_t nTemp;
    BITMAP *pBmp;
    TBmpScrollBarData *pData;
    
    pData = (TBmpScrollBarData *)Window->pPrivData;
    
    pBmp = pData->pBmp;
    nTemp = pBmp->bmHeight;
    
    if (FALSE == IsWindowEnabled (Window)) {
        // LEFT BUTTOM
        DrawBitmap (hdc, pData->Button0Rect.left, pData->Button0Rect.top, pBmp, 
            pBmp->bmHeight * 2, 0, pBmp->bmHeight, pBmp->bmHeight);
            
        // DOWN
        DrawBitmap (hdc, pData->Button1Rect.left, pData->Button1Rect.top, pBmp, 
            pBmp->bmHeight * 5, 0, pBmp->bmHeight, pBmp->bmHeight);
            
        // Bar
        SetFgColor(hdc, RGB (249, 249, 247));
        FillRect (hdc, &pData->BarRect);
    }
    else {
        // LEFT BUTTOM
        if (GetBmpScrollBarButtonState (pData, TRUE)) {
            // DOWN
            DrawBitmap (hdc, pData->Button0Rect.left, pData->Button0Rect.top, pBmp, 
                pBmp->bmHeight * 1, 0, pBmp->bmHeight, pBmp->bmHeight);
        }
        else {
            // NORMAL
            DrawBitmap (hdc, pData->Button0Rect.left, pData->Button0Rect.top, pBmp, 
                pBmp->bmHeight * 0, 0, pBmp->bmHeight, pBmp->bmHeight);
        }
        
        // RIGHT BUTTOM
        if (GetBmpScrollBarButtonState (pData, FALSE)) {
            // DOWN
            DrawBitmap (hdc, pData->Button1Rect.left, pData->Button1Rect.top, pBmp, 
                pBmp->bmHeight * 4, 0, pBmp->bmHeight, pBmp->bmHeight);
        }
        else {
            // NORMAL
            DrawBitmap (hdc, pData->Button1Rect.left, pData->Button1Rect.top, pBmp, 
                pBmp->bmHeight * 3, 0, pBmp->bmHeight, pBmp->bmHeight);
        }
        
        // Bar
        SetFgColor(hdc, RGB (249, 249, 247));
        FillRect (hdc, &pData->BarRect);
        
        if (pData->nMaxPos == pData->nMinPos) {
            nTemp = 0;
        }
        else {
            nTemp = pData->nCurPos * (pData->BarRect.right - pData->BarRect.left - 
                pBmp->bmHeight) / (pData->nMaxPos - pData->nMinPos);
        }
        if (GetBmpScrollBarSliderState (pData)) {
            // DOWN
            DrawBitmap (hdc, pData->BarRect.left + nTemp, pData->BarRect.top, pBmp, 
                pBmp->bmHeight * 12, 0, pBmp->bmHeight, pBmp->bmHeight);
        }
        else {
            // NORMAL
            DrawBitmap (hdc, pData->BarRect.left + nTemp, pData->BarRect.top, pBmp, 
                pBmp->bmHeight * 12, 0, pBmp->bmHeight, pBmp->bmHeight);
        }
    }
    
    SetRect (&pData->SliderRect, pData->BarRect.left + nTemp, pData->BarRect.top, 
        pData->BarRect.left + nTemp + pBmp->bmHeight, pData->BarRect.bottom);
}

static void DrawVBmpScrollBar(WINDOW *Window, DC* hdc, uint32_t wParam, uint32_t lParam)
{
    int32_t nTemp;
    BITMAP *pBmp;
    TBmpScrollBarData *pData;
    
    pData = (TBmpScrollBarData *)Window->pPrivData;
    
    pBmp = pData->pBmp;
    nTemp = pBmp->bmHeight;
    
    if (FALSE == IsWindowEnabled (Window)) {
        // TOP BUTTOM
        DrawBitmap (hdc, pData->Button0Rect.left, pData->Button0Rect.top, pBmp, 
            pBmp->bmHeight * 8, 0, pBmp->bmHeight, pBmp->bmHeight);
            
        // BOTTOM BUTTOM
        DrawBitmap (hdc, pData->Button1Rect.left, pData->Button1Rect.top, pBmp, 
            pBmp->bmHeight * 11, 0, pBmp->bmHeight, pBmp->bmHeight);
            
        // Bar
        SetFgColor(hdc, RGB (249, 249, 247));
        FillRect (hdc, &pData->BarRect);
    }
    else {
        // TOP BUTTOM
        if (GetBmpScrollBarButtonState (pData, TRUE)) {
            // DOWN
            DrawBitmap (hdc, pData->Button0Rect.left, pData->Button0Rect.top, pBmp, 
                pBmp->bmHeight * 7, 0, pBmp->bmHeight, pBmp->bmHeight);
        }
        else {
            // NORMAL
            DrawBitmap (hdc, pData->Button0Rect.left, pData->Button0Rect.top, pBmp, 
                pBmp->bmHeight * 6, 0, pBmp->bmHeight, pBmp->bmHeight);
        }
        
        // BOTTOM BUTTOM
        if (GetBmpScrollBarButtonState (pData, FALSE)) {
            // DOWN
            DrawBitmap (hdc, pData->Button1Rect.left, pData->Button1Rect.top, pBmp, 
                pBmp->bmHeight * 10, 0, pBmp->bmHeight, pBmp->bmHeight);
        }
        else {
            // NORMAL
            DrawBitmap (hdc, pData->Button1Rect.left, pData->Button1Rect.top, pBmp, 
                pBmp->bmHeight * 9, 0, pBmp->bmHeight, pBmp->bmHeight);
        }
        
        // Bar
        SetFgColor(hdc, RGB (249, 249, 247));
        FillRect (hdc, &pData->BarRect);
            
        // Slider
        if (pData->nMaxPos == pData->nMinPos) {
            nTemp = 0;
        }
        else {
            nTemp = pData->nCurPos * (pData->BarRect.bottom - pData->BarRect.top - 
                pBmp->bmHeight) / (pData->nMaxPos - pData->nMinPos);
        }
        if (GetBmpScrollBarSliderState (pData)) {
            // DOWN
            DrawBitmap (hdc, pData->BarRect.left, pData->BarRect.top + nTemp, pBmp, 
                pBmp->bmHeight * 12, 0, pBmp->bmHeight, pBmp->bmHeight);
        }
        else {
            // NORMAL
            DrawBitmap (hdc, pData->BarRect.left, pData->BarRect.top + nTemp, pBmp, 
                pBmp->bmHeight * 12, 0, pBmp->bmHeight, pBmp->bmHeight);
        }
    }
    SetRect (&pData->SliderRect, pData->BarRect.left, pData->BarRect.top + nTemp, 
        pData->BarRect.right, pData->BarRect.top + nTemp + pBmp->bmHeight);
}

static void BuildBmpScrollBarInfo(WINDOW *Window)
{
    int32_t nTemp;
    RECT ClientRect;
    BITMAP *pBmp;
    TBmpScrollBarData *pData;
    
    pData = (TBmpScrollBarData *)Window->pPrivData;
    
    GetClientRect (Window, &ClientRect);
    
    pBmp = pData->pBmp;
    nTemp = pBmp->bmHeight;
    
    if (GetBmpScrollBarType (pData)) {
        SetRect (&pData->Button0Rect, ClientRect.left, ClientRect.top, 
            ClientRect.left + nTemp, ClientRect.bottom);
        SetRect (&pData->Button1Rect, ClientRect.right - nTemp, ClientRect.top, 
            ClientRect.right, ClientRect.bottom);
        SetRect (&pData->BarRect , ClientRect.left + nTemp, ClientRect.top, 
            ClientRect.right - nTemp, ClientRect.bottom);
        SetRect (&pData->SliderRect, ClientRect.left + nTemp, ClientRect.top, 
            ClientRect.left + nTemp * 2, ClientRect.bottom);
    }
    else {
        SetRect (&pData->Button0Rect, ClientRect.left, ClientRect.top, 
            ClientRect.right, ClientRect.top + nTemp);
        SetRect (&pData->Button1Rect, ClientRect.left, ClientRect.bottom  - nTemp, 
            ClientRect.right, ClientRect.bottom);
        SetRect (&pData->BarRect , ClientRect.left, ClientRect.top + nTemp, 
            ClientRect.right, ClientRect.bottom  - nTemp);
        SetRect (&pData->SliderRect, ClientRect.left, ClientRect.top + nTemp, 
            ClientRect.right, ClientRect.top + nTemp * 2);
    }
}

static int32_t BmpScrollBarProc(WINDOW *Window, uint32_t Message, uint32_t wParam, uint32_t lParam)
{
    RECT ClientRect;
    TBmpScrollBarData *pData;
    uint16_t nPos = 0, nSBCode = SB_NONE;
    
    pData = (TBmpScrollBarData *)Window->pPrivData;
    
    switch(Message){
    case EV_CREATE:
    {
        BITMAP *pBmp;
        int32_t nMin, nMax, nTemp;
        
        pBmp = (BITMAP *)lParam;
        if (NULL == pBmp) {
            return -1;
        }
        
        pData = (TBmpScrollBarData *) Malloc (sizeof(TBmpScrollBarData));
        if (NULL == pData) {
            return -2;
        }
        
        GetClientRect (Window, &ClientRect);
        
        pData->nStatus   = Window->nStyle;
        
        if (GetBmpScrollBarType (pData)) {
            nMin = ClientRect.left;
            nMax = ClientRect.right;
        }
        else {
            nMin = ClientRect.top;
            nMax = ClientRect.bottom;
        }
        nTemp = pBmp->bmHeight;
        
        if (nMax - nMin < 4 * nTemp) {
            Free (pData);
            return -3;
        }
        
        pData->nMinPos   = nMin;
        pData->nMaxPos   = nMax;
        pData->nCurPos   = nMin;
        pData->nPageStep = 5;
        
        pData->pBmp      = pBmp;
        
        pData->pCapWnd   = NULL;
        
        Window->pPrivData = pData;
        
        BuildBmpScrollBarInfo (Window);
        break;
    }
        
    case EV_PAINT:
    {
        DC* hdc;
        
        hdc = BeginPaint (Window, wParam, lParam);
        if (NULL == hdc) {
            break;
        }
        
        if (GetBmpScrollBarType (pData)) {
            DrawHBmpScrollBar (Window, hdc, wParam, lParam);
        }
        else {
            DrawVBmpScrollBar (Window, hdc, wParam, lParam);
        }
        
        EndPaint (Window, hdc, wParam, lParam);
        return 0;
        break;
    }
        
    case EV_LBUTTONDOWN:
    {
        POINT p;
        
        p.x = LOuint16_t (lParam);
        p.y = HIuint16_t (lParam);
        
        ScreenToClient (Window, &p);
        
        DefWinProc(Window, Message, wParam, lParam);
        
        CopyRect (&ClientRect, &pData->Button0Rect);
        if (TRUE == PtInRect (&ClientRect, &p)) {
            SetBmpScrollBarButtonState (pData, TRUE, TRUE);
            SetBmpScrollBarButtonState (pData, FALSE, FALSE);
            SetBmpScrollBarSliderState (pData, FALSE);
            
            nSBCode = SB_LINEUP;
            nPos    = 0;
            goto ENDLBD;
        }
        
        CopyRect (&ClientRect, &pData->Button1Rect);
        if (TRUE == PtInRect (&ClientRect, &p)) {
            SetBmpScrollBarButtonState (pData, FALSE, TRUE);
            SetBmpScrollBarButtonState (pData, TRUE, FALSE);
            SetBmpScrollBarSliderState (pData, FALSE);
            
            nSBCode = SB_LINEDOWN;
            nPos    = 0;
            goto ENDLBD;
        }
        
        CopyRect (&ClientRect, &pData->SliderRect);
        if (TRUE == PtInRect (&ClientRect, &p)) {
            SetBmpScrollBarButtonState (pData, TRUE, FALSE);
            SetBmpScrollBarButtonState (pData, FALSE, FALSE);
            SetBmpScrollBarSliderState (pData, TRUE);
            
            nSBCode = SB_THUMBTRACK;
            nPos    = pData->nCurPos;
            goto ENDLBD;
        }
        
        CopyRect (&ClientRect, &pData->BarRect);
        if (TRUE == PtInRect (&ClientRect, &p)) {
            SetBmpScrollBarButtonState (pData, TRUE, FALSE);
            SetBmpScrollBarButtonState (pData, FALSE, FALSE);
            SetBmpScrollBarSliderState (pData, FALSE);
            
            if (GetBmpScrollBarType (pData)) {
                if (p.x < pData->SliderRect.left) {
                    nSBCode = SB_PAGEUP;
                }
                else {
                    nSBCode = SB_PAGEDOWN;
                }
            }
            else {
                if (p.y < pData->SliderRect.top) {
                    nSBCode = SB_PAGEUP;
                }
                else {
                    nSBCode = SB_PAGEDOWN;
                }
            }
            nPos    = 0;
            goto ENDLBD;
        }
        
ENDLBD:
        if (SB_NONE != nSBCode && GetCapture () != Window) {
            pData->pCapWnd = SetCapture (Window);
            
            if (GetBmpScrollBarType (pData)) {
                SendMessage (Window, EV_HSCROLL, (uint32_t)MAKEuint32_t(nSBCode, nPos), (uint32_t)NULL);
            }
            else {
                SendMessage (Window, EV_VSCROLL, (uint32_t)MAKEuint32_t(nSBCode, nPos), (uint32_t)NULL);
            }
            
            InvalidateRect(Window, NULL, FALSE);
        }
        return 0;
        break;
    }
        
    case EV_LBUTTONUP:
    {
        if (GetCapture () == Window) {
            SetCapture (pData->pCapWnd);
        }
        else {
            break;
        }
        
        if (GetBmpScrollBarButtonState (pData, TRUE)) {
            SetBmpScrollBarButtonState (pData, TRUE, FALSE);
            
            goto ENDLBU;
        }
        
        if (GetBmpScrollBarButtonState (pData, FALSE)) {
            SetBmpScrollBarButtonState (pData, FALSE, FALSE);
            
            goto ENDLBU;
        }
        
        if (GetBmpScrollBarSliderState (pData)) {
            SetBmpScrollBarSliderState (pData, FALSE);
            
            nPos = pData->nCurPos;
            nSBCode = SB_THUMBPOSITION;
            
            goto ENDLBU;
        }
        
ENDLBU:
        if (SB_NONE != nSBCode) {
            if (GetBmpScrollBarType (pData)) {
                SendMessage (Window, EV_HSCROLL, (uint32_t)MAKEuint32_t(nSBCode, nPos), (uint32_t)NULL);
            }
            else {
                SendMessage (Window, EV_VSCROLL, (uint32_t)MAKEuint32_t(nSBCode, nPos), (uint32_t)NULL);
            }
        }
        
        InvalidateRect(Window, NULL, FALSE);
        return 0;
        break;
    }
    
    case EV_MOUSEMOVE:
    {
        POINT p;
        
        p.x = LOuint16_t (lParam);
        p.y = HIuint16_t (lParam);
        
        ScreenToClient (Window, &p);
        
        DefWinProc(Window, Message, wParam, lParam);
        
        GetClientRect (Window, &ClientRect);
        
        if (GetBmpScrollBarSliderState (pData)) {
            if (GetBmpScrollBarType (pData)) {
                if (p.x <= ClientRect.left) {
                    nPos = pData->nMinPos;
                }
                else if (p.x >= ClientRect.right) {
                    nPos = pData->nMaxPos;
                }
                else {
                    nPos = (uint16_t)((uint32_t)p.x * (uint32_t)(pData->nMaxPos - pData->nMinPos)) / 
                        (uint32_t)(ClientRect.right - ClientRect.left);
                }
            }
            else {
                if (p.y <= ClientRect.top) {
                    nPos = pData->nMinPos;
                }
                else if (p.y >= ClientRect.bottom) {
                    nPos = pData->nMaxPos;
                }
                else {
                    nPos = (uint16_t)((uint32_t)p.y * (uint32_t)(pData->nMaxPos - pData->nMinPos)) / 
                        (uint32_t)(ClientRect.bottom - ClientRect.top);
                }
            }
            nSBCode = SB_THUMBTRACK;
            
            if (SB_NONE != nSBCode) {
                if (GetBmpScrollBarType (pData)) {
                    SendMessage (Window, EV_HSCROLL, (uint32_t)MAKEuint32_t(nSBCode, nPos), (uint32_t)NULL);
                }
                else {
                    SendMessage (Window, EV_VSCROLL, (uint32_t)MAKEuint32_t(nSBCode, nPos), (uint32_t)NULL);
                }
            }
            
            InvalidateRect(Window, NULL, FALSE);
        }
        return 0;
        break;
    }
        
    case EV_VSCROLL:
    case EV_HSCROLL:
    {
        switch (LOuint16_t (wParam)) {
        case SB_LINEUP:
            pData->nCurPos--;
            if (pData->nCurPos < pData->nMinPos) {
                pData->nCurPos = pData->nMinPos;
                return -1;
            }
            break;
            
        case SB_LINEDOWN:
            pData->nCurPos++;
            if (pData->nCurPos > pData->nMaxPos) {
                pData->nCurPos = pData->nMaxPos;
                return -1;
            }
            break;
            
        case SB_PAGEUP:
            if (pData->nCurPos <= pData->nMinPos) {
                return -1;
            }
            
            pData->nCurPos -= pData->nPageStep;
            if (pData->nCurPos < pData->nMinPos) {
                pData->nCurPos = pData->nMinPos;
            }
            break;
            
        case SB_PAGEDOWN:
            if (pData->nCurPos >= pData->nMaxPos) {
                return -1;
            }
            
            pData->nCurPos += pData->nPageStep;
            if (pData->nCurPos > pData->nMaxPos) {
                pData->nCurPos = pData->nMaxPos;
            }
            break;
            
        case SB_THUMBTRACK:
            if (HIuint16_t (wParam) == pData->nCurPos) {
                return 0;
            }
            pData->nCurPos = HIuint16_t (wParam);
            break;
            
        case SB_THUMBPOSITION:
            break;
        }
        
        PostMessage (GetParent (Window), Message, wParam, (uint32_t)Window);
        break;
    }
    
    case SBM_SETPOS:
        if ((int16_t)wParam < pData->nMinPos || (int16_t)wParam > pData->nMaxPos) {
            return pData->nCurPos;
        }
        pData->nCurPos = wParam;
        
        if (TRUE == (bool32_t)lParam) {
            InvalidateRect(Window, NULL, FALSE);
        }
        
        return pData->nCurPos;
        break;
        
    case SBM_GETPOS:
        return pData->nCurPos;
        break;
        
    case SBM_SETRANGE:
        pData->nMinPos = wParam;
        pData->nMaxPos = lParam;
        if (pData->nCurPos < pData->nMinPos) {
            pData->nCurPos = pData->nMinPos;
        }
        if (pData->nCurPos > pData->nMaxPos) {
            pData->nCurPos = pData->nMaxPos;
        }
        return pData->nCurPos;
        break;
        
    case SBM_GETRANGE:
        *(int32_t *)wParam = pData->nMinPos;
        *(int32_t *)lParam = pData->nMaxPos;
        break;
        
    case EV_SETFOCUS:
        pData->nStatus |= SBS_FOCUS;
        InvalidateRect(Window, NULL, FALSE);
        break;
        
    case EV_RESIZE:
    {
        RECT ScrollRect;
        
        if (GetBmpScrollBarType (pData)) {
            GetWindowScrollBarRect (GetParent (Window), TRUE, &ScrollRect);
        }
        else {
            GetWindowScrollBarRect (GetParent (Window), FALSE, &ScrollRect);
        }
        MoveWindow (Window, ScrollRect.left, ScrollRect.top, 
            ScrollRect.right - ScrollRect.left, ScrollRect.bottom -ScrollRect.top, TRUE);
        break;
    }
        
    case EV_KILLFOCUS:
        pData->nStatus &= (~SBS_FOCUS);
        InvalidateRect(Window, NULL, FALSE);
        break;
        
    case EV_ENABLE:
        InvalidateRect(Window, NULL, FALSE);
        break;
        
    case EV_DESTROY:
        Free (pData);
        break;
        
    default:
        break;
    }
    
	return DefWinProc (Window, Message, wParam, lParam);
}

bool32_t RegisterBmpScrollBarClass(void)
{
    WNDCLASS BmpScrollBarClass;
    
    memset (&BmpScrollBarClass, 0, sizeof (WNDCLASS));
    BmpScrollBarClass.pClassName = "bmpscrollbar";
    BmpScrollBarClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    BmpScrollBarClass.WinProc    = BmpScrollBarProc;
    
    return RegisterClass (&BmpScrollBarClass);
}
