/*
  Copyright (C), 2008, zhanbin
  File name:    listbox.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081230
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081230    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

static bool32_t ListBoxIsSort(TListBoxData *pData)
{
    return (LBS_SORT == (pData->nStatus & LBS_SORT));
}

static bool32_t ListBoxIsNotify(TListBoxData *pData)
{
    return (LBS_NOTIFY == (pData->nStatus & LBS_NOTIFY));
}

static int32_t AddCalcFirstShowItem(TListBoxData *pData, int32_t nPos)
{
    struct list_head *_head_ = &pData->ItemListHead;
    #undef _item_
    #define _item_ ItemListNode
    
    if (pData->nItemCount <= pData->nCanShow + 1) {
        pData->nFirstShowItem = 0;
        pData->pFirstShowItem = list_entry((_head_)->next, TListBoxItem, _item_);
    }
    else {
        if (nPos <= pData->nFirstShowItem) {
            pData->nFirstShowItem++;
        }
    }
    
    if (nPos <= pData->nCurSelItem) {
        pData->nCurSelItem++;
    }
    
    return 0;
}

static void CalcListBoxScrollBar(WINDOW *Window)
{
    TListBoxData *pData;
    int32_t nMin, nMax, nPos;
    
    pData = (TListBoxData *)Window->pPrivData;
    
    if (pData->nItemCount <= pData->nCanShow) {
        nMin = 0;
        nMax = 0;
        nPos = 0;
        SetScrollRange (Window, SB_VERT, nMin, nMax, FALSE);
        SetScrollPos (Window, SB_VERT, nPos);
        EnableScrollBar (Window, SB_VERT, FALSE);
    }
    else {
        nMin = 0;
        nMax = pData->nItemCount - pData->nCanShow;
        nPos = pData->nFirstShowItem;
        SetScrollRange (Window, SB_VERT, nMin, nMax, FALSE);
        SetScrollPos (Window, SB_VERT, nPos);
        EnableScrollBar (Window, SB_VERT, TRUE);
    }
}

static int32_t ListBoxAddItem(TListBoxData *pData, TListBoxItem *pItem, int32_t nPos)
{
    int32_t i;
    
    if (ListBoxIsSort (pData)) {
        TListBoxItem *_list_;
        struct list_head *_head_ = &pData->ItemListHead;
        #undef _item_
        #define _item_ ItemListNode
        
        i = 0;
        for ((_list_) = list_entry((_head_)->next, TListBoxItem, _item_); 
            &((_list_)->_item_) != (_head_);
            (_list_) = list_entry((_list_)->_item_.next, TListBoxItem, _item_)) {
            if (strcmp (pItem->spName, _list_->spName) <= 0) {
                pData->nItemCount++;
                list_add (&pItem->_item_, &_list_->_item_);
                
                return i;
            }
            
            i++;
        }
        
        pData->nItemCount++;
        list_add_tail (&pItem->_item_, &_list_->_item_);
        
        return i;
    }
    else {
        TListBoxItem *_list_;
        struct list_head *_head_ = &pData->ItemListHead;
        #undef _item_
        #define _item_ ItemListNode
        
        if (nPos < 0) {
            pData->nItemCount++;
            list_add (&pItem->_item_, _head_);
            
            return 0;
        }
        
        if (nPos > pData->nItemCount) {
            nPos = pData->nItemCount;
        }
        
        i = 0;
        for ((_list_) = list_entry((_head_)->next, TListBoxItem, _item_); 
            &((_list_)->_item_) != (_head_);
            (_list_) = list_entry((_list_)->_item_.next, TListBoxItem, _item_)) {
            if (i == nPos) {
                pData->nItemCount++;
                list_add_tail (&pItem->_item_, &_list_->_item_);
                
                return nPos;
            }
            
            i++;
        }
        
        pData->nItemCount++;
        list_add_tail (&pItem->_item_, &_list_->_item_);
        
        return i;
    }
}

static int32_t ListBoxRemoveItem(TListBoxData *pData, int32_t nPos)
{
    int32_t i;
    TListBoxItem *_list_;
    struct list_head *_head_ = &pData->ItemListHead;
    #undef _item_
    #define _item_ ItemListNode
    
    if (nPos < 0 || nPos >= pData->nItemCount) {
        return -1;
    }
    
    i = 0;
    for ((_list_) = list_entry((_head_)->next, TListBoxItem, _item_); 
        &((_list_)->_item_) != (_head_);
        (_list_) = list_entry((_list_)->_item_.next, TListBoxItem, _item_)) {
        if (i == nPos) {
            if (nPos == pData->nFirstShowItem) {
                if (pData->nItemCount == 1) {
                    pData->nFirstShowItem = -1;
                    pData->pFirstShowItem = NULL;
                }
                else if (nPos == 0) {
                    pData->nFirstShowItem = 0;
                    pData->pFirstShowItem = list_entry((_list_)->_item_.next, TListBoxItem, _item_);
                }
                else {
                    pData->nFirstShowItem--;
                    pData->pFirstShowItem = list_entry((pData->pFirstShowItem)->_item_.prev, TListBoxItem, _item_);
                }
            }
            else if (nPos > pData->nFirstShowItem) {
                if (pData->nItemCount - pData->nFirstShowItem <= pData->nCanShow + 1) {
                    if (pData->nFirstShowItem > 0) {
                        pData->nFirstShowItem--;
                        pData->pFirstShowItem = list_entry((pData->pFirstShowItem)->_item_.prev, TListBoxItem, _item_);
                    }
                }
            }
            
            if (nPos == pData->nCurSelItem) {
                pData->nCurSelItem = -1;
                pData->pCurSelItem = NULL;
            }
            
            pData->nItemCount--;
            list_del (&_list_->_item_);
            if (_list_->spName) {
                Free (_list_->spName);
            }
            Free (_list_);
            
            return nPos;
        }
        
        i++;
    }
    
    return -1;
}

static void DrawListBoxItem(DC* hdc, TListBoxData *pData, TListBoxItem *pItem, RECT *lpRect)
{
    if (pData->pCurSelItem == pItem) {
        SetFgColor(hdc, pData->SelColor);
        FillRect (hdc, lpRect);
        
        SetFgColor(hdc, RGB (255, 255, 255));
    }
    else {
        SetFgColor(hdc, RGB (0, 0, 0));
    }
    
    DrawText (hdc, pItem->spName, strlen (pItem->spName), lpRect, DT_LEFT);
}

static int32_t ListBoxPaint(WINDOW *Window, DC* hdc)
{
    int32_t i, nCount;
    TListBoxData *pData;
    RECT WndRect, ItemRect;
    TListBoxItem* _list_;
    #undef _item_
    #define _item_ ItemListNode
    
    pData = (TListBoxData *)Window->pPrivData;
    
    if (pData->nItemCount <= 0) {
        return 0;
    }
    
    GetClientRect (Window, &WndRect);
    
    nCount = pData->nItemCount - pData->nFirstShowItem;
    if (nCount > pData->nCanShow) {
        nCount = pData->nCanShow + 1;
    }
    
    _list_ = pData->pFirstShowItem;
    for (i = 0; i < nCount; i++) {
        SetRect (&ItemRect, WndRect.left, WndRect.top + i * pData->nItemHeight, 
            WndRect.right, WndRect.top + (i + 1)* pData->nItemHeight);
        DrawListBoxItem (hdc, pData, _list_, &ItemRect);
        (_list_) = list_entry((_list_)->_item_.next, TListBoxItem, _item_);
    }
    
    return 0;
}

static int32_t ListBoxLBD(WINDOW *Window, uint32_t wParam, uint32_t lParam)
{
    POINT p;
    int32_t i, nCount, nOldSelItem;
    TListBoxData *pData;
    RECT WndRect, ItemRect;
    TListBoxItem* _list_;
    #undef _item_
    #define _item_ ItemListNode
    
    pData = (TListBoxData *)Window->pPrivData;
    if (pData->nItemCount == 0) {
        return 1;
    }
    
    p.x = LOuint16_t (lParam);
    p.y = HIuint16_t (lParam);
    
    ScreenToClient (Window, &p);
    nCount = p.y / pData->nItemHeight;
    
    if (pData->nFirstShowItem + nCount >= pData->nItemCount) {
        return 1;
    }
    
    _list_ = pData->pFirstShowItem;
    for (i = 0; i < nCount; i++) {
        (_list_) = list_entry((_list_)->_item_.next, TListBoxItem, _item_);
    }
    
    if (pData->pCurSelItem == _list_) {
        return 1;
    }
    
    nOldSelItem = pData->nCurSelItem;
    
    pData->nCurSelItem = pData->nFirstShowItem + nCount;
    pData->pCurSelItem = _list_;
    
    if (nCount == pData->nCanShow) {
        if (pData->nFirstShowItem + pData->nCanShow < pData->nItemCount) {
            pData->nFirstShowItem++;
            _list_ = pData->pFirstShowItem;
            pData->pFirstShowItem = list_entry((_list_)->_item_.next, TListBoxItem, _item_);
            
            CalcListBoxScrollBar (Window);
            InvalidateRect (Window, NULL, TRUE);
        }
    }
    else {
        GetClientRect (Window, &WndRect);
        
        if (nOldSelItem >= 0) {
            nOldSelItem -= pData->nFirstShowItem;
            
            SetRect (&ItemRect, WndRect.left, WndRect.top + nOldSelItem * pData->nItemHeight, 
                WndRect.right, WndRect.top + (nOldSelItem + 1)* pData->nItemHeight);
                
            InvalidateRect (Window, &ItemRect, TRUE);
        }
        
        SetRect (&ItemRect, WndRect.left, WndRect.top + nCount * pData->nItemHeight, 
            WndRect.right, WndRect.top + (nCount + 1)* pData->nItemHeight);
        
        InvalidateRect (Window, &ItemRect, TRUE);
    }
    
    if (ListBoxIsNotify (pData)) {
        NotifyParent (Window, GetDlgCtrlID(Window), LBN_SELCHANGE);
    }
    
    return 0;
}

static int32_t ListBoxKeyDown(WINDOW *Window,uint32_t wParam,uint32_t lParam)
{
    int32_t nCount;
    TListBoxData *pData;
    RECT WndRect, ItemRect;
    TListBoxItem* _list_;
    #undef _item_
    #define _item_ ItemListNode
    
    pData = (TListBoxData *)Window->pPrivData;
    if (pData->nItemCount == 0) {
        return 1;
    }
    
    switch (wParam) {
    case VK_UP:
        if (pData->nCurSelItem <= 0) {
            return 1;
        }
        pData->nCurSelItem--;
        _list_ = pData->pCurSelItem;
        pData->pCurSelItem = list_entry((_list_)->_item_.prev, TListBoxItem, _item_);
        
        if (pData->nCurSelItem < pData->nFirstShowItem) {
            pData->nFirstShowItem--;
            _list_ = pData->pFirstShowItem;
            pData->pFirstShowItem = list_entry((_list_)->_item_.prev, TListBoxItem, _item_);
            
            CalcListBoxScrollBar (Window);
            InvalidateRect (Window, NULL, TRUE);
        }
        else {
            nCount = pData->nCurSelItem - pData->nFirstShowItem;
            
            GetClientRect (Window, &WndRect);
            
            SetRect (&ItemRect, WndRect.left, WndRect.top + nCount * pData->nItemHeight, 
                WndRect.right, WndRect.top + (nCount + 2)* pData->nItemHeight);
                
            InvalidateRect (Window, &ItemRect, TRUE);
        }
        
        if (ListBoxIsNotify (pData)) {
            NotifyParent (Window, GetDlgCtrlID(Window), LBN_SELCHANGE);
        }
        return 0;
        break;
        
    case VK_DOWN:
        if (pData->nCurSelItem >= pData->nItemCount - 1) {
            return 1;
        }
        pData->nCurSelItem++;
        _list_ = pData->pCurSelItem;
        pData->pCurSelItem = list_entry((_list_)->_item_.next, TListBoxItem, _item_);
        if (pData->nCurSelItem >= pData->nFirstShowItem + pData->nCanShow) {
            pData->nFirstShowItem++;
            _list_ = pData->pFirstShowItem;
            pData->pFirstShowItem = list_entry((_list_)->_item_.next, TListBoxItem, _item_);
            
            CalcListBoxScrollBar (Window);
            InvalidateRect (Window, NULL, TRUE);
        }
        else {
            nCount = pData->nCurSelItem - pData->nFirstShowItem;
            
            GetClientRect (Window, &WndRect);
            
            SetRect (&ItemRect, WndRect.left, WndRect.top + (nCount - 1) * pData->nItemHeight, 
                WndRect.right, WndRect.top + (nCount + 1)* pData->nItemHeight);
                
            InvalidateRect (Window, &ItemRect, TRUE);
        }
        
        if (ListBoxIsNotify (pData)) {
            NotifyParent (Window, GetDlgCtrlID(Window), LBN_SELCHANGE);
        }
        return 0;
        break;
        
    case VK_HOME:
        break;
        
    case VK_END:
        break;
        
    case VK_LEFT:
        break;
        
    case VK_RIGHT:
        break;

    case VK_PRIOR:
        break;
        
    case VK_NEXT:
        break;
        
    case VK_RETURN:
        if (ListBoxIsNotify (pData) && pData->nCurSelItem >= 0) {
            NotifyParent (Window, GetDlgCtrlID(Window), LBN_ENTER);
        }
        return 0;
        break;
    }
    
    return 1;
}

static int32_t ListBoxProc(WINDOW *Window,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    RECT WndRect;
    TListBoxData *pData;
    
    pData = (TListBoxData *)Window->pPrivData;
    
    switch(Message){
    case EV_CREATE:
    {
        pData = (TListBoxData *) Malloc (sizeof(TListBoxData));
        if (pData == NULL) {
            return -1;
        }
        
        GetClientRect (Window, &WndRect);
        
        pData->nStatus        = Window->nStyle;
        pData->nFirstShowItem = -1;
        pData->pFirstShowItem = NULL;
        pData->nItemHeight    = 20;
        pData->nCanShow       = (WndRect.bottom - WndRect.top) / pData->nItemHeight;
        pData->nCurSelItem    = -1;
        pData->pCurSelItem    = NULL;
        pData->nItemCount     = 0;
        LIST_HEAD_RESET(&pData->ItemListHead);
        pData->SelColor       = RGB(156, 166, 189);
        
        SetScrollRange (Window, SB_VERT, 0, 0, FALSE);
        
        Window->pPrivData = pData;
        break;
    }
        
    case EV_ERASEBKGND:
    {
        DC* hdc;
        RECT WndRect;
        
        hdc = BeginErase (Window, wParam, lParam);
        if (NULL == hdc) {
            break;
        }
        
        GetClientRect (Window, &WndRect);
        
        SetFgColor(hdc, RGB (255, 255, 255));
        FillRect (hdc, &WndRect);
        
        if (TRUE == WindowHaveBoard (Window)) {
            SetFgColor (hdc, RGB (0, 30, 160));
            DrawRect (hdc, &WndRect);
        }
        
        EndErase (Window, hdc, wParam, lParam);
        return 0;
        break;
    }
        
    case EV_PAINT:
    {
        DC* hdc;
        
        hdc = BeginPaint (Window, wParam, lParam);
        if (NULL == hdc) {
            break;
        }
        
        ListBoxPaint (Window, hdc);
        
        EndPaint (Window, hdc, wParam, lParam);
        return 0;
        break;
    }
        
    case EV_LBUTTONDOWN:
    {
        DefWinProc (Window, Message, wParam, lParam);
                
        ListBoxLBD (Window, wParam, lParam);
        return 0;
        break;
    }
        
    case EV_LBUTTONDBLCLK:
    {
        DefWinProc(Window, Message, wParam, lParam);
        
        if (ListBoxIsNotify (pData)) {
            NotifyParent (Window, GetDlgCtrlID(Window), LBN_DBLCLK);
        }
        return 0;
        break;
    }
        
    case EV_KEYDOWN:
    {
        ListBoxKeyDown (Window, wParam, lParam);
        return 0;
        break;
    }
        
    case LB_ADDSTRING:
    case LB_INSERTSTRING:
    {
        int32_t nPos;
        int8_t *pStr;
        TListBoxItem *pItem;
        
        pStr = (int8_t *)lParam;
        pItem = (TListBoxItem *)Malloc (sizeof (TListBoxItem));
        if (NULL == pItem) {
            return -1;
        }
        
        pItem->spName = (int8_t *)Malloc (strlen (pStr) + 1);
        if (NULL == pItem->spName) {
            return -2;
        }
        strcpy (pItem->spName, pStr);
        
        if (Message == LB_ADDSTRING) {
            nPos = -1;
        }
        else {
            nPos = wParam;
        }
        nPos = ListBoxAddItem (pData, pItem, nPos);
        AddCalcFirstShowItem (pData, nPos);
        CalcListBoxScrollBar (Window);
        
        InvalidateRect (Window, NULL, TRUE);
        return 0;
        break;
    }
        
    case LB_DELETESTRING:
    {
        int32_t nPos;
        
        nPos = ListBoxRemoveItem (pData, wParam);
        if (nPos >= 0) {
            CalcListBoxScrollBar (Window);
            
            InvalidateRect (Window, NULL, TRUE);
        }
        return 0;
        break;
    }
        
    case LB_SETCURSEL:
    {
        int32_t i, nPos, nOld;
        TListBoxItem *_list_;
        struct list_head *_head_ = &pData->ItemListHead;
        #undef _item_
        #define _item_ ItemListNode
        
        nPos = wParam;
        if (nPos < 0 || nPos >= pData->nItemCount) {
            return LB_ERR;
        }
        
        i = 0;
        for ((_list_) = list_entry((_head_)->next, TListBoxItem, _item_); 
            &((_list_)->_item_) != (_head_);
            (_list_) = list_entry((_list_)->_item_.next, TListBoxItem, _item_)) {
            if (i == nPos) {
                nOld = pData->nCurSelItem;
                pData->nCurSelItem = nPos;
                pData->pCurSelItem = _list_;
                
                return nOld;
            }
            
            i++;
        }
        
        return LB_ERR;
        break;
    }
        
    case LB_GETCURSEL:
        if (pData->nCurSelItem < 0) {
            return LB_ERR;
        }
        
        return pData->nCurSelItem;
        break;
        
    case EV_VSCROLL:
    {
        int32_t i, nPos, nCount;
        TListBoxItem* _list_;
        #undef _item_
        #define _item_ ItemListNode
        
        switch (LOuint16_t (wParam)) {
        case SB_LINEUP:
        case SB_PAGEUP:
            nPos = GetScrollPos (Window, SB_VERT);
            nCount = pData->nFirstShowItem - nPos;
            for (i = 0; i < nCount; i++) {
                pData->nFirstShowItem--;
                _list_ = pData->pFirstShowItem;
                pData->pFirstShowItem = list_entry((_list_)->_item_.prev, TListBoxItem, _item_);
            }
            
            InvalidateRect (Window, NULL, TRUE);
            break;
            
        case SB_LINEDOWN:
        case SB_PAGEDOWN:
            nPos = GetScrollPos (Window, SB_VERT);
            nCount = nPos - pData->nFirstShowItem;
            for (i = 0; i < nCount; i++) {
                pData->nFirstShowItem++;
                _list_ = pData->pFirstShowItem;
                pData->pFirstShowItem = list_entry((_list_)->_item_.next, TListBoxItem, _item_);
            }
            
            InvalidateRect (Window, NULL, TRUE);
            break;
            
        case SB_THUMBTRACK:
            nPos = GetScrollPos (Window, SB_VERT);
            if (nPos < pData->nFirstShowItem) {
                nCount = pData->nFirstShowItem - nPos;
                for (i = 0; i < nCount; i++) {
                    pData->nFirstShowItem--;
                    _list_ = pData->pFirstShowItem;
                    pData->pFirstShowItem = list_entry((_list_)->_item_.prev, TListBoxItem, _item_);
                }
                
                InvalidateRect (Window, NULL, TRUE);
            }
            else if (nPos > pData->nFirstShowItem) {
                nCount = nPos - pData->nFirstShowItem;
                for (i = 0; i < nCount; i++) {
                    pData->nFirstShowItem++;
                    _list_ = pData->pFirstShowItem;
                    pData->pFirstShowItem = list_entry((_list_)->_item_.next, TListBoxItem, _item_);
                }
                
                InvalidateRect (Window, NULL, TRUE);
            }
            else {
            }
            break;
        }
        break;
    }
        
    case EV_SETFOCUS:
        pData->nStatus |= LBS_FOCUS;
        pData->SelColor = RGB(0, 0, 180);
        InvalidateRect(Window, NULL, TRUE);
        break;
        
    case EV_KILLFOCUS:
        pData->nStatus &= (~LBS_FOCUS);
        pData->SelColor = RGB(156, 166, 189);
        InvalidateRect(Window, NULL, TRUE);
        break;
        
    case EV_DESTROY:
        pData = (TListBoxData *)Window->pPrivData;
        Free (pData);
        break;
    }
    
	return DefWinProc(Window, Message, wParam, lParam);
}

bool32_t RegisterListBoxClass(void)
{
    WNDCLASS ListBoxClass;
    
    memset (&ListBoxClass, 0, sizeof (WNDCLASS));
    ListBoxClass.pClassName = "listbox";
    ListBoxClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    ListBoxClass.WinProc    = ListBoxProc;
    
    return RegisterClass (&ListBoxClass);
}
