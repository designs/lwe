/*
  Copyright (C), 2008, zhanbin
  File name:    static.c
  Author:       zhanbin <zhanbin98154@163.com>
  Version:      0.1
  Date:         20081219
  Description:
  Others:
  Function List:
  History:
    Date        Author          Modification
    20081219    zhanbin         �����ļ�
*/
/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include    "window.h"

static int32_t DefStaticProc(WINDOW *Window,uint32_t Message,uint32_t wParam,uint32_t lParam)
{
    switch(Message){
    case EV_CREATE:
        Window->pPrivData = (void *)lParam;
        break;
        
    case EV_PAINT:
    {
        DC* hdc;
        RECT WndRect;
        
        hdc = BeginPaint (Window, wParam, lParam);
        if (NULL == hdc) {
            break;
        }
        
        GetClientRect (Window, &WndRect);
        
        if (FALSE == IsWindowEnabled (Window)) {
            SetFgColor (hdc, RGB(128, 128, 128));
        }
        else {
            SetFgColor (hdc, RGB(0, 0, 0));
        }
        
        switch (Window->nStyle & SS_TYPEMASK) {
        case SS_LEFT:
            DrawText (hdc, Window->pCaption, strlen (Window->pCaption), &WndRect, DT_LEFT);
            break;
            
        case SS_CENTER:
            DrawText (hdc, Window->pCaption, strlen (Window->pCaption), &WndRect, DT_CENTER);
            break;
            
        case SS_RIGHT:
            DrawText (hdc, Window->pCaption, strlen (Window->pCaption), &WndRect, DT_RIGHT);
            break;
            
        case SS_BITMAP:
            DrawBitmap (hdc, WndRect.left, WndRect.top, (BITMAP *)Window->pPrivData, 0, 0, 0, 0);
            break;
            
        case SS_ICON:
            DrawIcon (hdc, WndRect.left, WndRect.top, 0, 0, (ICON *)Window->pPrivData);
            break;
        }
        
        EndPaint (Window, hdc, wParam, lParam);
        
        return 0;
        break;
    }
        
    case EV_GETTEXT:
    {
        int32_t nLen;
        int8_t * text = (int8_t *)lParam;
        
        nLen = strlen (Window->pCaption);
        memcpy (text, Window->pCaption, nLen);
        return nLen;
        break;
    }
        
    case EV_SETTEXT:
    {
        int8_t * text = (int8_t *)lParam;
        Free (Window->pCaption);
        Window->pCaption = (int8_t *)Malloc (strlen (text) + 1);
        strcpy (Window->pCaption, text);
        InvalidateRect(Window, NULL, TRUE);
        break;
    }
    
    default:
        break;
    }

	return DefWinProc(Window, Message, wParam, lParam);
}

bool32_t RegisterStaticClass(void)
{
    WNDCLASS StaticClass;

    memset (&StaticClass, 0, sizeof (WNDCLASS));
    StaticClass.pClassName = "static";
    StaticClass.pCursor    = GetSystemCursor (CURSOR_ARROW_ID);
    StaticClass.WinProc    = DefStaticProc;
    
    return RegisterClass (&StaticClass);
}
