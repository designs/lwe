#ifndef MGEXT_H
#define MGEXT_H

#ifdef  __cplusplus
extern "C" {
#endif

typedef int32_t (*STRCMP) (const int8_t* s1, const int8_t* s2, size_t n);

/************************** List View control ********************************/
//#ifdef _EXT_CTRL_LISTVIEW

    /**
     * \defgroup mgext_ctrl_listview ListView control
     * @{
     */

#define CTRL_LISTVIEW       ("ListView")

/** Listview return value */
#define LV_OKAY                 0
#define LV_ERR                  (-1)
#define LV_ERRSPACE             (-2)

/** 
 * Structure of the listview item info, contains information about an item.
 * This structure is used for creating or retrieving an item. 
 */
typedef struct _LVITEM
{
    /** the rows of the item */
    int32_t nItem;
    /** attached additional data of this item */
    int32_t itemData;
} LVITEM;
typedef LVITEM *PLVITEM;

/** listview icon flags */
#define LVFLAG_BITMAP	0x0001
#define LVFLAG_ICON	    0x0002

/** 
 * Struct of the listview subitem info, contains information about a subitem.
 * This structure is used for creating or retrieving a subitem.
 */ 
typedef struct _LVSUBITEM
{
    /** subitem flags */
    uint32_t flags;
    /** the Subitem vertical position(rows) */
    int32_t nItem;
    /** the Subitem horizontal position(columns) */
    int32_t subItem;
    /** text content of this subitem */
    int8_t *pszText;
    /** max text len */
    int32_t nTextMax;
    /** text color of the subitem */
    int32_t nTextColor;
    /** image of the subitem, can be bitmap or icon */
    uint32_t image; 
} LVSUBITEM;
typedef LVSUBITEM *PLVSUBITEM;

//FIXME
//TODO
/** Contains information for sorting listview */
typedef struct _LVSORTDATA
{
    /** Sorting column index */
    int32_t ncol;
    /** low sorted or high sorted */
    int32_t losorted;
} LVSORTDATA;
typedef LVSORTDATA *PLVSORTDATA;

typedef int32_t (*PFNLVCOMPARE) (int32_t nItem1, int32_t nItem2, PLVSORTDATA sortData);


/** Column and header flags */
#define COLUME_TXT_LEFTALIGN		0x0000         // column text left align, default
#define COLUME_TXT_RIGHTALIGN		0x0001         // column text right align
#define COLUME_TXT_CENTERALIGN	0x0002         // column text center align

#define HEADER_TXT_LEFTALIGN		0x0000         // header text left align, default
#define HEADER_TXT_RIGHTALIGN		0x0004         // header text right align
#define HEADER_TXT_CENTERALIGN	0x0008         // header text center align

/** 
 * Struct of the listview column info, contains information about a column.
 * This structure is used for creating or retrieving a column.
 */
typedef struct _LVCOLUMN
{
    /** the horizontal position */
    int32_t nCols;
    /** column's width */
    int32_t width;
    /** the title of this column */
    int8_t *pszHeadText;
    /** max text len */
    int32_t nTextMax;
    /** image of the column header, can be bitmap or icon */
    uint32_t image; 
    /** Comparision function associated with the column */
    PFNLVCOMPARE pfnCompare;
    /** Column and header flags */
    uint32_t colFlags;
} LVCOLUMN;
typedef LVCOLUMN *PLVCOLUMN;

/** listview search flags */
#define LVFF_TEXT	    0x0001
#define LVFF_ADDDATA	0x0002

/** Contains information for finding a certain item info */
typedef struct _LVFINDINFO
{
    /** 
     * Type of search to perform. 
     * This member can be set to one or more of the following values:
     * - LVFF_TEXT
     *   Searches based on the item(subitems) text.
     * - LVFF_ADDDATA
     *   Searches based on the attached additional item data.
     */
    uint32_t flags;
    /** Search index to begin with, 0 from the beginning */
    int32_t iStart;
    /** pszInfo containing nCols columns' text */
    int32_t nCols;
    /** all the subitem's content of this item */
    int8_t **pszInfo;
    /** the additional item data */
    uint32_t addData;

    /** The found item's row, reserved */
    int32_t nItem;
    /** The found subitem's column, reserved */
    int32_t nSubitem;

} LVFINDINFO;
typedef LVFINDINFO *PLVFINDINFO;

/** Contains listview general notification information */
typedef struct _LVNM_NORMAL
{
    /** wParam parameter of the message */
    uint32_t wParam;
    /** lParam parameter of the message */
    uint32_t lParam;
} LVNM_NORMAL;
typedef LVNM_NORMAL *PLVNM_NORMAL;

/** keydown notification information */
typedef LVNM_NORMAL LVNM_KEYDOWN;
typedef LVNM_KEYDOWN *PLVNM_KEYDOWN;

/** Contains listview notification information when mouse down on the header  */
typedef LVNM_NORMAL LVNM_HEADRDOWN;
typedef LVNM_HEADRDOWN *PLVNM_HEADRDOWN;

/** Contains listview notification information when mouse up on the header  */
typedef LVNM_NORMAL LVNM_HEADRUP;
typedef LVNM_HEADRUP *PLVNM_HEADUP;

/** Contains listview notification information when mouse down on the item area */
typedef LVNM_NORMAL LVNM_ITEMRDOWN;
typedef LVNM_ITEMRDOWN *PLVNM_ITEMRDOWN;

/** Contains listview notification information when mouse up on the item area */
typedef LVNM_NORMAL LVNM_ITEMRUP;
typedef LVNM_ITEMRUP *PLVNM_ITEMRUP;

    /**
     * \defgroup mgext_ctrl_listview_styles Styles of listview control
     * @{
     */

/**
 * \def LVS_NOTIFY
 * \brief Notifies the parent window.
 *
 * Causes the listview to notify the listview parent window 
 * with a notification message when the user make actions, such as clicks, doubleclicks, ...,etc.
 */
#define LVS_NOTIFY              0x00010000L

/**
 * \def LVS_SORT
 * \brief Sorts strings automatically.
 *
 * Causes the listview to sort strings automatically. 
 */
#define LVS_SORT                0x00020000L

/**
 * \def LVS_MULTIPLESEL
 * \brief Causes the listview to allow the user to select multiple items.
 */
#define LVS_MULTIPLESEL         0x00080000L              //reserved

/**
 * \def LVS_CHECKBOX
 * \brief Displays a check box in an item.
 */
#define LVS_CHECKBOX            0x10000000L              //reserved

/**
 * \def LVS_AUTOCHECK
 * \brief If the list box has LVS_CHECKBOX style, this
 *        style tell the box to auto-switch the check box between 
 *        checked or un-checked when the user click the check mark box of an item.
 */
#define LVS_AUTOCHECK           0x20000000L              //reserved

#define LVS_AUTOCHECKBOX        (LVS_CHECKBOX | LVS_AUTOCHECK)    //reserved


    /** @} end of mgext_ctrl_listview_styles */

    /**
     * \defgroup mgext_ctrl_listview_msgs Messages of ListView control
     * @{
     */

/**
 *  \def LVM_ADDITEM
 *  \brief Adds a item to listview, this item is also called a row. 
 *  
 *  \code 
 *  LVM_ADDITEM
 *  PLVITEM p;
 *
 *  wParam = 0;
 *  lParam =(uint32_t)p;
 *  \endcode
 *
 *  \param p Pointes to a LVITEM structure that contains the information of 
 *           the new item to be added.
 *  \return Returns the index of the new item if successful, or -1 otherwise.
 */
#define LVM_ADDITEM            0xF110

/**
 * \def LVM_FILLSUBITEM
 * \brief Sets the content of a subitem, indentified by rows and columns.
 * 
 * \code
 * LVM_FILLSUBITEM
 * PLVSUBITEM p;
 *
 * lParam = (uint32_t)p;
 * wParam = 0;
 * \endcode
 *
 * \param p the Pointer of the subitem to be added.
 * \return Returns LV_OKAY if successful, or LV_ERR otherwise.
 */
#define LVM_FILLSUBITEM        0xF111

/**
 * \def LVM_ADDCOLUMN
 * \brief Adds a new column to listview, indentified by column.
 * 
 * \code
 *  LVM_ADDCOLUMN
 *  PLVCOLUMN p;
 *
 *  wParam = 0;
 *  lParam =(uint32_t)p;
 * \endcode
 *
 * \param p Pointes to a LVCOLUMN structure that contains the information about the new 
 *          column to be added.
 */
#define LVM_ADDCOLUMN          0xF112

/**
 * \def LVM_DELITEM
 * \brief Deletes an item form listview, also called a row.
 *
 * \code
 * LVM_DELITEM
 * int32_t nRow;
 *
 * wParam = (uint32_t)nRow;
 * lParam = 0;
 * \endcode
 *
 * \param nRow the index of the item to be removed.
 * \return Returns LV_OKAY if successful, or LV_ERR otherwise.
 */
#define LVM_DELITEM            0xF113

/**
 * \def LVM_CLEARSUBITEM
 * \brief Clears the content of a subitem, indentified by rows and columns.
 * 
 * \code
 * LVM_CLEARSUBITEM
 * PLVSUBITEM p
 *
 * lParam = (uint32_t)p;
 * wParam = 0;
 * \endcode
 *
 * \param p the Pointer of the subitem.
 * \return Returns LV_OKAY if successful, or LV_ERR otherwise.
 */
#define LVM_CLEARSUBITEM       0xF114

/**
 * \def LVM_DELCOLUMN
 * \brief Deletes a column from listview, all subitem in this column will be removed.  
 *
 * \code
 * LVM_DELCOLUMN
 * int32_t nCols;
 *
 * wParam = (uint32_t)nCol;
 * lParam = 0;
 * \endcode
 *
 * \param nCol the index of the column to be removed.
 * \return Returns LV_OKAY if successful, or LV_ERR otherwise.
 */
#define LVM_DELCOLUMN          0xF115

/**
 * \def LVM_COL_SORT
 * \brief Sorts all subitems according to a certain column. 
 *
 * \code
 * LVM_COL_SORT
 * int32_t ncol;
 *
 * wParam = (uint32_t)ncol;
 * lParam = 0;
 * \endcode
 *
 * \param ncol Index of the column.
 * \return Returns LV_OKAY if successful, or LV_ERR otherwise.
 */
#define LVM_COL_SORT            0xF116

/**
 * \def LVM_SETSUBITEMCOLOR
 * \brief Sets the text color of a subitem.
 *
 * \code
 * LVM_SETSUBITEMCOLOR
 * PLVSUBITEM psub;
 *
 * lParam = (uint32_t)psub;
 * \endcode
 *
 * \param psub constains the information of the subitem to set.
 * \return Aays returns 0;
 */
#define LVM_SETSUBITEMCOLOR       0xF117

/**
 * \def LVM_FINDITEM
 *
 * \code
 * LVM_FINDITEM
 * PLVFINDINFO plvfi;
 *
 * wParam = 0;
 * lParam = (uint32_t)plvfi;
 * \endcode
 *
 * \param plvfi Points to a LVFINDINFO structure containing information for searching.
 *
 * \return Returns the index of the found item if successful, or -1 otherwise.
 */
#define LVM_FINDITEM           0xF118

/**
 *
 * \def LVM_GETSUBITEMTEXT
 * \brief Retrieves the text of a listview subitem.
 *
 * \code 
 * LVM_GETSUBITEMTEXT
 * PLVSUBITEM p;
 *
 * wParam = 0;
 * lParam = (uint32_t)p;
 * \endcode
 * 
 * \param p Points to a LVSUBITEM structure.
 *
 * \return Returns len of the text if successful, -1 otherwise.
 */

#define LVM_GETSUBITEMTEXT         0xF119

/**
 * \def LVM_GETITEMCOUNT
 * \brief Gets the number of all the items(rows) in a listview.
 *
 * \code
 * LVM_GETITEMCOUNT
 *
 * wParam = 0;
 * lParam = 0;
 * \endcode
 *
 * \return The number of the items.
 */
#define LVM_GETITEMCOUNT       0xF11A

/**
 * \def LVM_GETCOLUMNCOUNT
 * \brief Gets the number of all the columns in listview.
 *
 * \code
 * LVM_GETCOLUMNCOUNT
 *
 * wParam = 0;
 * lParam = 0;
 * \endcode
 *
 * \return the number of all the columns in listview.
 */
#define LVM_GETCOLUMNCOUNT     0xF11B

/**
 * \def LVM_GETSELECTEDITEM
 * \brief Gets the current selected item. This message is a internal message of listview.
 *
 * \code
 * LVM_GETSELECTEDITEM
 *
 * wParam = 0;
 * lParam = 0;
 * \endcode
 *
 * \return the current selected item.
 */
#define LVM_GETSELECTEDITEM    0xF11C

/**
 * \def LVM_DELALLITEM
 * \brief Removes all the items in listview.
 *
 * \code
 * LVM_DEALLITEM
 *
 * wParam = 0;
 * lParam = 0;
 * \endcode
 *
 * \return Returns TRUE if successful, or FALSE otherwise.
 */
#define LVM_DELALLITEM           0xF11D

/**
 * \def LVM_MODIFYHEAD
 * \brief Changes the title of a column.
 *
 * \code
 * LVM_MODIFYHEAD
 * PLVCOLUMN pcol;
 *
 * wParam = 0;
 * lParam = (uint32_t)pcol;
 * \endcode
 *
 * \param pcol Pointer to a LVCOLUMN struct.
 * \return Returns LV_OKAY if successfull, or LV_ERR otherwise.
 */
#define LVM_MODIFYHEAD           0xF11E

/**
 * \def LVM_SELECTITEM
 * \brief Selects an item.
 *
 * \code
 * LVM_SELECTITEM
 * int32_t nItem;
 *
 * wParam = (uint32_t)nItem;
 * lParam = 0;
 * \endcode
 *
 * \param nItem Index of the item to select.
 * \return Aays returns 0.
 */
#define LVM_SELECTITEM		0xF11F 


/**
 * \def LVM_SHOWITEM
 * \brief Makes the item is entirely visible in the list view.
 *
 * \code
 * LVM_SHOWITEM
 * int32_t nRow;
 *
 * wParam = (uint32_t)nRow;
 * lParam = 0;
 * \endcode
 *
 * \param nRow Index of the item to show.
 * \return Aays returns 0.
 */
#define LVM_SHOWITEM		0xF120 

/**
 * \def LVM_GETSUBITEMLEN
 * \brief Gets the text len of the subitem.
 *
 * \code
 * LVM_GETSUBITEMLEN
 * PLVSUBITEM psub;
 *
 * wParam = 0;
 * lParam = (uint32_t)psub;
 * \endcode
 *
 * \param psub Pointer to a LVSUBITEM struct containing subitem information.
 * \return Returns the text len if successful, or LV_ERR otherwise.
 */
#define LVM_GETSUBITEMLEN   	0xF121

/**
 * \def LVM_SETCOLUMN
 * \brief Sets the attributes of a listview column.
 *
 * \code
 * LVM_SETCOLUMN
 * PLVCOLUMN pcol;
 *
 * wParam = 0;
 * lParam = (uint32_t)pcol;
 * \endcode
 *
 * \param pcol Points to a LVCOLUMN structure containing the new column information.
 *
 * \return Returns LV_OKAY if successful, or LV_ERR otherwise.
 */
#define LVM_SETCOLUMN		0xF122

/**
 * \def LVM_SETSUBITEMTEXT
 * \brief Sets the text of a subitem.
 *
 * \code
 * LVM_SETSUBITEMTEXT
 * PLVSUBITEM psub;
 *
 * wParam = 0;
 * lParam = (uint32_t)psub;
 * \endcode
 *
 * \param psub constains the information of the subitem to set.
 * \return Returns LV_OKAY if successful, or LV_ERR otherwise.
 */
#define LVM_SETSUBITEMTEXT	0xF123

/**
 * \def LVM_SETSUBITEM
 * \brief Sets the attributes of a subitem.
 *
 * \code
 * LVM_SETSUBITEM
 * PLVSUBITEM psub;
 *
 * wParam = 0;
 * lParam = (uint32_t)psub;
 * \endcode
 *
 * \param psub constains the information of the subitem to set.
 * \return Returns LV_OKAY if successful, or LV_ERR otherwise.
 */
#define LVM_SETSUBITEM		0xF124

/**
 * \def LVM_GETCOLUMN
 * \brief Retrieves the information about a listview column.
 *
 * \code
 * LVM_GETCOLUMN
 * int32_t nCol;
 * PLVCOLUMN pcol;
 *
 * wParam = (uint32_t)nCol;
 * lParam = (uint32_t)pcol;
 * \endcode
 *
 * \param nCol Index of the column.
 * \param pcol Points to a LVCOLUMN structure for retrieving the information 
 *             about the column.
 *
 * \return Returns TRUE if successful, or FALSE otherwise.
 */
#define LVM_GETCOLUMN		0xF125 

/**
 * \def LVM_GETCOLUMNWIDTH
 * \brief Retrieves the width of a listview column.
 *
 * \code
 * LVM_GETCOLUMNWIDTH
 * int32_t nCol;
 *
 * wParam = (uint32_t)nCol;
 * lParam = (uint32_t)0;
 * \endcode
 *
 * \param nCol Index of the column.
 *
 * \return Returns the column width if successful, or -1 otherwise.
 */
#define LVM_GETCOLUMNWIDTH	0xF126 

/**
 * \def LVM_GETITEM
 * \brief Retrieves the item's attributes.
 *
 * \code
 * LVM_GETITEM
 * PLVITEM pitem;
 *
 * wParam = 0;
 * lParam = (uint32_t)pitem;
 * \endcode
 *
 * \param pitem Points to an LVITEM structure for retrieving the information
 *              about an item, the nItem field identifies the item.
 *
 * \return Returns LV_OKAY if successful, or LV_ERR otherwise.
 */
#define LVM_GETITEM		0xF127 

/**
 * \def LVM_GETITEMSTATE
 * \brief Retrieves the state of a listview item.
 *
 * \code
 * LVM_GETITEMSTATE
 * int32_t nRow;
 * UINT mask;
 *
 * wParam = (uint32_t)nRow;
 * lParam = (uint32_t)mask;
 * \endcode
 *
 * \param nRow Index of the item.
 * \param mask Contains state information to retrieve, can be the combination
 *             of the following values.
 *
 * \return Returns the current state of the specified item.
 */
#define LVM_GETITEMSTATE	0xF128 

/**
 * \def LVM_GETSELECTEDCOLUMN
 * \brief Retrieves the index of the currently selected column of a listview.
 *
 * \code
 * LVM_GETSELECTEDCOLUMN
 *
 * wParam = 0;
 * lParam = 0;
 * \endcode
 *
 * \return Returns the index of the selected column.
 */
#define LVM_GETSELECTEDCOLUMN	0xF129 

/**
 * \def LVM_GETSELECTEDCOUNT
 * \brief Retrieves the number of the selected items in a listview.
 *
 * \code
 * LVM_GETSELECTEDCOUNT
 *
 * wParam = 0;
 * lParam = 0;
 * \endcode
 *
 * \return Returns the number of the selected items.
 */
#define LVM_GETSELECTEDCOUNT	0xF130 

/**
 * \def LVM_GETTOPVISIBLE
 * \brief Retrieves the index of the topmost visible item in a listview.
 *
 * \code
 * LVM_GETTOPVISIBLE
 *
 * wParam = 0;
 * lParam = 0;
 * \endcode
 *
 * \return Returns the index of the topmost visible item if successful, or zero.
 */
#define LVM_GETTOPVISIBLE	0xF131 

#define LVM_NULL		0xF132 

#define LVM_SETITEMSTATE	0xF133 

/**
 * \def LVM_SORTITEMS
 * \brief Uses an application-defined comparision function to sort the items.
 *
 * \code
 * LVM_SORTITEMS
 * PLVSORTDATA sortData;
 * PFNLVCOMPARE pfnCompare;
 *
 * wParam = (uint32_t)sortData;
 * lParam = (uint32_t)pfnCompare;
 * \endcode
 *
 * \param sortData Sorting datas passed to the comparision function.
 * \param pfnCompare Pointer to the application-defined comparision function. The
 *                   comparision function is called during the sort operation
 *                   each time the relative order of the two items needs to be 
 *                   compared.
 *
 *                   The comparison function must return a negative value if the 
 *                   first item precedes the second, a positive value if 
 *                   the first item follows the second, or zero if the two 
 *                   items are equivalent.
 *
 *                   The comparision function has the following form:
 *
 *                   int32_t CompareFunction(int32_t nItem1, int32_t nItem2, PLVSORTDATA sortData);
 *
 *                   nItem1 is the index of the first item, nItem2 is the index
 *                   of the second item, and sortData is passed to CompareFunction
 *                   as the third parameter.
 *
 * \return Returns TRUE if successful, or FALSE otherwise.
 */
#define LVM_SORTITEMS		0xF134 

/**
 * \def LVM_SETITEMHEIGHT
 * \brief Changes the height of all the items.
 *
 * \code
 * LVM_SETITEMHEIGHT
 * int32_t height;
 *
 * wParam = (uint32_t)height;
 * lParam = 0;
 * \endcode
 *
 * \param height The new height of the item.
 * \return Returns TRUE if successful, or FALSE otherwise.
 */
#define LVM_SETITEMHEIGHT	0xF135 

/**
 * \def LVM_SETHEADHEIGHT
 * \brief Changes the height of the header.
 *
 * \code
 * LVM_SETHEADHEIGHT
 * int32_t height;
 *
 * wParam = (uint32_t)height;
 * lParam = 0;
 * \endcode
 *
 * \param height The new height of the header.
 * \return Returns TRUE if successful, or FALSE otherwise.
 */
#define LVM_SETHEADHEIGHT	0xF136 

/**
 * \def LVM_GETITEMADDDATA
 * \brief Gets the 32-bit data value associated with an item.
 * 
 * An application sends an LVM_GETITEMADDDATA message to a listview to get the 
 * 32-bit data value stored for the item with index of \a wParam; 
 * By default this is zero. An application must set the 
 * item data value by sending an LVM_SETITEMADDDATA message to the listview for 
 * this value to have meaning.
 *
 * \code
 * LVM_GETITEMADDDATA
 * int32_t index;
 *
 * wParam = (uint32_t)index;
 * lParam = 0;
 * \endcode
 *
 * \param index The index of the specified item.
 *
 * \return The 32-bit data value associated with an item on success, otherwise
 *         -1 to indicate an error.
 */
#define LVM_GETITEMADDDATA       0xF137

/**
 * \def LVM_SETITEMADDDATA
 * \brief Associates a 32-bit data value with an item.
 * 
 * An application sends an LVM_SETITEMADDDATA message to associate a 32-bit data 
 * value specified in the \a lParam parameter with an item in the listview.
 *
 * \code
 * LVM_SETITEMADDDATA
 * int32_t index;
 * uint32_t addData;
 *
 * wParam = (uint32_t)index;
 * lParam = (uint32_t)addData;
 * \endcode
 *
 * \param index The index of the specified item.
 * \param addData the 32-bit data value which will associated with the item.
 *
 * \return One of the following values:
 *          - LV_OKAY\n         Success
 *          - LV_ERR\n          Invalid item index
 *
 */
#define LVM_SETITEMADDDATA       0xF138

/**
 * \def LVM_CHOOSEITEM
 * \Selects and shows an item.
 *
 * \code
 * LVM_CHOOSEITEM
 * int32_t nItem;
 *
 * wParam = (uint32_t)Item;
 * lParam = 0;
 * \endcode
 *
 * \param nItem Index of the item to choose.
 * \return Aays returns 0.
 */
#define LVM_CHOOSEITEM		0xF139

/**
 * \def LVM_SETSTRCMPFUNC
 * \brief Sets the STRCMP function used to sort items.
 *
 * An application sends a LVM_SETSTRCMPFUNC message to set a 
 * new STRCMP function to sort items in the Listview control.
 *
 * Note that you should send this message before adding 
 * any item to the Listview control.
 *
 * \code
 * static int32_t _strcmp (const int8_t* s1, const int8_t* s2, size_t n)
 * {
 *      ...
 *      return 0;
 * }
 *
 * LVM_SETSTRCMPFUNC
 *
 * wParam = 0;
 * lParam = (uint32_t) _strcmp;
 * \endcode
 *
 * \param _strcmp Your own function to compare two strings.
 *
 * \return One of the following values:
 *          - 0\n     Success
 *          - -1\n    Not an empty TreeView control
 */
#define LVM_SETSTRCMPFUNC       0xF140

    /** @} end of mgext_ctrl_listview_msgs */

    /**
     * \defgroup mgext_ctrl_listview_ncs Notification code of ListView control
     * @{
     */

/**
 * \def LVN_ITEMRDOWN
 * \brief This notification code informs parent window the right mouse button down
 *        on a listview item.
 */
#define LVN_ITEMRDOWN          4

/**
 * \def LVN_ITEMRUP
 * \brief This notification code informs parent window the right mouse button up
 *        on a listview item.
 */
#define LVN_ITEMRUP            5

/**
 * \def LVN_HEADRDOWN
 * \brief This notification code informs parent window the right mouse button down
 *        on the listview header.
 */
#define LVN_HEADRDOWN          6

/**
 * \def LVN_HEADRUP
 * \brief This notification code informs parent window the right mouse button up
 *        on the listview header.
 */
#define LVN_HEADRUP            7

/**
 * \def LVN_KEYDOWN
 * \brief This notification code informs parent window that a key has been pressed.
 */
#define LVN_KEYDOWN            8

/**
 *
 * \def LVN_ITEMDBCLK
 * \brief This notification code informs parent window the current selected item 
 *        has be double clicked.
 */
#define LVN_ITEMDBCLK          9

/**
 * \def LVN_ITEMCLK
 * \brief This notification code informs parent window the current selected item 
 *        has been clicked.
 */
#define LVN_ITEMCLK            10

/**
 * \def LVN_SELCHANGE
 * \brief This notification code informs parent window the current selected item 
 *        has changed.
 */
#define LVN_SELCHANGE          11

/**
 * \def LVN_COLCHANGE
 * \brief This notification code informs parent window the current selected column 
 *        has changed.
 */
#define LVN_COLCHANGE          12


    /** @} end of mgext_ctrl_listview_ncs */

    /** @} end of mgext_ctrl_listview */

//#endif  /* _EXT_CTRL_LISTVIEW */

#ifdef  __cplusplus
}
#endif

#endif /* MGEXT_H */
