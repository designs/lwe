#ifndef __BMP_BUTTON_H__
#define __BMP_BUTTON_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define BUTTON_PUSHED       0x00010000
#define BUTTON_FOCUS        0x00020000

typedef struct _TBmpButtonData
{
    int32_t                     nStatus;
    
    BITMAP*                 pBmp;
    WINDOW *               pOldCaptureWnd;
}TBmpButtonData;

bool32_t RegisterBmpButtonClass(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif
